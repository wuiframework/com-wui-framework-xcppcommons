# com-wui-framework-xcppcommons v2019.1.0

> Commons classes for the WUI Framework cross-compile executables written in C++.

## Requirements

This library depends on the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder). 
See the WUI Builder requirements before you build this project.

This library has some special requirements:

* winsdk - for more information see the [README.md](resource/libs/winsdk/README.md) file.

Other requirements are managed automatically through dependency manager:

* [boost](http://www.boost.org/) - portable C++ source libraries 
* [openssl](https://www.openssl.org/) - cryptography and SSL/TLS Toolkit
* [nlohmann-json](https://github.com/nlohmann/json) - JSON for modern C++

## Project build

The project build is fully automated. For more information about the project build,
see the [WUI Builder](https://bitbucket.org/wuiframework/com-wui-framework-builder) documentation.

## Documentation

This project provides automatically generated documentation in [Doxygen](http://www.doxygen.org/index.html) 
from the C++ source by running the `docs` command.

> NOTE: The documentation is accessible also from the {projectRoot}/build/target/docs/index.html file after a successful creation.

## History

### v2019.1.0
Fixed issue with http header key case-sensitivity.
### v2019.0.0
Updated versions of dependencies. Added full multi-platform support.
### v2018.1.1
Fixed autostrip for unpack of packages with long file names. Clean up. Fixed invoke of elevated process without argumnets. 
Added possibility to detach spawn by terminal options.
### v2018.1.0
Minor JSONP parser enhancements and fixed quoting for Pack API. Covert of HTTP tests to MockServer configuration.
### v2018.0.3
Added support for openssl on ARM platform. Dependencies 7zip and p7zip are used as common dependency instead resource.
Fixed unpack auto-strip for packages with inconsistent naming. Added verbose mode switch for Terminal methods.
### v2018.0.2
Fixed issue with file name resolving from HTTP header "disposition". Added getters for appdata and localappdata paths,
file path expand with glob pattern (limited to *, **, ?), and lite task manager.
### v2018.0.1
Added support to unpack .tar.xz archive. Environment variables passed to child process supports symbol expand. Fixed stdin initialization of child process.
### v2018.0.0
Fixed issue with terminal command and CWD resolving. Added bypass of std streams redirection. Changed version format.
### v2.2.0
Added child process log data limit and other log enhancements. Fixed some minor bugs. Openssl upgrade to 1.1.0f. Linux and Mac are now supported.
### v2.1.1
Added URL encoder and decoder. Configuration file formats migrated from XML to JSONP.
### v2.1.0
Added ResponseFactory. All platform dependent features ported also on linux. Anonymization filter for all logged data. Better support
for enum's aliases.
### v2.0.3
Detached child process uses common boost::process::child with manual detach instead boost::process::spawn to be able to get process PID. 
Application loader force set CWD to application executable directory.
### v2.0.2
Update of boost version. New child process management. API clean up and documentation update. Increase of code coverage.
### v2.0.1
Syntax update. LogIt enhancements. Structure clean up.
### v2.0.0
Namespaces refactoring.
### v1.0.1
Added support for the C++ Boost Framework.
### v1.0.0
Initial release.

## License

This software is owned or controlled by NXP Semiconductors. 
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.
  
See the `LICENSE.txt` file for more details.

---

Author Jakub Cieslar, 
Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), 
Copyright (c) 2017-2019 [NXP](http://nxp.com/)
