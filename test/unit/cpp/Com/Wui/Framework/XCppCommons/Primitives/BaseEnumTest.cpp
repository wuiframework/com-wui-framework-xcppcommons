/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Primitives {
    class TestEnum : public BaseEnum<TestEnum> {
     WUI_ENUM_DECLARE(TestEnum);
     public:
        static const TestEnum ENA;
        static const TestEnum ENB;
        static const TestEnum EN_Q;
        static const TestEnum EN_RAW;
    };

    WUI_ENUM_IMPLEMENT(TestEnum);
    WUI_ENUM_CONST_IMPLEMENT(TestEnum, ENA, "ena");
    WUI_ENUM_CONST_IMPLEMENT(TestEnum, ENB, "enb");
    WUI_ENUM_CONST_IMPLEMENT(TestEnum, EN_Q, "EnQ");
    WUI_ENUM_CONST_IMPLEMENT(TestEnum, EN_RAW, "EnRaw");

    TEST(BaseEnumTest, Operators) {
        TestEnum testEnum = TestEnum::ENA;

        ASSERT_EQ(testEnum, TestEnum::ENA);
        ASSERT_NE(TestEnum::ENA, TestEnum::ENB);
        ASSERT_EQ(TestEnum::EN_Q, "enq");

        testing::internal::CaptureStdout();
        std::cout << testEnum;
        ASSERT_STREQ("ena", testing::internal::GetCapturedStdout().c_str());
    }

    TEST(BaseEnumTest, toString) {
        ASSERT_STREQ("ena", TestEnum::ENA.toString().c_str());
        ASSERT_STREQ("enb", TestEnum::ENB.toString().c_str());
        ASSERT_STREQ("EnQ", TestEnum::EN_Q.toString().c_str());
        ASSERT_STREQ("EnRaw", TestEnum::EN_RAW.toString().c_str());
    }

    TEST(BaseEnumTest, fromString) {
        ASSERT_EQ(TestEnum::ENB, TestEnum::fromString("ENB"));
        ASSERT_EQ(TestEnum::ENA, TestEnum::fromString("ena"));
        ASSERT_EQ(TestEnum::ENA, TestEnum::fromString("ENA"));
        ASSERT_EQ(TestEnum::EN_Q, TestEnum::fromString("EN_Q"));
        ASSERT_EQ(TestEnum::EN_Q, TestEnum::fromString("En_Q"));
        ASSERT_EQ(TestEnum::EN_Q, TestEnum::fromString("enq"));
        ASSERT_EQ(TestEnum::EN_RAW, TestEnum::fromString("EN_RAW"));
        ASSERT_EQ(TestEnum::EN_RAW, TestEnum::fromString("En_Raw"));
        ASSERT_EQ(TestEnum::EN_RAW, TestEnum::fromString("enraw"));
        ASSERT_EQ(TestEnum::unknown, TestEnum::fromString("en_ra_w"));
    }

    TEST(BaseEnumTest, Json) {
        json some = {{"type", TestEnum::ENA}};
        ASSERT_STREQ("{\"type\":\"ena\"}", some.dump().c_str());
        ASSERT_EQ(TestEnum::ENA, TestEnum::fromString(some["type"]));
        ASSERT_EQ(TestEnum::unknown, TestEnum::fromString("hehe"));
    }
}  // namespace Com::Wui::Framework::XCppCommons::Primitives
