/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Primitives {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    TEST(StringTest, NewLine) {
#ifdef WIN_PLATFORM
        ASSERT_STREQ("\r\n", String::NewLine().c_str());
#else
        ASSERT_STREQ("\n", String::NewLine().c_str());
#endif
    }

    TEST(StringTest, Space) {
        ASSERT_STREQ("    ", String::Space(4).c_str());
        ASSERT_STREQ("", String::Space(0).c_str());
        ASSERT_STREQ("", String::Space(-2).c_str());
    }

    TEST(StringTest, Tab) {
        ASSERT_STREQ("\t\t\t\t", String::Tab(4).c_str());
        ASSERT_STREQ("", String::Tab(0).c_str());
        ASSERT_STREQ("", String::Tab(-4).c_str());
    }

    TEST(StringTest, Length) {
        ASSERT_EQ(0u, String::Length(""));
        ASSERT_EQ(6u, String::Length("String"));
        ASSERT_EQ(13u, String::Length("1400483705187"));
    }

    TEST(StringTest, IsEmpty) {
        ASSERT_EQ(true, String::IsEmpty(""));
        ASSERT_EQ(false, String::IsEmpty("test string"));
    }

    TEST(StringTest, ToUpperCase) {
        ASSERT_STREQ("HELLO WORLD", String::ToUpperCase("heLLo WoRlD").c_str());
        ASSERT_STREQ("", String::ToUpperCase("").c_str());
    }

    TEST(StringTest, ToLowerCase) {
        ASSERT_STREQ("", String::ToLowerCase("").c_str());
        ASSERT_STREQ("hello world", String::ToLowerCase("HEllO wOrlD").c_str());
    }

    TEST(StringTest, IndexOf) {
        ASSERT_EQ(1, String::IndexOf("string", "t"));
        ASSERT_EQ(7, String::IndexOf("stringstring", "t", true, 3));
        ASSERT_EQ(0, String::IndexOf("stringstring", ""));
        ASSERT_EQ(2, String::IndexOf("stringstring", "ri"));
        ASSERT_EQ(8, String::IndexOf("stringstring", "ri", true, 4));
        ASSERT_EQ(std::string::npos, static_cast<size_t>(String::IndexOf("stringstring", "u")));
        ASSERT_EQ(4, String::IndexOf("string", "n", false));
        ASSERT_EQ(7, String::IndexOf("stringstring", "t", false, 5));
        ASSERT_EQ(std::string::npos, static_cast<size_t>(String::IndexOf("stringstring", "", false)));
    }

    TEST(StringTest, getCharacterAt) {
        ASSERT_STREQ("s", String::getCharacterAt("test", 2).c_str());
        ASSERT_STREQ("", String::getCharacterAt("test", 10).c_str());
        ASSERT_STREQ("", String::getCharacterAt("", 10).c_str());
        ASSERT_STREQ("", String::getCharacterAt("test", -1).c_str());
        ASSERT_STREQ("t", String::getCharacterAt("test", 0).c_str());
        ASSERT_STREQ("t", String::getCharacterAt("test", 3).c_str());
        ASSERT_STREQ("", String::getCharacterAt("test", 4).c_str());
    }

    TEST(StringTest, getCodeAt) {
        ASSERT_EQ(115, String::getCodeAt("test", 2));
        ASSERT_EQ(-1, String::getCodeAt("test", 10));
        ASSERT_EQ(-1, String::getCodeAt("", 10));
        ASSERT_EQ(-1, String::getCodeAt("", 0));
        ASSERT_EQ(116, String::getCodeAt("test", 0));
        ASSERT_EQ(116, String::getCodeAt("test", 3));
    }

    TEST(StringTest, getCrc) {
        ASSERT_EQ(323425605u, String::getCrc("test string"));
        ASSERT_EQ(2112057749u, String::getCrc("tets string"));
        // ASSERT_EQ(-839019276, String::getCrc("按钮的工具提示文本"));
    }

    TEST(StringTest, getSha1) {
        ASSERT_STREQ("661295c9cbf9d6b2f6428414504a8deed3020641", String::getSha1("test string").c_str());
        ASSERT_STREQ("7510ea390b14653c2c3157453664fe33f0dc3cdc", String::getSha1("test strings").c_str());
        ASSERT_STREQ("685c78b5ab454df6b7b152bf3ea481a5677c6dee", String::getSha1("test stringsa").c_str());
        ASSERT_STREQ("879f29896cfeff0d8637d5ed9cb4c43b34006f2a", String::getSha1("test stringsak").c_str());
    }

    TEST(StringTest, Contains) {
        ASSERT_TRUE(String::Contains("template string to match", "string to"));
        ASSERT_FALSE(String::Contains("template string to match", "unknown"));
        ASSERT_TRUE(String::Contains("String", "tri"));
        ASSERT_FALSE(String::Contains("StRiNg", "tri"));

        std::vector<string> searchStrings = {"st", "Ng"};
        ASSERT_TRUE(String::Contains("StRiNg", searchStrings));

        ASSERT_FALSE(String::Contains("string", "ssttrriinngg"));
        ASSERT_TRUE(String::Contains("test string", "s"));
        ASSERT_FALSE(String::Contains("test string", ""));
        ASSERT_FALSE(String::Contains("", "find me"));
        ASSERT_FALSE(String::Contains("", ""));
        ASSERT_TRUE(String::Contains("test", "test"));
        ASSERT_FALSE(String::Contains("tester", "tes ter"));

        searchStrings = {"", ""};
        ASSERT_FALSE(String::Contains("StRiNg", searchStrings));
    }

    TEST(StringTest, ContainsIgnoreCase) {
        ASSERT_TRUE(String::ContainsIgnoreCase("StRiNg", "string"));
        ASSERT_FALSE(String::ContainsIgnoreCase("StRiNg", "sttr"));
        ASSERT_FALSE(String::ContainsIgnoreCase("", "sttr"));
        ASSERT_FALSE(String::ContainsIgnoreCase("StRiNg", ""));

        std::vector<string> searchStrings = {"str", "Ngr", "ng"};
        ASSERT_TRUE(String::ContainsIgnoreCase("StRiNg", searchStrings));
    }

    TEST(StringTest, OccurrenceCount) {
        ASSERT_EQ(1, String::OccurrenceCount("StRiNg", "St"));
        ASSERT_EQ(0, String::OccurrenceCount("StRiNg", "tr"));
        ASSERT_EQ(0, String::OccurrenceCount("StRiNg", ""));
        ASSERT_EQ(3, String::OccurrenceCount("stringstringstring", "string"));
        ASSERT_EQ(2, String::OccurrenceCount("string strin gstring", "string"));
        ASSERT_EQ(1, String::OccurrenceCount("string strin gstring", "string "));
        ASSERT_EQ(0, String::OccurrenceCount("stringstringstring", "string "));
        ASSERT_EQ(0, String::OccurrenceCount("", "a"));
    }

    TEST(StringTest, StartsWith) {
        ASSERT_TRUE(String::StartsWith("template string to match", "template"));
        ASSERT_FALSE(String::StartsWith("template string to match", "unknown"));
        ASSERT_FALSE(String::StartsWith("", "unknown"));
        ASSERT_TRUE(String::StartsWith("StRiNg", "St"));
        ASSERT_FALSE(String::StartsWith("StRiNg", "Ri"));
        ASSERT_FALSE(String::StartsWith("StRiNg", "st"));
    }

    TEST(StringTest, EndsWith) {
        ASSERT_TRUE(String::EndsWith("StRiNg", "Ng"));
        ASSERT_FALSE(String::EndsWith("StRiNg", "Ri"));
    }

    TEST(StringTest, PatternMatched) {
        ASSERT_TRUE(String::PatternMatched("*", "string"));
        ASSERT_TRUE(String::PatternMatched("string", "string"));
        ASSERT_TRUE(String::PatternMatched("string*", "string"));
        ASSERT_TRUE(String::PatternMatched("string*", "stringStr"));
        ASSERT_TRUE(String::PatternMatched("*string", "Strstring"));
        ASSERT_TRUE(String::PatternMatched("*string*", "StrstringStr"));
        ASSERT_FALSE(String::PatternMatched("string*", "StrstringStr"));
        ASSERT_FALSE(String::PatternMatched("*test*", "7t77est7"));
        ASSERT_FALSE(String::PatternMatched("string*", "Strstring"));
        ASSERT_FALSE(String::PatternMatched("*test*", ""));
        ASSERT_FALSE(String::PatternMatched("str*ing", "string2"));
        ASSERT_FALSE(String::PatternMatched("", "string2"));
        ASSERT_TRUE(String::PatternMatched("str**i*ng", "string"));
    }

    TEST(StringTest, Substring) {
        ASSERT_STREQ(String::Substring("StRiNg", 2, 4).c_str(), "Ri");
        ASSERT_STREQ(String::Substring("StRiNg", 2).c_str(), "RiNg");
        ASSERT_STREQ(String::Substring("karlejatojde", 3, 9).c_str(), "lejato");
        ASSERT_STREQ(String::Substring("StRiNg", -212).c_str(), "");
    }

    TEST(StringTest, Replace) {
        ASSERT_STREQ("this string was replaced", String::Replace("this string to be replaced", "to be", "was").c_str());
        ASSERT_STREQ("StStNgStStNgStStNg", String::Replace("StRiNgStRiNgStRiNg", "Ri", "St").c_str());
    }

    TEST(StringTest, Remove) {
        ASSERT_STREQ("this string was removed", String::Remove("this string ALPHA was removed", "ALPHA ").c_str());
        ASSERT_STREQ("StNg", String::Remove("StRiNg", "Ri").c_str());
        ASSERT_STREQ("StRiNg", String::Remove("StRiNg", "").c_str());
        ASSERT_STREQ("", String::Remove("StRiNg", "StRiNg").c_str());
        ASSERT_STREQ("", String::Remove("", "StRiNg").c_str());
        ASSERT_STREQ("StRiNg", String::Remove("StRiNg", "pr").c_str());
    }

    TEST(StringTest, Split) {
        std::vector<string> data = {"*"};
        auto result = String::Split("St*Ri*Ng", data);
        ASSERT_STREQ("St", result[0].c_str());
        ASSERT_STREQ("Ri", result[1].c_str());
        ASSERT_STREQ("Ng", result[2].c_str());

        data = {";", ":"};
        result = String::Split("name1:value1;name2:value2", data);
        ASSERT_STREQ("name1", result[0].c_str());
        ASSERT_STREQ("value1", result[1].c_str());
        ASSERT_STREQ("name2", result[2].c_str());
        ASSERT_STREQ("value2", result[3].c_str());

        data = {"*"};
        result = String::Split("*string*", data);
        ASSERT_STREQ("", result[0].c_str());
        ASSERT_STREQ("string", result[1].c_str());
        ASSERT_STREQ("", result[2].c_str());

        data.clear();
        result = String::Split("string", data);
        ASSERT_STREQ("string", result[0].c_str());
    }

    TEST(StringTest, Format) {
        ASSERT_STREQ("hello world in 21' century.", String::Format("hello {0} in {1}' century.", "world", 21).c_str());
        ASSERT_STREQ("0 1 2 1 0", String::Format("{0} {1} {2} {1} {0}", 0, 1, 2).c_str());
        ASSERT_STREQ("0 {1} 11 {2} 22", String::Format("{0} {{1}} {1} {{2}} {2}", 0, 11, 22).c_str());
        ASSERT_ANY_THROW(String::Format("{0}: {1:X}", "hex", 10));
    }

    TEST(StringTest, StripSlashes) {
        ASSERT_STREQ("f'oo", String::StripSlashes("f'oo").c_str());
        ASSERT_STREQ("foo", String::StripSlashes("f\\o\\o\\\\").c_str());
        ASSERT_STREQ("string", String::StripSlashes("string").c_str());
        ASSERT_STREQ("str\u0000ing", String::StripSlashes("str\0ing").c_str());
        ASSERT_STREQ("", String::StripSlashes("").c_str());
    }

    TEST(StringTest, StripTags) {
        ASSERT_STREQ("string", String::StripTags("<b>string</b>").c_str());
        ASSERT_STREQ("", String::StripTags("<script type='text/javascript'>var prop=1;</script>").c_str());
        ASSERT_STREQ("Html string and plain text",
                     String::StripTags("<script type='text/javascript'>var prop=1;</script><b>Html string</b> and plain text").c_str());
        ASSERT_STREQ("<b>string", String::StripTags("<b>string</b>", "<b>").c_str());
    }

    TEST(StringTest, StripComments) {
        ASSERT_STREQ("abc", String::StripComments("/*string*/abc").c_str());
        ASSERT_STREQ("", String::StripComments("/*string*/").c_str());
        ASSERT_STREQ("", String::StripComments("").c_str());
    }

    TEST(StringTest, HardWrap) {
        string foo = String::HardWrap("this is very long test string this is very long test string this is very long test string", 50);
        foo = String::Replace(foo, String::NewLine().c_str(), "##EOL##");
        ASSERT_STREQ("this is very long test string this is very long te##EOL##st string this is very long test string", foo.c_str());

        string input("You will be able to read and write code for a large number of platforms "
                             "-- everything from microcontrollers to the most advanced scientific systems can be written in C, "
                             " and many modern operating systems are written in C. Check out this article in Digikey's techzone and "
                             "get to know which analog and power products from NXP help in designing for functional safety. "
                             "The H-Bridge driver HB2001, for example, responds to a growing need for critical functions in high"
                             " reliability systems. Find out more about our Safe Assure program and the products supported here");


        foo = String::HardWrap(input);
        foo = String::Replace(foo, String::NewLine(), "##EOL##");

        ASSERT_EQ(String::getCodeAt(foo, 300), '#');
        ASSERT_EQ(String::getCodeAt(foo, 301), '#');
        ASSERT_EQ(String::getCodeAt(foo, 302), 'E');
        ASSERT_EQ(String::getCodeAt(foo, 303), 'O');
        ASSERT_EQ(String::getCodeAt(foo, 304), 'L');
        ASSERT_EQ(String::getCodeAt(foo, 305), '#');
        ASSERT_EQ(String::getCodeAt(foo, 306), '#');
    }

    TEST(StringTest, ToArray) {
        std::vector<char> result;
        result = String::ToArray("String");
        ASSERT_EQ(result.size(), static_cast<unsigned>(6));
        ASSERT_EQ('S', result[0]);
        ASSERT_EQ('t', result[1]);
        ASSERT_EQ('r', result[2]);
        ASSERT_EQ('i', result[3]);
        ASSERT_EQ('n', result[4]);
        ASSERT_EQ('g', result[5]);
    }

    TEST(StringTest, ToInteger) {
        ASSERT_EQ(1, String::ToInteger("1"));
        ASSERT_EQ(0, String::ToInteger("0"));
        ASSERT_NE(std::numeric_limits<int>::max(), String::ToInteger("0"));
        ASSERT_EQ(16443, String::ToInteger("16443"));
        ASSERT_EQ(std::numeric_limits<int>::max(), String::ToInteger("r"));
        ASSERT_EQ(std::numeric_limits<int>::max(), String::ToInteger(""));
    }

    TEST(StringTest, ToDouble) {
        ASSERT_EQ(16443.0, String::ToDouble("16443"));
        ASSERT_EQ(16443.99, String::ToDouble("16443.99"));
        ASSERT_TRUE(std::isnan(String::ToDouble("r")));
        ASSERT_TRUE(std::isnan(String::ToDouble("")));
        ASSERT_FALSE(std::isnan(String::ToDouble("3")));
}

    TEST(StringTest, ToBoolean) {
        ASSERT_FALSE(String::ToBoolean("false"));
        ASSERT_FALSE(String::ToBoolean("foo"));
        ASSERT_FALSE(String::ToBoolean("FALSE"));
        ASSERT_TRUE(String::ToBoolean("true"));
        ASSERT_TRUE(String::ToBoolean("TRUE"));
        ASSERT_TRUE(String::ToBoolean("On"));
        ASSERT_TRUE(String::ToBoolean("1"));
        ASSERT_FALSE(String::ToBoolean(""));
    }

    TEST(StringTest, IsEqual) {
        ASSERT_TRUE(String::IsEqual("false", "FAlSE"));
        ASSERT_FALSE(String::IsEqual("false", "FALSch"));
        ASSERT_TRUE(String::IsEqual("foo", "foo"));
        ASSERT_TRUE(String::IsEqual("", ""));
    }
    TEST(StringTest, ToHexadecimal) {
        ASSERT_EQ(1952805748, String::ToHexadecimal("test"));
    }

    TEST(StringTest, VersionIsLower) {
        ASSERT_FALSE(String::VersionIsLower("", "1.1.0"));
        ASSERT_FALSE(String::VersionIsLower("1.1.0", ""));
        ASSERT_FALSE(String::VersionIsLower("", ""));
        ASSERT_TRUE(String::VersionIsLower("1.0.0", "1.1.0"));
        ASSERT_TRUE(String::VersionIsLower("1.0.0-beta", "1.1.0"));
        ASSERT_TRUE(String::VersionIsLower("1.0.0asd", "1.1.0"));
        ASSERT_TRUE(String::VersionIsLower("1.0.0.0", "1.1.0"));
        ASSERT_TRUE(String::VersionIsLower("1.0", "1.1.0"));
        ASSERT_FALSE(String::VersionIsLower("1.1", "1.x.x"));
        ASSERT_FALSE(String::VersionIsLower("2.0.0.0", "1.1.x"));
        ASSERT_FALSE(String::VersionIsLower("3.3.3", "3.3.3"));
        ASSERT_TRUE(String::VersionIsLower("1.0.2", "1.0.2b12"));
        ASSERT_TRUE(String::VersionIsLower("10.10", "11.0"));
        ASSERT_FALSE(String::VersionIsLower("1.2-rc3", "1.2-a1"));
        ASSERT_FALSE(String::VersionIsLower("1.x.2.*", "1.x.x.*"));
        ASSERT_FALSE(String::VersionIsLower("2.0.0.0", "1.0.0.0"));
        ASSERT_FALSE(String::VersionIsLower("2.1.*.", "2.*."));
        ASSERT_FALSE(String::VersionIsLower("2.*.*.", "2.*."));
        ASSERT_FALSE(String::VersionIsLower("2.11.0.windows.1", "2.10.2"));
    }
}  // namespace Com::Wui::Framework::XCppCommons::Primitives
