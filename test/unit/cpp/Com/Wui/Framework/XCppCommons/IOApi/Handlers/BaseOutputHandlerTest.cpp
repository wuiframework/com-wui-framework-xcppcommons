/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers {

    class TestHandler : public BaseOutputHandler {
     public:
        explicit TestHandler(const string &$name = "")
                : BaseOutputHandler($name) {
        }

        void SetAtt() {
            this->setNewLine("NL");
            this->setEncoding("ENC");
        }
    };

    TEST(BaseOutputHandlerTest, Construction) {
        string name = BaseOutputHandler().Name();
        string name2 = BaseOutputHandler().Name();
        int point = name.find_last_of("_");
        int point2 = name2.find_last_of("_");
        ASSERT_STREQ("OutputHandler", name.substr(0, point).c_str());
        ASSERT_EQ(boost::lexical_cast<int>(name.substr(point + 1)) + 1,
                  boost::lexical_cast<int>(name2.substr(point2 + 1)));

        ASSERT_STREQ("unknown", BaseOutputHandler("unknown").Name().c_str());
    }

    TEST(BaseOutputHandlerTest, Encoding) {
        TestHandler testHandler;

        ASSERT_STREQ("", testHandler.Encoding().c_str());
        testHandler.Init();
        ASSERT_STREQ("UTF-8", testHandler.Encoding().c_str());
        testHandler.SetAtt();
        ASSERT_STREQ("ENC", testHandler.Encoding().c_str());
    }

    TEST(BaseOutputHandlerTest, NewLine) {
        TestHandler testHandler;

        ASSERT_STREQ("", testHandler.NewLine().c_str());
        testHandler.SetAtt();
        ASSERT_STREQ("NL", testHandler.NewLine().c_str());
    }

    TEST(BaseOutputHandlerTest, Print) {
        ASSERT_THROW(BaseOutputHandler().Print(""), std::runtime_error);
    }

    TEST(BaseOutputHandlerTest, Println) {
        ASSERT_THROW(BaseOutputHandler().Println(""), std::runtime_error);
    }

    TEST(BaseOutputHandlerTest, Clear) {
        ASSERT_THROW(BaseOutputHandler().Clear(), std::runtime_error);
    }
}  // namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers
