/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers {

    class TestConsole : public ConsoleHandler {
     public:
        TestConsole() {
            this->setNewLine("<EOL>");
        }
    };

    TEST(ConsoleHandlerTest, Construction) {
        ConsoleHandler consoleHandler;
        string name = consoleHandler.Name();

        ASSERT_STREQ("OutputHandler", name.substr(0, name.find_last_of("_")).c_str());
    }

    TEST(ConsoleHandlerTest, Print) {
        ConsoleHandler consoleHandler;
        testing::internal::CaptureStdout();
        consoleHandler.Print("test message");
        ASSERT_STREQ("test message", testing::internal::GetCapturedStdout().c_str());
    }

    TEST(ConsoleHandlerTest, Println) {
        TestConsole testConsole;
        testing::internal::CaptureStdout();
        testConsole.Println("test message");
        ASSERT_STREQ("test message<EOL>", testing::internal::GetCapturedStdout().c_str());
    }
}  // namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers
