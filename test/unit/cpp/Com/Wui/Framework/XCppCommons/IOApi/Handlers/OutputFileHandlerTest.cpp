/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using boost::posix_time::ptime;
    using boost::posix_time::second_clock;
    using boost::gregorian::day_clock;
    namespace fs = boost::filesystem;

    static string backCwd;

    class OutputFileHandlerTest : public testing::Test {
     public:
        static void SetUpTestCase() {
            backCwd = boost::filesystem::current_path().string();
            string tmp = boost::filesystem::path(backCwd + "/test/runtimeData").normalize().make_preferred().string();

            boost::filesystem::create_directory(tmp);

            chdir(tmp.c_str());
        }

        static void TearDownTestCase() {
            chdir(backCwd.c_str());
            testing::internal::CaptureStdout();
            Com::Wui::Framework::XCppCommons::System::IO::FileSystem::Delete(backCwd + "/test/runtimeData");
            testing::internal::GetCapturedStdout();
        }
    };

    TEST_F(OutputFileHandlerTest, Construction) {
        OutputFileHandler outputFileHandler;
        outputFileHandler.NewLine();
#ifdef WIN_PLATFORM
        ASSERT_STREQ("\r\n", outputFileHandler.NewLine().c_str());
#else
        ASSERT_STREQ("\n", outputFileHandler.NewLine().c_str());
#endif
    }

    TEST_F(OutputFileHandlerTest, Init) {
        OutputFileHandler outputFileHandler;
        ASSERT_NO_THROW(fs::remove_all(fs::path("./log")));
        ASSERT_FALSE(fs::exists(fs::path("./log")));
        outputFileHandler.Init();
        ASSERT_TRUE(fs::exists(fs::path("./log")));

        ptime utc(day_clock::universal_day(), second_clock::universal_time().time_of_day());
        std::stringstream ss;
        ss << boost::str(boost::format("%04d/%02d") % utc.date().year() % utc.date().month().as_number());
        string dir = ss.str();
        ss.str("");
        ss << boost::str(boost::format("%02d_%02d_%04d.txt") % utc.date().day().as_number() %
                         utc.date().month().as_number() % utc.date().year());
        string file = ss.str();

        ASSERT_TRUE(fs::exists(fs::path("./log") / dir / file));
    }

    TEST_F(OutputFileHandlerTest, Print) {
        OutputFileHandler outputFileHandler;
        ASSERT_NO_THROW(fs::remove_all(fs::path("./log")));
        outputFileHandler.Init();
        string file = outputFileHandler.getOutputFilePath();
        std::ifstream inputFile;
        ASSERT_NO_THROW(inputFile.open(file));
        string data((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
        inputFile.close();
        ASSERT_STREQ("", data.c_str());
        outputFileHandler.Print("<temporary data>");
        ASSERT_NO_THROW(inputFile.open(file));
        data = string((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
        inputFile.close();
        ASSERT_STREQ("<temporary data>", data.c_str());
    }

    TEST_F(OutputFileHandlerTest, Println) {
        OutputFileHandler outputFileHandler;
        outputFileHandler.Init();
        string file = outputFileHandler.getOutputFilePath();
        std::ifstream inputFile;
        ASSERT_NO_THROW(inputFile.open(file));
        string dataRef((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
        inputFile.close();
        outputFileHandler.Println("?appended#stringline");
        ASSERT_NO_THROW(inputFile.open(file));
        string dataNew((std::istreambuf_iterator<char>(inputFile)), std::istreambuf_iterator<char>());
        inputFile.close();
        ASSERT_STREQ(("?appended#stringline" + outputFileHandler.NewLine()).c_str(),
                     dataNew.substr(dataRef.length()).c_str());
    }
}  // namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers
