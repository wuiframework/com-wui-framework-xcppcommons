/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::IOApi {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Enums::IOHandlerType;
    using Com::Wui::Framework::XCppCommons::Interfaces::IOHandler;

    using Com::Wui::Framework::XCppCommons::IOApi::Handlers::ConsoleHandler;
    using Com::Wui::Framework::XCppCommons::IOApi::Handlers::OutputFileHandler;

    TEST(IOHandlerFactoryTest, getHandlerType) {
        shared_ptr<IOHandler> handler(new ConsoleHandler("testConsole"));
        ASSERT_TRUE(IOHandlerType::CONSOLE == IOHandlerFactory::getHandlerType(handler));
        handler.reset(new OutputFileHandler("testFileHandler"));
        ASSERT_TRUE(IOHandlerType::OUTPUT_FILE == IOHandlerFactory::getHandlerType(handler));
        handler.reset();
        ASSERT_TRUE(IOHandlerType::CONSOLE == IOHandlerFactory::getHandlerType(handler));
    }

    TEST(IOHandlerFactoryTest, getHandler) {
        shared_ptr<IOHandler> handler1 = IOHandlerFactory::getHandler(IOHandlerType::CONSOLE, "handler1");
        shared_ptr<IOHandler> handler2 = IOHandlerFactory::getHandler(IOHandlerType::OUTPUT_FILE, "handler2");
        shared_ptr<IOHandler> handler3 = IOHandlerFactory::getHandler(IOHandlerType::OUTPUT_FILE, "handler3");
        ASSERT_TRUE(handler1);
        ASSERT_TRUE(handler2);
        ASSERT_TRUE(handler3);

        ASSERT_TRUE(IOHandlerType::CONSOLE == IOHandlerFactory::getHandlerType(handler1));
        ASSERT_TRUE(IOHandlerType::OUTPUT_FILE == IOHandlerFactory::getHandlerType(handler2));

        ASSERT_NE(handler1.get(), handler2.get());
        ASSERT_NE(handler2.get(), handler3.get());
        ASSERT_NE(handler1.get(), handler3.get());

        ASSERT_STREQ("handler2\\handlers\\output.txt", handler2->Name().c_str());
        ASSERT_STREQ("handler3\\handlers\\output.txt", handler3->Name().c_str());

        shared_ptr<IOHandler> tmpHandler = IOHandlerFactory::getHandler(IOHandlerType::CONSOLE, "handler1");
        ASSERT_EQ(handler1.get(), tmpHandler.get());
        tmpHandler = IOHandlerFactory::getHandler(IOHandlerType::OUTPUT_FILE, "handler3");
        ASSERT_EQ(handler3.get(), tmpHandler.get());
    }

    TEST(IOHandlerFactoryTest, DestroyAll) {
        shared_ptr<IOHandler> handler1 = IOHandlerFactory::getHandler(IOHandlerType::CONSOLE, "handler1");
        shared_ptr<IOHandler> handler2 = IOHandlerFactory::getHandler(IOHandlerType::OUTPUT_FILE, "handler2");

        IOHandlerFactory::DestroyAll();

        ASSERT_NE(handler1.get(), IOHandlerFactory::getHandler(IOHandlerType::CONSOLE, "handler1").get());
        ASSERT_NE(handler2.get(), IOHandlerFactory::getHandler(IOHandlerType::OUTPUT_FILE, "handler2").get());
    }
}  // namespace Com::Wui::Framework::XCppCommons::IOApi
