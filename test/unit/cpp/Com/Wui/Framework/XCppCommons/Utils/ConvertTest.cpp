/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Utils {
    TEST(ConvertTest, TimeToGMTFormat) {
        using boost::posix_time::ptime;
        using boost::posix_time::time_from_string;

        ptime testData(time_from_string("2017-01-18 23:59:47.000"));
        ASSERT_STREQ("Wed, 18 Jan 2017 23:59:47 GMT", Convert::TimeToGMTFormat(testData).c_str());
    }

    TEST(ConvertTest, TimeToSeconds) {
        ASSERT_EQ(0L, Convert::TimeToSeconds(0L));
        ASSERT_EQ(2147483646L, Convert::TimeToSeconds(2147483646000L));
    }

    TEST(ConvertTest, LastModifiedToTimestamp) {
        ASSERT_EQ(1498117847L, Convert::LastModifiedToTimestamp("Thu, 22 Jun 2017 07:50:47 GMT"));
    }
}  // namespace Com::Wui::Framework::XCpp::Commons::Utils
