/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Events::ThreadPool;
using Com::Wui::Framework::XCppCommons::Events::Args::EventArgs;

namespace Com::Wui::Framework::XCppCommons::Utils {
    TEST(IpcPipeObserverTest, Construction) {
        IpcPipeObserver ipc("application", 99);

        ASSERT_STREQ("application_99", ipc.getName().c_str());
    }

    TEST(IpcPipeObserverTest, Communication) {
        ThreadPool threadPool;
        std::vector<string> recArray;
        std::vector<string> sendArray;
        boost::barrier barrier(2);

        threadPool.AddThread("serverTask", [&recArray, &barrier](const EventArgs &args) {
            barrier.wait();
            IpcPipeObserver ipcServer("Communication", 99);
            if (ipcServer.Initialize(true)) {
                bool isRunning = true;
                while (isRunning) {
                    string recData = ipcServer.Receive();
                    if (recData.size() != 0) {
                        int cmp = recData.compare("stop");
                        if (cmp == 0) {
                            isRunning = false;
                            continue;
                        }
                        recArray.push_back(recData);
                    }
                    boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
                }
            }
        });

        threadPool.AddThread("clientTask", [&sendArray, &barrier](const EventArgs &args) {
            barrier.wait();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(100));
            IpcPipeObserver ipcClient("Communication", 99);
            if (ipcClient.Initialize(false)) {
                string data;
                for (int i = 0; i < 10; i++) {
                    data = "data " + boost::lexical_cast<string>(i);
                    sendArray.push_back(data);
                    ipcClient.Send(data);
                    boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
                }
                ipcClient.Send("stop");
            }
        });

        threadPool.Execute();

        ASSERT_EQ(sendArray.size(), recArray.size());
        for (unsigned int i = 0; i < sendArray.size(); i++) {
            ASSERT_STREQ(sendArray[i].c_str(), recArray[i].c_str());
        }
    }

    TEST(IpcPipeObserverTest, DISABLED_DelayedServer) {
        ThreadPool threadPool;
        boost::barrier barrier(2);

        boost::interprocess::message_queue::remove("DelayedServer_999_in");
        boost::interprocess::message_queue::remove("DelayedServer_999_out");

        IpcPipeObserver ipcServer("DelayedServer", 999);
        threadPool.AddThread("serverTask", [&barrier, &ipcServer](const EventArgs &args) {
            barrier.wait();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
            ASSERT_TRUE(ipcServer.Initialize(true));
            bool isRunning = true;
            while (isRunning) {
                string recData = ipcServer.Receive();
                if (recData.size() != 0) {
                    int cmp = recData.compare("stop");
                    if (cmp == 0) {
                        isRunning = false;
                        continue;
                    }
                }
                boost::this_thread::sleep_for(boost::chrono::milliseconds(5));
            }
        });

        IpcPipeObserver ipcClient("DelayedServer", 999);
        threadPool.AddThread("clientTask", [&barrier, &ipcClient, &threadPool](const EventArgs &args) {
            barrier.wait();
            if (ipcClient.Initialize(false, 500)) {
                ipcClient.Send("stop");
                boost::this_thread::sleep_for(boost::chrono::milliseconds(500));
                if (threadPool.IsRunning("serverTask")) {
                    threadPool.RemoveThread("serverTask");
                }
            } else {
                threadPool.RemoveThread("serverTask");
                ASSERT_TRUE(false) << "Initialize IPC client with timeout failed" << std::endl;
            }
        });

        threadPool.Execute();
    }
}  // namespace Com::Wui::Framework::XCppCommons::Utils
