/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Events::ThreadPool;
using Com::Wui::Framework::XCppCommons::Events::Args::EventArgs;
using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
using Com::Wui::Framework::XCppCommons::IOApi::IOHandlerFactory;
using Com::Wui::Framework::XCppCommons::IOApi::Handlers::OutputFileHandler;
using Com::Wui::Framework::XCppCommons::Enums::IOHandlerType;
using Com::Wui::Framework::XCppCommons::Interfaces::IOHandler;

namespace Com::Wui::Framework::XCppCommons::Utils {
    struct TestPair {
        LogLevel logLevel;
        std::vector<string> output;
    };

    class LogItTest : public testing::TestWithParam<TestPair> {
     public:
        static void TearDownTestCase() {
            LogIt::setLevel(LogLevel::ERROR);
        }

        void logThemAll() {
            LogIt::Info("<?info?>");
            LogIt::Warning("<?warning?>");
            LogIt::Debug("<?debug?>");
            LogIt::Error("<?error?>");
        }
    };

    TEST_P(LogItTest, ValidateLogType) {
        LogIt::setLevel(GetParam().logLevel);
        string output;
        LogIt::setOnPrint([&](const string &$msg) {
            output += $msg;
        });
        logThemAll();
        LogIt::setOnPrint(nullptr);
        std::vector<string> orig = GetParam().output;
        std::for_each(orig.begin(), orig.end(), [&output, &orig](const string &item) {
            ASSERT_TRUE(boost::algorithm::contains(output, item))
                                        << "+ " << output << " +" << std::endl << "- * " << item << " * -" << std::endl;
        });
    }

    INSTANTIATE_TEST_CASE_P(
            LogItProfiles,
            LogItTest,
            testing::Values(TestPair{LogLevel::INFO, std::vector<string>{
                                    "<?info?>"}},
                            TestPair{LogLevel::WARNING, std::vector<string>{
                                    "<?info?>", "<?warning?>"}},
                            TestPair{LogLevel::ERROR, std::vector<string>{
                                    "<?info?>", "<?warning?>", "<?error?>"}},
                            TestPair{LogLevel::DEBUG, std::vector<string>{
                                    "<?debug?>"}},
                            TestPair{LogLevel::ALL, std::vector<string>{
                                    "<?info?>", "<?warning?>", "<?debug?>", "<?error?>"}}));

    TEST(LogItTest, Format) {
        LogIt::setLevel(LogLevel::DEBUG);
        string output;
        bool callbackCalled = false;
        LogIt::setLevel(LogLevel::DEBUG);
        LogIt::setOnPrint([&](const string &$msg) {
            output += $msg;
            callbackCalled = true;
        });
        LogIt::Debug("< hello {0} awesome {1}'s world dimension. >", "future", 25);
        LogIt::setOnPrint(nullptr);
        ASSERT_TRUE(callbackCalled);
        int start = output.find_first_of('<');
        int size = output.find_last_of('>') - start + 1;
        ASSERT_FALSE(output.empty());
        ASSERT_STREQ("< hello future awesome 25's world dimension. >", output.substr(start, size).c_str());
    }

    TEST(LogItTest, Format_invalid) {
        LogIt::setLevel(LogLevel::DEBUG);
        string output;
        LogIt::setOnPrint([&](const string &$msg) {
            output += $msg;
        });
        LogIt::Debug("? hello {} awesome {1} ?", "future", 25, 10);
        LogIt::setOnPrint(nullptr);
        int start = output.find_first_of('?');
        int size = output.find_last_of('!') - start + 1;
        ASSERT_STREQ("? hello {} awesome {1} ?", boost::trim_copy(output.substr(start, size)).c_str());
    }

    TEST(LogItTest, Format_anonymized) {
#ifdef WIN_PLATFORM
        std::vector<string> testMap = {
                "TMP",
                "USERPROFILE",
                "HOMEPATH",
                "USERDOMAIN",
                "COMPUTERNAME",
                "USERNAME"
        };
#else
        std::vector<string> testMap = {
                "HOME",
                "HOSTNAME",
                "LOGNAME"
        };
#endif

        string data;
        string original;
        for (const auto &item : testMap) {
            char *tmp = getenv(item.c_str());
            if (tmp != nullptr) {
                data += "key: " + item + "; value: " + string(tmp) + "\n";
                original += "key: " + item + "; value: ";
#ifdef WIN_PLATFORM
                original += "%" + item + "%\n";
#else
                original += "$" + item + "\n";
#endif
            }
        }

        LogIt::setLevel(LogLevel::DEBUG);
        string output;
        LogIt::setOnPrint([&](const string &$msg) {
            output += $msg;
        });

        LogIt::Debug("?" + data + "?");
        LogIt::setOnPrint(nullptr);

        int start = output.find_first_of('?');
        int size = output.find_last_of('?') - start + 1;

        ASSERT_STREQ(("?" + original + "?").c_str(),
                     boost::trim_copy(output.substr(start, size)).c_str());
    }
}  // namespace Com::Wui::Framework::XCppCommons::Utils
