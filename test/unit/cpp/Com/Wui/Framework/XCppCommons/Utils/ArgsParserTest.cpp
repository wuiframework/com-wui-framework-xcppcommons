/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Structures::ProgramArgs;
using Com::Wui::Framework::XCppCommons::EnvironmentArgs;

namespace po = boost::program_options;

namespace Com::Wui::Framework::XCppCommons::Utils {
    class ApplicationTestArgs : public ProgramArgs {
     public:
        ApplicationTestArgs() {
            po::options_description appDescription("Application test options");

            appDescription.add_options()
                    ("testa,a", po::value<int>(&this->testA)->default_value(this->testA), "test option A")
                    ("testb,b", po::value<string>(&this->testB)->default_value(this->testB), "test option B");

            this->getDescription()->add(appDescription);
        }

        int getTestA() {
            return this->testA;
        }

        string getTestB() {
            return this->testB;
        }

     private:
        int testA = 10;
        string testB = "hello";
    };

    class AppException : public std::exception {
        const char *what() const throw() override {
            return ">>test exception<<";
        }
    };

    class ApplicationTestArgsThrowException : public ProgramArgs {
     public:
        void PrintHelp() override {
            throw AppException();
        }

        void PrintVersion() override {
            throw AppException();
        }
    };

    class ArgsParserTest : public testing::Test {
     protected:
        void SetUp() override {
            EnvironmentArgs::getInstance().Load("");
        }
    };

    TEST_F(ArgsParserTest, Parse_Help) {
        ApplicationTestArgs ata;

        ASSERT_EQ(false, ata.IsHelp());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("-h")};
        testing::internal::CaptureStdout();
        ArgsParser::Parse(ata, 2, (const char **) argv);
        testing::internal::GetCapturedStdout();
        ASSERT_EQ(true, ata.IsHelp());

        testing::internal::CaptureStdout();
        ata.PrintHelp();

        string output = testing::internal::GetCapturedStdout();

        ASSERT_LE(10, static_cast<int>(output.length()));
        string expected = ""
                "______________________________________________________________________\n"
                "\n"
                "XCppCommons\n"
                "    - Cpp library for cross-compile executables\n"
                "    copyright:  Copyright (c) 2014-2016 Freescale Semiconductor, Inc.\n"
                "                Copyright (c) 2017-2019 NXP.\n"
                "    author:     Jakub Cieslar, jakub.cieslar@nxp.com\n"
                "    license:    The BSD-3-Clause license for this file can be found\n"
                "                in the LICENSE.txt file included with this\n"
                "                distribution or at\n"
                "                https://spdx.org/licenses/BSD-3-Clause.html#licenseText.\n"
                "______________________________________________________________________\n"
                "//////////////////////////////////////////////////////////////////////\n"
                "\n"
                "Basic options:\n"
                "  -h [ --help ]                Prints application help description.\n"
                "  -v [ --version ]             Prints application version message.\n"
                "  --disable-console            Disable attaching to parent console.\n"
                "\n"
                "Application test options:\n"
                "  -a [ --testa ] arg (=10)     test option A\n"
                "  -b [ --testb ] arg (=hello)  test option B\n"
                "\n"
                "NOTE:\n"
                "    This tool can depends on several prerequisites for more\n"
                "    info see README.md distributed with this material.\n"
                "\n"
                "----------------------------------------------------------------------\n";
        ASSERT_STREQ(expected.c_str(), output.c_str());
    }

    TEST_F(ArgsParserTest, Parse_Version) {
        ApplicationTestArgs ata;

        EXPECT_EQ(false, ata.IsVersion());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("-v")};
        testing::internal::CaptureStdout();
        ArgsParser::Parse(ata, 2, (const char **) argv);
        testing::internal::GetCapturedStdout();
        EXPECT_EQ(true, ata.IsVersion());

        testing::internal::CaptureStdout();
        ata.PrintVersion();

        string output = testing::internal::GetCapturedStdout();
        ASSERT_TRUE(Com::Wui::Framework::XCppCommons::Primitives::String::PatternMatched("XCppCommons v*.*.*\n", output));
    }

    TEST_F(ArgsParserTest, Parse_TestA) {
        ApplicationTestArgs ata;

        EXPECT_EQ(10, ata.getTestA());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--testa=5")};
        ArgsParser::Parse(ata, 2, (const char **) argv);
        EXPECT_EQ(5, ata.getTestA());
    }

    TEST_F(ArgsParserTest, Parse_TestB) {
        ApplicationTestArgs ata;

        EXPECT_EQ("hello", ata.getTestB());
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("--testb=world")};
        ArgsParser::Parse(ata, 2, (const char **) argv);
        EXPECT_STREQ("world", ata.getTestB().c_str());
    }

    TEST_F(ArgsParserTest, Parse_ParseException) {
        ApplicationTestArgsThrowException args;
        const char *argv[] = {const_cast<char *>("program-path"), const_cast<char *>("-h")};
        ASSERT_EQ(-1, ArgsParser::Parse(args, 2, (const char **) argv));
    }
}  // namespace Com::Wui::Framework::XCppCommons::Utils
