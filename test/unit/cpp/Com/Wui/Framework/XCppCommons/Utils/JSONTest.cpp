/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Utils {
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;

    TEST(JSONTest, ParseJsonp_Positive) {
        string file = "test/resource/data/Com/Wui/Framework/XCppCommons/Utils/JSON/json_parse_positive_cases.jsonp";
        string jsonString = FileSystem::Read(file);
        json data;
        ASSERT_NO_THROW({
                            data = JSON::ParseJsonp(jsonString);
                        });
        ASSERT_NO_THROW({
                            ASSERT_STREQ(data["$interface"].get<std::string>().c_str(), "dollar at the beggining");
                        });
        ASSERT_NO_THROW({
                            ASSERT_STREQ(data["_projectName"].get<std::string>().c_str(), "underscore at the beggining");
                        });
        ASSERT_NO_THROW({
                            ASSERT_STREQ(data["projectVersion"].get<std::string>().c_str(), "character started string");
                        });
        ASSERT_NO_THROW({
                            ASSERT_STREQ(data["app2Name"].get<std::string>().c_str(), "number in the middle");
                        });
        ASSERT_NO_THROW({
                            ASSERT_STREQ(data["build_Time"].get<std::string>().c_str(), "underscore in the middle");
                        });
        ASSERT_NO_THROW({
                            ASSERT_STREQ(data["project$Description"].get<std::string>().c_str(), "dollar in the middle");
                        });
    }

    TEST(JSONTest, ParseJsonp_Number) {
        string file = "test/resource/data/Com/Wui/Framework/XCppCommons/Utils/JSON/json_parse_start_number.jsonp";
        string jsonString = FileSystem::Read(file);
        json data;
        ASSERT_ANY_THROW({
                             data = JSON::ParseJsonp(jsonString);
                         });
    }

    TEST(JSONTest, ParseJsonp_Star) {
        string file = "test/resource/data/Com/Wui/Framework/XCppCommons/Utils/JSON/json_parse_contain_star.jsonp";
        string jsonString = FileSystem::Read(file);
        json data;
        ASSERT_ANY_THROW({
                             data = JSON::ParseJsonp(jsonString);
                         });
    }

    TEST(JSONTest, ParseJsonp_Quot) {
        string file = "test/resource/data/Com/Wui/Framework/XCppCommons/Utils/JSON/json_parse_quot_mark_negative.jsonp";
        string jsonString = FileSystem::Read(file);
        json data;
        ASSERT_ANY_THROW({
                             data = JSON::ParseJsonp(jsonString);
                         });
    }

    TEST(JSONTest, ParseJsonp_Reserved) {
        string file = "test/resource/data/Com/Wui/Framework/XCppCommons/Utils/JSON/json_parse_reserved_word.jsonp";
        string jsonString = FileSystem::Read(file);
        json data;
        ASSERT_ANY_THROW({
                             data = JSON::ParseJsonp(jsonString);
                         });
    }

    TEST(JSONTest, CombineJsonp) {
        string firstFile = "test/resource/data/Com/Wui/Framework/XCppCommons/Utils/JSON/json_combine_first.jsonp";
        string secondFile = "test/resource/data/Com/Wui/Framework/XCppCommons/Utils/JSON/json_combine_second.jsonp";
        string jsonString = FileSystem::Read(firstFile);
        json firstJSON = JSON::ParseJsonp(jsonString);
        jsonString = FileSystem::Read(secondFile);
        json secondJSON = JSON::ParseJsonp(jsonString);
        json result = JSON::CombineJson(firstJSON, secondJSON);
        ASSERT_STREQ(result["key1"].get<std::string>().c_str(), "B_value1");
        ASSERT_STREQ(result["key2"].get<std::string>().c_str(), "A_value2");
        ASSERT_EQ(result["key3"][0].get<int>(), 4);
        ASSERT_STREQ(result["object1"]["A_key1"].get<std::string>().c_str(), "A_value1");
        ASSERT_STREQ(result["object1"]["A_key2"].get<std::string>().c_str(), "B_value2");
        ASSERT_STREQ(result["object1"]["B_key1"].get<std::string>().c_str(), "B_value1");
        ASSERT_STREQ(result["object2"]["A_key1"].get<std::string>().c_str(), "A_value1");
        ASSERT_STREQ(result["object3"]["key1"].get<std::string>().c_str(), "B_value1");
    }
}  // namespace Com::Wui::Framework::XCpp::Commons::Utils
