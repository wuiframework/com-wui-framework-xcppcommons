/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Utils {

    TEST(ObjectValidatorTest, IsSet) {
        boost::any undefinedProperty;
        ASSERT_FALSE(ObjectValidator::IsSet(undefinedProperty));
        undefinedProperty = 3.14f;
        ASSERT_TRUE(ObjectValidator::IsSet(undefinedProperty));
    }

    TEST(ObjectValidatorTest, IsEmptyOrNull) {
        ASSERT_TRUE(ObjectValidator::IsEmptyOrNull(std::string()));
        ASSERT_TRUE(ObjectValidator::IsEmptyOrNull(std::string("")));
        ASSERT_TRUE(ObjectValidator::IsEmptyOrNull(nullptr));
        ASSERT_FALSE(ObjectValidator::IsEmptyOrNull(std::string("foo")));
        ASSERT_FALSE(ObjectValidator::IsEmptyOrNull(42));
        ASSERT_TRUE(ObjectValidator::IsEmptyOrNull(boost::any()));
    }

    TEST(ObjectValidatorTest, IsBoolean) {
        ASSERT_TRUE(ObjectValidator::IsBoolean(true));
        ASSERT_TRUE(ObjectValidator::IsBoolean(false));
        ASSERT_FALSE(ObjectValidator::IsBoolean(nullptr));
        ASSERT_FALSE(ObjectValidator::IsBoolean(""));
        ASSERT_FALSE(ObjectValidator::IsBoolean(" "));
        ASSERT_FALSE(ObjectValidator::IsBoolean("asd"));
        ASSERT_FALSE(ObjectValidator::IsBoolean(std::string("asd")));
        ASSERT_FALSE(ObjectValidator::IsBoolean(0));
        ASSERT_FALSE(ObjectValidator::IsBoolean(1));
        ASSERT_FALSE(ObjectValidator::IsBoolean(12345));
        ASSERT_FALSE(ObjectValidator::IsBoolean(0.25f));
        bool undefined;
        ASSERT_TRUE(ObjectValidator::IsBoolean(undefined));
        ASSERT_FALSE(ObjectValidator::IsBoolean("true"));
    }

    TEST(ObjectValidatorTest, IsInteger) {
        ASSERT_FALSE(ObjectValidator::IsInteger(true));
        ASSERT_FALSE(ObjectValidator::IsInteger(33.14));
        ASSERT_FALSE(ObjectValidator::IsInteger(33.14f));
        ASSERT_FALSE(ObjectValidator::IsInteger("33.14"));
        ASSERT_FALSE(ObjectValidator::IsInteger(string()));
        ASSERT_FALSE(ObjectValidator::IsInteger(""));
        ASSERT_FALSE(ObjectValidator::IsInteger(nullptr));
        ASSERT_FALSE(ObjectValidator::IsInteger(std::string("asd")));
        int undefined;
        ASSERT_TRUE(ObjectValidator::IsInteger(undefined));
        int validConversionInt = Com::Wui::Framework::XCppCommons::Primitives::String::ToInteger("111");
        ASSERT_TRUE(ObjectValidator::IsInteger(validConversionInt));
    }

    TEST(ObjectValidatorTest, IsDouble) {
        ASSERT_FALSE(ObjectValidator::IsDouble(true));
        ASSERT_TRUE(ObjectValidator::IsDouble(33.14));
        ASSERT_FALSE(ObjectValidator::IsDouble(33));
        ASSERT_FALSE(ObjectValidator::IsDouble(33.14f));
        ASSERT_FALSE(ObjectValidator::IsDouble("33.14"));
        ASSERT_FALSE(ObjectValidator::IsDouble(string()));
        ASSERT_FALSE(ObjectValidator::IsDouble(""));
        ASSERT_FALSE(ObjectValidator::IsDouble(nullptr));
        ASSERT_FALSE(ObjectValidator::IsDouble(std::string("asd")));
        double undefined;
        ASSERT_TRUE(ObjectValidator::IsDouble(undefined));
        double validConversion = Com::Wui::Framework::XCppCommons::Primitives::String::ToDouble("111.1");
        ASSERT_TRUE(ObjectValidator::IsDouble(validConversion));
        double conversion = Com::Wui::Framework::XCppCommons::Primitives::String::ToDouble("1");
        ASSERT_TRUE(ObjectValidator::IsDouble(conversion));
        conversion = Com::Wui::Framework::XCppCommons::Primitives::String::ToDouble("1.0");
        ASSERT_TRUE(ObjectValidator::IsDouble(conversion));
    }

    TEST(ObjectValidatorTest, IsString) {
        ASSERT_FALSE(ObjectValidator::IsString(true));
        ASSERT_FALSE(ObjectValidator::IsString(33.14));
        ASSERT_FALSE(ObjectValidator::IsString(33.14f));
        ASSERT_TRUE(ObjectValidator::IsString("33.14"));
        ASSERT_TRUE(ObjectValidator::IsString(string()));
        ASSERT_TRUE(ObjectValidator::IsString(""));
        ASSERT_FALSE(ObjectValidator::IsString(nullptr));
        ASSERT_TRUE(ObjectValidator::IsString(std::string("asd")));
        double undefined;
        ASSERT_FALSE(ObjectValidator::IsString(undefined));
    }

    TEST(ObjectValidatorTest, IsDigit) {
        ASSERT_TRUE(ObjectValidator::IsDigit(123));
        ASSERT_TRUE(ObjectValidator::IsDigit(12.34));
        ASSERT_TRUE(ObjectValidator::IsDigit("123"));
        ASSERT_TRUE(ObjectValidator::IsDigit(string("123")));
        ASSERT_TRUE(ObjectValidator::IsDigit("12.34"));
        ASSERT_FALSE(ObjectValidator::IsDigit(""));
        ASSERT_FALSE(ObjectValidator::IsDigit(string("")));
        ASSERT_FALSE(ObjectValidator::IsDigit(" "));
        ASSERT_FALSE(ObjectValidator::IsDigit(nullptr));
        ASSERT_FALSE(ObjectValidator::IsDigit("asd"));
        int undefined;
        ASSERT_TRUE(ObjectValidator::IsDigit(undefined));
        ASSERT_FALSE(ObjectValidator::IsDigit("+1 sec"));
    }

    TEST(ObjectValidatorTest, IsHexadecimal) {
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("FFFFFF"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("E963A535"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("0xFFFFFF"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("0XFFFfFF"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("#FFFfFF"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("0XFF"));
        ASSERT_FALSE(ObjectValidator::IsHexadecimal("0X"));
        ASSERT_FALSE(ObjectValidator::IsHexadecimal("#"));
        ASSERT_FALSE(ObjectValidator::IsHexadecimal("#t"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("#11"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("0x00"));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("#00"));
        ASSERT_FALSE(ObjectValidator::IsHexadecimal(""));
        ASSERT_TRUE(ObjectValidator::IsHexadecimal("0"));
    }
}  // namespace Com::Wui::Framework::XCppCommons::Utils
