/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Events {
    TEST(IntervalTimerTest, Periodic) {
        boost::asio::io_service ioService;

        boost::posix_time::ptime start, stop;
        int cnt = 0;
        IntervalTimer intervalTimer(boost::posix_time::seconds(1), [&]() {
            if (++cnt >= 3) {
                intervalTimer.Stop();
            }
        }, &ioService);

        start = boost::posix_time::microsec_clock::local_time();
        intervalTimer.Start();
        ioService.run();
        stop = boost::posix_time::microsec_clock::local_time();

        int diff = (stop - start).seconds();
        ASSERT_EQ(3, diff);
    }
}  // namespace Com::Wui::Framework::XCppCommons::Events
