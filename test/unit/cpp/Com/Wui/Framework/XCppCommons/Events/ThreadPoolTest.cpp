/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Events {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    class TestClass {
     public:
        void testThread1(const Args::EventArgs &$args) {
            std::cout << "run testThread1" << std::endl;
        }
    };

    class CustomEventArgs : public Args::EventArgs {
     public:
        string getTestString() {
            return "customEventArgs";
        }
    };

    TEST(ThreadPoolTest, AddThread) {
        ThreadPool tp;

        tp.AddThread("testThread1", std::bind(&TestClass::testThread1, TestClass(), std::placeholders::_1));

        tp.AddThread("testThread2", [](const Args::EventArgs &args) {
            std::cout << "run testThread2" << std::endl;
        });

        EXPECT_EQ(true, true);
    }

    TEST(ThreadPoolTest, RemoveThread) {
        ThreadPool tp;
        tp.AddThread("testThread1", [&tp](...) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
            while (true) {
                try {
                    boost::this_thread::sleep_for(boost::chrono::milliseconds(50));
                }
                catch (boost::thread_interrupted const &) {
                    break;
                }
            }
#pragma clang diagnostic pop
        });

        tp.AddThread("testThread2", [&tp](...) {
            boost::this_thread::sleep_for(boost::chrono::milliseconds(200));
            tp.RemoveThread("testThread1");
        });


        tp.Execute(true);
        ASSERT_EQ(true, true);
    }

    TEST(ThreadPoolTest, IsRunning) {
        boost::barrier barrier(2);
        ThreadPool tp;
        ASSERT_EQ(false, tp.IsRunning("testThread1"));

        tp.AddThread("testThread1", [&tp, &barrier](...) {
            ASSERT_EQ(true, tp.IsRunning("testThread1"));
            barrier.wait();
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-noreturn"
            while (true) {
                boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
            }
#pragma clang diagnostic pop
        });

        tp.AddThread("testThread2", [&tp, &barrier](const Args::EventArgs &args) {
            barrier.wait();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(10));
            ASSERT_EQ(true, tp.IsRunning("testThread1"));
            tp.RemoveThread("testThread1");
            ASSERT_EQ(false, tp.IsRunning("testThread1"));
        });

        tp.Execute(true);
        ASSERT_EQ(false, tp.IsRunning("testThread1"));
    }

    TEST(ThreadPoolTest, setThreadArgs) {
        CustomEventArgs testArgs;
        ThreadPool tp;

        tp.AddThread("testThread1", [&tp](const Args::EventArgs &args) {
            ASSERT_STREQ("customEventArgs",
                         const_cast<CustomEventArgs &>(
                                 static_cast<const CustomEventArgs &>(args)).getTestString().c_str());
        });

        tp.setThreadArgs("testThread1", testArgs);

        tp.Execute(true);
    }

    TEST(ThreadPoolTest, Execute) {
        boost::barrier barrier(2);
        boost::chrono::high_resolution_clock::time_point start = boost::chrono::high_resolution_clock::now();
        ThreadPool tp;
        std::vector<int> threadOut;
        boost::mutex lock;

        tp.AddThread("testThread1", [&threadOut, &barrier, &start, &lock](const Args::EventArgs &args) {
            barrier.wait();
            for (int i = 0; i < 20; i++) {
                lock.lock();
                threadOut.push_back(0);
                lock.unlock();
                boost::this_thread::sleep_for(boost::chrono::milliseconds(3));
            }
        });

        tp.AddThread("testThread2", [&threadOut, &barrier, &start, &lock](const Args::EventArgs &args) {
            barrier.wait();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(2));
            for (int i = 0; i < 20; i++) {
                lock.lock();
                threadOut.push_back(1);
                lock.unlock();
                boost::this_thread::sleep_for(boost::chrono::milliseconds(4));
            }
        });

        tp.Execute();

        ASSERT_LT(0, static_cast<int>(threadOut.size()));
        bool isIn = false;
        for (unsigned int i = 0; i < threadOut.size() - 2; i++) {
            if (((threadOut[i] != 0) && (threadOut[i + 1] == 0) && (threadOut[i + 2] != 0)) ||
                ((threadOut[i] == 0) && (threadOut[i + 1] != 0) && (threadOut[i + 2] == 0))) {
                isIn = true;
                break;
            }
        }

        ASSERT_EQ(true, isIn);
    }
}  // namespace Com::Wui::Framework::XCppCommons::Events
