/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Events::Args {

    class DummyOwner {
    };

    TEST(EventArgsTest, getOwner) {
        DummyOwner dummyOwner;
        EventArgs eventArgs;
        EventArgs eventArgs1(&dummyOwner);
        EventArgs eventArgs2(eventArgs1);

        EXPECT_EQ(&eventArgs, eventArgs.getOwner());
        EXPECT_EQ(&dummyOwner, eventArgs1.getOwner());
        EXPECT_EQ(&dummyOwner, eventArgs2.getOwner());
    }
}  // namespace Com::Wui::Framework::XCppCommons::Events::Args
