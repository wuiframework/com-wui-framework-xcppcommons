/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    TEST(ProcessOptionsTest, Construction) {
        ProcessOptions processOptions("cwd", {{"key1", "value1"},
                                              {"key2", "value2"}}, true);
        ASSERT_STREQ("cwd", processOptions.getCwd().c_str());
        ASSERT_STREQ("value1", processOptions.getEnv().at("key1").c_str());
        ASSERT_STREQ("value2", processOptions.getEnv().at("key2").c_str());
        ASSERT_TRUE(processOptions.isShell());

        processOptions = ProcessOptions({{"cwd",   "testCwd"},
                                         {"env",   {
                                                           {"key1", "tvalue1"},
                                                           {"key2", "tvalue2"}
                                                   }},
                                         {"shell", true}});
        ASSERT_STREQ("testCwd", processOptions.getCwd().c_str());
        ASSERT_STREQ("tvalue1", processOptions.getEnv().at("key1").c_str());
        ASSERT_STREQ("tvalue2", processOptions.getEnv().at("key2").c_str());
        ASSERT_TRUE(processOptions.isShell());
    }

    TEST(ProcessOptionsTest, Construction_CopyMove) {
        ProcessOptions po1("cwd1", {{"key1", "value1"}}, true);

        auto checkValues = [&](ProcessOptions const &$opt) {
            ASSERT_STREQ("cwd1", $opt.getCwd().c_str());
            ASSERT_STREQ("value1", $opt.getEnv().at("key1").c_str());
            ASSERT_TRUE($opt.isShell());
        };

        ProcessOptions po2(po1);  // copy ctor
        checkValues(po2);
        ASSERT_FALSE(&po1.getEnv().at("key1") == &po2.getEnv().at("key1"));

        intptr_t mapItemPtr = reinterpret_cast<intptr_t>(&po2.getEnv().at("key1"));  // store before move, will be released
        ProcessOptions po3 = std::move(po2);  // move ctor
        checkValues(po3);
        intptr_t p3 = reinterpret_cast<intptr_t>(&po3.getEnv().at("key1"));
        ASSERT_EQ(mapItemPtr, p3);

        ProcessOptions po4;
        po4 = po3;  // copy operator
        checkValues(po4);

        ProcessOptions po5;
        po5 = std::move(po3);  // move operator
        checkValues(po5);
    }

    TEST(ProcessOptionsTest, Equality) {
        ProcessOptions first("cwd1", {{"key1", "value1"},
                                      {"key2", "value2"}}, true);
        ProcessOptions second(first);
        ASSERT_EQ(first, second);
        auto list = second.getEnv();
        list["key2"] = "newValue";
        second.setEnv(list);
        ASSERT_NE(first, second);
    }

    TEST(ProcessOptionsTest, ToString) {
        ProcessOptions processOptions("cwdVal", {{"key1", "value1"},
                                                 {"key2", "value2"}}, true);
        string origin = json({{"cwd",     "cwdVal"},
                              {"env",     {{"key1", "value1"}, {"key2", "value2"}}},
                              {"shell",   true},
                              {"verbose", true}}).dump();

        ASSERT_STREQ(origin.c_str(), processOptions.ToString().c_str());
        std::ostringstream oss;
        oss << processOptions;
        ASSERT_STREQ(origin.c_str(), oss.str().c_str());
    }
}
