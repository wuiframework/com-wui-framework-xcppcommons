/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::IO {
    TEST(PathTest, Quote) {
        ASSERT_STREQ("c:/dir/some", Path::Quote("c:/dir/some").c_str());
        ASSERT_STREQ("\"c:/dir/some\"", Path::Quote("\"c:/dir/some\"").c_str());
        ASSERT_STREQ("\"c:/dir/space here/some\"", Path::Quote("c:/dir/space here/some").c_str());
        ASSERT_STREQ("\"c:/dir/space here/some\"", Path::Quote("\"c:/dir/space here/some\"").c_str());
    }
}
