/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::Net {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;

    class UrlAdv : public Url {
     public:
        UrlAdv() {
            Url();
        }

        UrlAdv(const string &$protocol, const string &$auth, const string &$hostName, int $port,
               const string &$pathName, const string &$query, const string &$hash) {
            this->setProtocol($protocol);
            this->setAuth($auth);
            this->setHostname($hostName);
            this->setPort($port);
            this->setPathname($pathName);
            this->setQuery($query);
            this->setHash($hash);
        }
    };

    TEST(UrlTest, FromString) {
        Url url("http://user:pass@example.com:8000/foo/bar?baz=quux#frag");
        ASSERT_STREQ("http://user:pass@example.com:8000/foo/bar?baz=quux#frag", url.getHref().c_str());
        ASSERT_STREQ("http:", url.getProtocol().c_str());
        ASSERT_STREQ("example.com:8000", url.getHost().c_str());
        ASSERT_STREQ("user:pass", url.getAuth().c_str());
        ASSERT_EQ(8000, url.getPort());
        ASSERT_STREQ("example.com", url.getHostname().c_str());
        ASSERT_STREQ("#frag", url.getHash().c_str());
        ASSERT_STREQ("?baz=quux", url.getSearch().c_str());
        ASSERT_STREQ("baz=quux", url.getQuery().c_str());
        ASSERT_STREQ("/foo/bar", url.getPathname().c_str());
        ASSERT_STREQ("/foo/bar?baz=quux", url.getPath().c_str());
    }

    TEST(UrlTest, Parse_long) {
        Url url("https://github-cloud.s3.amazonaws.com/releases/23216272/89aa6018-b7c7-11e6-8a11-"
                        "43b6187be2e0.exe?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAIS"
                        "TNZFOVBIJMK3TQ%2F20170202%2Fus-east-1%2Fs3%2Faws4_request&X-Amz-Date=2017"
                        "0202T122634Z&X-Amz-Expires=300&X-Amz-Signature=afdd4b1b36deef553ecbcbb173"
                        "aae79f99762fd9ce85e4884db23980f58154c7&X-Amz-SignedHeaders=host&actor_id="
                        "0&response-content-disposition=attachment%3B%20filename%3DGit-2.11.0-64-b"
                        "it.exe&response-content-type=application%2Foctet-stream");

        ASSERT_STREQ("https://github-cloud.s3.amazonaws.com/releases/23216272/89aa6018-b7c7-11e6-8a11-43b6187be2e0.exe"
                             "?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAISTNZFOVBIJMK3TQ%2F20170202%2Fus-"
                             "east-1%2Fs3%2Faws4_request&X-Amz-Date=20170202T122634Z&X-Amz-Expires=300&X-Amz-Signature"
                             "=afdd4b1b36deef553ecbcbb173aae79f99762fd9ce85e4884db23980f58154c7&X-Amz-SignedHeaders=ho"
                             "st&actor_id=0&response-content-disposition=attachment%3B%20filename%3DGit-2.11.0-64-bit."
                             "exe&response-content-type=application%2Foctet-stream",
                     url.getHref().c_str());
        ASSERT_STREQ("https:", url.getProtocol().c_str());
        ASSERT_STREQ("github-cloud.s3.amazonaws.com", url.getHost().c_str());
        ASSERT_STREQ("", url.getAuth().c_str());
        ASSERT_EQ(0, url.getPort());
        ASSERT_STREQ("github-cloud.s3.amazonaws.com", url.getHostname().c_str());
        ASSERT_STREQ("", url.getHash().c_str());
        ASSERT_STREQ("?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAISTNZFOVBIJMK3TQ%2F20170202%2Fus-"
                             "east-1%2Fs3%2Faws4_request&X-Amz-Date=20170202T122634Z&X-Amz-Expires=300&X-Amz-Signature"
                             "=afdd4b1b36deef553ecbcbb173aae79f99762fd9ce85e4884db23980f58154c7&X-Amz-SignedHeaders=ho"
                             "st&actor_id=0&response-content-disposition=attachment%3B%20filename%3DGit-2.11.0-64-bit."
                             "exe&response-content-type=application%2Foctet-stream",
                     url.getSearch().c_str());
        ASSERT_STREQ("X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAISTNZFOVBIJMK3TQ%2F20170202%2Fus-"
                             "east-1%2Fs3%2Faws4_request&X-Amz-Date=20170202T122634Z&X-Amz-Expires=300&X-Amz-Signature"
                             "=afdd4b1b36deef553ecbcbb173aae79f99762fd9ce85e4884db23980f58154c7&X-Amz-SignedHeaders=ho"
                             "st&actor_id=0&response-content-disposition=attachment%3B%20filename%3DGit-2.11.0-64-bit."
                             "exe&response-content-type=application%2Foctet-stream",
                     url.getQuery().c_str());
        ASSERT_STREQ("/releases/23216272/89aa6018-b7c7-11e6-8a11-43b6187be2e0.exe",
                     url.getPathname().c_str());
        ASSERT_STREQ("/releases/23216272/89aa6018-b7c7-11e6-8a11-43b6187be2e0.exe"
                             "?X-Amz-Algorithm=AWS4-HMAC-SHA256&X-Amz-Credential=AKIAISTNZFOVBIJMK3TQ%2F20170202%2Fus-"
                             "east-1%2Fs3%2Faws4_request&X-Amz-Date=20170202T122634Z&X-Amz-Expires=300&X-Amz-Signature"
                             "=afdd4b1b36deef553ecbcbb173aae79f99762fd9ce85e4884db23980f58154c7&X-Amz-SignedHeaders=ho"
                             "st&actor_id=0&response-content-disposition=attachment%3B%20filename%3DGit-2.11.0-64-bit."
                             "exe&response-content-type=application%2Foctet-stream",
                     url.getPath().c_str());
    }

    TEST(UrlTest, ToString) {
        ASSERT_STREQ("http://user:pass@test.url:222/path/sub/last?a=b&c=d#yoops",
                     UrlAdv("http:", "user:pass", "test.url", 222, "/path/sub/last", "a=b&c=d", "#yoops").ToString().c_str());
        ASSERT_STREQ("https://ex.net/path?x=y&z=w#crop=true&size=false",
                     UrlAdv("https:", "", "ex.net", 0, "/path", "x=y&z=w", "#crop=true&size=false").ToString().c_str());
        ASSERT_STREQ("", UrlAdv().ToString().c_str());
    }

    TEST(UrlTest, EncodeComponent) {
        ASSERT_STREQ("%3B%2C%2F%3F%3A%40%26%3D%2B%24", Url::EncodeComponent(";,/?:@&=+$").c_str());
        ASSERT_STREQ("-_.!~*'()", Url::EncodeComponent("-_.!~*'()").c_str());
        ASSERT_STREQ("%23", Url::EncodeComponent("#").c_str());
        ASSERT_STREQ("ABC%20abc%20123", Url::EncodeComponent("ABC abc 123").c_str());

        ASSERT_STREQ("http%3A%2F%2Flocalhost%2F%20sn%23ow%3F.html%C5%BE", Url::EncodeComponent("http://localhost/ sn#ow?.htmlž").c_str());
        ASSERT_STREQ("ht%5Ctp%3A%2F%2Flocal%20385!%40%23%24%25%5E%26*()host%2F%20sn%23ow%3F.html%C5%BE",
                     Url::EncodeComponent("ht\\tp://local 385!@#$%^&*()host/ sn#ow?.htmlž").c_str());
        ASSERT_STREQ("http%3A%2F%2Floca%5B%3B.%2F'%5D%5C%7D%7B%22%7C%22%3A%3F%3Elh%2F*%2B%60~ost%2F%20sn%23ow%3F.html%C5%BE",
                     Url::EncodeComponent("http://loca[;./']\\}{\"|\":?>lh/*+`~ost/ sn#ow?.htmlž").c_str());
    }

    TEST(UrlTest, DecodeComponent) {
        ASSERT_STREQ(";,/?:@&=+$", Url::DecodeComponent("%3B%2C%2F%3F%3A%40%26%3D%2B%24").c_str());
        ASSERT_STREQ("-_.!~*'()", Url::DecodeComponent("-_.!~*'()").c_str());
        ASSERT_STREQ("#", Url::DecodeComponent("%23").c_str());
        ASSERT_STREQ("ABC abc 123", Url::DecodeComponent("ABC%20abc%20123").c_str());

        ASSERT_STREQ("http://localhost/ sn#ow?.htmlž", Url::DecodeComponent("http%3A%2F%2Flocalhost%2F%20sn%23ow%3F.html%C5%BE").c_str());
        ASSERT_STREQ("ht\\tp://local 385!@#$%^&*()host/ sn#ow?.htmlž",
                     Url::DecodeComponent(
                             "ht%5Ctp%3A%2F%2Flocal%20385%21%40%23%24%25%5E%26%2A%28%29host%2F%20sn%23ow%3F.html%C5%BE").c_str());
        ASSERT_STREQ("http://loca[;./']\\}{\"|\":?>lh/*+`~ost/ sn#ow?.htmlž", Url::DecodeComponent(
                "http%3A%2F%2Floca%5B%3B.%2F%27%5D%5C%7D%7B%22%7C%22%3A%3F%3Elh%2F%2A%2B%60~ost%2F%20sn%23ow%3F.html%C5%BE").c_str());
    }

    TEST(UrlTest, Encode) {
        ASSERT_STREQ(";,/?:@&=+$", Url::Encode(";,/?:@&=+$").c_str());
        ASSERT_STREQ("-_.!~*'()", Url::Encode("-_.!~*'()").c_str());
        ASSERT_STREQ("#", Url::Encode("#").c_str());
        ASSERT_STREQ("ABC%20abc%20123", Url::Encode("ABC abc 123").c_str());

        ASSERT_STREQ("http://localhost/foo/bar.html?fizz=buzz#readme",
                     Url::Encode("http://localhost/foo/bar.html?fizz=buzz#readme").c_str());

        ASSERT_STREQ("http://[::1]:8080/foo/bar",
                     Url::Encode("http://[::1]:8080/foo/bar").c_str());

        ASSERT_STREQ("http://localhost/%0A%0D%0C%20snow.html",
                     Url::Encode("http://localhost/\n\r\f snow.html").c_str());

        ASSERT_STREQ("http://localhost/%20snow%F0.html",
                     Url::Encode("http://localhost/%20snow%F0.html").c_str());

        ASSERT_STREQ("http://www.google.com/a%20file%20with%20spaces.html",
                     Url::Encode("http://www.google.com/a file with spaces.html").c_str());
    }

    TEST(UrlTest, Decode) {
        ASSERT_STREQ("http://localhost/foo/bar.html?fizz=buzz#readme",
                     Url::Decode("http://localhost/foo/bar.html?fizz=buzz#readme").c_str());

        ASSERT_STREQ("http://[::1]:8080/foo/bar",
                     Url::Decode("http://[::1]:8080/foo/bar").c_str());

        ASSERT_STREQ("http://localhost/\n\r\f snow.html",
                     Url::Decode("http://localhost/%0A%0D%0C%20snow.html").c_str());

        ASSERT_STREQ("http://localhost/ snow\xF0.html",
                     Url::Decode("http://localhost/%20snow%F0.html").c_str());

        ASSERT_STREQ("http://www.google.com/a file with spaces.html",
                     Url::Decode("http://www.google.com/a%20file%20with%20spaces.html").c_str());
    }
}
