/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "TaskManagerTest.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::System::Process::TaskManager;

    TEST_F(TaskManagerTest, Find_Existing) {
        this->createDetachedProcess([this](auto $pid) -> bool {
            const auto processes = TaskManager::Find(this->executableName);

            // cannot use ASSERT_* in here, because lambda returns bool
            if (!processes.empty()) {
                EXPECT_STREQ(processes.back().getPath().c_str(), this->executablePath.c_str());
                EXPECT_EQ(processes.back().getPid(), $pid);
            } else {
                ADD_FAILURE() << "Found 0 processes called " << this->executableName;
            }

            return true;
        });
    }

    TEST_F(TaskManagerTest, Find_Non_Existent) {
        ASSERT_TRUE(TaskManager::Find("foo-boo-foo-boo").empty());
    }

    TEST_F(TaskManagerTest, Terminate) {
        this->createDetachedProcess([](auto $pid) -> bool {
           const auto terminated = TaskManager::Terminate($pid);

            EXPECT_TRUE(terminated);

            return !terminated;
        });
    }
}
