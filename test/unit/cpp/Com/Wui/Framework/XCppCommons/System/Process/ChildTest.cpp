/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef WIN_PLATFORM

#include <sys/types.h>
#include <sys/stat.h>

#endif

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;
    using Com::Wui::Framework::XCppCommons::Primitives::String;

    class ChildTest : public testing::Test {
     protected:
        void SetUp() override {
#if defined(WIN_PLATFORM)
            string testName = "UnitTestRunner.exe";
            this->testResourceDir += "/Win";
            this->lineBreak = "\r\n";
#elif defined(MAC_PLATFORM)
            string testName = "UnitTestRunner";
            this->testResourceDir += "/Mac";
            this->lineBreak = "\n";
#else
            string testName = "UnitTestRunner";
            this->testResourceDir += "/Linux";
            this->lineBreak = "\n";
#endif
            this->baseDir = boost::filesystem::canonical(testName).parent_path().make_preferred().string();
            this->baseDir = boost::filesystem::path(this->baseDir + this->testResourceDir).normalize().string();
        }

     public:
        string baseDir = "";
        string testResourceDir = "../../test/resource/data/Com/Wui/Framework/XCppCommons/System/Process";
        string lineBreak;
    };

    constexpr static const char *getPingArgumentForTimeout() {
#if defined(WIN_PLATFORM) || defined(LINUX_PLATFORM)
        return "-w";
#elif MAC_PLATFORM
        return "-t";
#else
#error "getPingArgumentForTimeout is not implemented on this platform"
#endif
    }

    TEST_F(ChildTest, Execute_cmd) {
#ifdef WIN_PLATFORM
        string cmd = "cmd /c dir";
#else
        string cmd = "bash -lic \"ls\"";
#endif
        string cwd = "../../";

        string out;
        int exitCode = -1;
        Child::Execute(cmd, ExecuteOptions(cwd), [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            exitCode = $exitCode;
        })->WaitForExit();
        ASSERT_EQ(0, exitCode) << "Execution of command failed.";
        boost::regex ex(".*package\\.conf\\.json.*");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any)) << "Working directory not match.";
    }

    TEST_F(ChildTest, DISABLED_Execute_wui) {
        string cmd = "\"wui\" --version";

        string out;
        int exitCode = -1;
        Child::Execute(cmd, ExecuteOptions(), [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            exitCode = $exitCode;
        });
        ASSERT_EQ(0, exitCode) << "Execution of command failed.";

        boost::regex ex("WUI Builder v\\d\\.\\d\\.\\d.*");
        ASSERT_TRUE(boost::regex_match(out, ex)) << out << " - not match WUI version regex: " << ex << std::endl;

        out = "";
        cmd = "wui --path";
        Child::Execute(cmd, ExecuteOptions(), [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            exitCode = $exitCode;
        })->WaitForExit();
        ASSERT_EQ(0, exitCode) << "Execution of command failed.";

        boost::algorithm::trim(out);
        ASSERT_TRUE(boost::filesystem::exists(out)) << out << " - WUI path is not correct." << std::endl;
    }

    TEST_F(ChildTest, Execute_ping) {
        string out;
        int exitCode = -1;

#ifdef WIN_PLATFORM
        string cmd = "ping 127.0.0.1";
#else
        string cmd = "ping -c4 127.0.0.1";
#endif

        Child::Execute(cmd, ExecuteOptions(), [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            exitCode = $exitCode;
        })->WaitForExit();

        ASSERT_EQ(0, exitCode) << "Execution of command failed.";

        boost::regex ex(".*P* \\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}.*");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any))
                                    << "Ping output is not valid, spawn failed" << std::endl << out << std::endl
                                    << "not match regex: " << ex.str() << std::endl;
    }

    TEST_F(ChildTest, Execute_testCaseA) {
#ifdef WIN_PLATFORM
        string cmd = "testCaseA.cmd";
#else
        string cmd = "./testCaseA.sh";

        struct stat stat1;
        if (stat((testResourceDir + "/" + cmd).c_str(), &stat1) == 0) {
            chmod((testResourceDir + "/" + cmd).c_str(), stat1.st_mode | S_IEXEC);
        }
#endif
        string cwd = testResourceDir;
        string out, err;
        int exitCode = -1;

        Child::Execute(cmd, ExecuteOptions(cwd), [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            err = $stdErr;
            exitCode = $exitCode;
        })->WaitForExit();
        ASSERT_EQ(0, exitCode) << "Execution of command failed." << std::endl << out << std::endl << err;
    }

    TEST_F(ChildTest, Execute_stdiotest) {
#ifdef WIN_PLATFORM
        string cmd = "\"stdio test.exe\" --stdout=5 --stdout-prefix=stdout_prefixed_ --stderr=4 --stderr-prefix=stderr_prefixed_ "
                "--delay=1000 --exit=111";
#else
        string cmd = "'./stdio test' --stdout=5 --stdout-prefix=stdout_prefixed_ --stderr=4 --stderr-prefix=stderr_prefixed_ "
                "--delay=1000 --exit=111";

        struct stat stat1;
        stat((testResourceDir + "/stdio test").c_str(), &stat1);
        chmod((testResourceDir + "/stdio test").c_str(), stat1.st_mode | S_IEXEC);
#endif
        ExecuteOptions executeOptions(testResourceDir);
        string out, err;
        int exitCode = -1;
        bool callbackCalled = false;
        Child::Execute(cmd, executeOptions, [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            err = $stdErr;
            exitCode = $exitCode;
            callbackCalled = true;
        })->WaitForExit();
        ASSERT_TRUE(callbackCalled);
        ASSERT_EQ(111, exitCode);

        string testOutStr = "stdout_prefixed_0<?>stdout_prefixed_1<?>stdout_prefixed_2<?>stdout_prefixed_3<?>stdout_prefixed_4<?>";
        string testErrStr = "stderr_prefixed_0<?>stderr_prefixed_1<?>stderr_prefixed_2<?>stderr_prefixed_3<?>";

        ASSERT_STREQ(String::Replace(testOutStr, "<?>", lineBreak).c_str(), out.c_str());
        ASSERT_STREQ(String::Replace(testErrStr, "<?>", lineBreak).c_str(), err.c_str());

#ifdef WIN_PLATFORM
        SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX | SEM_NOALIGNMENTFAULTEXCEPT);
        string cmd2 = "\"stdio test.exe\" --throw";
#else
        string cmd2 = "'./stdio test' --throw";
#endif
        Child::Execute(cmd2, executeOptions,
                       [&](int $exitCode, const string &$stdOut, const string &$stdErr) {
                           exitCode = $exitCode;
                           err = $stdErr;
                       })->WaitForExit();
#ifdef WIN_PLATFORM
        ASSERT_EQ(3, exitCode);
#else
        ASSERT_EQ(134, exitCode);
#endif
        ASSERT_FALSE(err.empty());
    }

    TEST_F(ChildTest, Execute_multicmd) {
#ifdef WIN_PLATFORM
        string cmd = "\"stdio test.exe\" --stdout=2 --stdout-prefix=prefix --exit 22";
#else
        string cmd = "'./stdio test' --stdout=2 --stdout-prefix=prefix --exit 22";
#endif

        bool callbackCalled = false;
        Child::Execute(cmd, ExecuteOptions(testResourceDir),
                       [&](int $exitCode, const string &$stdOut, const string &$stdErr) {
                           callbackCalled = true;
                           ASSERT_EQ(22, $exitCode);
                           ASSERT_STREQ(String::Replace("prefix0<?>prefix1<?>", "<?>", this->lineBreak).c_str(), $stdOut.c_str());
                       })->WaitForExit();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        cmd = "dir ..\\..\\";
#else
        cmd = "ls -l ../..";
#endif

        callbackCalled = false;
        Child::Execute(cmd, ExecuteOptions("", {}, true),
                       [&](int $exitCode, const string &$stdOut, const string &$stdErr) {
                           callbackCalled = true;
                           ASSERT_EQ(0, $exitCode);
                           boost::regex ex(".*package\\.conf\\.json.*");
                           ASSERT_TRUE(boost::regex_match($stdOut, ex, boost::regex_constants::match_any))
                                                       << "Working directory not match.";
                       })->WaitForExit();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        cmd = "HELP";
#else
        cmd = "make --help";
#endif

        callbackCalled = false;
        Child::Execute(cmd, ExecuteOptions(json({{"shell", true}})),
                       [&](int $exitCode, const string &$stdOut, const string &$stdErr) {
                           callbackCalled = true;
#ifdef  WIN_PLATFORM
                           ASSERT_EQ(1, $exitCode);
                           boost::regex ex(".*ASSOC.*");
#else
                           ASSERT_EQ(0, $exitCode);
                           boost::regex ex(".*Usage.*");
#endif
                           ASSERT_TRUE(boost::regex_match($stdOut, ex, boost::regex_constants::match_any))
                                                       << "Output data do not contains shell help.";
                       })->WaitForExit();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(ChildTest, Execute_echo) {
#ifdef WIN_PLATFORM
        string args = "%PROCESSOR_ARCHITECTURE";
#else
        string args = "`uname -s`-`uname -p`";
#endif
        string out, err;
        int exitCode = -1;
        Child::Execute("echo " + args, ExecuteOptions(), [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            err = $stdErr;
            exitCode = $exitCode;
        })->WaitForExit();
        ASSERT_EQ(0, exitCode);
    }

    TEST_F(ChildTest, Execute_wmic) {
#ifdef  WIN_PLATFORM
        string cmd = "wmic os get osarchitecture";
#else
        string cmd = "uname -m";
#endif
        string out, err;
        int exitCode = -1;
        Child::Execute(cmd, ExecuteOptions(), [&](int $exitCode, const string $stdOut, const string $stdErr) {
            exitCode = $exitCode;
        })->WaitForExit();
        ASSERT_EQ(0, exitCode);
    }

    TEST_F(ChildTest, Execute_longStdData) {
#ifdef WIN_PLATFORM
        string cmd = "\"stdio test.exe\" --stdout=9999 --stdout-prefix=stdout_prefixed_ --exit=111";
#else
        string cmd = "'./stdio test' --stdout=9999 --stdout-prefix=stdout_prefixed_ --exit=111";
#endif
        ExecuteOptions executeOptions(testResourceDir, {}, false, 0, 218870);
        string out, err;
        int exitCode = -1;
        Child::Execute(cmd, executeOptions, [&](int $exitCode, const string $stdOut, const string $stdErr) {
            out = $stdOut;
            err = $stdErr;
            exitCode = $exitCode;
        })->WaitForExit();
        ASSERT_EQ(111, exitCode);
#ifdef WIN_PLATFORM
        ASSERT_EQ(218868, static_cast<int>(out.size()));
#else
        ASSERT_EQ(208869, static_cast<int>(out.size()));
#endif
    }

    TEST_F(ChildTest, Execute_environment) {
#ifdef WIN_PLATFORM
        string cmd = "echo %PATH%";
#else
        string cmd = "echo $PATH";
#endif
        Child::Execute(cmd, ExecuteOptions(), [&](int $exitCode, const string &$stdOut, const string &$stdErr) {
            ASSERT_EQ(0, $exitCode);
            ASSERT_STREQ(getenv("PATH"), boost::trim_copy($stdOut).c_str());
        });
        Child::Execute(cmd, ExecuteOptions(json({{"env", {{"PATH", boost::filesystem::current_path().string()}}}})),
                       [&](int $exitCode, const string &$stdOut, const string &$stdErr) {
                           ASSERT_EQ(0, $exitCode);
                           ASSERT_STREQ(boost::filesystem::current_path().string().c_str(), boost::trim_copy($stdOut).c_str());
                       })->WaitForExit();
    }

    TEST_F(ChildTest, Execute_bufferLimit) {
#ifdef WIN_PLATFORM
        string cmd = "\"stdio test.exe\"";
#else
        string cmd = "'./stdio test'";
#endif
        Child::Execute(cmd + " --stdout=99999 --stdout-prefix=stdout_prefixed_data_string_ --exit=0",
                       ExecuteOptions(json({{"maxBuffer", 9999},
                                            {"cwd",       testResourceDir}})),
                       [](int $exitCode, const string &$stdOut, const string &$stdErr) {
                           ASSERT_EQ(OUT_OF_MEM, $exitCode);
                       })->WaitForExit();
    }

    TEST_F(ChildTest, Execute_symbol_resolve) {
#ifdef WIN_PLATFORM
        std::map<int, std::pair<string, string>> cmdMap = {
                {0, {"echo %TEST_VAR_PATH%", "%TEST_VAR_PATH%"}},
                {1, {"echo %TEST_VAR_PATH%", "TEST_SYMBOL"}},
                {2, {"dir %TEST_VAR_PATH%",  ""}}
        };
#else
        std::map<int, std::pair<string, string>> cmdMap = {
                {0, {"echo $TEST_VAR_PATH", ""}},
                {1, {"echo $TEST_VAR_PATH", "TEST_SYMBOL"}},
                {2, {"ls $TEST_VAR_PATH",  ""}}
        };
#endif
        Child::Execute(cmdMap[0].first, ExecuteOptions(), [&](int $exitCode, const string &$stdOut, const string &) {
            ASSERT_EQ(0, $exitCode);
            ASSERT_STREQ(cmdMap[0].second.c_str(), boost::trim_copy($stdOut).c_str());

            Child::Execute(cmdMap[1].first, ExecuteOptions(json({{"env", {{"TEST_VAR_PATH", "TEST_SYMBOL"}}}})),
                           [&](int $exitCode2, const string &$stdOut2, const string &) {
                               ASSERT_EQ(0, $exitCode2);
                               ASSERT_STREQ(cmdMap[1].second.c_str(), boost::trim_copy($stdOut2).c_str());

                               Child::Execute(cmdMap[2].first,
                                              ExecuteOptions(json({{"env", {{"TEST_VAR_PATH", FileSystem::ResolvePath("../../")}}}})),
                                              [&](int $exitCode3, const string &$stdOut3, const string &$stdErr3) {
                                                  ASSERT_EQ(0, $exitCode3);
                                                  boost::regex ex(".*package\\.conf\\.json.*");
                                                  ASSERT_TRUE(boost::regex_match($stdOut3, ex, boost::regex_constants::match_any))
                                                                              << "Can't resolve previously entered symbol %TEST_VAR_PATH%";
                                              })->WaitForExit();
                           })->WaitForExit();
        })->WaitForExit();
    }

    TEST_F(ChildTest, Spawn) {
#ifdef WIN_PLATFORM
        string cmd = "ping.exe";
        std::vector<string> args = {"127.0.0.1"};
#else
        string cmd = "ping";
        std::vector<string> args = {"127.0.0.1", "-c4"};
#endif
        string stdOut, stdErr;
        Child::Spawn(cmd, args, SpawnOptions(json({{"shell", true}})))
                ->setOnStdOutData([&](const string &data) {
                    stdOut += data;
                })
                ->setOnStdErrData([&](const string &data) {
                    stdErr += data;
                })
                ->setOnError([](auto data) {
                    ASSERT_TRUE(false) << "An error could not be raised for ping.";
                })
                ->setOnClose([&](auto data) {
                    ASSERT_EQ(0, data);
                    boost::regex ex(".*P* \\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}.*");
                    ASSERT_TRUE(boost::regex_match(stdOut, ex, boost::regex_constants::match_any))
                                                << "Ping output is not valid, spawn failed" << std::endl << stdOut << std::endl
                                                << "not match regex: " << ex.str() << std::endl;
                })
                ->WaitForExit();

        stdOut.clear();
        stdErr.clear();

#ifdef WIN_PLATFORM
        args = {"%PATH%"};
#else
        args = {"$PATH"};
#endif
        cmd = "echo";

        Child::Spawn("echo", args, SpawnOptions(json({{"shell", true}})))
                ->setOnStdOutData([&](auto data) {
                    stdOut += data;
                })
                ->setOnStdErrData([&](auto data) {
                    stdErr += data;
                })
                ->setOnError([](auto data) {
                    std::cerr << data;
                })
                ->setOnClose([&](int exitCode) {
                    ASSERT_EQ(0, exitCode);
                    ASSERT_STREQ(getenv("PATH"), boost::trim_copy(stdOut).c_str());
                })
                ->WaitForExit();

        stdOut.clear();
        stdErr.clear();

#ifdef WIN_PLATFORM
        args = {"%PATH%"};
#else
        args = {"$PATH"};
#endif
        Child::Spawn("echo", args, SpawnOptions(json({{"shell", true},
                                                      {"env",   {{"PATH", boost::filesystem::current_path().string()}}}})))
                ->setOnStdOutData([&](auto data) {
                    stdOut += data;
                })
                ->setOnClose([&](int exitCode) {
                    ASSERT_EQ(0, exitCode);
                    stdOut = boost::trim_copy(stdOut).c_str();
                    ASSERT_STREQ(boost::filesystem::current_path().string().c_str(), stdOut.c_str());
                })
                ->WaitForExit();

        stdOut.clear();
        stdErr.clear();

#ifdef WIN_PLATFORM
        args = {"%WUI_TEST_CHILD_ENV%"};
#else
        args = {"$WUI_TEST_CHILD_ENV"};
#endif
        Child::Spawn("echo", args, SpawnOptions(json({{"shell", true},
                                                      {"env",   {{"WUI_TEST_CHILD_ENV", "SOME_DATA"}}}})))
                ->setOnStdOutData([&](auto data) {
                    stdOut += data;
                })
                ->setOnClose([&](int exitCode) {
                    ASSERT_EQ(0, exitCode);
                    stdOut = boost::trim_copy(stdOut).c_str();
                    ASSERT_STREQ("SOME_DATA", stdOut.c_str());
                })
                ->WaitForExit();
    }

    TEST_F(ChildTest, Spawn_git) {
        string cmd = "git";
        const std::vector<string> args{"--version"};
        string out;

        Child::Spawn(cmd, args, SpawnOptions(json({{"shell", true}})))
                ->setOnStdOutData([&](auto data) {
                    out += data;
                })
                ->WaitForExit();
        boost::regex ex("git version \\d{1,3}\\.\\d{1,3}\\.\\d{1,4}.*");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any))
                                    << "git output is not valid version." << std::endl;
    }

    TEST_F(ChildTest, Spawn_parallel) {
#ifdef WIN_PLATFORM
        string cmd = "stdio test.exe";
#else
        string cmd = "stdio test";
#endif
        string outA;
        string outB;

        Com::Wui::Framework::XCppCommons::Events::ThreadPool threadPool;

        boost::barrier barrier(2);
        testing::internal::CaptureStdout();
        threadPool.AddThread("parallel_A", [&](...) {
            barrier.wait();
            Child::Spawn(cmd, {"--stdout=10", "--stdout-prefix=A", "--delay=200"},
                         SpawnOptions(testResourceDir))
                    ->setOnStdOutData([&](auto data) {
                        std::cout << "A";
                    })
                    ->setOnStdErrData([&](auto data) {
                        std::cerr << data << std::endl;
                    })
                    ->setOnClose([&](auto data) {
                        std::cout << "F";
                    })
                    ->WaitForExit();
        });

        threadPool.AddThread("parallel_B", [&](...) {
            barrier.wait();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(250));

            Child::Spawn(cmd, {"--stdout=5", "--stdout-prefix=B", "--delay=500"},
                         SpawnOptions(testResourceDir))
                    ->setOnStdOutData([&](auto data) {
                        std::cout << "B";
                    })
                    ->setOnStdErrData([&](auto data) {
                        std::cerr << data << std::endl;
                    })
                    ->setOnClose([&](auto data) {
                        std::cout << "Q";
                    })
                    ->WaitForExit();
        });

        threadPool.Execute(true);

        using Com::Wui::Framework::XCppCommons::Primitives::String;
        string interleaved = testing::internal::GetCapturedStdout();
        ASSERT_TRUE(String::Contains(interleaved, "AB") && String::Contains(interleaved, "BA")) << "Which is: " << interleaved;
    }

    TEST_F(ChildTest, Spawn_detached_ping) {
        const auto cmd = "ping";
        const auto waitingTime = std::chrono::seconds(4);
        const std::vector<string> args{"localhost", getPingArgumentForTimeout(), std::to_string(waitingTime.count())};

        auto process = Child::Spawn(cmd, args, SpawnOptions(json({
                                                                     {"detached", true},
                                                                     {"shell", true}
                                                                 })));
        ASSERT_NE(process, nullptr);
        const auto pid = process->getPid();
        ASSERT_GT(pid, 0);

        const auto isProcessRunning = [](const int $pid) {
#if WIN_PLATFORM
            const HANDLE process = OpenProcess(SYNCHRONIZE, FALSE, $pid);
            const DWORD returnCode = WaitForSingleObject(process, 0);

            CloseHandle(process);

            return returnCode == WAIT_TIMEOUT;
#elif defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
            return kill($pid, 0) == 0;
#endif
        };

        EXPECT_TRUE(isProcessRunning(pid));

        // wait longer to be "sure"
        std::this_thread::sleep_for(waitingTime * 2);

        // behaviour is such that when original process is still running, the forked process is kept alive when wait() is not called
        process->WaitForExit();

        EXPECT_FALSE(isProcessRunning(pid));
    }

    TEST_F(ChildTest, DISABLED_Spawn_temp) {
        auto temp = [](const function<void(bool $status)> &$callback) {
            using Com::Wui::Framework::XCppCommons::System::ResponseApi::ResponseFactory;
            ResponseFactory::getResponse($callback);
        };

        std::cout << std::endl;
        string outA;
        string outB;

        Com::Wui::Framework::XCppCommons::Events::ThreadPool threadPool;

        boost::barrier barrier(2);
        threadPool.AddThread("parallel_A", [&](...) {
            barrier.wait();
            for (;;) {
                temp(nullptr);
            }
        });

        threadPool.AddThread("parallel_B", [&](...) {
            barrier.wait();
            for (;;) {
                temp(nullptr);
            }
        });

        threadPool.Execute(true);

        ASSERT_EQ(true, true);
    }
}
