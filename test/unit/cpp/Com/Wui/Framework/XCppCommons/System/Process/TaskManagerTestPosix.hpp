/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTESTPOSIX_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTESTPOSIX_HPP_

#if defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::System::Process::TaskManager;

    class TaskManagerTest : public testing::Test {
     protected:
        void createDetachedProcess(function<bool(unsigned int)> $action) {
            const int pid = fork();

            if (this->isChild(pid)) {
                // every check here is completely useless, because this is now running in a separate process that has no direct
                // relationship with the original one, thus we cannot call FAIL() on errors, but even if the following command fails,
                // the process will be marked as <defunct> but will still be running (so we can terminate it in the test)
                execl(this->executablePath.c_str(), this->executableName.c_str(), this->args.c_str(), nullptr);
            } else if (this->isParent(pid)) {
                // ideally, the child process should signal to parent process that it's running, but for simplicity, wait
                std::this_thread::sleep_for(std::chrono::seconds(2));

                const auto shouldTerminate = $action(static_cast<unsigned int>(pid));

                if (shouldTerminate) {
                    if (kill(pid, SIGKILL) == -1) {
                        FAIL() << "Failed to kill the process with PID " << std::to_string(pid)
                               << ", either it's already terminated or you have to kill it manually";
                    }
                }
            } else if (this->isError(pid)) {
                FAIL() << "Failed to spawn a new process, system error code:" << std::to_string(errno);
            }
        }

        const string executablePath = "/bin/sleep";
        const string executableName = "sleep";
        // cannot use infinity, because this would work only on Linux and not OS X
        const string args = std::to_string(std::numeric_limits<int>::max());

     private:
        bool isParent(const int $pid) const {
            return $pid > 0;
        }

        bool isChild(const int $pid) const {
            return $pid == 0;
        }

        bool isError(const int $pid) const {
            return $pid == -1;
        }
    };
}

#endif  // defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTESTPOSIX_HPP_
