/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

#ifdef CreateDirectory
#undef CreateDirectory
#endif

namespace Com::Wui::Framework::XCppCommons::System::IO {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
    using Com::Wui::Framework::XCppCommons::Interfaces::IResponse;
    using Com::Wui::Framework::XCppCommons::System::ResponseApi::ResponseFactory;
    using Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers::BaseResponse;
    using Com::Wui::Framework::XCppCommons::Primitives::String;
    using Com::Wui::Framework::XCppCommons::Primitives::ArrayList;

    namespace fs = boost::filesystem;

    class FileSystemTest
            : public testing::Test {
     protected:
        void SetUp() override {
#ifdef WIN_PLATFORM
            this->baseDir = fs::canonical("UnitTestRunner.exe").parent_path().make_preferred().string();
#else
            this->baseDir = fs::canonical("UnitTestRunner").parent_path().make_preferred().string();
#endif
        }

     public:
        string baseDir = "";

        const std::vector<unsigned char> binData = {0x00, 0x01, 0x02, 0x03, 0x04, 0xb9, 0x78, 0x00,
                                                    0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c,
                                                    0x00, 0x0d, 0x0e, 0x0f, 0x10, 0x00, 0x11, 0x12,
                                                    0x13, 0x01, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19,
                                                    0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21,
                                                    0x22, 0x00, 0x23, 0x24, 0x25, 0x26, 0x27, 0x28,
                                                    0x29, 0x2a, 0x2b, 0x00, 0x2c, 0x00, 0x2d, 0x00};
    };

    TEST_F(FileSystemTest, Exists) {
        bool callbackCalled = false;
#ifdef WIN_PLATFORM
        string unitTestName = "UnitTestRunner.exe";
#else
        string unitTestName = "UnitTestRunner";
#endif
        ASSERT_TRUE(FileSystem::Exists(unitTestName, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Exists("UnknownTestFile.test", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
#ifdef WIN_PLATFORM
        string home = "c:/Windows";
#else
        string home = "/home";
#endif
        ASSERT_TRUE(FileSystem::Exists(home, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
#ifdef WIN_PLATFORM
        string unknwnhome = "c:/UnknownTestFolder";
#else
        string unknwnhome = "/UnknownTestFolder";
#endif
        ASSERT_FALSE(FileSystem::Exists(unknwnhome, [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Exists("", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Exists("`./,;[o]/*-+.[=-012`", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Exists(this->baseDir + "/" + unitTestName, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Exists(this->baseDir + "/log", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Exists(this->baseDir + "/unknown-folder", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Exists(this->baseDir + "/?&#jioo.;,", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, CreateDirectory) {
        ASSERT_FALSE(FileSystem::Exists("testDirectory"));
        bool callbackCalled = false;
        ASSERT_TRUE(FileSystem::CreateDirectory("testDirectory", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        ASSERT_TRUE(FileSystem::Exists("testDirectory"));
        fs::remove(boost::filesystem::canonical("./testDirectory"));
        ASSERT_FALSE(FileSystem::Exists("testDirectory"));

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::CreateDirectory("", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);
#ifdef WIN_PLATFORM
        callbackCalled = false;
        ASSERT_FALSE(FileSystem::CreateDirectory("*-+.[=-012`", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);
#endif
        callbackCalled = false;
        ASSERT_TRUE(FileSystem::CreateDirectory(this->baseDir + "/testDirectory", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);
        ASSERT_TRUE(FileSystem::Exists(this->baseDir + "/testDirectory"));
        fs::remove(this->baseDir + "/testDirectory");

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::CreateDirectory("testDirectory/dir2", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::CreateDirectory("testDirectory/dir2/dir3", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        ASSERT_TRUE(FileSystem::Exists("testDirectory/dir2"));
        ASSERT_TRUE(FileSystem::Exists("testDirectory/dir2/dir3"));
        fs::remove_all(boost::filesystem::canonical("testDirectory"));
        ASSERT_FALSE(FileSystem::Exists("testDirectory"));
        ASSERT_FALSE(FileSystem::Exists("testDirectory/dir2"));
        ASSERT_FALSE(FileSystem::Exists("testDirectory/dir2/dir3"));
    }

    TEST_F(FileSystemTest, getTempPath) {
        string handPath;
        bool callbackCalled = false;
        string path = FileSystem::getTempPath([&](const string &$path) {
            callbackCalled = true;
            handPath = $path;
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_STREQ(path.c_str(), handPath.c_str());
        ASSERT_TRUE(FileSystem::Exists(path));
    }

    TEST_F(FileSystemTest, getAppDataPath) {
        string handPath;
        bool callbackCalled = false;
        string path = FileSystem::getAppDataPath([&](const string &$path) {
            callbackCalled = true;
            handPath = $path;
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_STREQ(path.c_str(), handPath.c_str());
#ifdef LINUX_PLATFORM
        ASSERT_STREQ(path.c_str(), "/var/lib");
#elif MAC_PLATFORM
        ASSERT_STREQ(path.c_str(), "/Library/Application Support");
#endif
        ASSERT_TRUE(FileSystem::Exists(path));
    }

    TEST_F(FileSystemTest, getLocalAppDataPath) {
        string handPath;
        bool callbackCalled = false;
        string path = FileSystem::getLocalAppDataPath([&](const string &$path) {
            callbackCalled = true;
            handPath = $path;
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_STREQ(path.c_str(), handPath.c_str());
#ifdef LINUX_PLATFORM
        ASSERT_STREQ(path.c_str(), getenv("HOME"));
#elif MAC_PLATFORM
        ASSERT_STREQ(path.c_str(), string(string(getenv("HOME")) + "/Library/Application Support").c_str());
#endif
        ASSERT_TRUE(FileSystem::Exists(path));
    }

    TEST_F(FileSystemTest, Rename) {
        bool callbackCalled = false;

        ASSERT_FALSE(FileSystem::Exists("testDirectory"));
        ASSERT_TRUE(FileSystem::CreateDirectory("testDirectory"));

        ASSERT_TRUE(FileSystem::Rename("testDirectory", "renamedDirectory", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        ASSERT_TRUE(FileSystem::Exists("renamedDirectory"));
        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Rename("renamedDirectory", "renamedDirectory", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        fs::remove(boost::filesystem::canonical("./renamedDirectory"));
        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Rename("", "", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Rename("`./,;[o]/*-+.[=-012`", "`./,;[o]/*-+.[=-012`", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        ASSERT_TRUE(FileSystem::Write(this->baseDir + "/testFile.txt", "some data", true));
        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Rename(this->baseDir + "/testFile.txt", this->baseDir + "/ex.txt", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Rename(this->baseDir + "/ex.txt", this->baseDir + "/testDir/ex.txt", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Rename(this->baseDir + "/ex.txt", this->baseDir + "/ex", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Rename(this->baseDir + "/ex", this->baseDir + "/target.txt", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        fs::remove(this->baseDir + "/target.txt");
    }

    TEST_F(FileSystemTest, Write) {
        ASSERT_FALSE(FileSystem::Exists("testFile.log"));

        bool callbackCalled = false;
        ASSERT_TRUE(FileSystem::Write("testFile.log", "test data written to file", false, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        std::ifstream ifstream;
        ifstream.open("testFile.log");
        string data = string((std::istreambuf_iterator<char>(ifstream)), std::istreambuf_iterator<char>());
        ifstream.close();
        ASSERT_STREQ("test data written to file", data.c_str());

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Write("testFile.log", "\nsecond data pack", true, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        ifstream.open("testFile.log");
        data = string((std::istreambuf_iterator<char>(ifstream)), std::istreambuf_iterator<char>());
        ifstream.close();
        ASSERT_STREQ("test data written to file\nsecond data pack", data.c_str());

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Write("testFile.log", "append disabled", false, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        ifstream.open("testFile.log");
        data = string((std::istreambuf_iterator<char>(ifstream)), std::istreambuf_iterator<char>());
        ifstream.close();
        ASSERT_STREQ("append disabled", data.c_str());

        fs::remove("testFile.log");

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Write(this->baseDir + "/testFile.txt", "some data in test file", false, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        ifstream.open(this->baseDir + "/testFile.txt");
        data = string((std::istreambuf_iterator<char>(ifstream)), std::istreambuf_iterator<char>());
        ifstream.close();
        ASSERT_STREQ("some data in test file", data.c_str());
        fs::remove(this->baseDir + "/testFile.txt");

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Write(this->baseDir, "some data", true, [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Write("", "", true, [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        string binDataString(this->binData.begin(), this->binData.end());
        string binFileLoc(this->baseDir + "/testBinFile");

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Write(binFileLoc, binDataString, false, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        std::ifstream inBinFile(binFileLoc, std::ios::binary);
        ASSERT_TRUE(inBinFile.is_open());

        std::vector<unsigned char> inBinFileData;

        {
            inBinFile.unsetf(std::ios::skipws);

            std::streampos fileSize;
            inBinFile.seekg(0, std::ios::end);
            fileSize = inBinFile.tellg();
            inBinFile.seekg(0, std::ios::beg);

            inBinFileData.reserve(static_cast<unsigned int>(fileSize));

            inBinFileData.insert(inBinFileData.begin(),
                                 std::istream_iterator<unsigned char>(inBinFile),
                                 std::istream_iterator<unsigned char>());
        }

        inBinFile.close();

        ASSERT_EQ(this->binData.size(), inBinFileData.size());

        unsigned idx = 0;
        std::for_each(inBinFileData.begin(), inBinFileData.end(), [&](unsigned char $value) {
            ASSERT_TRUE($value == this->binData[idx]);
            idx++;
        });

        fs::remove(binFileLoc);

        callbackCalled = false;
        ASSERT_FALSE(FileSystem::Exists("testDir"));
        ASSERT_TRUE(FileSystem::Write("testDir/testFile.log", "data data", false, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);
        ASSERT_TRUE(FileSystem::Exists("testDir"));
        ASSERT_TRUE(FileSystem::Exists("testDir/testFile.log"));

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Write("testDir/testDir2/testFile2.log", "data data", false, [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);
        ASSERT_TRUE(FileSystem::Exists("testDir/testDir2/testFile2.log"));

        fs::remove_all("testDir");
    }

    TEST_F(FileSystemTest, Read) {
        ASSERT_FALSE(FileSystem::Exists("testReadFile.txt"));
        std::ofstream ofstream;
        ofstream.open("testReadFile.txt", std::ios_base::trunc | std::ios_base::binary);
        ofstream << "test data to be red\nsecond line";
        ofstream.close();

        bool callbackCalled = false;
        string callData;
        string data = FileSystem::Read("testReadFile.txt", [&](const string &$data) {
            callbackCalled = true;
            callData = $data;
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_STREQ(data.c_str(), callData.c_str());
        ASSERT_STREQ("test data to be red\nsecond line", data.c_str());

        callbackCalled = false;
        data = FileSystem::Read(this->baseDir + "/testReadFile.txt", [&](const string &$data) {
            callbackCalled = true;
            callData = $data;
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_STREQ(data.c_str(), callData.c_str());
        ASSERT_STREQ("test data to be red\nsecond line", data.c_str());
        fs::remove("testReadFile.txt");

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Read("", [&](const string &$data) {
            callbackCalled = true;
            ASSERT_TRUE($data.empty());
        }).empty());
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Read(this->baseDir, [&](const string &$data) {
            callbackCalled = true;
            ASSERT_TRUE($data.empty());
        }).empty());
        ASSERT_TRUE(callbackCalled);

        string binFilePath = fs::canonical("../../test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/testBinFile").string();
        string binFileData = FileSystem::Read(binFilePath);
        ASSERT_EQ(this->binData.size(), binFileData.size());

        unsigned idx = 0;
        std::for_each(binFileData.begin(), binFileData.end(), [&](unsigned char $value) {
            ASSERT_EQ($value, this->binData[idx]);
            idx++;
        });

        string zipReadPath = fs::canonical("../../test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest1.zip").string();
        string zipReadData = FileSystem::Read(zipReadPath);

        std::ifstream inZipFile(zipReadPath, std::ios::binary);
        std::vector<unsigned char> inZipFileData;

        {
            ASSERT_TRUE(inZipFile.is_open());

            inZipFile.unsetf(std::ios::skipws);

            std::streampos fileSize;
            inZipFile.seekg(0, std::ios::end);
            fileSize = inZipFile.tellg();
            inZipFile.seekg(0, std::ios::beg);

            inZipFileData.reserve(static_cast<unsigned int>(fileSize));

            inZipFileData.insert(inZipFileData.begin(),
                                 std::istream_iterator<unsigned char>(inZipFile),
                                 std::istream_iterator<unsigned char>());
        }

        inZipFile.close();

        ASSERT_TRUE(inZipFileData.size() == zipReadData.size());
        idx = 0;
        std::for_each(inZipFileData.begin(), inZipFileData.end(), [&](unsigned char $value) {
            ASSERT_TRUE($value == static_cast<unsigned char>(zipReadData[idx]));
            idx++;
        });
    }

    TEST_F(FileSystemTest, Delete) {
        testing::internal::CaptureStdout();
        FileSystem::Write("deletefile.bin", "test copy file", true);
        ASSERT_TRUE(FileSystem::Exists("deletefile.bin"));

        bool callbackCalled = false;
        ASSERT_TRUE(FileSystem::Delete("deletefile.bin", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);
        ASSERT_FALSE(FileSystem::Exists("deletefile.bin"));

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Delete("unknown_file_test_delete.un", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        FileSystem::CreateDirectory("deletedir");
        FileSystem::CreateDirectory("deletedir/subdir");
        FileSystem::CreateDirectory("deletedir/subdir2/subdirA");
        FileSystem::Write("deletedir/testFileA.txt", "testFileA", false);
        FileSystem::Write("deletedir/subdir/testFileB.txb", "testfileB", false);
        FileSystem::Write("deletedir/subdir/testFileC.tcb", "testfileC", false);
        FileSystem::Write("deletedir/subdir2/subdirA/testFileX.tcx", "testFileX", false);
        FileSystem::Write("deletedir/subdir2/subdirA/testFileY.txy", "testFileY", false);

        callbackCalled = false;
        ASSERT_TRUE(FileSystem::Delete("deletedir", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);
        ASSERT_FALSE(FileSystem::Exists("deletedir"));
        testing::internal::GetCapturedStdout();
    }

    TEST_F(FileSystemTest, Copy) {
        testing::internal::CaptureStdout();
        ASSERT_TRUE(FileSystem::Write("copyfile.bin", "test copy file", true));
        ASSERT_TRUE(FileSystem::Exists("copyfile.bin"));

        bool callbackCalled = false;
        FileSystem::Copy("copyfile.bin", "copiedfile.bin2", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_TRUE(FileSystem::Exists("copyfile.bin") && FileSystem::Exists("copiedfile.bin2"));

        fs::remove("copyfile.bin");
        fs::remove("copiedfile.bin2");

        // test copy directory
        ASSERT_TRUE(FileSystem::CreateDirectory("copydir"));
        ASSERT_TRUE(FileSystem::CreateDirectory("copydir/subdir"));
        ASSERT_TRUE(FileSystem::CreateDirectory("copydir/subdir2/subdirA"));
        ASSERT_TRUE(FileSystem::Write("copydir/testFileA.txt", "testFileA", false));
        ASSERT_TRUE(FileSystem::Write("copydir/subdir/testFileB.txb", "testfileB", false));
        ASSERT_TRUE(FileSystem::Write("copydir/subdir/testFileC.tcb", "testfileC", false));
        ASSERT_TRUE(FileSystem::Write("copydir/subdir2/subdirA/testFileX.tcx", "testFileX", false));
        ASSERT_TRUE(FileSystem::Write("copydir/subdir2/subdirA/testFileY.txy", "testFileY", false));

        callbackCalled = false;
        FileSystem::Copy("copydir", "copied dir", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        });
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        FileSystem::Copy("copydir/testFileA.txt", "copied dir/dummy/copiedFile.log", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        });
        ASSERT_TRUE(callbackCalled);

        ASSERT_TRUE(FileSystem::Exists("copied dir"));
        ASSERT_TRUE(FileSystem::Exists("copied dir/testFileA.txt"));
        ASSERT_TRUE(FileSystem::Exists("copied dir/subdir/testFileB.txb"));
        ASSERT_TRUE(FileSystem::Exists("copied dir/subdir/testFileC.tcb"));
        ASSERT_TRUE(FileSystem::Exists("copied dir/subdir2/subdirA/testFileX.tcx"));
        ASSERT_TRUE(FileSystem::Exists("copied dir/subdir2/subdirA/testFileY.txy"));
        ASSERT_TRUE(FileSystem::Exists("copied dir/dummy/copiedFile.log"));

        ASSERT_TRUE(FileSystem::Delete("copydir"));
        ASSERT_TRUE(FileSystem::Delete("copied dir"));
        testing::internal::GetCapturedStdout();
    }

    TEST_F(FileSystemTest, IsEmpty) {
        bool callbackCalled = false;
        ASSERT_FALSE(FileSystem::IsEmpty("emptyDir", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        FileSystem::CreateDirectory("emptyDir");
        callbackCalled = false;
        ASSERT_TRUE(FileSystem::IsEmpty("emptyDir", [&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        FileSystem::CreateDirectory("emptyDir/sub Dir");
        callbackCalled = false;
        ASSERT_FALSE(FileSystem::IsEmpty("emptyDir", [&](bool $status) {
            callbackCalled = true;
            ASSERT_FALSE($status);
        }));
        ASSERT_TRUE(callbackCalled);

        testing::internal::CaptureStdout();
        FileSystem::Delete("emptyDir");
        testing::internal::GetCapturedStdout();
    }

    TEST_F(FileSystemTest, Download_bin_files) {
        string url = "http://localhost:8888/git-for-windows/git/releases/download/v2.11.0.windows.1/Git-2.11.0-64-bit_1.exe";
        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(url, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            ASSERT_TRUE(FileSystem::Delete($filePath));
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

        url = "http://localhost:8888/files/v3.6/cmake-3.6.3-win64-x64.msi";
        callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(url, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            ASSERT_TRUE(FileSystem::Delete($filePath));
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, Download_correctName) {
        string url = "http://localhost:8888/boostorg/release/1.64.0/source/boost_1_64_0.zip";
        bool callbackCalled = false;
        string filePath;
        testing::internal::CaptureStdout();
        FileSystem::Download(url, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            ASSERT_TRUE(String::PatternMatched("*boost_1_64_0*.zip", $filePath)) << $filePath;
            ASSERT_TRUE(FileSystem::Delete($filePath));
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, Download_text_file) {
        string url = "http://localhost:8888/wuiframework/com-wui-framework/raw/c6df5bb0cf1c971b6b6d2d4994443a1e63a962de/README.md_1";

        string data;
        bool callbackCalled = false;
        string filePath;
        testing::internal::CaptureStdout();
        FileSystem::Download(url, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            data = FileSystem::Read($filePath);
            ASSERT_TRUE(FileSystem::Delete($filePath));
            std::cout << data << std::endl;
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

        json headers = {
                {"User-Agent", "Mozilla /5.0 (Compatible MSIE 9.0;Windows NT 6.1;WOW64; Trident/5.0)"}
        };
        json options = {
                {"method",       "GET"},
                {"url",          url},
                {"headers",      headers},
                {"body",         ""},
                {"streamOutput", true}
        };

        string fileStream;
        callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(options, [&](const string $data) {
            callbackCalled = true;
            ASSERT_FALSE($data.empty());
            fileStream = $data;
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

        ASSERT_STREQ(fileStream.c_str(), data.c_str());
    }

    TEST_F(FileSystemTest, Download_icon) {
        string url = "http://localhost:8888/wuiframework/com-wui-framework-connector/raw/"
                     "6860fdef19cb7fe5e3bb0050e4aba4276f78cc73/resource/graphics/icon.ico";

        json options = {
                {"method", "GET"},
                {"url",    url}
        };
        string filePath;
        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(options, [&](const string $filePath) {
            callbackCalled = true;
            filePath = $filePath;
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
        ASSERT_TRUE(boost::filesystem::exists(filePath));
        ASSERT_STREQ(FileSystem::Read(fs::canonical("../../resource/graphics/icon.ico").string()).c_str(),
                     FileSystem::Read(filePath).c_str());

        ASSERT_TRUE(FileSystem::Delete(filePath));
    }

    TEST_F(FileSystemTest, Download_Object) {
        string url = "http://localhost:8888/webapps/download/AutoDL?BundleId=216431";

        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(url, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            ASSERT_TRUE(FileSystem::Delete($filePath));
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, Download_http_html) {
        string url = "http://localhost:8888/";

        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(url, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            ASSERT_TRUE(FileSystem::Delete($filePath));
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, Download_https_html) {
        string url = "http://localhost:8888/webapp/Download?colCode=L4.1.30_MX8DV_BETA_GPU_TOOL";

        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(url, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            ASSERT_TRUE(FileSystem::Delete($filePath));
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, Download_post) {
        json options = {};
        options["url"] = "http://localhost:8888/xorigin/";
        options["method"] = "POST";
        options["headers"] = json({{"Charset",      "UTF-8"},
                                   {"Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"},
                                   {"User-Agent",   "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 "
                                                    "(KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"}});
        options["body"] = "jsonData";

        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Download(options, [&](const string $filePath) {
            callbackCalled = true;
            ASSERT_TRUE(fs::exists($filePath));
            ASSERT_TRUE(FileSystem::Delete($filePath));
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, Download_unknown_url) {
        // mistake in .net instead of .com
        string url = "https://localhost.wuiframework.net/com-wui-framework-rest-commons/DownloadTest/build/target"
                     "/test/resource/data/Com/Wui/Framework/Rest/Commons/RuntimeTests/selfextractor.conf.json";


        json options = {
                {"method",       "GET"},
                {"url",          url},
                {"headers",      {
                                         {"User-Agent", "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 "
                                                        "(KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"}
                                 }
                },
                {"body",         ""},
                {"streamOutput", true}
        };
        string data;
        testing::internal::CaptureStdout();
        ASSERT_NO_THROW(FileSystem::Download(options, [&data](const string &$data) {
            data = $data;
        }));

        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(data.empty()) << "data: " << data;

        // mistake in /UNKNOWN/ instead of /RuntimeTests/
        url = "https://localhost.wuiframework.com/com-wui-framework-rest-commons/DownloadTest/build/target"
              "/test/resource/data/Com/Wui/Framework/Rest/Commons/UNKNOWN/selfextractor.conf.json";
        options["url"] = url;
        options["streamOutput"] = false;
        data = "";
        testing::internal::CaptureStdout();
        ASSERT_NO_THROW(FileSystem::Download(options, [&data](const string &$data) {
            data = $data;
        }));
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(data.empty()) << "data: " << data;
    }

    TEST_F(FileSystemTest, Download_abort) {
        string url = "http://localhost:8888/git-for-windows/git/releases/download/v2.13.1.windows.1/Git-2.13.1-32-bit.exe_1";
        shared_ptr<IResponse> response = ResponseFactory::getResponse(BaseResponse());

        Com::Wui::Framework::XCppCommons::Events::ThreadPool threadPool;
        boost::mutex mtx;
        boost::barrier barrier(2);

        testing::internal::CaptureStdout();
        threadPool.AddThread("parallel_A", [&](...) {
            barrier.wait();
            FileSystem::Download(url, response);
        });

        threadPool.AddThread("parallel_B", [&](...) {
            barrier.wait();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
            response->Abort();
        });

        threadPool.Execute(true);
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(true);
    }

    TEST_F(FileSystemTest, Pack) {
        string path = fs::canonical("../../test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest").string();

        path += "/*";

        bool callbackCalled = false;
        FileSystem::Pack(path, json(), [&](const string $output) {
            callbackCalled = true;
            ASSERT_STRNE("", $output.c_str());
            ASSERT_TRUE(fs::exists($output));
            ASSERT_TRUE(FileSystem::Delete($output));
        });
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(FileSystemTest, Unpack) {
        string path = fs::canonical("../../test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest1.zip").string();
        std::vector<string> dirList({"f1", "f2", "bin1.bin", "doc1.txt"});

        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        FileSystem::Unpack(path, json(), [&](const string $output) {
            callbackCalled = true;
            for (auto &it : dirList) {
                ASSERT_TRUE((fs::exists($output + "/" + it))) << "at " << $output;
            }

            ASSERT_TRUE(FileSystem::Delete($output));
        });
        ASSERT_TRUE(callbackCalled);

        json options = {
                {"autoStrip", false}
        };
        callbackCalled = false;
        FileSystem::Unpack(path, options, [&](const string $output) {
            callbackCalled = true;
            ASSERT_FALSE($output.empty());
            ASSERT_TRUE(fs::exists($output + "/archTest"));
            for (auto &it : dirList) {
                ASSERT_TRUE(fs::exists($output + "/archTest/" + it));
            }

            ASSERT_TRUE(FileSystem::Delete($output));
        });
        ASSERT_TRUE(callbackCalled);

        path = fs::canonical("../../test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest_strip.zip").string();
        options = json({{"override", false}});
        callbackCalled = false;
        FileSystem::Unpack(path, options, [&](const string $output) {
            callbackCalled = true;
            ASSERT_FALSE($output.empty());
            for (auto &it : dirList) {
                ASSERT_TRUE((fs::exists($output + "/" + it)));
            }
            bool callbackCalledB = false;
            FileSystem::Unpack(path, options,
                               [&](const string $output2) {
                                   callbackCalledB = true;
                                   ASSERT_FALSE($output2.empty());
                               });
            ASSERT_TRUE(callbackCalledB);

            ASSERT_TRUE(FileSystem::Delete($output));
        });
        ASSERT_TRUE(callbackCalled);

        path = fs::canonical("../../test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest2.tar.bz2").string();
        callbackCalled = false;
        FileSystem::Unpack(path, json(), [&](const string $output) {
            callbackCalled = true;
            ASSERT_FALSE($output.empty());
            for (auto &it : dirList) {
                ASSERT_TRUE((fs::exists($output + "/" + it)));
            }

            ASSERT_TRUE(FileSystem::Delete($output));
        });
        ASSERT_TRUE(callbackCalled);

        path = fs::canonical("../../test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest3.tgz").string();
        callbackCalled = false;
        FileSystem::Unpack(path, json(), [&](const string $output) {
            callbackCalled = true;
            ASSERT_FALSE($output.empty());
            for (auto &it : dirList) {
                ASSERT_TRUE((fs::exists($output + "/" + it)));
            }

            ASSERT_TRUE(FileSystem::Delete($output));
        });
        ASSERT_TRUE(callbackCalled);
        testing::internal::GetCapturedStdout();
    }

    TEST_F(FileSystemTest, Expand) {
        const string base = "test/resource/data/Com/Wui/Framework/XCppCommons/System";

#ifdef __clang__
        const auto match = [&](const std::vector<string> &$expected, const std::vector<string> &$data) {
#else
        const auto match = [&](const std::vector<string> &$expected, const std::vector<string> &$data,
                               const string &$file = __builtin_FILE(),
                               int $line = __builtin_LINE()) {
#endif
            bool status = $expected.size() == $data.size();
            if (status) {
                for (const auto &item : $expected) {
                    if (std::find(std::cbegin($data), std::cend($data), item) == std::cend($data)) {
                        status = false;
                        break;
                    }
                }
            }

#ifdef __clang__
            ASSERT_TRUE(status) << "+ \t" << ArrayList::Join($expected, "\n\t") << std::endl << "- \t" << ArrayList::Join($data, "\n\t") <<
                                std::endl;
#else
            ASSERT_TRUE(status) << "+ \t" << ArrayList::Join($expected, "\n\t") << std::endl << "- \t" << ArrayList::Join($data, "\n\t") <<
                                std::endl << "at: " << $file << ":" << $line;
#endif
        };

        match({
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/IO",
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/Process"
              },
              FileSystem::Expand(base + "/*"));

        match({
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest/doc1.txt",
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest/f1/f1doc1.txt",
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest/f2/f2doc1.txt"
              },
              FileSystem::Expand(base + "/**/*.txt"));

        match({
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/IO/archTest/f1/f1doc1.txt"
              },
              FileSystem::Expand(base + "/**/f1/**/*.txt"));

        match({
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/Process/Win/stdio test.exe",
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/Process/Win/testCaseA.cmd",
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/Process/Win/testCaseB.cmd"
              },
              FileSystem::Expand(base + "/**/Win/*"));

        match({
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/Process/Win/stdio test.exe",
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/Process/Win/testCaseA.cmd",
                      "test/resource/data/Com/Wui/Framework/XCppCommons/System/Process/Win/testCaseB.cmd"
              },
              FileSystem::Expand(base + "/**/?i?/*"));
    }
}
