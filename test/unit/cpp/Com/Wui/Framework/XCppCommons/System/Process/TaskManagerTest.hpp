/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTEST_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTEST_HPP_

#if defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
#include "TaskManagerTestPosix.hpp"
#elif WIN_PLATFORM
#include "TaskManagerTestWindows.hpp"
#endif

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTEST_HPP_
