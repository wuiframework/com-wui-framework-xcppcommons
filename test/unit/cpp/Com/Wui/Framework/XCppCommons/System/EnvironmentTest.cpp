/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons {
    using Com::Wui::Framework::XCppCommons::System::Environment;

    TEST(EnvironmentTest, Expand) {
        EXPECT_STREQ(Environment::Expand("").c_str(), "");

#ifdef WINDOWS
        EXPECT_STREQ(Environment::Expand("%_unknown_varible_%").c_str(), "%_unknown_varible_%");
        EXPECT_STREQ(Environment::Expand("_unknown_varible_").c_str(), "_unknown_varible_");

        EXPECT_FALSE(Environment::Expand("%PATH%").empty());
#elif defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
        EXPECT_STREQ(Environment::Expand("$_unknown_varible_").c_str(), "$_unknown_varible_");
        EXPECT_STREQ(Environment::Expand("_unknown_varible_").c_str(), "_unknown_varible_");

        EXPECT_STREQ(Environment::Expand("$HOME").c_str(), std::getenv("HOME"));
        EXPECT_STREQ(Environment::Expand("HOME").c_str(), std::getenv("HOME"));
#endif
    }
}  // namespace Com::Wui::Framework::XCppCommons
