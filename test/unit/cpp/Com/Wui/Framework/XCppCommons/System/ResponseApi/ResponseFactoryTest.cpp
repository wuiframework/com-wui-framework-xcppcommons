/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::ResponseApi {
    using Com::Wui::Framework::XCppCommons::Interfaces::IResponse;
    using Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers::CallbackResponse;

    class TestResponse : public Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers::BaseResponse {
     public:
        void Send(int $exitCode) const override {
            std::cout << "TestResponse: " << $exitCode << std::endl;
        }

        static void testRead(const string &$data, const function<void(const string &$data)> &$callback = nullptr) {
            TestResponse::testRead($data, ResponseFactory::getResponse($callback));
        }

        static void testRead(const string &$data, const shared_ptr<IResponse> &$response) {
            ResponseFactory::getResponse($response)->Send($data);
        }
    };

    TEST(ResponseFactoryTest, getResponse_inFunctionCall) {
        auto testFunc = [&](const string &$data) {};
        TestResponse::testRead("somedata", testFunc);
        ASSERT_TRUE(true);

        TestResponse::testRead("somedata");
        ASSERT_TRUE(true);
    }

    TEST(ResponseFactoryTest, getResponse_callback) {
        bool callbackCalled = false;
        auto response = ResponseFactory::getResponse([&](int $ec) {
            callbackCalled = true;
            ASSERT_EQ(66, $ec);
        });
        response->Send(66);
        ASSERT_TRUE(callbackCalled);
    }

    TEST(ResponseFactoryTest, getResponse_response) {
        bool callbackCalled = false;
        CallbackResponse rootResponse([&](const string &$data) {
            callbackCalled = true;
            ASSERT_STREQ("hello world", $data.c_str());
        });
        auto response = ResponseFactory::getResponse(rootResponse);
        response->Send(string("hello world"));
        ASSERT_TRUE(callbackCalled);

        testing::internal::CaptureStdout();
        shared_ptr<IResponse> response2;
        {
            response2 = ResponseFactory::getResponse(TestResponse());
        }
        response2->Send(111);

        auto response3 = response2;
        response3->Send(666);
        string result = testing::internal::GetCapturedStdout();
        ASSERT_STREQ("TestResponse: 111\nTestResponse: 666\n", result.c_str());
    }

    TEST(ResponseFactoryTest, getResponse_ptr) {
        shared_ptr<TestResponse> tResponse = std::make_shared<TestResponse>();

        auto response = ResponseFactory::getResponse(tResponse);

        ASSERT_EQ(tResponse, response);

        testing::internal::CaptureStdout();
        response->Send(536);
        string result = testing::internal::GetCapturedStdout();
        ASSERT_STREQ("TestResponse: 536\n", result.c_str());
    }

    TEST(ResponseFactoryTest, getResponse_null) {
        shared_ptr<TestResponse> tResponse = nullptr;

        auto response = ResponseFactory::getResponse(tResponse);
        ASSERT_TRUE(response != nullptr);
    }
}
