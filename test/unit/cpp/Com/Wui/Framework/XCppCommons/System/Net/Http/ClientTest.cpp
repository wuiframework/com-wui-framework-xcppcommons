/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    using Com::Wui::Framework::XCppCommons::Enums::HttpClientType;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Enums::LogLevel;

    class ClientTest : public testing::Test {
    };

    TEST_F(ClientTest, Request_Https) {
        Client client;

        json url = Url("http://localhost:8888/wuiframework/com-wui-framework/raw/"
                       "c6df5bb0cf1c971b6b6d2d4994443a1e63a962de/README.md_1").ToJson();

        bool isFinished = false;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_EQ(200, response.getStatusCode());
            response.setOnClose([]() {
                ASSERT_TRUE(false) << "response closed";
            });

            response.setOnEnd([&]() {
                isFinished = true;
            });
        });

        request.setOnError([&](const string error) {
            ASSERT_TRUE(false) << error;
        });
        request.End();
        client.WaitForFinished();

        ASSERT_TRUE(isFinished);
    }

    TEST_F(ClientTest, Request_Https_HEAD) {
        Client client;

        json url = Url("http://localhost:8888/wuiframework/com-wui-framework/raw/"
                       "c6df5bb0cf1c971b6b6d2d4994443a1e63a962de/README.md_2").ToJson();

        url["method"] = "HEAD";

        bool isFinished = false;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_EQ(200, response.getStatusCode());

            response.setOnData([](const string &$data) {
                ASSERT_TRUE(false) << "no data should be received for HEAD method";
            });

            response.setOnClose([]() {
                ASSERT_TRUE(false) << "response closed";
            });

            response.setOnEnd([&]() {
                isFinished = true;
            });
        });

        request.setOnError([&](const string error) {
            ASSERT_TRUE(false) << error;
        });
        request.End();
        client.WaitForFinished();

        ASSERT_TRUE(isFinished);
    }

    TEST_F(ClientTest, Request_https_POST) {
        Client client;

        json url = Url("http://localhost:8888/xorigin/").ToJson();
        url["method"] = "POST";
        url["headers"] = json({{"Charset",      "UTF-8"},
                               {"Content-Type", "application/x-www-form-urlencoded; charset=UTF-8"},
                               {"User-Agent",   "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 "
                                                "(KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"}});
        url["body"] = "jsonData";

        bool isFinished = false;
        string data;
        int length = 0;
        int contentLength = 0;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_EQ(200, response.getStatusCode());
            string cln = response.getHeaderValue("content-length");
            if (!cln.empty()) {
                contentLength = boost::lexical_cast<int>(cln);
            }
            response.setOnData([&](const string &$data) {
                data += $data;
                length += $data.size();
            });

            response.setOnClose([]() {
                ASSERT_TRUE(false) << "response closed";
            });

            response.setOnEnd([&]() {
                isFinished = true;
            });
        });

        request.setOnError([&](const string error) {
            ASSERT_TRUE(false) << error;
        });

        request.Write(url["body"].get<string>());
        request.End();
        client.WaitForFinished();

        ASSERT_TRUE(isFinished);
        ASSERT_EQ(length, contentLength) << std::endl << "data: " << data << std::endl;
    }

    TEST_F(ClientTest, Request_Http) {
        Client client;

        json url = Url("http://localhost:8888/everyoneinsilico.txt").ToJson();

        bool isFinished = false;
        int length = 0;
        int contentLength = 0;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_EQ(200, response.getStatusCode());
            string cln = response.getHeaderValue("content-length");
            if (!cln.empty()) {
                contentLength = boost::lexical_cast<int>(cln);
            }
            response.setOnData([&](const string &data) {
                length += data.size();
            });

            response.setOnClose([&]() {
                ASSERT_TRUE(false) << "response closed";
            });

            response.setOnEnd([&]() {
                isFinished = true;
            });
        });

        request.End();
        client.WaitForFinished();

        ASSERT_TRUE(isFinished);
        ASSERT_EQ(length, contentLength);
    }

    TEST_F(ClientTest, Request_Http_page) {
        Client client;

        json url = Url("http://localhost:8888").ToJson();

        bool isFinished = false;
        int length = 0;
        int contentLength = 0;
        string data;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_EQ(200, response.getStatusCode());
            string cln = response.getHeaderValue("content-length");
            if (!cln.empty()) {
                contentLength = boost::lexical_cast<int>(cln);
            }
            response.setOnData([&](const string &$data) {
                length += $data.size();
                data += $data;
            });

            response.setOnClose([&]() {
                ASSERT_TRUE(false) << "response closed";
            });

            response.setOnEnd([&]() {
                isFinished = true;
            });
        });

        request.End();
        client.WaitForFinished();
        ASSERT_TRUE(isFinished);
        ASSERT_EQ(length, contentLength);
    }

    TEST_F(ClientTest, Request_Https_page) {
        Client client;

        json url = Url("http://localhost:8888/security/login?TARGET=https%3A%2F%2Fwww.nxp.com%2Fruhp%2FmyAccount.html").ToJson();

        bool isFinished = false;
        int length = 0;
        int contentLength = 0;
        string data;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_EQ(200, response.getStatusCode());
            string cln = response.getHeaderValue("content-length");
            if (!cln.empty()) {
                contentLength = boost::lexical_cast<int>(cln);
            }
            response.setOnData([&](const string &$data) {
                length += $data.size();
                data += $data;
            });

            response.setOnClose([&]() {
                ASSERT_TRUE(false) << "response closed";
            });

            response.setOnEnd([&]() {
                isFinished = true;
            });
        });

        request.End();
        client.WaitForFinished();
        ASSERT_TRUE(isFinished);
        ASSERT_EQ(length, contentLength);
    }

    TEST_F(ClientTest, Request_redirect_inside) {
        json url = Url("http://localhost:8888/git-for-windows/git/releases/download/"
                       "v2.11.0.windows.1/Git-2.11.0-64-bit.exe").ToJson();
        Client client;
        bool callbackCalled = false;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_TRUE(response.getStatusCode() != 301 || response.getStatusCode() != 302) << "No redirect found.";
            callbackCalled = true;
        });

        request.End();
        client.WaitForFinished();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(ClientTest, Request_not_existing_url) {
        json url = Url("http://localhost:8888/git-for-windows/git/releases/download/not-existing-url"
                       "v2.11.0.windows.1/Git-2.11.0-64-bit.exe").ToJson();
        Client client;
        bool callbackCalled = false;
        auto request = client.Request(url, [&](ClientResponse &response) {
            ASSERT_EQ(404, response.getStatusCode());
            callbackCalled = true;
        });

        request.End();
        client.WaitForFinished();
        ASSERT_TRUE(callbackCalled);
    }
}  // namespace Com::Wui::Framework::XCppCommons::Net::Http
