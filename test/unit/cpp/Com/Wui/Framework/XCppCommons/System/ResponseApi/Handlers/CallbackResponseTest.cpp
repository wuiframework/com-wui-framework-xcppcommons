/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers {

    TEST(CallbackResponseTest, Send) {
        bool callbackCalled = false;
        CallbackResponse response([&](int $errorCode) {
            callbackCalled = true;
            ASSERT_EQ(23, $errorCode);
        });
        response.Send(23);
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        response = CallbackResponse([&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        });
        response.Send(true);
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        response = CallbackResponse([&](bool $status, const string &$msg) {
            callbackCalled = true;
            ASSERT_FALSE($status);
            ASSERT_STREQ("hello world", $msg.c_str());
        });
        response.Send(false, "hello world");
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        response = CallbackResponse([&](const string &$data) {
            callbackCalled = true;
            ASSERT_STREQ("hello awesome world", $data.c_str());
        });
        response.Send(string("hello awesome world"));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        response = CallbackResponse([&](const json &$object) {
            callbackCalled = true;
            ASSERT_STREQ(json({{"propA", true},
                               {"propB", 33.5}}).dump().c_str(), $object.dump().c_str());
        });
        response.Send(json({{"propA", true},
                            {"propB", 33.5}}));
        ASSERT_TRUE(callbackCalled);
    }

    TEST(CallbackResponseTest, OnComplete) {
        bool callbackCalled = false;
        CallbackResponse response([&](bool $status) {
            callbackCalled = true;
            ASSERT_TRUE($status);
        });
        response.OnComplete(true);
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        response = CallbackResponse([&](const string &$data) {
            callbackCalled = true;
            ASSERT_STREQ("hello world", $data.c_str());
        });
        response.OnComplete(string("hello world"));
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        response = CallbackResponse([&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            ASSERT_EQ(55, $exitCode);
            ASSERT_STREQ("std out", $std[0].c_str());
            ASSERT_STREQ("std err", $std[1].c_str());
        });
        response.OnComplete(55, {"std out", "std err"});
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        response = CallbackResponse([&](const string &$data) {
            callbackCalled = true;
            ASSERT_STREQ("hello awesome world", $data.c_str());
        });
        response.OnComplete(json(), string("hello awesome world"));
        ASSERT_TRUE(callbackCalled);
    }
}
