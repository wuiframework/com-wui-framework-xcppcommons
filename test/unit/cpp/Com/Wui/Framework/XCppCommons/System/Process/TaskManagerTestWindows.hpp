/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTESTWINDOWS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTESTWINDOWS_HPP_

#ifdef WIN_PLATFORM

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::System::Process::TaskManager;

    class TaskManagerTest : public testing::Test {
     protected:
        void createDetachedProcess(function<bool(unsigned int)> $action) {
            STARTUPINFO startupInfo = { sizeof(startupInfo) };
            PROCESS_INFORMATION processInfo = {0};

            // casting hacks for compatibility purposes
            const char *cmd = this->executablePath.c_str();
            char *args = const_cast< char*>(this->args.c_str());

            if (CreateProcess(cmd, args, nullptr, nullptr, TRUE, 0, nullptr, nullptr, &startupInfo, &processInfo)) {
                const auto shouldTerminate = $action(static_cast<unsigned int>(processInfo.dwProcessId));

                if (shouldTerminate) {
                    if (!TerminateProcess(processInfo.hProcess, 255)) {
                        ADD_FAILURE() << "Failed to kill the process with ID " << std::to_string(processInfo.dwProcessId)
                                      << ", either it's already terminated or you have to kill it manually";
                    }
                }
            } else {
                ADD_FAILURE() << "Failed to spawn a new process, system error code: " << std::to_string(GetLastError());
            }

            CloseHandle(processInfo.hProcess);
            CloseHandle(processInfo.hThread);
        }

        const string executablePath = "C:\\Windows\\System32\\timeout.exe";
        const string executableName = "timeout";
        const string args = "/T -1";
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGERTESTWINDOWS_HPP_
