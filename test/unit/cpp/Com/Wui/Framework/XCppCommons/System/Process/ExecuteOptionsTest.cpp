/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::Process {

    TEST(ExecuteOptionsTest, Construction) {
        ExecuteOptions executeOptions("cwd", {{"key1", "value1"},
                                              {"key2", "value2"}}, true, 11, 22);
        ASSERT_STREQ("cwd", executeOptions.getCwd().c_str());
        ASSERT_STREQ("value1", executeOptions.getEnv().at("key1").c_str());
        ASSERT_STREQ("value2", executeOptions.getEnv().at("key2").c_str());
        ASSERT_TRUE(executeOptions.isShell());
        ASSERT_EQ(11, executeOptions.getTimeout());
        ASSERT_EQ(22, executeOptions.getMaxBuffer());

        executeOptions = ExecuteOptions({{"cwd",       "testCwd"},
                                         {"env",       {
                                                               {"key1", "tvalue1"},
                                                               {"key2", "tvalue2"}
                                                       }},
                                         {"shell",     true},
                                         {"timeout",   33},
                                         {"maxBuffer", 44}});
        ASSERT_STREQ("testCwd", executeOptions.getCwd().c_str());
        ASSERT_STREQ("tvalue1", executeOptions.getEnv().at("key1").c_str());
        ASSERT_STREQ("tvalue2", executeOptions.getEnv().at("key2").c_str());
        ASSERT_TRUE(executeOptions.isShell());
        ASSERT_EQ(33, executeOptions.getTimeout());
        ASSERT_EQ(44, executeOptions.getMaxBuffer());
    }

    TEST(ExecuteOptionsTest, Construction_CopyMove) {
        ExecuteOptions ex1("cwd1", {{"key1", "val1"}}, true, 11, 22);

        auto checkValues = [&](ExecuteOptions const &$opt) {
            ASSERT_STREQ("cwd1", $opt.getCwd().c_str());
            ASSERT_STREQ("val1", $opt.getEnv().at("key1").c_str());
            ASSERT_TRUE($opt.isShell());
            ASSERT_EQ(11, $opt.getTimeout());
            ASSERT_EQ(22, $opt.getMaxBuffer());
        };

        ExecuteOptions ex2(ex1);
        checkValues(ex2);
        ASSERT_FALSE(&ex1.getEnv().at("key1") == &ex2.getEnv().at("key1"));
        intptr_t mapItemPtr = reinterpret_cast<intptr_t>(&ex2.getEnv().at("key1"));
        ExecuteOptions ex3 = std::move(ex2);
        checkValues(ex3);
        ASSERT_EQ(mapItemPtr, reinterpret_cast<intptr_t>(&ex3.getEnv().at("key1")));

        ExecuteOptions ex4;
        ex4 = ex3;
        checkValues(ex4);

        ExecuteOptions ex5;
        ex5 = std::move(ex3);
        checkValues(ex5);
    }

    TEST(ExecuteOptionsTest, Equality) {
        ExecuteOptions first("cwd1", {{"key1", "value1"},
                                      {"key2", "value2"}}, true, 111, 222);
        ExecuteOptions second(first);
        ASSERT_EQ(first, second);
        auto list = second.getEnv();
        list["key2"] = "newValue";
        second.setEnv(list);
        ASSERT_NE(first, second);
    }

    TEST(ExecuteOptionsTest, ToString) {
        ExecuteOptions executeOptions("cwd1", {{"key1", "value1"},
                                               {"key2", "value2"}}, true, 111, 222);
        string origin = json({{"cwd",       "cwd1"},
                              {"env",       {{"key1", "value1"}, {"key2", "value2"}}},
                              {"shell",     true},
                              {"timeout",   111},
                              {"maxBuffer", 222},
                              {"verbose",   true}}).dump();

        ASSERT_STREQ(origin.c_str(), executeOptions.ToString().c_str());
        std::ostringstream oss;
        oss << executeOptions;
        ASSERT_STREQ(origin.c_str(), oss.str().c_str());
    }
}
