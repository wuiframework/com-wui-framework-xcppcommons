/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::Process {

    TEST(SpawnOptionsTest, Construction) {
        SpawnOptions spawnOptions("cwd", {{"key1", "value1"},
                                          {"key2", "value2"}}, true, true);
        ASSERT_STREQ("cwd", spawnOptions.getCwd().c_str());
        ASSERT_STREQ("value1", spawnOptions.getEnv().at("key1").c_str());
        ASSERT_STREQ("value2", spawnOptions.getEnv().at("key2").c_str());
        ASSERT_TRUE(spawnOptions.isShell());
        ASSERT_TRUE(spawnOptions.isDetached());

        spawnOptions = SpawnOptions({{"cwd",      "testCwd"},
                                     {"env",      {
                                                          {"key1", "tvalue1"},
                                                          {"key2", "tvalue2"}
                                                  }},
                                     {"shell",    true},
                                     {"detached", true}});
        ASSERT_STREQ("testCwd", spawnOptions.getCwd().c_str());
        ASSERT_STREQ("tvalue1", spawnOptions.getEnv().at("key1").c_str());
        ASSERT_STREQ("tvalue2", spawnOptions.getEnv().at("key2").c_str());
        ASSERT_TRUE(spawnOptions.isShell());
        ASSERT_TRUE(spawnOptions.isDetached());
    }

    TEST(SpawnOptionsTest, Construnction_CopyMove) {
        SpawnOptions sw1("cwd1", {{"key1", "val1"}}, true, true);

        auto checkValues = [&](SpawnOptions const &$opt) {
            ASSERT_STREQ("cwd1", $opt.getCwd().c_str());
            ASSERT_STREQ("val1", $opt.getEnv().at("key1").c_str());
            ASSERT_TRUE($opt.isShell());
            ASSERT_TRUE($opt.isDetached());
        };

        SpawnOptions sw2(sw1);
        checkValues(sw2);
        ASSERT_FALSE(&sw1.getEnv().at("key1") == &sw2.getEnv().at("key1"));
        intptr_t mapItemPtr = reinterpret_cast<intptr_t>(&sw2.getEnv().at("key1"));
        SpawnOptions sw3 = std::move(sw2);
        checkValues(sw3);
        ASSERT_EQ(mapItemPtr, reinterpret_cast<intptr_t>(&sw3.getEnv().at("key1")));

        SpawnOptions sw4;
        sw4 = sw3;
        checkValues(sw4);

        SpawnOptions sw5;
        sw5 = std::move(sw3);
        checkValues(sw5);
    }

    TEST(SpawnOptionsTest, Equality) {
        SpawnOptions first("cwd1", {{"key1", "value1"},
                                    {"key2", "value2"}}, true, true);
        SpawnOptions second(first);
        ASSERT_EQ(first, second);
        auto list = second.getEnv();
        list["key2"] = "newValue";
        second.setEnv(list);
        ASSERT_NE(first, second);
    }

    TEST(SpawnOptionsTest, ToString) {
        SpawnOptions spawnOptions("cwd1", {{"key1", "value1"},
                                           {"key2", "value2"}}, true, false);
        string origin = json({{"cwd",      "cwd1"},
                              {"env",      {{"key1", "value1"}, {"key2", "value2"}}},
                              {"shell",    true},
                              {"detached", false},
                              {"verbose",  true}}).dump();

        ASSERT_STREQ(origin.c_str(), spawnOptions.ToString().c_str());
        std::ostringstream oss;
        oss << spawnOptions;
        ASSERT_STREQ(origin.c_str(), oss.str().c_str());
    }
}
