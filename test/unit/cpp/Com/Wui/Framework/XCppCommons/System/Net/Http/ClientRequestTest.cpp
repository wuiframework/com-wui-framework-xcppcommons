/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    struct TestPair {
        json url;
        string request;
    };

    class ClientRequestTest : public testing::TestWithParam<TestPair> {
     public:
    };

    TEST_F(ClientRequestTest, Request_method_err) {
        json url = Url("http://test.com/resource.path").ToJson();
        url["method"] = "NA";

        ClientRequest clientRequest(url, [](const string &$data) {
            ASSERT_STREQ("", $data.c_str());
        }, nullptr);

        clientRequest.setOnError([](const string &$error) {
            ASSERT_TRUE(boost::contains($error, "Selected ClientRequest method is not supported"));
        });
    }

    TEST_P(ClientRequestTest, ValidateRequest) {
        json options = GetParam().url;
        string request = GetParam().request;

        if (options["url"].is_string()) {
            options["url"] = Url(options["url"].get<string>()).ToJson();
            options["url"]["method"] = options["method"];
            options["url"]["headers"] = options["headers"];
        }

        ClientRequest clientRequest(options["url"], [&request](const string &$data) {
            ASSERT_STREQ(request.c_str(), $data.c_str());
        }, nullptr);

        clientRequest.Write(options["body"].get<string>());
        clientRequest.End();
    }

    INSTANTIATE_TEST_CASE_P(
            ClientRequestData,
            ClientRequestTest,
            testing::Values(
                    TestPair {
                            json({
                                         {"method",       "GET"},
                                         {"url",          "http://customurl.url/test/custom.zip?hello=world"},
                                         {"headers",      {
                                                                  {"header1", "value1"},
                                                                  {"header2", "value2"}
                                                          }},
                                         {"body",         "test body"},
                                         {"streamOutput", true}
                                 }),
                            "GET /test/custom.zip?hello=world HTTP/1.1\r\nHost: customurl.url\r\nheader1: value1\r\n"
                                    "header2: value2\r\n\r\n"},
                    TestPair {
                            json({
                                         {"method",       "POST"},
                                         {"url",          "http://customurl.url/test/custom.zip?hello=world"},
                                         {"headers",      {
                                                                  {"header1", "value1"},
                                                                  {"header2", "value2"}
                                                          }},
                                         {"body",         "test body"},
                                         {"streamOutput", true}
                                 }),
                            "POST /test/custom.zip?hello=world HTTP/1.1\r\nHost: customurl.url\r\nheader1: value1\r\n"
                                    "header2: value2\r\ncontent-length: 9\r\n\r\ntest body"},
                    TestPair {
                            json({
                                         {"method",       "HEAD"},
                                         {"url",          "http://customurl.url/test/custom.zip?hello=world"},
                                         {"headers",      {
                                                                  {"header1", "value1"},
                                                                  {"header2", "value2"}
                                                          }},
                                         {"body",         "test body"},
                                         {"streamOutput", true}
                                 }),
                            "HEAD /test/custom.zip?hello=world HTTP/1.1\r\nHost: customurl.url\r\nheader1: value1\r\n"
                                    "header2: value2\r\n\r\n"}));

}  // namespace Com::Wui::Framework::XCppCommons::Net::Http
