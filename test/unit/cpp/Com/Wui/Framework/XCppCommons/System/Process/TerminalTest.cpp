/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"
#include "gmock/gmock.h"

#ifndef WIN_PLATFORM

#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>

#endif

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;
    using Com::Wui::Framework::XCppCommons::Primitives::String;

    class TerminalTest : public testing::Test {
     protected:
        void SetUp() override {
#if defined(WIN_PLATFORM)
            string testName = "UnitTestRunner.exe";
            this->testResourceDir += "/Win";
            this->lineBreak = "\r\n";
#elif defined(MAC_PLATFORM)
            string testName = "UnitTestRunner";
            this->testResourceDir += "/Mac";
            this->lineBreak = "\n";
#else
            string testName = "UnitTestRunner";
            this->testResourceDir += "/Linux";
            this->lineBreak = "\n";
#endif
            this->baseDir = boost::filesystem::canonical(testName).parent_path().make_preferred().string();
            this->baseDir = (boost::filesystem::path(this->baseDir) / this->testResourceDir).normalize().make_preferred().string();
        }

     public:
        string baseDir = "";
        string testResourceDir = "../../test/resource/data/Com/Wui/Framework/XCppCommons/System/Process";
        string lineBreak;
    };

    TEST_F(TerminalTest, UnquoteCmdPath) {
        std::vector<std::pair<string, string>> map = {
                {R"(c:/hehe/some.exe)",                          R"(c:/hehe/some.exe)"},
                {R"("c:/hehe/some.exe --hello")",                R"(c:/hehe/some.exe --hello)"},
                {R"("c:/hehe/some.exe")",                        R"(c:/hehe/some.exe)"},
                {R"("c:/hehe/some.exe" --hello)",                R"(c:/hehe/some.exe --hello)"},
                {R"(c:/hehe/some.exe "--some=1")",               R"(c:/hehe/some.exe "--some=1")"},
                {R"("c:/hehe/some.exe --some="text"")",          R"(c:/hehe/some.exe --some="text")"},
                {R"("c:/hehe/some.exe" --some="text")",          R"(c:/hehe/some.exe --some="text")"},
                {R"(c:/hehe/some.exe --some="cmd /c "exec"")",   R"(c:/hehe/some.exe --some="cmd /c "exec"")"},
                {R"("c:/hehe/some.exe" --some="cmd /c "exec"")", R"(c:/hehe/some.exe --some="cmd /c "exec"")"},
                {R"("c:/hehe/some.exe --some="cmd /c "exec""")", R"(c:/hehe/some.exe --some="cmd /c "exec"")"},
                {R"("c:/hehe/some.exe --some="cmd /c "exec"")",  R"("c:/hehe/some.exe --some="cmd /c "exec"")"}
        };

        std::for_each(map.begin(), map.end(), [&](const std::pair<string, string> &$item) {
            ASSERT_STREQ($item.second.c_str(), Terminal::UnquoteCmdPath($item.first).c_str());
        });
    }

    TEST_F(TerminalTest, ResolveCmd) {
        string cmd = "UnitTestRunner.exe";
        string cwd;
        string res = (boost::filesystem::current_path() / "UnitTestRunner.exe").make_preferred().string();

        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, cwd).c_str());

        cmd = "ping";
        bool callbackCalled = false;
        Child::Execute("where " + cmd, ExecuteOptions(json({{"shell", true}})),
                       [&](int $exitCode, const string $stdOut, const string $stdErr) {
                           callbackCalled = true;
                           res = boost::trim_copy($stdOut);
                       })->WaitForExit();

        ASSERT_TRUE(callbackCalled);
        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, cwd).c_str());

        cmd = "stdio test.exe";
        cwd = this->testResourceDir;
        res = boost::filesystem::canonical(
                boost::filesystem::current_path() / this->testResourceDir / "stdio test.exe").make_preferred().string();

        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, cwd).c_str());

        cmd = "\"stdio test.exe\"";
        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, cwd).c_str());

        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, "\"" + cwd + "\"").c_str());

        cmd = "\"stdio test.exe\" --help";
        res = "";
        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, cwd).c_str());

        cmd = "\"stdio test.exe --help\"";
        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, cwd).c_str());

        cmd = "testCaseA.cmd";
        cwd = this->baseDir;
        res = boost::filesystem::canonical(
                boost::filesystem::current_path() / this->testResourceDir / "testCaseA.cmd").make_preferred().string();
        ASSERT_STRCASEEQ(res.c_str(), Terminal::ResolveCmd(cmd, cwd).c_str());
    }

    TEST_F(TerminalTest, Execute_cmd) {
#ifdef WIN_PLATFORM
        string cmd = "cmd";
        const std::vector<string> args{"/c", "dir"};
#else
        string cmd = "/bin/sh";
        const std::vector<string> args{"-c", "ls"};
#endif
        string cwd = "../../";
        string out;
        int exitCode = -1;
        bool callbackCalled = false;
        Terminal::Execute(cmd, args, TerminalOptions(cwd), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            exitCode = $exitCode;
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_EQ(0, exitCode) << "Execution of command failed.";
        boost::regex ex(".*package\\.conf\\.json.*");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any)) << "Working directory not match.";
    }

    TEST_F(TerminalTest, DISABLED_Execute_wui) {
//        string cmd = "wui";
//        string cwd = "../../";
//        std::vector<string> args{"--version"};
//
//        string out;
//        int exitCode = -1;
//        bool callbackCalled = false;
//        Terminal::Execute(cmd, args, TerminalOptions(cwd), [&](int $exitCode, const std::vector<string> &$std) {
//            callbackCalled = true;
//            out = $std[0];
//            exitCode = $exitCode;
//        });
//        ASSERT_TRUE(callbackCalled);
//        ASSERT_EQ(0, exitCode) << "Execution of command failed.";
//
//        boost::regex ex("WUI Builder v\\d\\.\\d\\.\\d.*");
//        ASSERT_TRUE(boost::regex_match(out, ex)) << out << " - not match WUI version regex: " << ex << std::endl;
//
//        out = "";
//        args.clear();
//        args.push_back("--path");
//        Terminal::Execute(cmd, args, TerminalOptions(), [&](int $exitCode, const string $stdOut, const string $stdErr) {
//            out = $stdOut;
//            exitCode = $exitCode;
//        });
//        ASSERT_EQ(0, exitCode) << "Execution of command failed.";
//
//        boost::algorithm::trim(out);
//        ASSERT_TRUE(boost::filesystem::exists(out)) << out << " - WUI path is not correct." << std::endl;
    }

    TEST_F(TerminalTest, Execute_ping) {
        string out;
        int exitCode = -1;
        testing::internal::CaptureStdout();
        bool callbackCalled = false;
#ifdef WIN_PLATFORM
        std::vector<string> args = {"127.0.0.1"};
#else
        std::vector<string> args = {"-c4", "127.0.0.1"};
#endif
        Terminal::Execute("ping", args, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            exitCode = $exitCode;
        });
        ASSERT_TRUE(callbackCalled);
        testing::internal::GetCapturedStdout();
        ASSERT_EQ(0, exitCode) << "Execution of command failed.";

        boost::regex ex(R"(.*P* \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}.*)");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any))
                                    << "Ping output is not valid, spawn failed" << std::endl << out << std::endl
                                    << "not match regex: " << ex.str() << std::endl;
    }

    TEST_F(TerminalTest, Execute_testCaseA) {
#ifdef WIN_PLATFORM
        string cmd = "testCaseA.cmd";
#else
        string cmd = "./testCaseA.sh";

        struct stat stat1;
        if (stat((testResourceDir + "/" + cmd).c_str(), &stat1) == 0) {
            chmod((testResourceDir + "/" + cmd).c_str(), stat1.st_mode | S_IEXEC);
        }
#endif
        std::vector<string> args{""};
        string cwd = this->baseDir;
        string out, err;
        int exitCode = -1;
        testing::internal::CaptureStdout();
        bool callbackCalled = false;
        Terminal::Execute(cmd, args, TerminalOptions(json({{"cwd", cwd}})), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            err = $std[1];
            exitCode = $exitCode;
        });
        ASSERT_TRUE(callbackCalled);
        testing::internal::GetCapturedStdout();
        ASSERT_EQ(0, exitCode) << "Execution of command failed." << std::endl << out << std::endl << err;
    }

    TEST_F(TerminalTest, Execute_stdiotest) {
        string out, err;
        int exitCode = -1;
        TerminalOptions executeOptions(this->baseDir);
        bool callbackCalled = false;

#ifdef WIN_PLATFORM
        string cmd = "stdio test.exe";
#else
        string cmd = "'./stdio test'";
#endif

        std::vector<string> args{"--stdout=5", "--stdout-prefix=stdout_prefixed_", "--stderr=4",
                                 "--stderr-prefix=stderr_prefixed_", "--delay=1000", "--exit=111"};

        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, args, executeOptions, [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            err = $std[1];
            exitCode = $exitCode;
        });
        testing::internal::GetCapturedStdout();

        ASSERT_TRUE(callbackCalled);
        ASSERT_EQ(111, exitCode);

        string testOutStr = "stdout_prefixed_0<?>stdout_prefixed_1<?>stdout_prefixed_2<?>stdout_prefixed_3<?>stdout_prefixed_4<?>";
        string testErrStr = "stderr_prefixed_0<?>stderr_prefixed_1<?>stderr_prefixed_2<?>stderr_prefixed_3<?>";

        ASSERT_STREQ(String::Replace(testOutStr, "<?>", lineBreak).c_str(), out.c_str());
        ASSERT_STREQ(String::Replace(testErrStr, "<?>", lineBreak).c_str(), err.c_str());

#ifdef WIN_PLATFORM
        SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX | SEM_NOALIGNMENTFAULTEXCEPT);
#endif

        testing::internal::CaptureStdout();
        callbackCalled = false;
        Terminal::Execute(cmd, {"--throw"}, executeOptions, [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            exitCode = $exitCode;
            err = $std[1];
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        ASSERT_EQ(3, exitCode);
#else
        ASSERT_EQ(134, exitCode);
#endif
        ASSERT_FALSE(err.empty());
    }

    TEST_F(TerminalTest, Execute_stdio_resolve) {
        bool callbackCalled = false;
#ifdef WIN_PLATFORM
        string cmd = "stdio test.exe";
#else
        string cmd = "./stdio test";
#endif
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, {"--stdout=2", "--stdout-prefix=prefix", "--exit 22"}, TerminalOptions(this->baseDir),
                          [&](int $exitCode, const std::vector<string> &$std) {
                              callbackCalled = true;
                              ASSERT_EQ(22, $exitCode);
                              ASSERT_STREQ(String::Replace("prefix0<?>prefix1<?>", "<?>", this->lineBreak).c_str(), $std[0].c_str());
                          });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        cmd = "\"stdio test.exe\"";
#else
        cmd = "'./stdio test'";
#endif
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, {"--stdout=2", "--stdout-prefix=prefix", "--exit 22"}, TerminalOptions(this->baseDir),
                          [&](int $exitCode, const std::vector<string> &$std) {
                              callbackCalled = true;
                              ASSERT_EQ(22, $exitCode);
                              ASSERT_STREQ(String::Replace("prefix0<?>prefix1<?>", "<?>", this->lineBreak).c_str(), $std[0].c_str());
                          });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        cmd = R"("stdio test.exe" --stdout=2 --stdout-prefix="prefix" --exit 33)";
#else
        cmd = "\"./stdio test --stdout=2 --stdout-prefix=\"prefix\" --exit 33\"";
#endif
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, {}, TerminalOptions(this->baseDir),
                          [&](int $exitCode, const std::vector<string> &$std) {
                              callbackCalled = true;
                              ASSERT_EQ(33, $exitCode);
                              ASSERT_STREQ(String::Replace("prefix0<?>prefix1<?>", "<?>", this->lineBreak).c_str(), $std[0].c_str());
                          });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(TerminalTest, Execute_multicmd) {
        bool callbackCalled = false;
#ifdef WIN_PLATFORM
        string cmd = "\"stdio test.exe\"";
#else
        string cmd = "'./stdio test'";
#endif
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd + " --stdout=2 --stdout-prefix=prefix --exit 22", {}, TerminalOptions(this->baseDir),
                          [&](int $exitCode, const std::vector<string> &$std) {
                              callbackCalled = true;
                              ASSERT_EQ(22, $exitCode);
                              ASSERT_STREQ(String::Replace("prefix0<?>prefix1<?>", "<?>", this->lineBreak).c_str(), $std[0].c_str());
                          });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        cmd = "dir ..\\..\\";
#else
        cmd = "ls ../..";
#endif
        callbackCalled = false;
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, {}, TerminalOptions("", {}, true),
                          [&](int $exitCode, const std::vector<string> &$std) {
                              callbackCalled = true;
                              ASSERT_EQ(0, $exitCode);
                              boost::regex ex(".*package\\.conf\\.json.*");
                              ASSERT_TRUE(boost::regex_match($std[0], ex, boost::regex_constants::match_any))
                                                          << "Working directory not match.";
                          });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        cmd = "HELP";
#else
        cmd = "make --help";
#endif
        callbackCalled = false;
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, {}, TerminalOptions(json({{"shell",   true},
                                                         {"verbose", false}})),
                          [&](int $exitCode, const std::vector<string> &$std) {
                              callbackCalled = true;
#ifdef WIN_PLATFORM
                              ASSERT_EQ(1, $exitCode);
                              string re = ".*ASSOC.*";
#else
                              ASSERT_EQ(0, $exitCode);
                              string re = "*FILE*";
#endif
                              boost::regex ex(re);
                              ASSERT_TRUE(boost::regex_match($std[0], ex, boost::regex_constants::match_any))
                                                          << "Output data do not contains shell help.";
                          });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(TerminalTest, Execute_echo) {
        string cmd = "echo";
#ifdef WIN_PLATFORM
        std::vector<string> args{"%PROCESSOR_ARCHITECTURE"};
#else
        std::vector<string> args{"`uname -s`-`uname -p`"};
#endif

        string out, err;
        int exitCode = -1;
        testing::internal::CaptureStdout();
        bool callbackCalled = false;
        Terminal::Execute(cmd, args, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            err = $std[1];
            exitCode = $exitCode;
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
        ASSERT_EQ(0, exitCode);
    }

#ifndef WIN_PLATFORM

    TEST_F(TerminalTest, Execute_internal_command) {
        string cmd = "set";
        std::vector<string> args{};
        string out;
        int exitCode = -1;
        bool callbackCalled = false;
        Terminal::Execute(cmd, args, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            exitCode = $exitCode;
        });
        ASSERT_TRUE(callbackCalled);
        ASSERT_EQ(0, exitCode);
        boost::regex ex(".*PATH=.*");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any));
        ex.set_expression(".*PWD=.*");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any));
    }
#endif

    TEST_F(TerminalTest, Execute_wmic) {
#ifdef WIN_PLATFORM
        string cmd = "wmic";
        std::vector<string> args{"os", "get", "osarchitecture"};
#else
        string cmd = "uname";
        std::vector<string> args{"-m"};
#endif
        string out, err;
        int exitCode = -1;
        bool callbackCalled = false;
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, args, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            exitCode = $exitCode;
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
        ASSERT_EQ(0, exitCode);
    }

    TEST_F(TerminalTest, Execute_longStdData) {
        bool callbackCalled = false;
#ifdef WIN_PLATFORM
        string cmd = "stdio test.exe";
#else
        string cmd = "'./stdio test'";
#endif
        std::vector<string> args{"--stdout=9000", "--stdout-prefix=stdout_prefixed_", "--exit=111"};
        TerminalOptions executeOptions(this->baseDir);
        string out, err;
        int exitCode = -1;
        testing::internal::CaptureStdout();
        Terminal::Execute(cmd, args, executeOptions, [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            err = $std[1];
            exitCode = $exitCode;
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
        ASSERT_EQ(111, exitCode);

#ifdef WIN_PLATFORM
        ASSERT_EQ(196890, static_cast<int>(out.size()));
#else
        ASSERT_EQ(187890, static_cast<int>(out.size()));
#endif

        exitCode = -1;
        out.clear();
        err.clear();
        args.clear();
        args = {"--stdout=9999", "--stdout-prefix=stdout_prefixed_", "--exit=111"};
        testing::internal::CaptureStdout();
        callbackCalled = false;
        Terminal::Execute(cmd, args, executeOptions, [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            out = $std[0];
            err = $std[1];
            exitCode = $exitCode;
        });
        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        ASSERT_EQ(8, exitCode);
#else
        ASSERT_EQ(OUT_OF_MEM, exitCode);
#endif
        ASSERT_GT(218868, static_cast<int>(out.size()));
    }

    TEST_F(TerminalTest, Execute_environment) {
#ifdef WIN_PLATFORM
        string envPathName = "%PATH%";
#else
        string envPathName = "$PATH";
#endif
        testing::internal::CaptureStdout();
        Terminal::Execute("echo " + envPathName, {}, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            ASSERT_EQ(0, $exitCode);
            ASSERT_STREQ(getenv("PATH"), boost::trim_copy($std[0]).c_str());
        });
        testing::internal::GetCapturedStdout();

        testing::internal::CaptureStdout();
        Terminal::Execute("echo " + envPathName, {},
                          TerminalOptions(json({{"env", {{"PATH", boost::filesystem::current_path().string()}}}})),
                          [&](int $exitCode, const std::vector<string> &$std) {
                              ASSERT_EQ(0, $exitCode);
                              ASSERT_STREQ(boost::filesystem::current_path().string().c_str(), boost::trim_copy($std[0]).c_str());
                          });
        testing::internal::GetCapturedStdout();
    }

    TEST_F(TerminalTest, Execute_symbol_resolve) {
        testing::internal::CaptureStdout();
#ifdef WIN_PLATFORM
        string envTestVarName = "%TEST_VAR_PATH%";
#else
        string envTestVarName = "$TEST_VAR_PATH";
#endif
        Terminal::Execute("echo " + envTestVarName, {}, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            ASSERT_EQ(0, $exitCode);
#ifdef WIN_PLATFORM
            ASSERT_STREQ("%TEST_VAR_PATH%", boost::trim_copy($std[0]).c_str());
#else
            ASSERT_STREQ("", boost::trim_copy($std[0]).c_str());
#endif

            Terminal::Execute("echo " + envTestVarName, {}, TerminalOptions(json({{"env", {{"TEST_VAR_PATH", "TEST_SYMBOL"}}}})),
                              [&](int $exitCode2, const std::vector<string> &$std2) {
                                  ASSERT_EQ(0, $exitCode2);
                                  ASSERT_STREQ("TEST_SYMBOL", boost::trim_copy($std2[0]).c_str());
#ifdef WIN_PLATFORM
                                  string listDirCmd = "dir";
#else
                                  string listDirCmd = "ls";
#endif
                                  Terminal::Execute(listDirCmd, {envTestVarName},
                                                    TerminalOptions(
                                                            json({{"env", {{"TEST_VAR_PATH", FileSystem::ResolvePath("../../")}}}})),
                                                    [&](int $exitCode3, const std::vector<string> &$std3) {
                                                        ASSERT_EQ(0, $exitCode3);
                                                        boost::regex ex(".*package\\.conf\\.json.*");
                                                        ASSERT_TRUE(boost::regex_match($std3[0], ex, boost::regex_constants::match_any))
                                                                                    << "Can't resolve previously entered "
                                                                                       "symbol %TEST_VAR_PATH%";
                                                    });
                              });
        });
        testing::internal::GetCapturedStdout();
    }

    TEST_F(TerminalTest, Spawn) {
        Com::Wui::Framework::XCppCommons::Utils::LogIt::setLevel(Com::Wui::Framework::XCppCommons::Enums::LogLevel::ALL);
        bool callbackCalled = false;
#ifdef WIN_PLATFORM
        std::vector<string> args = {"127.0.0.1"};
#else
        std::vector<string> args = {"-c4", "127.0.0.1"};
#endif
        Terminal::Spawn("ping", args, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            ASSERT_EQ(0, $exitCode);
            boost::regex ex(R"(.*P* \d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}.*)");
            ASSERT_TRUE(boost::regex_match($std[0], ex, boost::regex_constants::match_any))
                                        << "Ping output is not valid, spawn failed" << std::endl << $std[0] << std::endl
                                        << "not match regex: " << ex.str() << std::endl;
        });
        ASSERT_TRUE(callbackCalled);

#ifdef WIN_PLATFORM
        string envPathName = "%PATH%";
        json baseOptions = {};
#else
        string envPathName = "$PATH";
        json baseOptions = {{"shell", true}};
#endif
        callbackCalled = false;
        Terminal::Spawn("echo", {envPathName}, TerminalOptions(baseOptions), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            ASSERT_EQ(0, $exitCode);
            ASSERT_STREQ(getenv("PATH"), boost::trim_copy($std[0]).c_str());
        });
        ASSERT_TRUE(callbackCalled);

        callbackCalled = false;
        json options = baseOptions;
        options["env"] = {{"PATH", boost::filesystem::current_path().string()}};
        Terminal::Spawn("echo", {envPathName},
                        TerminalOptions(options),
                        [&](int $exitCode, const std::vector<string> &$std) {
                            callbackCalled = true;
                            ASSERT_EQ(0, $exitCode);
                            ASSERT_STREQ(boost::filesystem::current_path().string().c_str(), boost::trim_copy($std[0]).c_str());
                        });
        ASSERT_TRUE(callbackCalled);


#ifdef WIN_PLATFORM
        string envTestName = "%WUI_TEST_CHILD_ENV%";
#else
        string envTestName = "$WUI_TEST_CHILD_ENV";
#endif

        options = baseOptions;
        options["env"] = {{"WUI_TEST_CHILD_ENV", "SOME_DATA"}};
        Terminal::Spawn("echo", {envTestName}, TerminalOptions(options),
                        [&](int $exitCode, const std::vector<string> &$std) {
                            callbackCalled = true;
                            ASSERT_EQ(0, $exitCode);
                            ASSERT_STREQ("SOME_DATA", boost::trim_copy($std[0]).c_str());
                        });
        ASSERT_TRUE(callbackCalled);
    }

    TEST_F(TerminalTest, Spawn_git) {
        string cmd = "git";
        const std::vector<string> args{"--version"};
        string out;

        bool callbackCalled = false;
        Terminal::Spawn(cmd, args, TerminalOptions(), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            ASSERT_EQ(0, $exitCode);
            out = $std[0];
        });
        ASSERT_TRUE(callbackCalled);
        boost::regex ex(R"(git version \d{1,3}\.\d{1,3}\.\d{1,4}.*)");
        ASSERT_TRUE(boost::regex_match(out, ex, boost::regex_constants::match_any))
                                    << "git output is not valid version." << std::endl;
    }

    TEST_F(TerminalTest, DISABLED_Spawn_detached) {
        Com::Wui::Framework::XCppCommons::Utils::LogIt::setLevel(Com::Wui::Framework::XCppCommons::Enums::LogLevel::ALL);
        bool callbackCalled = false;
#ifdef WIN_PLATFORM
        std::vector<string> args = {"127.0.0.1", "-t"};
#else
        std::vector<string> args = {"-c4", "127.0.0.1"};
#endif
        Terminal::Spawn("ping", args, TerminalOptions(json({{"detached", true}})), [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            ASSERT_EQ(0, $exitCode);
        });
        ASSERT_FALSE(callbackCalled);
    }

    TEST_F(TerminalTest, DISABLED_Spawn_parallel) {
//        shared_ptr<ResponseConnector> connector(new ResponseConnector());
//        connector->setOnSend([](const IResponseData &$data) {
//            if ($data.getData().find("args") == $data.getData().end()) {
//                std::cout << boost::regex_replace($data.getData().value("returnValue", ""), boost::regex(R"(\d|\r|\n)"), "");
//            }
//        });
//        shared_ptr<ResponseManager> manager(new ResponseManager(nullptr, connector));
//        Terminal::setResponse(manager);

        string outA;
        string outB;

        Com::Wui::Framework::XCppCommons::Events::ThreadPool threadPool;
        boost::mutex mtx;
        boost::barrier barrier(2);

        testing::internal::CaptureStdout();
        threadPool.AddThread("parallel_A", [&](...) {
            barrier.wait();
            Terminal::Spawn("stdio test.exe", {"--stdout=10", "--stdout-prefix=A", "--delay=400"}, TerminalOptions(this->baseDir));
        });

        threadPool.AddThread("parallel_B", [&](...) {
            barrier.wait();
            Terminal::Spawn("stdio test.exe", {"--stdout=5", "--stdout-prefix=B", "--delay=1000"}, TerminalOptions(this->baseDir));
        });

        threadPool.Execute(true);

        using Com::Wui::Framework::XCppCommons::Primitives::String;
        string interleaved = testing::internal::GetCapturedStdout();
        ASSERT_TRUE(String::Contains(interleaved, "AB") && String::Contains(interleaved, "BA"))
                                    << "Which is: " << interleaved;
    }

    TEST_F(TerminalTest, Elevate) {
        string cmd = "ping";
#ifdef WIN_PLATFORM
        const std::vector<string> args{"localhost"};
#else
        const std::vector<string> args{"-c4", "localhost"};
#endif
        string cwd = "../../";
        string out;
        bool callbackCalled = false;
        testing::internal::CaptureStdout();

        Terminal::Elevate(cmd, args, cwd, [&](int $exitCode, const std::vector<string> &$std) {
            callbackCalled = true;
            ASSERT_EQ(0, $exitCode) << "Elevation of command failed.";
            ASSERT_GT(boost::lexical_cast<int>($std[0]), 0);
        });

        testing::internal::GetCapturedStdout();
        ASSERT_TRUE(callbackCalled);
    }

    std::vector<int> transformStringsToInts(const std::vector<string> &$input) {
        std::vector<int> output;
        std::transform($input.begin(), $input.end(), std::back_inserter(output), [](const string &$elem) {
            try {
                return boost::lexical_cast<int>($elem);
            } catch (boost::bad_lexical_cast &ex) {
                return 0;
            }
        });
        return output;
    }

    // TODO(B58790) When runs only this or some tests like TerminalTest* etc, all OK, but if run all tests then GTEST is stopped, on LINUX
    TEST_F(TerminalTest, DISABLED_Open) {
        Com::Wui::Framework::XCppCommons::Events::ThreadPool threadPool;
        using Com::Wui::Framework::XCppCommons::Primitives::String;
        testing::internal::CaptureStdout();
        std::vector<int> beforeList = {};
#ifdef WIN_PLATFORM
        string cmd = "notepad.exe";
#else
        string cmd = "top";
#endif

        auto getPids = [&cmd](const string &$name) -> std::vector<int> {
            std::vector<int> pidList = {};
#ifdef WIN_PLATFORM
            auto tasks = TaskManager::Find($name);
            for (auto &item : tasks) {
                pidList.emplace_back(item.getPid());
            }
#else
            string stdOut;
            Terminal::Execute("ps axf | grep " + cmd +
                              " | grep -v grep | awk '{print $1}'", {}, TerminalOptions(),
                              [&](int $exitCode, const std::vector<string> &$std) {
                                  stdOut = $std[0];
                              });
            auto cmdPIDsStr = String::Split(boost::trim_copy(stdOut), {"\n"});
            cmdPIDsStr.erase(std::remove_if(cmdPIDsStr.begin(), cmdPIDsStr.end(), [](const string &$elem) { return $elem.empty(); }),
                             cmdPIDsStr.end());

            pidList = transformStringsToInts(cmdPIDsStr);
#endif
            std::sort(pidList.begin(), pidList.end());
            return pidList;
        };

        auto killPid = [](int $pid) {
#ifdef WIN_PLATFORM
            TaskManager::Terminate(static_cast<unsigned int>($pid));
#else
            Terminal::Execute("kill -2 " + std::to_string($pid), {}, "", [&](int $exitCode, const std::vector<string> &$std) {
            });
#endif
        };

        boost::barrier barrier{2};
        threadPool.AddThread("A", [&](...) {
            beforeList = getPids(cmd);
            barrier.wait();
            bool callbackCalled = false;
            Terminal::Open(cmd, false, [&](int $exitCode) {
                callbackCalled = true;
#ifdef WIN_PLATFORM
                ASSERT_EQ(SIGTERM + 128, $exitCode);
#else
                ASSERT_EQ(0, $exitCode);
#endif
            });
            ASSERT_TRUE(callbackCalled);
        });

        string out;
        threadPool.AddThread("B", [&](...) {
            barrier.wait();
            boost::this_thread::sleep_for(boost::chrono::milliseconds(500));

            std::vector<int> differences, afterList = getPids(cmd);
            std::set_symmetric_difference(
                    beforeList.begin(),
                    beforeList.end(),
                    afterList.begin(),
                    afterList.end(),
                    std::back_inserter(differences));

            killPid(differences.back());
        });

        threadPool.Execute();

        testing::internal::GetCapturedStdout();
    }
}
