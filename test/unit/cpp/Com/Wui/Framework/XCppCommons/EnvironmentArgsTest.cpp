/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons {
    using Com::Wui::Framework::XCppCommons::Primitives::String;

    TEST(EnvironmentArgsTest, Load) {
        EnvironmentArgs::getInstance().Load("");

        ASSERT_STREQ("com-wui-framework-xcppcommons", EnvironmentArgs::getInstance().getProjectName().c_str());
        ASSERT_TRUE(String::PatternMatched("*.*.*", EnvironmentArgs::getInstance().getProjectVersion()));
        ASSERT_STREQ("XCppCommons", EnvironmentArgs::getInstance().getAppName().c_str());
        string matchPattern = R"(\d{4}-\d{2}-\d{2}(\w|\s)\d{2}:\d{2}:\d{2}.\d{3}\w.*)";
        boost::regex expr{matchPattern};
        bool match = boost::regex_match(EnvironmentArgs::getInstance().getBuildTime(), expr);
        ASSERT_TRUE(match) << "Build time not match date time format: " << EnvironmentArgs::getInstance().getBuildTime();
    }
}  // namespace Com::Wui::Framework::XCppCommons
