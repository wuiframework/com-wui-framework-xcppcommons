/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Structures {
    TEST(IpcPipeTest, Construction_server) {
        IpcPipe pipe("testPipe", true);

        ASSERT_STREQ("testPipe", pipe.getPipeName().c_str());
    }

    TEST(IpcPipeTest, Construction_client) {
        IpcPipe pipe("testPipe", false);

        ASSERT_STREQ("testPipe", pipe.getPipeName().c_str());
    }

    TEST(IpcPipeTest, MsgDelimiter) {
        IpcPipe pipe("testPipe", true);

        ASSERT_STREQ("\n", pipe.getMsgDelimiter().c_str());
        pipe.setMsgDelimiter("\r\n");
        ASSERT_STREQ("\r\n", pipe.getMsgDelimiter().c_str());
    }

    TEST(IpcPipeTest, ReadWrite) {
        IpcPipe pipeA("testPipe", true);
        IpcPipe pipeB("testPipe", false);

        ASSERT_TRUE(pipeA.Open() && pipeB.Open());

        pipeA.Write("ain");
        ASSERT_STREQ("ain", pipeB.Read().c_str());
        pipeB.Write("aout");
        ASSERT_STREQ("aout", pipeA.Read().c_str());
    }
}  // namespace Com::Wui::Framework::XCppCommons::Structures
