/* ********************************************************************************************************* *
 *
 * Copyright (c) 2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const EnvironmentHelper = Com.Wui.Framework.Localhost.Utils.EnvironmentHelper;
const terminal = Loader.getInstance().getTerminal();
const filesystem = Loader.getInstance().getFileSystemHandler();
const properties = Loader.getInstance().getAppProperties();
const execSync = require("child_process").spawnSync;
const path = require("path");

Process = function ($cwd, $args, $done) {
    let msysRoot = properties.externalModules + "/msys2";

    $args.forEach(($arg) => {
        if ($arg.startsWith("-libs=")) {
            msysRoot = $arg.replace("-msys-root=", "");
        }
    });

    let buildProducts = [];
    if (EnvironmentHelper.IsWindows()) {
        buildProducts.push($cwd + "/libssl.a");
    } else if (EnvironmentHelper.IsLinux()) {
        buildProducts.push($cwd + "/build-gcc/libssl.a", $cwd + "/build-arm/libssl.a");
    } else if (EnvironmentHelper.IsMac()) {
        buildProducts.push($cwd + "/build-gcc/libssl.a", $cwd + "/build-clang/libssl.a");
    }

    let allBuildProductsExist = true;
    buildProducts.forEach(($buildProduct) => {
        LogIt.Info("Inspecting build product " + $buildProduct);

        if (!filesystem.Exists($buildProduct)) {
            LogIt.Info("Build product does not exist");

            allBuildProductsExist = false;
        }
    });

    if (!allBuildProductsExist) {
        let configureArgsForGCC = ["no-shared", "no-asm", "no-afalgeng", "no-tests", "CFLAGS=-fPIC"];
        const configureArgsForLinuxARM = configureArgsForGCC.concat("--cross-compile-prefix=aarch64-linux-gnu-", "linux-generic64");
        const configureArgsForOSXClang = configureArgsForGCC.concat("darwin64-x86_64-cc");
        if (EnvironmentHelper.IsWindows()) {
            configureArgsForGCC.push(EnvironmentHelper.Is64bit() ? "mingw64" : "mingw")
        } else if (EnvironmentHelper.IsLinux() || EnvironmentHelper.IsMac()) {
            configureArgsForGCC.push("gcc");
        }

        if (EnvironmentHelper.IsWindows()) {
            if (msysRoot === "" || !filesystem.Exists(msysRoot)) {
                LogIt.Info("Searching for MSYS2 installation directory in system paths...");
                const ex = execSync("where", ["msys2_shell"]);
                if (ex.stderr.toString().length > 0) {
                    LogIt.Error("Searching for MSYS2 failed.\r\n" + "\t" + ex.stderr.toString());
                }
                const searchResult = ex.stdout.toString().split("\r\n")[0];
                msysRoot = path.dirname(searchResult);
            }
            if (!filesystem.Exists(msysRoot + "/usr/bin/sh.exe") || !filesystem.Exists(msysRoot + "/usr/bin/perl.exe")) {
                LogIt.Error("Msys2 is not installed properly. (/usr/bin/sh or /usr/bin/perl missing)");
            }
            let args = ["./Configure"].concat(configureArgsForGCC);
            if ($args[0] >= "1.1.0") {
                LogIt.Info("Using openssl version 1.1.0+, will use perl for configuration.");
                args = ["-c", "\"perl ./Configure"].concat(configureArgsForGCC).concat([["\""]]);
            }
            terminal.Spawn(
                "sh.exe", args,
                {cwd: $cwd, env: {PATH: msysRoot + "/usr/bin"}},
                (exitCode) => {
                    if (exitCode !== 0) {
                        LogIt.Error("Cannot configure openssl project for build.");
                    } else {
                        LogIt.Info("Openssl project has been configured for build.");
                        terminal.Spawn(
                            "sh.exe", ["-c", "make", "-j" + EnvironmentHelper.getCores()],
                            {cwd: $cwd, env: {PATH: msysRoot + "/usr/bin"}},
                            (exitCode) => {
                                if (exitCode !== 0) {
                                    LogIt.Error("Build of openssl package failed.");
                                } else {
                                    LogIt.Info("Openssl build succeed.");
                                    $done();
                                }
                            });
                    }
                });
        } else {
            const build = ($installPath, $configureArgs, $makeArgs, $clean, $callback) => {
                terminal.Spawn(
                    "./Configure", $configureArgs,
                    {cwd: $cwd},
                    (exitCode) => {
                        if (exitCode !== 0) {
                            LogIt.Error("Cannot configure openssl project for build.");
                        } else {
                            LogIt.Info("Openssl project has been configured for build.");
                            terminal.Spawn(
                                ($clean ? "make clean && " : "") + "make", $makeArgs.concat(["-j" + EnvironmentHelper.getCores()]),
                                {cwd: $cwd},
                                (exitCode) => {
                                    if (exitCode !== 0) {
                                        LogIt.Error("Build of openssl package failed.");
                                    } else {
                                        ["libssl.a", "libcrypto.a"].forEach(($lib) => {
                                            filesystem.Write($installPath + "/" + $lib, filesystem.Read($cwd + "/" + $lib));
                                            filesystem.Delete($cwd + "/" + $lib);
                                        });
                                        $callback();
                                    }
                                });
                        }
                    });
            };

            build($cwd + "/build-gcc",
                configureArgsForGCC, [], true, () => {
                    if (EnvironmentHelper.IsLinux()) {
                        build($cwd + "/build-arm",
                            configureArgsForLinuxARM, [], true, () => {
                                LogIt.Info("Openssl build succeed.");
                                $done();
                            });
                    } else {
                        build($cwd + "/build-clang",
                            configureArgsForOSXClang, [], true, () => {
                                LogIt.Info("Openssl build succeed.");
                                $done();
                            });
                    }
                });
        }
    } else {
        LogIt.Info("Openssl products already exist, install script skipped.");
        $done();
    }
};
