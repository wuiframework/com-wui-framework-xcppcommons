/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
const Loader = Com.Wui.Framework.Builder.Loader;
const LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
const filesystem = Loader.getInstance().getFileSystemHandler();

Process = function ($cwd, $args, $done) {
    const files = filesystem.Expand(Loader.getInstance().getAppProperties().projectBase + "/**/resource/libs/promise/promiseAdv.hpp");
    if (files.length > 0) {
        filesystem.Copy(files[0], $cwd + "/promiseAdv.hpp", ($status) => {
            if ($status) {
                LogIt.Info("Installation succeed.");
                $done();
            } else {
                LogIt.Error("Installation of promise-cpp failed. Overridden promiseAdv.hpp can not be copied to dependency.");
            }
        });
    } else {
        LogIt.Info("Can not find promiseAdv.hpp in project ./**/resource/libs/promise/ directory. Install script skipped.");
        $done();
    }
};
