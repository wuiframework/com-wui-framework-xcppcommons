# * ********************************************************************************************************* *
# *
# * Copyright (c) 2016 Freescale Semiconductor, Inc.
# * Copyright (c) 2017 NXP
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

macro(OPENSSL_CONFIGURE app_target test_target test_lib_target path version attributes)
    message(STATUS "Running OPENSSL_CONFIGURE with (\n\tapp_target: ${app_target},\n\ttest_target: ${test_target},\n\tattributes: ${attributes}\n).")

    PARSE_ARGS("${attributes}")

    set(OPENSSL_INCLUDE_DIRS)
    set(OPENSSL_LIBRARIES)

    set(_BASE_PATH ${path})
    if (NOT IS_ABSOLUTE ${_BASE_PATH})
        set(_BASE_PATH ${CMAKE_SOURCE_DIR}/${_BASE_PATH})
    endif ()
    if (UNIX)
        if (TOOLCHAIN_TYPE STREQUAL "arm")
            set(_LIBS_PATH ${_BASE_PATH}/build-arm)
        elseif (TOOLCHAIN_TYPE STREQUAL "clang")
            set(_LIBS_PATH ${_BASE_PATH}/build-clang)
        else ()
            set(_LIBS_PATH ${_BASE_PATH}/build-gcc)
        endif ()
    else ()
        set(_LIBS_PATH ${_BASE_PATH})
    endif ()

    set(OPENSSL_INCLUDE_DIRS ${_BASE_PATH}/include)
    set(OPENSSL_LIBRARIES ${_LIBS_PATH}/libssl.a ${_LIBS_PATH}/libcrypto.a)

    if (WIN32)
        set(OPENSSL_LIBRARIES ${OPENSSL_LIBRARIES} crypt32)
    endif ()

    target_include_directories(${app_target} PUBLIC ${OPENSSL_INCLUDE_DIRS})
    target_include_directories(${test_target} PUBLIC ${OPENSSL_INCLUDE_DIRS})
    target_include_directories(${test_lib_target} PUBLIC ${OPENSSL_INCLUDE_DIRS})

    target_link_libraries(${app_target} ${OPENSSL_LIBRARIES})
    target_link_libraries(${test_target} ${OPENSSL_LIBRARIES})
    target_link_libraries(${test_lib_target} ${OPENSSL_LIBRARIES})

endmacro()
