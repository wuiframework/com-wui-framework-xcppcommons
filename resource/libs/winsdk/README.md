# Windows SDK for MinGW

> Main purpose of this library is ability to include missing headers from Windows SDK into the MinGW

## License

Header files included in this library do not have any special copyrights/license or are covered by Microsoft copyrigts.

---

Author Jakub Cieslar - B48779, Copyright (c) 2014-2016 [Freescale Semiconductor, Inc.](http://freescale.com/), Copyright (c) 2017 [NXP](http://nxp.com/)
