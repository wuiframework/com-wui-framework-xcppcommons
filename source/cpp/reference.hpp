/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_HPP_

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/any.hpp>
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/program_options/variables_map.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/unordered_map.hpp>
#include <boost/assign.hpp>
#include <boost/regex.hpp>
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/thread.hpp>
#include <boost/thread/barrier.hpp>
#include <boost/bind.hpp>
#include <boost/foreach.hpp>
#include <boost/iterator/iterator_adaptor.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/format.hpp>
#include <boost/uuid/sha1.hpp>
#include <boost/locale/encoding_utf.hpp>
#include <boost/process.hpp>

#include <openssl/sha.h>
#include <openssl/evp.h>
#include <openssl/md5.h>

#include <json.hpp>
#include <promiseAdv.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <cstdlib>
#include <unordered_map>
#include <functional>

// global-using-start
using std::string;
using std::function;
using std::shared_ptr;
using std::unique_ptr;
using std::weak_ptr;
using boost::any;
using nlohmann::json;
// global-using-stop

#include "interfacesMap.hpp"
#include "Com/Wui/Framework/XCppCommons/sourceFilesMap.hpp"

#include "Com/Wui/Framework/XCppCommons/Interfaces/IBaseObject.hpp"
#include "Com/Wui/Framework/XCppCommons/Interfaces/INonCopyable.hpp"
#include "Com/Wui/Framework/XCppCommons/Interfaces/INonMovable.hpp"
#include "Com/Wui/Framework/XCppCommons/Primitives/BaseObject.hpp"
#include "Com/Wui/Framework/XCppCommons/System/IO/FileSystem.hpp"
#include "Com/Wui/Framework/XCppCommons/Primitives/BaseEnum.hpp"
#include "Com/Wui/Framework/XCppCommons/Enums/HttpMethodType.hpp"
#include "Com/Wui/Framework/XCppCommons/Primitives/String.hpp"
#include "Com/Wui/Framework/XCppCommons/Primitives/ArrayList.hpp"
#include "Com/Wui/Framework/XCppCommons/Structures/ProgramArgs.hpp"
#include "Com/Wui/Framework/XCppCommons/Enums/IOHandlerType.hpp"
#include "Com/Wui/Framework/XCppCommons/Enums/LogLevel.hpp"
#include "Com/Wui/Framework/XCppCommons/Interfaces/IOHandler.hpp"
#include "Com/Wui/Framework/XCppCommons/IOApi/Handlers/BaseOutputHandler.hpp"
#include "Com/Wui/Framework/XCppCommons/IOApi/Handlers/ConsoleHandler.hpp"
#include "Com/Wui/Framework/XCppCommons/IOApi/Handlers/OutputFileHandler.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/LogIt.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Environment.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/JSON.hpp"
#include "Com/Wui/Framework/XCppCommons/EnvironmentArgs.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/ArgsParser.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/ObjectDecoder.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/ObjectEncoder.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Net/Http/ClientRequest.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Net/Http/ClientResponse.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/ProcessOptions.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/ExecuteOptions.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/SpawnOptions.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Net/Url.hpp"

// generated-code-start
#include "Com/Wui/Framework/XCppCommons/Application.hpp"
#include "Com/Wui/Framework/XCppCommons/Enums/HttpClientType.hpp"
#include "Com/Wui/Framework/XCppCommons/Enums/HttpResponseType.hpp"
#include "Com/Wui/Framework/XCppCommons/Events/Args/EventArgs.hpp"
#include "Com/Wui/Framework/XCppCommons/Events/IntervalTimer.hpp"
#include "Com/Wui/Framework/XCppCommons/Events/ThreadPool.hpp"
#include "Com/Wui/Framework/XCppCommons/Interfaces/IConnector.hpp"
#include "Com/Wui/Framework/XCppCommons/Interfaces/IResponse.hpp"
#include "Com/Wui/Framework/XCppCommons/IOApi/IOHandlerFactory.hpp"
#include "Com/Wui/Framework/XCppCommons/Loader.hpp"
#include "Com/Wui/Framework/XCppCommons/Structures/IpcPipe.hpp"
#include "Com/Wui/Framework/XCppCommons/System/IO/FileGlob.hpp"
#include "Com/Wui/Framework/XCppCommons/System/IO/Path.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Net/Http/Client.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/Child.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/Detail/InitializerPosix.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/Detail/InitializerWindows.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/TaskInfo.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/TaskManager.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/Terminal.hpp"
#include "Com/Wui/Framework/XCppCommons/System/Process/TerminalOptions.hpp"
#include "Com/Wui/Framework/XCppCommons/System/ResponseApi/Handlers/BaseResponse.hpp"
#include "Com/Wui/Framework/XCppCommons/System/ResponseApi/Handlers/CallbackResponse.hpp"
#include "Com/Wui/Framework/XCppCommons/System/ResponseApi/ResponseFactory.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/Convert.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/IpcPipeObserver.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/ObjectValidator.hpp"
#include "Com/Wui/Framework/XCppCommons/Utils/Reflection.hpp"
// generated-code-end


#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_HPP_  NOLINT
