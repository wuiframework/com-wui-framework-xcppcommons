/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

/**
 * WARNING: this file is excluded from project compiler source files!
 * This file can contains string replace features provided by WUI builder and current project configuration.
 * See pre-processed file after WUI Builder - Build task finished
 * in <project-root>/build/compiled/source/cpp/ApplicationMain.cpp
 */

#include "reference.hpp"

// The main entry point for the application.
int main(int $argc, char **$argv) {
    return Com::Wui::Framework::XCppCommons::Utils::Reflection::Invoke(
            "<? @var project.loaderClass ?>.Load", $argc,
            (const char **) $argv);
}
