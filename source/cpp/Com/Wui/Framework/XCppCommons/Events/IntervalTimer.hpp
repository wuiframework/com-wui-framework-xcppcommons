/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_INTERVALTIMER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_INTERVALTIMER_HPP_

namespace Com::Wui::Framework::XCppCommons::Events {
    /**
     * IntervalTimer implements asynchronous interval timer.
     */
    class IntervalTimer : public Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Constructs new timer from specified interval and tick callback.
         * @param $interval Specify timer tick interval.
         * @param $tickCallback Specify tick callback function to be called after timer tick.
         * @param $ioService Specify boost::io_service for timer async routine or use internal by default.
         */
        IntervalTimer(const boost::posix_time::time_duration &$interval,
                      const function<void()> &$tickCallback,
                      const boost::asio::io_service * $ioService = nullptr);

        /**
         * @return Returns timer interval.
         */
        const boost::posix_time::time_duration &getInterval() const;

        /**
         * Sets new interval for timer.
         * @param $interval Specify time interval.
         */
        void setInterval(const boost::posix_time::time_duration &$interval);

        /**
         * @return Returns timer tick callback reference.
         */
        const function<void()> &getTickCallback() const;

        /**
         * Sets new timer tick callback function.
         * @param $tickCallback Specify callback function.
         */
        void setTickCallback(const function<void()> &$tickCallback);

        /**
         * Starts interval timer.
         */
        void Start();

        /**
         * Stops interval timer.
         */
        void Stop();

     private:
        boost::asio::io_service *ioService;
        boost::posix_time::time_duration interval;
        function<void()> tickCallback;
        unique_ptr<boost::asio::deadline_timer> timer;

        void timerTick(const boost::system::error_code &);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_INTERVALTIMER_HPP_
