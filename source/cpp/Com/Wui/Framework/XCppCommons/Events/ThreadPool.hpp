/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_THREADPOOL_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_THREADPOOL_HPP_

namespace Com::Wui::Framework::XCppCommons::Events {
    /**
     * ThreadPool class provides asynchronous handling of custom events group.
     */
    class ThreadPool : public Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
        typedef Com::Wui::Framework::XCppCommons::Events::Args::EventArgs EventArgs;

     public:
        class ThreadInfo {
         public:
            /**
             * Constructs thread info object which contains thread information
             * necessary to start them in ThreadPool::Execute.
             * When thread is started this instance also holds reference to thread object.
             * @param $handler Reference to function which is invoked in separate (daemon) thread.
             * @param $args Reference to thread argument options.
             */
            explicit ThreadInfo(function<void(const EventArgs &)> &$handler,
                                const EventArgs &$args = EventArgs());

            /**
             * Thread arguments setter.
             * @param $args An argument reference to set.
             */
            void setArgs(const EventArgs &$args);

            /**
             * Checks for thread state.
             * @return Returns true if thread is alive, false otherwise.
             */
            bool IsRunning() const;

            /**
             * Invoke handler function initialized by constructor.
             * NO thread will be created by calling this method.
             */
            void Run();

            /**
             * Getter for thread pointer.
             * @return Returns pointer to thread.
             */
            boost::thread* getThread() const;

            /**
             * Setter for thread pointer.
             * @param $thread
             */
            void setThread(boost::thread* $thread);

         private:
            function<void(const EventArgs &)> handler;
            EventArgs args;
            boost::thread* thread;
            bool isRunning = false;
        };

        /**
         * Checks state of the specified taskName.
         * @param $threadName A thread identifier.
         * @return Returns true if thread is alive, false otherwise
         */
        bool IsRunning(const string &$threadName);

        /**
         * Adds thread to thread pool.
         * @param $threadName A thread identifier.
         * @param $handler Reference to function which is invoked in separate (daemon) thread.
         * @param $args Reference to thread argument options.
         */
        void AddThread(const string &$threadName, function<void(const EventArgs &)> $handler,
                       const EventArgs &$args = EventArgs());

        /**
         * @param $threadName A thread identifier.
         * @param $args Reference to thread argument options.
         */
        void setThreadArgs(const string &$threadName, const EventArgs &$args);

        /**
         * Remove thread from thread pool and interrupt them if thread is running.
         * @param $threadName A thread identifier.
         */
        void RemoveThread(const string &$threadName);

        /**
         * Starts all threads configured in thread pool by AddThread method.
         * @param $isBlocking This method returns when all threads finished
         * by default (waiting for join()) or returns immediately if isBlocking is false.
         */
        void Execute(bool $isBlocking = true);

     private:
        boost::thread_group threadGroup = {};
        std::unordered_map<string, shared_ptr<ThreadInfo>> threadMap;
        bool isServiceRunning = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_THREADPOOL_HPP_
