/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Events::ThreadPool;
using Com::Wui::Framework::XCppCommons::Events::Args::EventArgs;

ThreadPool::ThreadInfo::ThreadInfo(function<void(const EventArgs &)> &$handler, const EventArgs &$args)
        : handler($handler),
          args($args) {
}

void ThreadPool::ThreadInfo::setArgs(const EventArgs &$args) {
    this->args = $args;
}

boost::thread *ThreadPool::ThreadInfo::getThread() const {
    return this->thread;
}

void ThreadPool::ThreadInfo::setThread(boost::thread *$thread) {
    this->thread = $thread;
}

bool ThreadPool::ThreadInfo::IsRunning() const {
    return this->isRunning;
}

void ThreadPool::ThreadInfo::Run() {
    this->isRunning = true;
    this->handler(this->args);
    this->isRunning = false;
}

bool ThreadPool::IsRunning(const string &$threadName) {
    auto val = this->threadMap.find($threadName);
    return val != this->threadMap.end() && val->second->IsRunning();
}

void ThreadPool::AddThread(const string &$threadName, function<void(const EventArgs &)> $handler,
                           const EventArgs &$args) {
    if (!this->isServiceRunning) {
        this->threadMap.emplace($threadName, std::make_shared<ThreadInfo>($handler, $args));
    }
}

void ThreadPool::setThreadArgs(const string &$threadName, const EventArgs &$args) {
    if (!this->isServiceRunning) {
        this->threadMap.at($threadName)->setArgs($args);
    }
}

void ThreadPool::RemoveThread(const string &$threadName) {
    auto it = this->threadMap.find($threadName);
    if (it != this->threadMap.end()) {
        if (this->isServiceRunning) {
            auto tPtr = it->second->getThread();
            if (tPtr != nullptr) {
                tPtr->interrupt();
            }
        }
        this->threadMap.erase($threadName);
    }
}

void ThreadPool::Execute(bool $isBlocking) {
    this->isServiceRunning = true;

    for (auto &it : this->threadMap) {
        auto t = new boost::thread(boost::bind(&ThreadInfo::Run, it.second));
        it.second->setThread(t);
        threadGroup.add_thread(t);
    }

    threadGroup.join_all();

    this->isServiceRunning = false;
}
