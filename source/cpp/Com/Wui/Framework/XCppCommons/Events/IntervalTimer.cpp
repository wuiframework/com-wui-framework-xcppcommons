/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Events {

    IntervalTimer::IntervalTimer(const boost::posix_time::time_duration &$interval,
                                 const function<void()> &$tickCallback,
                                 const boost::asio::io_service *$ioService)
            : interval($interval),
              tickCallback($tickCallback) {
        this->ioService = const_cast<boost::asio::io_service *>($ioService);
    }

    const boost::posix_time::time_duration &IntervalTimer::getInterval() const {
        return interval;
    }

    void IntervalTimer::setInterval(const boost::posix_time::time_duration &$interval) {
        this->interval = $interval;
    }

    const function<void()> &IntervalTimer::getTickCallback() const {
        return this->tickCallback;
    }

    void IntervalTimer::setTickCallback(const function<void()> &$tickCallback) {
        this->tickCallback = $tickCallback;
    }

    void IntervalTimer::Start() {
        if ((this->ioService != nullptr) && this->tickCallback) {
            this->timer = std::make_unique<boost::asio::deadline_timer>(*this->ioService, this->interval);
            this->timer->async_wait(boost::bind(&IntervalTimer::timerTick, this, boost::asio::placeholders::error));
        }
    }

    void IntervalTimer::Stop() {
        this->tickCallback = {};
        this->timer->cancel();
    }

    void IntervalTimer::timerTick(const boost::system::error_code &$ec) {
        if ($ec == boost::system::errc::success) {
            this->timer->expires_at(this->timer->expires_at() + this->interval);
            this->timer->async_wait(boost::bind(&IntervalTimer::timerTick, this, boost::asio::placeholders::error));

            if (this->tickCallback) {
                this->tickCallback();
            }
        }
    }
}
