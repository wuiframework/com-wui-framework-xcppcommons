/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Events::Args::EventArgs;

EventArgs::EventArgs() {
    this->owner = static_cast<void *>(this);
}

EventArgs::EventArgs(void *$owner) {
    this->owner = $owner;
}

void *EventArgs::getOwner() const {
    return this->owner;
}
