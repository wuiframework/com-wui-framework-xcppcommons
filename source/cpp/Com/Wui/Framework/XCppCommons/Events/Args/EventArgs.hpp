/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_ARGS_EVENTARGS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_ARGS_EVENTARGS_HPP_

namespace Com::Wui::Framework::XCppCommons::Events::Args {
    /**
     * EventArgs class provides basic event structure.
     */
    class EventArgs : public Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Construct args with itself owner.
         */
        EventArgs();

        /**
         * Construct args with specified owner.
         * @param $owner A pointer to owner.
         */
        explicit EventArgs(void *$owner);

        /**
         * Specified object, which own current args.
         * @return Returns current event args owner object.
         */
        void *getOwner() const;

     private:
        void *owner;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_EVENTS_ARGS_EVENTARGS_HPP_
