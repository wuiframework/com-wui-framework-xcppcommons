/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_ENUMS_HTTPRESPONSETYPE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_ENUMS_HTTPRESPONSETYPE_HPP_

namespace Com::Wui::Framework::XCppCommons::Enums {
    enum HttpResponseType : int {
        CONTINUE = 100,
        SWITCHNIG = 101,
        OK = 200,
        MOVED = 301,
        FOUND = 302,
        NOT_MODIFIED = 304,
        BAD_REQUEST = 400,
        UNAUTHORIZED = 401,
        FORBIDDEN = 403,
        NOT_FOUND = 404,
        INTERNAL_SERVER_ERROR = 500
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_ENUMS_HTTPRESPONSETYPE_HPP_
