/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Primitives::BaseEnum;
using Com::Wui::Framework::XCppCommons::Enums::HttpMethodType;

namespace Com::Wui::Framework::XCppCommons::Enums {
    WUI_ENUM_IMPLEMENT(HttpMethodType);

    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, GET);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, HEAD);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, POST);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, TRACE);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, WSS);
    WUI_ENUM_CONST_IMPLEMENT(HttpMethodType, DATA);
}
