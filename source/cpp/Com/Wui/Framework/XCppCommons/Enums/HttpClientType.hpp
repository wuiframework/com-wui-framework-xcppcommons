/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_ENUMS_HTTPCLIENTTYPE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_ENUMS_HTTPCLIENTTYPE_HPP_

namespace Com::Wui::Framework::XCppCommons::Enums {
    enum HttpClientType : int {
        HTTP,
        HTTPS,
        INVALID
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_ENUMS_HTTPCLIENTTYPE_HPP_
