/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Primitives::BaseEnum;
using Com::Wui::Framework::XCppCommons::Enums::LogLevel;

WUI_ENUM_IMPLEMENT(LogLevel);

WUI_ENUM_CONST_IMPLEMENT(LogLevel, ALL);
WUI_ENUM_CONST_IMPLEMENT(LogLevel, INFO);
WUI_ENUM_CONST_IMPLEMENT(LogLevel, WARNING);
WUI_ENUM_CONST_IMPLEMENT(LogLevel, ERROR);
WUI_ENUM_CONST_IMPLEMENT(LogLevel, DEBUG);
