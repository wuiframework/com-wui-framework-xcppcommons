/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers {

    CallbackResponse::CallbackResponse(const CallbackResponse &$other)
            : callback($other.callback) {}

    void CallbackResponse::Send(int $exitCode) const {
        this->callback.resolve($exitCode, string(""), string(""));
    }

    void CallbackResponse::Send(bool $status, const string &$message) const {
        this->callback.resolve($status, $message);
    }

    void CallbackResponse::Send(const string &$data) const {
        this->callback.resolve($data);
    }

    void CallbackResponse::Send(const json &$object) const {
        this->callback.resolve($object);
    }

    void CallbackResponse::OnComplete(bool $status) const {
        this->callback.resolve($status);
    }

    void CallbackResponse::OnComplete(const string &$data) const {
        this->callback.resolve($data);
    }

    void CallbackResponse::OnComplete(int $exitCode, const std::vector<string> &$std) const {
        this->callback.resolve($exitCode, $std);
    }

    void CallbackResponse::OnComplete(const json &$object, const string &$data) const {
        this->callback.resolve($data);
    }
}
