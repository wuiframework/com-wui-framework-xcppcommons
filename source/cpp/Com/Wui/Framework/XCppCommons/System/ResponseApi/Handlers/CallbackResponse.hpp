/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_HANDLERS_CALLBACKRESPONSE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_HANDLERS_CALLBACKRESPONSE_HPP_

namespace Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers {
    /**
     * CallbackResponse class stores callback method and invoke it by Send and OnComplete methods.
     */
    class CallbackResponse : public BaseResponse {
     public:
        /**
         * CallbackResponse can be constructed only from callback method.
         * @tparam T Callback type.
         * @param $callback Callback function.
         */
        template<typename T>
        explicit CallbackResponse(const T &$callback) {
            promise::Defer next;
            this->callback = promise::newPromise([](promise::Defer d) {
                d.resolve();
            }).then([&next]() {
                next = promise::newPromise([](promise::Defer d) {
                });
                return next;
            });

            this->callback.then($callback);
        }

        /**
         * Copy constructor.
         * @param $other Specify instance to be copied from.
         */
        CallbackResponse(const CallbackResponse &$other);

        void Send(int $exitCode) const override;

        void Send(bool $status, const string &$message = "") const override;

        void Send(const string &$data) const override;

        void Send(const json &$object) const override;

        void OnComplete(bool $status) const override;

        void OnComplete(const string &$data) const override;

        void OnComplete(int $exitCode, const std::vector<string> &$std) const override;

        void OnComplete(const json &$object, const string &$data = "") const override;

     private:
        promise::Defer callback;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_HANDLERS_CALLBACKRESPONSE_HPP_
