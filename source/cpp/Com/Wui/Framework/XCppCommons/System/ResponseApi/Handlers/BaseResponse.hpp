/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_HANDLERS_BASERESPONSE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_HANDLERS_BASERESPONSE_HPP_

namespace Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers {
    /**
     * BaseResponse class implements IResponse interface and all responses should be derived from this class.
     * Note: that for ResponseFactory the COPY constructor must be defined for each derived classes. And also
     * BaseResponse copy constructor must be called.
     */
    class BaseResponse : public Com::Wui::Framework::XCppCommons::Interfaces::IResponse {
     public:
        /**
         * Default constructor.
         */
        BaseResponse() = default;

        /**
         * Copy constructor.
         * @param $other Specify instance to be copied from.
         */
        BaseResponse(const BaseResponse &$other);

        void Send(int $exitCode) const override;

        void Send(bool $status, const string &$message = "") const override;

        void Send(const string &$data) const override;

        void Send(const json &$object) const override;

        void OnStart() const override;

        void OnStart(const string &$data) const override;

        void OnChange(const string &$data) const override;

        void OnChange(const json &$object) const override;

        void OnComplete(bool $status) const override;

        void OnComplete(const string &$data) const override;

        void OnComplete(int $exitCode, const std::vector<string> &$std) const override;

        void OnComplete(const json &$object, const string &$data = "") const override;

        void OnError(const string &$error) const override;

        void OnError(const std::exception &$ex) const override;

        void OnMessage() const override;

        void FireEvent(const string &$name) const override;

        int getId() const override;

        string getOwnerId() const override;

        void AddAbortHandler(const function<void()> &$handler) override;

        void Abort() override;

     protected:
        shared_ptr<Com::Wui::Framework::XCppCommons::Interfaces::IConnector> owner = nullptr;

     private:
        std::vector<function<void()>> threadsRegister = {};
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_HANDLERS_BASERESPONSE_HPP_
