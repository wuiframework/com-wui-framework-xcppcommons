/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::ResponseApi::Handlers {

    BaseResponse::BaseResponse(const BaseResponse &$other)
            : threadsRegister($other.threadsRegister),
              owner($other.owner) {}

    void BaseResponse::Send(int $exitCode) const {
    }

    void BaseResponse::Send(bool $status, const string &$message) const {
    }

    void BaseResponse::Send(const string &$data) const {
    }

    void BaseResponse::Send(const json &$object) const {
    }

    void BaseResponse::OnStart() const {
    }

    void BaseResponse::OnStart(const string &$data) const {
    }

    void BaseResponse::OnChange(const string &$data) const {
    }

    void BaseResponse::OnChange(const json &$object) const {
    }

    void BaseResponse::OnComplete(bool $status) const {
    }

    void BaseResponse::OnComplete(const string &$data) const {
    }

    void BaseResponse::OnComplete(int $exitCode, const std::vector<string> &$std) const {
    }

    void BaseResponse::OnComplete(const json &$object, const string &$data) const {
    }

    void BaseResponse::OnError(const string &$error) const {
    }

    void BaseResponse::OnError(const std::exception &$ex) const {
    }

    void BaseResponse::OnMessage() const {
        throw std::runtime_error("BaseResponse::OnMessage not implemented");
    }

    void BaseResponse::FireEvent(const string &$name) const {
        throw std::runtime_error("BaseResponse::FireEvent not implemented");
    }

    int BaseResponse::getId() const {
        return -1;
    }

    string BaseResponse::getOwnerId() const {
        return "";
    }

    void BaseResponse::AddAbortHandler(const function<void()> &$handler) {
        this->threadsRegister.emplace_back($handler);
    }

    void BaseResponse::Abort() {
        std::for_each(this->threadsRegister.begin(), this->threadsRegister.end(), [](auto item) {
            item();
        });
    }
}
