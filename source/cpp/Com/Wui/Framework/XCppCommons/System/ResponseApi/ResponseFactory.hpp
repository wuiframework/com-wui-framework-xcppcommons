/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_RESPONSEFACTORY_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_RESPONSEFACTORY_HPP_

// TODO(B58790) Replace experimental::negation with negation when migration C++14 => C++17 will be finished
#include <experimental/type_traits>

namespace Com::Wui::Framework::XCppCommons::System::ResponseApi {
    /**
     * ResponseFactory class contains static method to create response from callback or general response object.
     */
    class ResponseFactory {
        typedef Com::Wui::Framework::XCppCommons::Interfaces::IResponse IResponse;
        template<class T>
        struct is_shared_ptr : std::false_type {
        };

        template<class T>
        struct is_shared_ptr<std::shared_ptr<T>> : std::true_type {
        };

     public:
        template<typename T>
        static typename std::enable_if<std::experimental::negation<std::is_convertible<T *, IResponse *>>::value &&
                                       std::experimental::negation<is_shared_ptr<T>>::value, shared_ptr<IResponse>>::type
        getResponse(const T &$callback) {
            auto temp = std::make_shared<Handlers::CallbackResponse>($callback);
            return temp;
        }

        template<typename T>
        static typename std::enable_if<std::experimental::negation<std::is_convertible<T *, IResponse *>>::value &&
                                       is_shared_ptr<T>::value, shared_ptr<IResponse>>::type
        getResponse(const T &$response) {
            auto tmp = std::dynamic_pointer_cast<IResponse>($response);
            if (tmp == nullptr) {
                tmp = std::make_shared<Handlers::BaseResponse>();
            }
            return tmp;
        }

        template<typename T>
        static typename std::enable_if<std::is_convertible<T *, IResponse *>::value, shared_ptr<IResponse>>::type getResponse(
                const T &$response) {
            auto temp = std::make_shared<T>($response);
            return temp;
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_RESPONSEAPI_RESPONSEFACTORY_HPP_
