/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <list>

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::IO {

    FileGlob::FileGlob() {
        this->AddIgnorePattern("./");
        this->AddIgnorePattern("../");
    }

    void FileGlob::MatchPattern(const string &$pattern) {
        std::string pattern;
        unsigned lastSlashIndex = $pattern.length() - 1;
        unsigned numPeriods = 0;
        unsigned i = 0;
        if ($pattern[0] == '\\' && $pattern[1] == '\\') {
            i = 2;
        }

        while (i < $pattern.length()) {
            char ch = $pattern[i];

            if (ch == '\\' || ch == '/') {
                pattern += '/';

                lastSlashIndex = i;
                numPeriods = 0;
            } else if (ch == '.') {
                if (i - numPeriods - 1 == lastSlashIndex) {
                    numPeriods++;
                    if (numPeriods > 2) {
                        pattern += "/..";
                    } else {
                        pattern += '.';
                    }
                } else {
                    pattern += '.';
                }
            } else if (ch == '*' && (i + 1 < $pattern.length()) && $pattern[i + 1] == '*') {
                if (i - 1 != lastSlashIndex) {
                    // /some**/ => /some*/**/
                    pattern += "*/";
                }

                i += 2;

                pattern += "**";

                if ($pattern[i] != '/' && $pattern[i] != '\\') {
                    // /**some => /**/*some
                    pattern += "/*";
                } else if ((i + 1) < $pattern.length() && ($pattern[i + 1] == '\0' || $pattern[i + 1] == '@')) {
                    i++;

                    pattern += "/*/";
                }

                i--;
            } else if (ch == '@') {
                break;
            } else {
                pattern += $pattern[i];
            }

            i++;
        }

        this->Glob(pattern);
    }

    void FileGlob::AddIgnorePattern(const string &$pattern) {
        if (std::find(this->ignorePatterns.begin(), this->ignorePatterns.end(), $pattern) == this->ignorePatterns.end()) {
            this->ignorePatterns.emplace_back($pattern);
        }
    }

    bool FileGlob::WildMatch(const string &$pattern, const string &$item) {
        const char *patternC = $pattern.c_str();
        const char *itemC = $item.c_str();

        while (*itemC != 0 && *patternC != '*') {
            if (*patternC != '?') {
                if (toupper(*patternC) != toupper(*itemC)) {
                    return false;
                }
            }

            patternC++;
            itemC++;
        }

        const char *mp = nullptr;
        const char *cp = nullptr;
        while (*itemC != 0) {
            if (*patternC == '*') {
                if (*++patternC == 0) {
                    return true;
                }

                mp = patternC;
                cp = itemC + 1;
            } else {
                if (toupper(*patternC) == toupper(*itemC) || *patternC == '?') {
                    patternC++;
                    itemC++;
                } else {
                    patternC = mp;
                    itemC = cp++;
                }
            }
        }

        while (*patternC == '*') {
            patternC++;
        }

        return *patternC == 0;
    }

    void FileGlob::setMatchHandler(const std::function<void(const string &$path)> &$handler) {
        this->matchHandler = $handler;
    }

    bool FileGlob::MatchIgnorePattern(const string &$data) {
        for (auto &item : this->ignorePatterns) {
            if (this->WildMatch(item, $data)) {
                return true;
            }
        }

        return false;
    }

    void FileGlob::Glob(const string &$pattern) {
#ifdef WIN_PLATFORM
        std::string internPattern = $pattern;
        for (;;) {
            bool hasWildcard = false;
            bool matchFiles = false;
            unsigned baseLen = 0;
            unsigned recurseIndex = 0;
            unsigned i = 0;
            char patternBuf[_MAX_PATH * 2];  // NOLINT(runtime/arrays)
            snprintf(patternBuf, _MAX_PATH * 2, "%s", internPattern.c_str());
            for (char *pattern = patternBuf; *pattern != '\0'; ++pattern) {
                char ch = *pattern;
                if (ch == '?') {
                    hasWildcard = true;
                } else if (ch == '*') {
                    hasWildcard = true;

                    if (pattern[1] == '*') {
                        if (pattern == patternBuf || pattern[-1] == '/' || pattern[-1] == ':') {
                            char ch2 = pattern[2];
                            if (ch2 == '/') {
                                recurseIndex = i;
                                memcpy(pattern, pattern + 3, strlen(pattern) - 2);
                            } else if (ch2 == '\0') {
                                recurseIndex = i;
                                *pattern = '\0';
                            }
                        }
                    } else if (pattern[1] == '\0') {
                        matchFiles = true;
                    }
                }

                if (ch == '/' || ch == ':') {
                    if (hasWildcard) {
                        break;
                    }
                    baseLen = i + 1;
                }
                i++;
            }

            internPattern = std::string(patternBuf);
            if (!hasWildcard) {
                if (this->matchHandler != nullptr) {
                    this->matchHandler(internPattern);
                }
                return;
            }

            std::string basePath = internPattern.substr(0, baseLen);

            std::string matchPattern = internPattern.substr(baseLen, i - baseLen + 1);
            if (matchPattern.at(matchPattern.length() - 1) == '/') {
                matchPattern = matchPattern.substr(0, matchPattern.length() - 1);
            }

            std::list<std::string> fileList;
            WIN32_FIND_DATAA fd{};
            HANDLE handle = FindFirstFileA((basePath + "*").c_str(), &fd);
            if (handle != INVALID_HANDLE_VALUE) {
                for (;;) {
                    if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0u) {
                        if (this->WildMatch(matchPattern, fd.cFileName)) {
                            bool ignore;
                            int len = strlen(fd.cFileName);
                            fd.cFileName[len] = '/';
                            fd.cFileName[len + 1] = '\0';
                            ignore = MatchIgnorePattern(fd.cFileName);

                            fd.cFileName[len] = 0;
                            if (!ignore) {
                                fileList.emplace_back(fd.cFileName);
                            }
                        }
                    } else if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0u) {
                        if (this->WildMatch(matchPattern, fd.cFileName)) {
                            bool ignore = MatchIgnorePattern(fd.cFileName);

                            if (!ignore) {
                                fileList.emplace_back(fd.cFileName);
                            }
                        }
                    }

                    if (!FindNextFileA(handle, &fd)) {
                        break;
                    }
                }

                FindClose(handle);
            }

            fileList.sort();

            if (matchFiles) {
                for (auto &it : fileList) {
                    if (this->matchHandler != nullptr) {
                        this->matchHandler(basePath + it);
                    }
                }
            } else {
                for (auto &it : fileList) {
                    this->Glob(basePath + it + (patternBuf + i));
                }
            }

            fileList.clear();

            if (recurseIndex == 0) {
                return;
            }

            std::string mp = internPattern.substr(recurseIndex, std::string::npos);
            internPattern = internPattern.substr(0, recurseIndex).append("*/**/").append(mp);
        }
#else
#warning "FileGlob::Glob is not implemented on this platform"
#endif
    }
}
