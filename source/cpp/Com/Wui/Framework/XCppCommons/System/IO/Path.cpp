/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::IO {
    using Com::Wui::Framework::XCppCommons::Primitives::String;

#ifdef WIN_PLATFORM
    string pathSymbolRegExpr = R"((%([\w\d\s_]*)%)(.*))";
#else
    string pathSymbolRegExpr = R"((\$([\w\d\s_]*))(.*))";
#endif

    string Path::Expand(const string &$path, const std::map<string, string> &$env) {
        string path = $path;

        path = Path::processPath(path, [&](string const &$data, string const &$fullKey, string const &$key) {
            return String::Replace($data, $fullKey, Path::resolveSymbol($key, $env));
        });

        return path;
    }

    string Path::resolveSymbol(const string &$symbol, const std::map<string, string> &$env) {
        string retVal;
        if ($env.empty()) {
            char *pVar = getenv($symbol.c_str());
            if (pVar != nullptr) {
                retVal = string(pVar);
            }
        } else {
            // TODO(B58790) resolveSymbol for custom environment variables. With unit tests.
            throw std::runtime_error("Path::resolveSymbol not implemented case.");
        }
        return retVal;
    }

    string Path::processPath(const string &$path, const function<string(const string &, const string &, const string &)> &$resolver) {
        string retVal = $path;

        boost::regex expr(pathSymbolRegExpr);
        string::const_iterator begin = $path.begin(), end = $path.end();
        boost::match_results<string::const_iterator> what;
        boost::match_flag_type flags = boost::match_any;

        while (boost::regex_search(begin, end, what, expr, flags)) {
            retVal = $resolver(retVal, string(what[1].first, what[1].second), string(what[2].first, what[2].second));
            begin = what[0].second;
            flags |= boost::match_prev_avail;
        }

        return retVal;
    }

    string Path::Quote(const string &$path) {
        string retVal = $path;
        if (String::Contains(retVal, " ") && !String::StartsWith(retVal, "\"")) {
            retVal = "\"" + retVal + "\"";
        }
        return retVal;
    }
}
