/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_FILESYSTEM_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_FILESYSTEM_HPP_

#ifdef CreateDirectory
#undef CreateDirectory
#endif

namespace Com::Wui::Framework::XCppCommons::System::IO {
    /**
     * This class provides static method for handling operations with file system.
     */
    class FileSystem : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                       private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
        typedef Com::Wui::Framework::XCppCommons::Interfaces::IResponse IResponse;
     public:
        /**
         * Check for the existence of a file.
         * @param $fileName Name of the file to check.
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns true if exists, false otherwise.
         */
        static bool Exists(const string &$fileName, const function<void(bool $status)> &$callback = nullptr);

        /**
         * Check for the existence of a file.
         * @param $fileName Name of the file to check.
         * @param $response Specify response to send status data (IResponse::Send(<bool>)) before function returns.
         * @return Returns true if exists, false otherwise.
         */
        static bool Exists(const string &$fileName, const shared_ptr<IResponse> &$response);

        /**
         * Renames the specified file.
         * @param $fileName Original filename.
         * @param $newFileName New file name.
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns true if succeed, false otherwise.
         */
        static bool Rename(const string &$fileName, const string &$newFileName, const function<void(bool $status)> &$callback = nullptr);

        /**
         * Renames the specified file.
         * @param $fileName Original filename.
         * @param $newFileName New file name.
         * @param $response Specify response to send status data (IResponse::Send(<bool>)) before function returns.
         * @return Returns true if succeed, false otherwise.
         */
        static bool Rename(const string &$fileName, const string &$newFileName, const shared_ptr<IResponse> &$response);

        /**
         * Copy the content of the file.
         * @param $fileName Source file.
         * @param $newFileName Destination file.
         * @param $callback Callback method will be called before function finished. Set nullptr to ignore callback.
         */
        static void Copy(const string &$fileName, const string &$newFileName, const function<void(bool $status)> &$callback);

        /**
         * Copy the content of the file or directory.
         * @param $fileName Source file or directory.
         * @param $newFileName Destination file or directory.
         * @param $reponse Specify response.
         */
        static void Copy(const string &$fileName, const string &$newFileName, const shared_ptr<IResponse> &$response);

        /**
         * Delete the specified file or directory. If $fileName does not exist, true is returned.
         * @param $fileName File or directory to be deleted.
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns true if succeed, false otherwise.
         */
        static bool Delete(const string &$fileName, const function<void(bool $status)> &$callback = nullptr);

        /**
         * Delete the specified file or directory. If $fileName does not exist, true is returned.
         * @param $fileName File or directory to be deleted.
         * @param $response Specify response.
         * @return Returns true if succeed, false otherwise.
         */
        static bool Delete(const string &$fileName, const shared_ptr<IResponse> &$response);

        /**
         * Create directory.
         * @param $dirName Name of the new directory.
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns true if succeed, false otherwise.
         */
        static bool CreateDirectory(const string &$dirName, const function<void(bool $status)> &$callback = nullptr);

        /**
         * Create directory.
         * @param $dirName Name of the new directory.
         * @param $response Specify response.
         * @return Returns true if succeed, false otherwise.
         */
        static bool CreateDirectory(const string &$dirName, const shared_ptr<IResponse> &$response);

        /**
         * Reads file from specified $path.
         * @param $path Relative path to file in executable directory or embedded resource
         * identified by relative path in current project directory.
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return File or embedded resource content as string.
         */
        static string Read(const string &$fileName, const function<void(const string &$data)> &$callback = nullptr);

        /**
         * Reads file from specified $path.
         * @param $path Relative path to file in executable directory or embedded resource
         * identified by relative path in current project directory.
         * @param $response Specify response.
         * @return File or embedded resource content as string.
         */
        static string Read(const string &$fileName, const shared_ptr<IResponse> &$response);

        /**
         * Writes data into specified file. Function creates any subdirectories in $fileName.
         * @note Data written to embedded resource are NOT persistent.
         * @param $fileName Relative path to file in executable directory or embedded resource.
         * @param $data Data to write.
         * @param $append Append data to standing file content [default=false].
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns true if succeed, false otherwise.
         */
        static bool Write(const string &$fileName, const string &$data, bool $append = false,
                          const function<void(bool $status)> &$callback = nullptr);

        /**
         * Writes data into specified file.
         * @note Data written to embedded resource are NOT persistent.
         * @param $fileName Relative path to file in executable directory or embedded resource.
         * @param $data Data to write.
         * @param $append Append data to standing file content.
         * @param $response Specify response.
         * @return Returns true if succeed, false otherwise.
         */
        static bool Write(const string &$fileName, const string &$data, bool $append, const shared_ptr<IResponse> &$response);

        /**
         * Download file from an url. Supports HTTP and HTTPS protocols.
         * @param $url Location of file to be downloaded or options in JSON type.
         * @param $callback Function to be called when downloading finished.
         */
        static void Download(any $url, const function<void(const string &$data)> &$callback);

        /**
         * Download file from an url. Supports HTTP and HTTPS protocols.
         * @param $url Location of file to be downloaded or options in JSON type.
         * @param $response Specify response.
         */
        static void Download(any $url, const shared_ptr<IResponse> &$response);

        /**
         * Create archive from specified path.
         * @param $path Path to be packed.
         * @param $options Specify options for archive processor.
         * @param $callback Callback method will be called before function finished. Leave nullptr to ignore callback (not recommended).
         */
        static void Pack(const string &$path, const json &$options, const function<void(const string &$data)> &$callback);

        /**
         * Create archive from specified path.
         * @param $path Path to be packed.
         * @param $options Specify options for archive processor.
         * @param $response Specify response.
         */
        static void Pack(const string &$path, const json &$options, const shared_ptr<IResponse> &$response);

        /**
         * Unpack specified archive file.
         * @param $filePath Path to the archive.
         * @param $options Specify options for archive processor.
         * @param $callback Callback method will be called before function finished. Leave nullptr to ignore callback (not recommended).
         */
        static void Unpack(const string &$filePath, const json &$options,
                           const function<void(const string &$tmpPath)> &$callback);

        /**
         * Unpack specified archive file.
         * @param $filePath Path to the archive.
         * @param $options Specify options for archive processor.
         * @param $response Specify response.
         */
        static void Unpack(const string &$filePath, const json &$options, const shared_ptr<IResponse> &$response);

        /**
         * Check if a directory is empty
         * @param $path Path to directory.
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Return true if empty, false otherwise.
         */
        static bool IsEmpty(const string &$path, const function<void(bool $status)> &$callback = nullptr);

        /**
         * Check if a directory is empty
         * @param $path Path to directory.
         * @param $response Specify response.
         * @return Return true if empty, false otherwise.
         */
        static bool IsEmpty(const string &$path, const shared_ptr<IResponse> &$response);

        /**
         * Find the temp directory location specific for OS.
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns temp directory path.
         */
        static string getTempPath(const function<void(const string &$path)> &$callback = nullptr);

        /**
         * Find the temp directory location specific for OS.
         * @param $response Specify response.
         * @return Returns temp directory path.
         */
        static string getTempPath(const shared_ptr<IResponse> &$response);

        /**
         * Find the application data directory location for specific OS.
         * On Windows platform returns Roaming in most cases.
         * Linux yields always "/var/lib".
         * On OS X it will return probably "/Library/Application Support" or similar (in case of sandbox).
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns appdata directory path.
         */
        static string getAppDataPath(const function<void(const string &$path)> &$callback = nullptr);

        /**
         * Find the application data directory location for specific OS.
         * On Windows platform returns Roaming in most cases.
         * Linux yields always "/var/lib".
         * On OS X it will return probably "/Library/Application Support" or similar (in case of sandbox).
         * @param $response Specify response.
         * @return Returns appdata directory path.
         */
        static string getAppDataPath(const shared_ptr<IResponse> &$response);

        /**
         * Find the local application data directory location for specific OS.
         * Linux yields always "/home/<current-username>".
         * On OS X it will return probably "/Users/<current-username>/Library/Application Support" or similar (in case of sandbox).
         * @param $callback Callback method will be called before function finished. Leave default (nullptr) to ignore callback.
         * @return Returns local-appdata directory path.
         */
        static string getLocalAppDataPath(const function<void(const string &$path)> &$callback = nullptr);

        /**
         * Find the local application data directory location for specific OS.
         * Linux yields always "/home/<current-username>".
         * On OS X it will return probably "/Users/<current-username>/Library/Application Support" or similar (in case of sandbox).
         * @param $response Specify response.
         * @return Returns local-appdata directory path.
         */
        static string getLocalAppDataPath(const shared_ptr<IResponse> &$response);

        /**
         * Resolve path relative to specified base path or current working directory by default.
         * @param $path Specify path.
         * @param $baseDir Specify base directory path.
         * @return Returns resolved path.
         */
        static string ResolvePath(const string &$path, const string &$baseDir = boost::filesystem::current_path().string());

        /**
         * Normalize specified path.
         * @param $path Specify path to be normalized.
         * @return Returns normalized path.
         */
        static string NormalizePath(const string &$path);

        /**
         * Returns a unique array of all file or directory paths that match the given $pattern.
         * The wildcard expansion keys:
         * - ?  ... Any single character.
         * - *  ... Any character with undefined count of them.
         * - ** ... All subdirectories recursively
         * @param $pattern Specify pattern.
         * @return Returns vector of matched files.
         */
        static std::vector<string> Expand(const string &$pattern);

     private:
        static bool exists(const string &$path);

        static bool exists(const boost::filesystem::path &$path);

        static bool copyDir(const boost::filesystem::path &$source, const boost::filesystem::path &$destination,
                            unsigned int &$progress, unsigned int $total);

        static bool removeDir(const boost::filesystem::path &$dirName, unsigned int &$progress, unsigned int $total,
                              const shared_ptr<IResponse> &$response);

        static unsigned int countFiles(const string &$path);

        static string getApplicationSupportPath(bool $systemWide);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_FILESYSTEM_HPP_
