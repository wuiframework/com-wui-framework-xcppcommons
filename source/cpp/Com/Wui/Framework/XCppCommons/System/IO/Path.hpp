/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_PATH_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_PATH_HPP_

namespace Com::Wui::Framework::XCppCommons::System::IO {
    /**
     * This class provides static methods for handling operations with file system paths.
     */
    class Path {
     public:
        /**
         * Expands environment variables inside path. %<VAR>% on windows and $<VAR> on posix.
         * @param $path Specify path to expand.
         * @param $env Optional: specify custom environment variables. If not set "getenv()" is used.
         * @return Returns expanded path.
         */
        static string Expand(const string &$path, const std::map<string, string> &$env = {});

        /**
         * Quotes path if contains at least one space and if not quoted yet.
         * @param $path Specify path to be quoted.
         * @return Returns quoted path.
         */
        static string Quote(const string &$path);

     private:
        static string resolveSymbol(const string &$symbol, const std::map<string, string> &$env = {});

        static string processPath(const string &$path, const function<string(const string &, const string &, const string &)> &$resolver);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_PATH_HPP_
