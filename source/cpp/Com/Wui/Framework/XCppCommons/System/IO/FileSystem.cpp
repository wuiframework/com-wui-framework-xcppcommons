/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM
#include <CoreServices/CoreServices.h>
#endif

#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/device/file.hpp>

#include "../../sourceFilesMap.hpp"

#include "resourcesData.hpp"

#ifdef CreateDirectory
#undef CreateDirectory
#endif

namespace Com::Wui::Framework::XCppCommons::System::IO {
    using Com::Wui::Framework::XCppCommons::Interfaces::IResponse;
    using Com::Wui::Framework::XCppCommons::Primitives::String;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::System::Process::Terminal;
    using Com::Wui::Framework::XCppCommons::System::Process::TerminalOptions;
    using Com::Wui::Framework::XCppCommons::System::ResponseApi::ResponseFactory;

    namespace fs = boost::filesystem;

    string FileSystem::NormalizePath(const string &$path) {
        return fs::path($path).normalize().make_preferred().string();
    }

    string FileSystem::ResolvePath(const string &$path, const string &$baseDir) {
        string retVal;
        fs::path resolvePath($path);
        if (resolvePath.is_absolute()) {
            if (!resolvePath.has_relative_path()) {
                retVal = resolvePath.make_preferred().string();
            } else {
                retVal = resolvePath.normalize().make_preferred().string();
            }
        } else if (!$path.empty() && !$baseDir.empty()) {
            try {
                fs::path basePath($baseDir);
                if (basePath.is_absolute()) {
                    if (!basePath.has_relative_path()) {
                        basePath = basePath.make_preferred();
                    } else {
                        basePath = basePath.normalize().make_preferred();
                    }
                } else {
                    basePath = fs::canonical(basePath, fs::current_path()).make_preferred();
                }

                if (!fs::is_directory(basePath)) {
                    basePath = basePath.parent_path();
                }
                retVal = fs::canonical(resolvePath, basePath).make_preferred().string();
            } catch (std::exception &ex) {
                LogIt::Debug("Resolving path {0} in base {1} failed with {2}", $path, $baseDir, ex.what());
                retVal = "";
            }
        }

        return retVal;
    }

    string FileSystem::Read(const string &$fileName, const function<void(const string &$data)> &$callback) {
        return FileSystem::Read($fileName, ResponseFactory::getResponse($callback));
    }

    string FileSystem::Read(const string &$fileName, const shared_ptr<IResponse> &$response) {
        string data = Resources::ResourcesData::getString($fileName);

        if (data.empty()) {
            LogIt::Info("> read content from \"{0}\"", $fileName);
            string fileName = FileSystem::ResolvePath($fileName);
            if (FileSystem::exists(fileName) && fs::is_regular_file(fileName)) {
                std::ifstream t(fileName, std::ios_base::binary);
                data = string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
            }
        }

        ResponseFactory::getResponse($response)->Send(data);

        return data;
    }

    bool FileSystem::Write(const string &$fileName, const string &$data, const bool $append,
                           const function<void(bool $status)> &$callback) {
        return FileSystem::Write($fileName, $data, $append, ResponseFactory::getResponse($callback));
    }

    bool FileSystem::Write(const string &$fileName, const string &$data, bool $append, const shared_ptr<IResponse> &$response) {
        string fileName = FileSystem::NormalizePath($fileName);
        LogIt::Info("> write content to \"{0}\"", fileName);

        bool success = false;

        if (!$fileName.empty()) {
            bool basePathCreated = true;

            if (String::Contains(fileName, std::vector<string>{"\\", "/"})) {
                string basePath = fileName.substr(0, fileName.find_last_of("/\\"));

                if (!basePath.empty() && !fs::exists(basePath)) {
                    basePathCreated = FileSystem::CreateDirectory(basePath);
                }
            }

            if ((!fs::exists(fileName) || fs::is_regular_file(fileName)) && basePathCreated) {
                std::ios_base::openmode openMode = std::ios_base::out | std::ios_base::binary;
                if ($append) {
                    openMode |= std::ios_base::app;
                } else {
                    openMode |= std::ios_base::trunc;
                }

                try {
                    std::ofstream outputStream(fileName, openMode);
                    outputStream << $data;
                    outputStream.flush();
                    outputStream.close();
                    success = true;
                } catch (const std::exception &ex) {
                    success = false;
                    LogIt::Error("Write content failed with: {0}", ex.what());
                }
            } else {
                LogIt::Warning("Write content is supported only for file, not for \"{0}\"", fileName);
            }
        }

        ResponseFactory::getResponse($response)->Send(success);

        return success;
    }

    bool FileSystem::Exists(const string &$fileName, const function<void(bool $status)> &$callback) {
        return FileSystem::Exists($fileName, ResponseFactory::getResponse($callback));
    }

    bool FileSystem::Exists(const string &$fileName, const shared_ptr<IResponse> &$response) {
        bool status = FileSystem::exists($fileName);
        ResponseFactory::getResponse($response)->Send(status);
        LogIt::Info("> validate path \"{0}\" existence: {1}", $fileName, status);
        return status;
    }

    bool FileSystem::Rename(const string &$fileName, const string &$newFileName, const function<void(bool $status)> &$callback) {
        return FileSystem::Rename($fileName, $newFileName, ResponseFactory::getResponse($callback));
    }

    bool FileSystem::Rename(const string &$fileName, const string &$newFileName, const shared_ptr<IResponse> &$response) {
        LogIt::Info(R"(> rename from "{0}" to "{1}")", $fileName, $newFileName);
        bool success = false;
        string fileName = NormalizePath($fileName);
        string newFileName = NormalizePath($newFileName);
        boost::system::error_code error;

        if (!fileName.empty() && !newFileName.empty()) {
            fs::rename(fileName, newFileName, error);

            if (!error) {
                success = true;
            }
        }

        if (!success) {
            LogIt::Warning(R"(> rename from "{0}" to "{1}" FAILED)", $fileName, $newFileName);
        }

        ResponseFactory::getResponse($response)->Send(success);
        return success;
    }

    void FileSystem::Copy(const string &$fileName, const string &$newFileName, const function<void(bool)> &$callback) {
        FileSystem::Copy($fileName, $newFileName, ResponseFactory::getResponse($callback));
    }

    void FileSystem::Copy(const string &$fileName, const string &$newFileName, const shared_ptr<IResponse> &$response) {
        LogIt::Info(R"(> copy from "{0}" to "{1}")", $fileName, $newFileName);
        string fileName = NormalizePath($fileName);
        string newFileName = NormalizePath($newFileName);
        fs::path source(fileName);
        fs::path destination(newFileName);

        bool status = false;

        if (exists(fileName)) {
            try {
                unsigned int totalFiles = FileSystem::countFiles(fileName);

                if (!fs::is_directory(source)) {
                    string directory = destination.parent_path().string();
                    if (!exists(directory)) {
                        FileSystem::CreateDirectory(directory);
                    }
                    boost::system::error_code errorCode;
                    boost::filesystem::copy_file(source, destination, errorCode);
                    status = (errorCode == boost::system::errc::errc_t::success);
                } else {
                    unsigned int progress = 0;
                    status = copyDir(source, destination, progress, totalFiles);
                }
            } catch (std::exception &$ex) {
                LogIt::Error($ex);
            }
        }

        ResponseFactory::getResponse($response)->Send(status);
    }

    bool FileSystem::copyDir(const boost::filesystem::path &$source, const boost::filesystem::path &$destination, unsigned int &$progress,
                             unsigned int $total) {
        try {
            if (!exists($source.string()) || !fs::is_directory($source)) {
                LogIt::Error("Source directory " + $source.string() + " does not exist or is not a directory.");
                return false;
            }

            if (!exists($destination)) {
                if (!fs::create_directories($destination)) {
                    LogIt::Error("Unable to create destination directory " + $destination.string());
                    return false;
                }
            }
        } catch (fs::filesystem_error const &$ex) {
            LogIt::Error($ex);
            return false;
        }

        for (fs::directory_iterator file($source); file != fs::directory_iterator(); ++file) {
            try {
                fs::path current((*file).path());
                if (fs::is_directory(current)) {
                    $progress++;
                    if (!copyDir(current, $destination / current.filename(), $progress, $total)) {
                        return false;
                    }
                } else {
                    copy_file(current, $destination / current.filename());
                    $progress++;
                }

                LogIt::Debug("Copy progress: " + boost::to_string($progress) + "/" + boost::to_string($total));
            } catch (fs::filesystem_error const &$ex) {
                LogIt::Error($ex);
                return false;
            }
        }
        return true;
    }

    bool FileSystem::removeDir(const boost::filesystem::path &$dirName, unsigned int &$progress, unsigned int $total,
                               const shared_ptr<IResponse> &$response) {
        try {
            if (!exists($dirName.string()) || !fs::is_directory($dirName)) {
                LogIt::Error("Source directory " + $dirName.string() + " does not exist or is not a directory.");
                return false;
            }
        } catch (fs::filesystem_error const &$ex) {
            LogIt::Error($ex);
            return false;
        }

        for (fs::directory_iterator file($dirName); file != fs::directory_iterator(); ++file) {
            try {
                fs::path current((*file).path());
                if (fs::is_directory(current)) {
                    $progress++;
                    if (!removeDir(current, $progress, $total, $response)) {
                        return false;
                    }
                    if (fs::is_empty(current)) {
                        remove(current);
                    }
                } else {
                    remove(current);
                    $progress++;
                }

                if ($response != nullptr) {
                    $response->OnChange({{"currentValue", $progress},
                                         {"rangeStart",   0},
                                         {"rangeEnd",     $total}});
                }

                LogIt::Debug("Delete progress: " + boost::to_string($progress) + "/" + boost::to_string($total));
            } catch (const fs::filesystem_error &$ex) {
                LogIt::Error($ex);
                return false;
            }
        }
        return true;
    }

    bool FileSystem::Delete(const string &$fileName, const function<void(bool $status)> &$callback) {
        return FileSystem::Delete($fileName, ResponseFactory::getResponse($callback));
    }

    bool FileSystem::Delete(const string &$fileName, const shared_ptr<IResponse> &$response) {
        shared_ptr<IResponse> response = ResponseFactory::getResponse($response);
        string fileName = FileSystem::NormalizePath($fileName);
        LogIt::Info("> delete path \"{0}\"", fileName);

        response->OnStart();

        bool status = true;

        if (FileSystem::exists(fileName)) {
            if (fs::is_directory(fs::path(fileName))) {
                try {
                    unsigned int totalFiles = FileSystem::countFiles(fileName);
                    unsigned int progress = 0;

                    response->OnChange({{"currentValue", 0},
                                        {"rangeEnd",     100},
                                        {"rangeStart",   0}});


                    status = removeDir(fs::path(fileName), progress, totalFiles, response);
                    remove(fs::path(fileName));
                } catch (std::exception &ex) {
                    LogIt::Error(ex);
                    status = false;
                }
            } else {
                boost::system::error_code errorCode;

                response->OnChange({{"currentValue", 0},
                                    {"rangeEnd",     100},
                                    {"rangeStart",   0}});

                remove(fs::path(fileName), errorCode);

                if (errorCode) {
                    LogIt::Error("Can not delete path \"{0}\" due to error: {1}", $fileName, errorCode.message());
                    status = false;
                }
            }
        }

        response->OnComplete(status);

        return status;
    }

    bool FileSystem::CreateDirectory(const string &$dirName, const function<void(bool $status)> &$callback) {
        return FileSystem::CreateDirectory($dirName, ResponseFactory::getResponse($callback));
    }

    bool FileSystem::CreateDirectory(const string &$dirName, const shared_ptr<IResponse> &$response) {
        bool success = false;

        if (!$dirName.empty()) {
            string dirName = NormalizePath($dirName);
            boost::system::error_code error;
            fs::create_directories(dirName, error);

            if (!error) {
                success = true;
            }
        }

        LogIt::Info("> create directory at \"{0}\", success: {1}", $dirName, success);

        ResponseFactory::getResponse($response)->Send(success);

        return success;
    }

    void FileSystem::Download(any $url, const function<void(const string &)> &$callback) {
        FileSystem::Download(std::move($url), ResponseFactory::getResponse($callback));
    }

    void FileSystem::Download(any $url, const shared_ptr<IResponse> &$response) {
        using Com::Wui::Framework::XCppCommons::System::Net::Url;
        using Com::Wui::Framework::XCppCommons::Enums::HttpResponseType;
        using Com::Wui::Framework::XCppCommons::Enums::HttpClientType;
        using Com::Wui::Framework::XCppCommons::System::Net::Http::Client;
        using Com::Wui::Framework::XCppCommons::System::Net::Http::ClientResponse;
        using Com::Wui::Framework::XCppCommons::System::Net::Http::ClientRequest;

        shared_ptr<IResponse> response = ResponseFactory::getResponse($response);

        json options = {
                {"method",       "GET"},
                {"url",          ""},
                {"headers",      json({})},
                {"body",         ""},
                {"streamOutput", false}
        };

        if (boost::any_cast<string>(&$url) != nullptr) {
            options["url"] = *boost::any_cast<string>(&$url);
        } else if (boost::any_cast<char *>(&$url) != nullptr) {
            options["url"] = string(*boost::any_cast<char *>(&$url));
        } else if (boost::any_cast<json>(&$url) != nullptr) {
            json tmpOpt = *boost::any_cast<json>(&$url);
            for (json::const_iterator it = tmpOpt.begin(); it != tmpOpt.end(); ++it) {
                if (options.find(it.key()) != options.end()) {
                    options[it.key()] = it.value();
                }
            }
        } else {
            return;
        }

        if (options["url"].is_string()) {
            options["url"] = Url(options["url"].get<string>()).ToJson();
            options["url"]["method"] = options["method"];
            options["url"]["headers"] = options["headers"];
        }
        LogIt::Info("> download ({0}) id: {2}; from: {1}", options["method"], options["url"]["href"], response->getId());
        string headers = json(options["url"]["headers"]).dump();
        if (headers != "{}") {
            LogIt::Info("> with headers: {0}", headers);
        }
        if (!options["body"].empty()) {
            LogIt::Info("> with body: {0}", options["body"]);
        }

        auto prepareTemp = [&options](const string &$fileName) -> string {
            fs::path tmpPath = fs::temp_directory_path() /
                               Com::Wui::Framework::XCppCommons::EnvironmentArgs::getInstance().getProjectName();

            auto timeString = []() -> string {
                return std::to_string(static_cast<uint64_t>(time(nullptr)));
            };

            if (!exists(tmpPath.string())) {
                fs::create_directory(tmpPath);
            }

            if (!$fileName.empty()) {
                tmpPath /= $fileName;
            } else {
                string name = options["url"]["pathname"].get<string>();
                if (name.find('/') >= 0) {
                    name = name.substr(name.find_last_of('/'));
                    if (!name.empty()) {
                        tmpPath /= name;
                    } else {
                        tmpPath /= timeString();
                    }
                } else {
                    tmpPath /= timeString();
                }
            }
            tmpPath = tmpPath.make_preferred();

            if (exists(tmpPath.string())) {
                string pathExt = tmpPath.extension().string();
                boost::regex expr(".tar.[a-zA-Z0-9]+$", boost::regex::icase);
                boost::cmatch match;
                if (boost::regex_match(pathExt.c_str(), match, expr)) {
                    if (!match.empty() && match[0].length() > 0) {
                        pathExt = string(match[0].first, match[0].second);
                    }
                }
                int index = 2;
                fs::path newPath;
                do {
                    if (!pathExt.empty()) {
                        string np = tmpPath.string();
                        boost::replace_last(np, pathExt, "_" + std::to_string(index) + pathExt);
                        newPath = np;
                    } else {
                        newPath = tmpPath.string() + "_" + std::to_string(index);
                    }
                    index++;
                } while (exists(newPath.string()));
                tmpPath = newPath;
            }
            return tmpPath.make_preferred().string();
        };

        string tmpPath;

        if (boost::iequals(options["url"]["protocol"].get<string>(), "http:") ||
            boost::iequals(options["url"]["protocol"].get<string>(), "https:")) {
            HttpClientType clientType;

            if (boost::iequals(options["url"]["protocol"].get<string>(), "https:")) {
                clientType = HttpClientType::HTTPS;
            } else if (boost::iequals(options["url"]["protocol"].get<string>(), "http:")) {
                clientType = HttpClientType::HTTP;
            } else {
                clientType = HttpClientType::INVALID;
            }

            Client httpClient;
            unique_ptr<std::ostream> stream;
            bool streamOutput = options["streamOutput"];
            // cppcheck-suppress variableScope
            int totalDataSize = -1;
            // cppcheck-suppress variableScope
            int downloaded = 0;
            // cppcheck-suppress variableScope
            int counter = 0;  // strange, cannot be before lambda which process "data", must be out of all lambda scope???
            bool connectionClosed = false;
            bool aborting = false;

            ClientRequest request =
                    httpClient.Request(options["url"], [&](ClientResponse &$clientResponse) {
                        if ($clientResponse.getStatusCode() == HttpResponseType::OK) {
                            if (!streamOutput) {
                                string fileName;
                                string disposition = $clientResponse.getHeaderValue("content-disposition");
                                if (!disposition.empty() || boost::iequals(disposition, "_")) {
                                    boost::regex exprName("attachment;\\s*filename=\"?(.*?)\"?$", boost::regex::icase);
                                    boost::cmatch matchName;
                                    if (boost::regex_match(disposition.c_str(), matchName, exprName)) {
                                        if (!matchName.empty() && matchName.size() == 2) {
                                            fileName = string(matchName[1].first, matchName[1].second);
                                        }
                                    }
                                }
                                fileName = prepareTemp(fileName);
                                tmpPath = fileName;
                                stream = std::make_unique<std::ofstream>(fileName, std::ios_base::trunc | std::ios_base::binary);
                            } else {
                                stream = std::make_unique<std::ostringstream>();
                            }

                            string contentLength = $clientResponse.getHeaderValue("content-length");
                            totalDataSize = 0;
                            if (!contentLength.empty()) {
                                totalDataSize = boost::lexical_cast<int>($clientResponse.getHeaderValue("content-length"));
                            }
                            $clientResponse.setOnData([&](const string &$data) {
                                stream->write($data.c_str(), $data.length());
                                downloaded += $data.length();

                                if (counter++ >= 10000) {
                                    LogIt::Info("(" + std::to_string(floor(static_cast<float>(downloaded) / totalDataSize * 100)) + "%)");
                                    counter = 0;
                                }

                                response->OnChange(json({
                                                                {"currentValue", downloaded},
                                                                {"rangeEnd",     totalDataSize},
                                                                {"rangeStart",   0}
                                                        }));
                            });

                            $clientResponse.setOnClose([&]() {
                                if (downloaded != totalDataSize) {
                                    connectionClosed = true;
                                    if (!streamOutput) {
                                        stream->flush();
                                        auto *ofs = dynamic_cast<std::ofstream *>(stream.get());
                                        ofs->close();
                                        stream.release();
                                        if (exists(tmpPath)) {
                                            boost::system::error_code ec;
                                            fs::remove(tmpPath, ec);
                                            if (ec.value() != 0) {
                                                LogIt::Error("Removing of corrupted file \"{0}\" failed {1}.", tmpPath,
                                                             ec.message());
                                            }
                                        }
                                    } else {
                                        auto *oss = dynamic_cast<std::ostringstream *>(stream.get());
                                        if (oss != nullptr) {
                                            oss->str("");
                                        }
                                    }

                                    if (!aborting) {
                                        LogIt::Error("Connection has been lost.");
                                        response->OnError("Connection has been lost.");
                                    } else {
                                        LogIt::Info("Connection has been aborted.");
                                    }
                                }
                            });

                            $clientResponse.setOnEnd([&]() {
                                if (!connectionClosed) {
                                    if (downloaded == totalDataSize || totalDataSize == 0) {
                                        if (stream) {
                                            stream->flush();
                                        }
                                    }
                                    LogIt::Info("(100%)");
                                    if (!streamOutput) {
                                        if (stream) {
                                            auto fstream = dynamic_cast<std::ofstream *>(stream.get());
                                            fstream->flush();
                                            fstream->close();
                                        }
                                        response->OnComplete($clientResponse.getHeadersJson(), tmpPath);
                                    } else {
                                        response->OnComplete($clientResponse.getHeadersJson(),
                                                             dynamic_cast<std::ostringstream *>(stream.get())->str());
                                    }
                                }
                            });
                        } else if ($clientResponse.getStatusCode() == HttpResponseType::MOVED ||
                                   $clientResponse.getStatusCode() == HttpResponseType::FOUND) {
                            options["url"] = $clientResponse.getHeaderValue("location");
                            FileSystem::Download(options, response);
                        } else {
                            LogIt::Warning("Http client response failed with status {0}.", $clientResponse.getStatusCode());
                        }
                    });

            request.setOnError([&](const string $error) {
                LogIt::Error($error);
                response->OnError($error);
            });

            string body = options["body"];
            if (!body.empty()) {
                request.Write(body);
            }

            response->AddAbortHandler([&]() {
                LogIt::Info("Download aborting {0}", response->getId());
                aborting = true;
                request.Abort();
            });

            request.End();

            response->OnStart();

            httpClient.WaitForFinished();
        } else {
            string error = String::Format("Selected protocol \"{0}\" is not supported. Please use http://, https:// protocol instead.",
                                          options["url"]["protocol"].get<string>());
            LogIt::Error(error);
            response->OnError(error);
        }
    }

    json get7zipOptions(const json &$options) {
        json options = {
                {"cwd",        ""},
                {"output",     ""},
                {"type",       "zip"},
                {"autoStrip",  true},
                {"override",   true},
                {"pathTo7zip", "7z"}
        };

        if (!$options.empty() && $options.is_object()) {
            for (json::const_iterator it = $options.begin(); it != $options.end(); ++it) {
                if (options.find(it.key()) != options.end()) {
                    options[it.key()] = it.value();
                }
            }
        }

        if (options["pathTo7zip"].get<string>().empty() || boost::iequals(options["pathTo7zip"].get<string>(), "7z")) {
#ifdef WIN_PLATFORM
            options["pathTo7zip"] = "7za.exe";
            options["cwd"] = "./resource/libs/7zip";
#else
            options["pathTo7zip"] = "./7z";
            options["cwd"] = "./resource/libs/p7zip/bin";
#endif
            try {
                string cwd = options["cwd"];
                cwd = fs::canonical(cwd).make_preferred().string();
                if (!cwd.empty()) {
                    options["cwd"] = cwd;
#ifndef WIN_PLATFORM
                    struct stat stat1;
                    string path = cwd + "/" + options["pathTo7zip"].get<string>();
                    if (stat(path.c_str(), &stat1) == 0) {
                        if (chmod(path.c_str(), stat1.st_mode | S_IEXEC) != 0) {
                            LogIt::Error("Can not set run enable attribute to file \"{0}\".", path);
                        }
                    }
#endif
                }
            } catch (std::exception &ex) {
                LogIt::Warning("Can not locate 7zip in xcppcommons modules.");
            }
        }

        return options;
    }

    void FileSystem::Pack(const string &$path, const json &$options, const function<void(const string &)> &$callback) {
        FileSystem::Pack($path, $options, ResponseFactory::getResponse($callback));
    }

    void FileSystem::Pack(const string &$path, const json &$options, const shared_ptr<IResponse> &$response) {
        shared_ptr<IResponse> response = ResponseFactory::getResponse($response);
        string path = NormalizePath($path);
        std::vector<string> paths = {path};
        bool notFound = false;
        if (path.back() == '*') {
            boost::system::error_code ec;
            paths.clear();
            for (fs::recursive_directory_iterator dir(path.substr(0, path.length() - 1), ec), end;
                 dir != end; dir.increment(ec)) {
                if (ec != nullptr) {
                    dir.pop();
                    continue;
                }
                if (dir.depth() >= 0) {
                    dir.no_push();
                }
                fs::path tmp = (*dir).path();
                paths.emplace_back(tmp.make_preferred().string());
            }
        }

        if (!paths.empty()) {
            for (auto &itPath : paths) {
                if (!exists(itPath)) {
                    LogIt::Error("Unable to locate file \"{0}\" for archive creation.", itPath);
                    notFound = true;
                } else {
                    if (itPath.find(' ') >= 0) {
                        itPath = "\"" + itPath + "\"";
                    }
                }
            }
        } else {
            notFound = true;
            LogIt::Error("Unable to create archive from empty source \"{0}\".", $path);
        }

        if (!notFound) {
            json options = get7zipOptions($options);

            if (options["output"].get<string>().empty()) {
                string tmpOutput = fs::path(path).filename().string();
                if (path.back() == '*' || tmpOutput.length() == 0) {
                    tmpOutput = "archive";
                }
                fs::path tmpPath = fs::temp_directory_path() /
                                   Com::Wui::Framework::XCppCommons::EnvironmentArgs::getInstance().getProjectName();
                fs::path tmpArch = tmpPath / fs::path(tmpOutput + "." + options["type"].get<string>());

                string pathExt = tmpArch.extension().string();
                boost::regex expr(".tar.[a-zA-Z0-9]+$", boost::regex::icase);
                boost::cmatch match;
                if (boost::regex_match(pathExt.c_str(), match, expr)) {
                    if (!match.empty() && match[0].length() > 0) {
                        pathExt = string(match[0].first, match[0].second);
                    }
                }
                int index = 2;
                fs::path newPath;
                do {
                    if (!pathExt.empty()) {
                        string np = tmpArch.string();
                        boost::replace_last(np, pathExt, "_" + std::to_string(index) + pathExt);
                        newPath = np;
                    } else {
                        newPath = tmpArch.string() + "_" + std::to_string(index);
                    }
                    index++;
                } while (exists(newPath.string()));

                options["output"] = newPath.string();
            }
            string outPath = fs::path(options["output"].get<string>()).make_preferred().string();
            std::vector<string> attr = {"a", "-t" + options["type"].get<string>(), "-r", "\"" + outPath + "\""};
            attr.insert(attr.end(), paths.begin(), paths.end());

            try {
                Terminal::Execute(options["pathTo7zip"].get<string>(), attr,
                                  TerminalOptions(options["cwd"].get<string>()),
                                  [&](int $exitCode, const std::vector<string> &$std) {
                                      if ($exitCode != 0) {
                                          string error = String::Format("Unable to Create archive at \"{0}\".\n{1}\n{2}", outPath, $std[0],
                                                                        $std[1]);
                                          LogIt::Error(error);
                                          response->OnError(error);
                                      } else {
                                          response->Send(outPath);
                                      }
                                  });
            } catch (std::exception &ex) {
                LogIt::Error(ex);
                response->OnError(ex);
            }
        }
    }

    void FileSystem::Unpack(const string &$filePath, const json &$options,
                            const function<void(const string &$tmpPath)> &$callback) {
        FileSystem::Unpack($filePath, $options, ResponseFactory::getResponse($callback));
    }

    void FileSystem::Unpack(const string &$filePath, const json &$options, const shared_ptr<IResponse> &$response) {
        shared_ptr<IResponse> response = ResponseFactory::getResponse($response);
        string filePath = NormalizePath($filePath);
        std::vector<string> unpackInnerArchive({".tar.bz2", ".tar.gz", ".tar.xz", ".tgz"});
        string pathExtension = fs::path(filePath).extension().string();
        boost::cmatch pathMatch;
        boost::regex_match(filePath.c_str(), pathMatch, boost::regex("^.*(\\.tar\\.[a-zA-Z0-9]+)"));
        if (!pathMatch.empty() && pathMatch.size() == 2) {
            string tmp = string(pathMatch[1].first, pathMatch[1].second);
            if (!tmp.empty()) {
                pathExtension = tmp;
            }
        }

        json options = get7zipOptions($options);

        if (options["output"].get<string>().empty()) {
            fs::path tmpPath = (fs::temp_directory_path() /
                                Com::Wui::Framework::XCppCommons::EnvironmentArgs::getInstance().getProjectName());
            string tmp = (tmpPath / fs::path(filePath).filename()).string();

            boost::replace_last(tmp, pathExtension, "");
            options["output"] = fs::path(tmp).make_preferred().string();
        }

        auto onComplete = [&]() {
            response->OnComplete(options["output"].get<string>());
        };

        auto onError = [&]() {
            string error = String::Format("Archive \"{0}\" uncompress failed.", $filePath);
            LogIt::Error(error);
            response->OnError(error);
        };

        if (!options["override"].get<bool>() && exists(options["output"].get<string>())) {
            onComplete();
        } else {
            auto quote = [](const string &in) -> string {
                if (static_cast<int>(in.find(' ')) > 0 && static_cast<int>(in.find('\"')) == -1) {
                    return "\"" + in + "\"";
                }
                return in;
            };

            string cmd = options["pathTo7zip"];
            string file = fs::path($filePath).make_preferred().string();
            string output = fs::path(options["output"].get<string>()).make_preferred().string();

            if (FileSystem::Exists(output)) {
                FileSystem::Delete(output);
            }
            output = quote(output);
            file = quote(file);
            try {
                Terminal::Execute(cmd, std::vector<string>{"x", file, "-aoa", "-o" + output},
                                  TerminalOptions(options["cwd"].get<string>()),
                                  [&](int $exitCode, const std::vector<string> &$std) {
                                      if ($exitCode != 0) {
                                          LogIt::Error("Unpack of {0} failed \n{1}\n{2}", file, $std[0], $std[1]);
                                          onError();
                                      } else {
                                          auto onUncompress = [&]() {
                                              if (options["autoStrip"].get<bool>()) {
                                                  string origOutput = options["output"];
                                                  boost::system::error_code ec;
                                                  string subDir;
                                                  // goes through directory and stops if more than one sub dir found or file found
                                                  for (fs::recursive_directory_iterator dir(origOutput, ec), end;
                                                       dir != end; dir.increment(ec)) {
                                                      if (ec != nullptr) {
                                                          dir.pop();
                                                          continue;
                                                      }
                                                      if (dir.depth() >= 0) {
                                                          dir.no_push();
                                                      }
                                                      if (!fs::is_directory((*dir).path())) {
                                                          subDir = "";
                                                          break;
                                                      }
                                                      if (subDir.empty()) {
                                                          subDir = (*dir).path().string();
                                                      } else {
                                                          subDir = "";
                                                          break;
                                                      }
                                                  }
                                                  if (!subDir.empty()) {  // directory contains only one sub directory
                                                      const string &archPath = subDir;
                                                      string subDirName = fs::path(subDir).filename().string();
                                                      string archName = fs::path(archPath).filename().string();
                                                      boost::replace_last(archName, pathExtension, "");
                                                      if (archName.find(subDirName) == 0) {
                                                          string out = archPath;
                                                          boost::replace_last(out, fs::path("/").make_preferred().string() +
                                                                                   subDirName, "");

#ifdef WIN_PLATFORM
                                                          LogIt::Info(R"(Copy unpacked content from "{0}" to "{1}" with Robocopy.)",
                                                                      archPath, out);
                                                          Terminal::Spawn("Robocopy",
                                                                          {"\"" + archPath + "\"", "\"" + out + "\"", "/E", "/MT", "/NP",
                                                                           "/NFL", "/MOVE"}, "",
                                                                          [&](int $exitCode, const std::vector<string> &$std) {
                                                                              if ($exitCode == 1 || $exitCode == 3) {
                                                                                  onComplete();
                                                                              } else {
                                                                                  string tmpErr = String::Format("Archive \"{0}\" auto "
                                                                                                                 "strip has failed.", out);
                                                                                  LogIt::Error(tmpErr);
                                                                                  response->OnError(tmpErr);
                                                                              }
                                                                          });
#else

                                                          FileSystem::Copy(
                                                                  archPath,
                                                                  out,
                                                                  [&](bool $cpStat) {
                                                                      if ($cpStat) {
                                                                          FileSystem::Delete(archPath);
                                                                          onComplete();
                                                                      } else {
                                                                          string tmpErr = String::Format("Archive \"{0}\" auto strip"
                                                                                                         " has failed.", out);
                                                                          LogIt::Error(tmpErr);
                                                                          response->OnError(tmpErr);
                                                                      }
                                                                  });
#endif
                                                      } else {
                                                          onComplete();
                                                      }
                                                  } else {
                                                      onComplete();
                                                  }
                                              } else {
                                                  onComplete();
                                              }
                                          };

                                          if (std::find(unpackInnerArchive.begin(), unpackInnerArchive.end(),
                                                        pathExtension) != unpackInnerArchive.end()) {
                                              string extFile = fs::path(filePath).filename().string();
                                              boost::replace_last(extFile, ".bz2", "");
                                              boost::replace_last(extFile, ".gz", "");
                                              boost::replace_last(extFile, ".xz", "");
                                              boost::replace_last(extFile, ".tgz", ".tar");
                                              string tarArchive = fs::path(options["output"].get<string>() + "/" +
                                                                           extFile).make_preferred().string();
                                              try {
                                                  Terminal::Execute(
                                                          cmd,
                                                          std::vector<string>({"x", tarArchive, "-aoa", "-ttar", "-o" + output}),
                                                          TerminalOptions(options["cwd"].get<string>()),
                                                          [&](int $exitCodeI, const std::vector<string> &$stdI) {
                                                              if ($exitCodeI != 0) {
                                                                  onError();
                                                              } else {
                                                                  FileSystem::Delete(tarArchive);
                                                                  onUncompress();
                                                              }
                                                          });
                                              } catch (const std::exception &$ex) {
                                                  LogIt::Error($ex);
                                                  response->OnError($ex);
                                              }
                                          } else {
                                              onUncompress();
                                          }
                                      }
                                  });
            } catch (const std::exception &$ex) {
                LogIt::Error($ex);
                response->OnError($ex);
            }
        }
    }

    bool FileSystem::IsEmpty(const string &$path, const function<void(bool $status)> &$callback) {
        return FileSystem::IsEmpty($path, ResponseFactory::getResponse($callback));
    }

    bool FileSystem::IsEmpty(const string &$path, const shared_ptr<IResponse> &$response) {
        string path = NormalizePath($path);

        bool success = false;

        if (exists(path)) {
            success = fs::is_empty(path);
        }

        LogIt::Info("> validate if \"{0}\" is empty: {1}", $path, success);
        ResponseFactory::getResponse($response)->Send(success);

        return success;
    }

    bool FileSystem::exists(const string &$path) {
        boost::system::error_code error;
        if ($path.empty()) {
            return false;
        }
        string fileName = boost::filesystem::path(boost::replace_all_copy($path, "\"", "")).normalize().make_preferred().string();
        bool success = boost::filesystem::exists(fileName, error);
        if (error.value() != 0) {
            success = false;
            LogIt::Debug($path + ": " + error.message());
        }
        return success;
    }

    bool FileSystem::exists(const boost::filesystem::path &$path) {
        return FileSystem::exists($path.string());
    }

    string FileSystem::getTempPath(const function<void(const string &$path)> &$callback) {
        return FileSystem::getTempPath(ResponseFactory::getResponse($callback));
    }

    string FileSystem::getTempPath(const shared_ptr<IResponse> &$response) {
        string retVal = fs::temp_directory_path().string();

        ResponseFactory::getResponse($response)->Send(retVal);

        return retVal;
    }

    string FileSystem::getAppDataPath(const function<void(const string &)> &$callback) {
        return FileSystem::getAppDataPath(ResponseFactory::getResponse($callback));
    }

    string FileSystem::getAppDataPath(const shared_ptr<IResponse> &$response) {
        string retVal;

#ifdef WIN_PLATFORM
        retVal = Environment::Expand("%appdata%");
#elif LINUX_PLATFORM
        retVal = "/var/lib";
#elif MAC_PLATFORM
        retVal = FileSystem::getApplicationSupportPath(true);
#else
#warning getAppDataPath not implemented on this platform
#endif

        ResponseFactory::getResponse($response)->Send(retVal);

        return retVal;
    }

    string FileSystem::getLocalAppDataPath(const function<void(const string &)> &$callback) {
        return FileSystem::getLocalAppDataPath(ResponseFactory::getResponse($callback));
    }

    string FileSystem::getLocalAppDataPath(const shared_ptr<IResponse> &$response) {
        string retVal;

#ifdef WIN_PLATFORM
        retVal = Environment::Expand("%localappdata%");
#elif LINUX_PLATFORM
        retVal = Environment::Expand("$HOME");
#elif MAC_PLATFORM
        retVal = FileSystem::getApplicationSupportPath(false);
#else
#warning getLocalAppDataPath not implemented on this platform
#endif

        ResponseFactory::getResponse($response)->Send(retVal);

        return retVal;
    }

    unsigned int FileSystem::countFiles(const string &$path) {
        unsigned int retVal = 0;
        using boost::filesystem::recursive_directory_iterator;

        if (!boost::filesystem::is_directory($path)) {
            return 1;
        }

        try {
            recursive_directory_iterator dir($path);
            long numOfFiles = std::distance(dir, recursive_directory_iterator());
            retVal = static_cast<unsigned>(numOfFiles);
        }
        catch (std::exception &ex) {
            LogIt::Error(ex);
        }
        return retVal;
    }

    std::vector<string> FileSystem::Expand(const string &$pattern) {
        Com::Wui::Framework::XCppCommons::System::IO::FileGlob fileGlob;
        std::vector<string> items{};
        fileGlob.setMatchHandler([&](const string &$file) {
            items.emplace_back($file);
        });
        fileGlob.MatchPattern($pattern);
        return items;
    }

    string FileSystem::getApplicationSupportPath(const bool $systemWide) {
        string applicationSupportPath;

#ifdef MAC_PLATFORM
        // nxf45876: We're using deprecated FS* family functions (instead of e.g. NSSearch* related) to be able to compile this code as C++,
        // so that we're not forcing Objective-C(++) as the only compilation language for OS X.
        #pragma GCC diagnostic push
        #pragma GCC diagnostic ignored "-Wdeprecated-declarations"

        FSRef ref = {0};

        if (FSFindFolder($systemWide ? kLocalDomain : kUserDomain, kApplicationSupportFolderType, kDontCreateFolder, &ref) == noErr) {
            char path[PATH_MAX] = {0};

            if (FSRefMakePath(&ref, reinterpret_cast<UInt8 *>(&path), PATH_MAX) == noErr) {
                applicationSupportPath = path;
            }
        }

        #pragma GCC diagnostic pop
#endif

        if (applicationSupportPath.empty()) {
            throw std::runtime_error("Failed to get the Application Support path");
        }

        return applicationSupportPath;
    }
}
