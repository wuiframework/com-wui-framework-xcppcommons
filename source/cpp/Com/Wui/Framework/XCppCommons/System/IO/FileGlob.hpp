/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_FILEGLOB_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_FILEGLOB_HPP_

namespace Com::Wui::Framework::XCppCommons::System::IO {
    /**
     * FileGlob class contains implementation for file path expansion with easy-minimatch filter for results.
     */
    class FileGlob {
     public:
        /**
         * Construction.
         */
        FileGlob();

        /**
         * Find all matching files.
         * @param $pattern Specify pattern.
         */
        void MatchPattern(const string &$pattern);

        /**
         * Adds pattern to ignore in glob algorithm.
         * @param $pattern Specify pattern to ignore.
         */
        void AddIgnorePattern(const string &$pattern);

        /**
         * @param $handler Specify no null function to handle matched file/directory path.
         */
        void setMatchHandler(const std::function<void(const string &$path)> &$handler);

     protected:
        bool MatchIgnorePattern(const string &$data);

        void Glob(const string &$pattern);

        bool WildMatch(const string &$pattern, const string &$item);

     private:
        std::vector<string> ignorePatterns{};
        std::function<void(const string &$pat)> matchHandler = nullptr;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_IO_FILEGLOB_HPP_
