/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef LINUX_PLATFORM

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    namespace fs = boost::filesystem;

    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    std::vector<TaskInfo> TaskManager::Find(const string &$pattern) {
        std::vector<TaskInfo> retVal;

        fs::path p("/proc");
        if (fs::exists(p)) {
            fs::directory_iterator end;
            for (fs::directory_iterator it(p); it != end; ++it) {
                if (fs::is_directory(it->path())) {
                    try {
                        std::ifstream comm(string(it->path().c_str()) + "/comm");
                        if (comm.is_open()) {
                            string name;
                            getline(comm, name);

                            if (boost::iequals(name, $pattern)) {
                                boost::system::error_code error_code;
                                const fs::path executablePath = fs::canonical(fs::path(it->path().string() + "/exe"), error_code);
                                if (!error_code) {
                                    const auto pid = boost::lexical_cast<unsigned int>(it->path().filename().c_str());
                                    retVal.emplace_back(pid, executablePath.string());
                                } else {
                                    LogIt::Error("Failed to the read the {0} symbolic link", executablePath.string());
                                }
                            }

                            comm.close();
                        }
                    } catch (const boost::bad_lexical_cast &$error) {
                        LogIt::Error("Directory '{0}' does not represent valid PID, error: {1}",
                                     it->path().filename().string(), $error.what());
                    } catch (const std::exception &$error) {
                        LogIt::Error($error);
                    }
                }
            }
        }

        return retVal;
    }
}

#endif  // LINUX_PLATFORM
