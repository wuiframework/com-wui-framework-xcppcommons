/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#if defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    bool TaskManager::Terminate(unsigned int $pid) {
        return kill($pid, SIGTERM) != -1;
    }
}

#endif  // defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
