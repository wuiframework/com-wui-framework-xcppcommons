/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {

    ExecuteOptions::ExecuteOptions()
            : maxBuffer(DEFAULT_MAX_BUFFER) {}

    ExecuteOptions::ExecuteOptions(const string &$cwd, std::map<string, string> const &$env, bool $shell, int $timeout, int $maxBuffer)
            : ProcessOptions($cwd, $env, $shell),
              timeout($timeout),
              maxBuffer($maxBuffer) {}

    ExecuteOptions::ExecuteOptions(const json &$options)
            : ProcessOptions($options),
              maxBuffer(DEFAULT_MAX_BUFFER) {
        if ($options.find("timeout") != $options.end()) {
            this->setTimeout($options["timeout"]);
        }
        if ($options.find("maxBuffer") != $options.end()) {
            this->setMaxBuffer($options["maxBuffer"]);
        }
    }

    int ExecuteOptions::getTimeout() const {
        return this->timeout;
    }

    void ExecuteOptions::setTimeout(int $timeout) {
        this->timeout = $timeout;
    }

    int ExecuteOptions::getMaxBuffer() const {
        return this->maxBuffer;
    }

    void ExecuteOptions::setMaxBuffer(int $maxBuffer) {
        this->maxBuffer = $maxBuffer;
    }

    ExecuteOptions::ExecuteOptions(const ExecuteOptions &$other) = default;

    ExecuteOptions::ExecuteOptions(ExecuteOptions &&$other) noexcept
            : ProcessOptions(std::move($other)),
              timeout($other.timeout),
              maxBuffer($other.maxBuffer) {}


    bool ExecuteOptions::operator==(const ExecuteOptions &$rhs) const {
        return static_cast<const ProcessOptions &>(*this) == static_cast<const ProcessOptions &>($rhs) &&
               timeout == $rhs.timeout &&
               maxBuffer == $rhs.maxBuffer;
    }

    bool ExecuteOptions::operator!=(const ExecuteOptions &$rhs) const {
        return !($rhs == *this);
    }

    string ExecuteOptions::ToString() const {
        json data = json::parse(ProcessOptions::ToString());
        data["timeout"] = this->timeout;
        data["maxBuffer"] = this->maxBuffer;
        return data.dump();
    }

    std::ostream &operator<<(std::ostream &$os, const ExecuteOptions &$options) {
        $os << $options.ToString();
        return $os;
    }

    ExecuteOptions &ExecuteOptions::operator=(ExecuteOptions const &$other) {
        if (&$other != this) {
            this->ProcessOptions::operator=($other);
            this->timeout = $other.timeout;
            this->maxBuffer = $other.maxBuffer;
        }
        return *this;
    }

    ExecuteOptions &ExecuteOptions::operator=(ExecuteOptions &&$other) noexcept {
        if (&$other != this) {
            this->ProcessOptions::operator=($other);
            this->timeout = $other.timeout;
            this->maxBuffer = $other.maxBuffer;
        }
        return *this;
    }
}
