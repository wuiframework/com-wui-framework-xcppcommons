/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_EXECUTEOPTIONS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_EXECUTEOPTIONS_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * ExecuteOptions class contains each properties for Child::Execute method.
     */
    class ExecuteOptions : public ProcessOptions {
     public:
        /**
         * Default buffer size for stdout and stderr.
         */
        static const int DEFAULT_MAX_BUFFER = 200 * 1024;

        /**
         * Constructs default (empty) options.
         */
        ExecuteOptions();

        /**
         * Constructs options from values.
         * @param $cwd Specify demand working directory.
         * @param $env Specify environment variables map. Empty map will be ignored, whole variables will be removed/replaced otherwise.
         * @param $shell Set to true to force use shell.
         * @param $timeout Specify timeout for child process.
         * @param $maxBuffer Specify maximal buffer size for stdout/stderr. If data exceeds process will be terminated with
         * return value ERROR_NOT_ENOUGH_MEMORY.
         */
        explicit ExecuteOptions(const string &$cwd, std::map<string, string> const &$env = {}, bool $shell = false, int $timeout = 0,
                                int $maxBuffer = DEFAULT_MAX_BUFFER);

        /**
         * Constructs options from JSON configuration.
         * @param $options Specify options.
         */
        explicit ExecuteOptions(const json &$options);

        /**
         * Copy constructor.
         */
        ExecuteOptions(ExecuteOptions const &$other);

        /**
         * Move constructor.
         */
        ExecuteOptions(ExecuteOptions &&$other) noexcept;

        /**
         * Copy operator.
         */
        ExecuteOptions &operator=(ExecuteOptions const &$other);

        /**
         * Move operator.
         */
        ExecuteOptions &operator=(ExecuteOptions &&$other) noexcept;

        /**
         * Default destructor
         */
        virtual ~ExecuteOptions() = default;

        bool operator==(const ExecuteOptions &$rhs) const;

        bool operator!=(const ExecuteOptions &$rhs) const;

        string ToString() const override;

        /**
         * @return Returns timeout.
         */
        int getTimeout() const;

        /**
         * @param $timeout Specify timeout.
         */
        void setTimeout(int $timeout);

        /**
         * @return Returns maximal size for stdout/stderr buffer.
         */
        int getMaxBuffer() const;

        /**
         * @param $maxBuffer Specify maximal size for stdout/stderr buffer.
         */
        void setMaxBuffer(int $maxBuffer);

        friend std::ostream &operator<<(std::ostream &$os, const ExecuteOptions &$options);

     public:
        int timeout = 0;
        int maxBuffer = DEFAULT_MAX_BUFFER;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_EXECUTEOPTIONS_HPP_
