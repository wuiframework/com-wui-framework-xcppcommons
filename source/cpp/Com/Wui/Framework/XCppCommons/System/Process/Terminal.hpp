/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TERMINAL_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TERMINAL_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * Terminal class provides static methods for various operations with system terminal and processes.
     */
    class Terminal : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                     private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
        typedef Com::Wui::Framework::XCppCommons::Interfaces::IResponse IResponse;
     public:
        /**
        * Create new child process and wait for process end.
        * @param $cmd Specify command, which should be executed.
        * @param $args Specify command args, which should be passed to the executed process.
        * @param $cwd Specify current working directory for process execution.
        * @param $callback Callback function to be called when the executed process finished.
        */
        static void Execute(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                            const function<void(int, const std::vector<string> &)> &$callback = nullptr);

        /**
         * Create new child process and wait for process end.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $cwd Specify current working directory for process execution.
         * @param $response Specify response.
         */
        static void Execute(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                            const shared_ptr<IResponse> &$response);

        /**
         * Create new child process and wait for process end.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $options Specify terminal options.
         * @param $callback Callback function to be called when the executed process finished.
         */
        static void Execute(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                            const function<void(int, const std::vector<string> &)> &$callback = nullptr);

        /**
         * Create new child process and wait for process end.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $options Specify terminal options.
         * @param $response Specify response.
         */
        static void Execute(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                            const shared_ptr<IResponse> &$response);

        /**
         * Create new child process and provide async stdout/stderr capture data for callback.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $cwd Specify current working directory for process execution.
         * @param $callback Callback function to be called when the spawned process finished.
         */
        static void Spawn(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                          const function<void(int, const std::vector<string> &$std)> &$callback = nullptr);

        /**
         * Create new child process and provide async stdout/stderr capture data for callback.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $cwd Specify current working directory for process execution.
         * @param $response Specify response.
         */
        static void Spawn(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                          const shared_ptr<IResponse> &$response);

        /**
         * Create new child process and provide async stdout/stderr capture data for callback.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $options Specify terminal options.
         * @param $callback Callback function to be called when the spawned process finished.
         */
        static void Spawn(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                          const function<void(int, const std::vector<string> &)> &$callback = nullptr);

        /**
         * Create new child process and provide async stdout/stderr capture data for callback.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $options Specify terminal options.
         * @param $response Specify response.
         */
        static void Spawn(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                          const shared_ptr<IResponse> &$response);

        /**
         * Open specified path.
         * @param $path Specify path, which should be opened.
         * @param $callback Callback function to be called when the executed process finished.
         */
        static void Open(const string &$path, const function<void(int)> &$callback = nullptr);

        /**
         * Open specified path.
         * @param $path Specify path, which should be opened.
         * @param $detach Specify true to start as detached process, false otherwise.
         * @param $callback Callback function to be called when the executed process finished, or could be ignored if $detach is set.
         */
        static void Open(const string &$path, bool $detach, const function<void(int)> &$callback = nullptr);

        /**
         * Open specified path.
         * @param $path Specify path, which should be opened.
         * @param $response Specify response.
         */
        static void Open(const string &$path, const shared_ptr<IResponse> &$response);

        /**
         * Open specified path.
         * @param $path Specify path, which should be opened.
         * @param $detach Specify true to start as detached process, false otherwise.
         * @param $response Specify response.
         */
        static void Open(const string &$path, bool $detach, const shared_ptr<IResponse> &$response);

        /**
         * Create new elevated child process.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $cwd Specify current working directory for process execution.
         * @param $callback Callback function to be called when the spawned process finished.
         */
        static void Elevate(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                            const function<void(int, const std::vector<string> &)> &$callback = nullptr);

        /**
         * Create new elevated child process.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $options Specify terminal options.
         * @param $callback Callback function to be called when the spawned process finished.
         */
        static void Elevate(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                            const function<void(int, const std::vector<string> &)> &$callback = nullptr);

        /**
         * Create new elevated child process.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $cwd Specify current working directory for process execution.
         * @param $response Specify response.
         */
        static void Elevate(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                            const shared_ptr<IResponse> &$response);

        /**
         * Create new elevated child process.
         * @param $cmd Specify command, which should be executed.
         * @param $args Specify command args, which should be passed to the executed process.
         * @param $options Specify terminal options.
         * @param $response Specify response.
         */
        static void Elevate(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                            const shared_ptr<IResponse> &$response);

        /**
         * Remove quotes from command path. Note that removing algorithm is not standardized and designed strictly
         * for common usages in WUI Framework based application.
         * @param $cmd Specify input command.
         * @return Returns command without quotes if remove was possible.
         */
        static string UnquoteCmdPath(const string &$cmd);

        /**
         * Resolve command path in specified working directory.
         * @param $cmd Specify command.
         * @param $cwd Specify working directory.
         * @return Returns resolved command path or empty if not found.
         */
        static string ResolveCmd(const string &$cmd, const string &$cwd);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TERMINAL_HPP_
