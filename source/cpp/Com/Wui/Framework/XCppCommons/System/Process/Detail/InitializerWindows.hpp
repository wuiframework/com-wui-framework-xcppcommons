/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERWINDOWS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERWINDOWS_HPP_

#ifdef WIN_PLATFORM

#include <boost/process/detail/handler.hpp>

#include "../../../Utils/LogIt.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process::Detail {
    /**
     * TerminalProcessInitializer provides custom initialization of executor from boost::process for Windows.
     */
    struct Initializer : ::boost::process::detail::handler_base {
        template<typename WindowsExecutor>
        void on_setup(WindowsExecutor &$executor) const {
            Com::Wui::Framework::XCppCommons::Utils::LogIt::Debug("Setting up Windows executor with PID {0} and executable {1}",
                                                                  std::to_string($executor.proc_info.dwProcessId),
                                                                  getExecutorExecutable($executor));
            $executor.creation_flags = CREATE_NO_WINDOW;
        }

        template<typename WindowsExecutor>
        void on_success(WindowsExecutor &$executor) const {
            Com::Wui::Framework::XCppCommons::Utils::LogIt::Info("Windows process {0} successfully launched with PID {1}",
                                                                 getExecutorExecutable($executor),
                                                                 std::to_string($executor.proc_info.dwProcessId));
        }

        template<typename WindowsExecutor>
        void on_error(WindowsExecutor &$executor, const std::error_code &$error) const {
            Com::Wui::Framework::XCppCommons::Utils::LogIt::Error("Windows process {0} not launched due to error: {1}",
                                                                  getExecutorExecutable($executor), $error.message());
        }

        template<typename WindowsExecutor>
        std::string getExecutorExecutable(const WindowsExecutor &$executor) const {
            return $executor.exe != nullptr ? $executor.exe : "(unknown)";
        }
    };
}

#endif  // WIN_PLATFORM

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_DETAIL_INITIALIZERWINDOWS_HPP_
