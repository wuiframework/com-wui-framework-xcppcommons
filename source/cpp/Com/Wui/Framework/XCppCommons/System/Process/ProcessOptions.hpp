/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_PROCESSOPTIONS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_PROCESSOPTIONS_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * ProcessOptions class is base for other child process configurations.
     */
    class ProcessOptions : public Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Construct default (empty) options.
         */
        ProcessOptions();

        /**
         * Constructs options from values.
         * @param $cwd Specify demand working directory.
         * @param $env Specify environment variables map.
         * @param $shell Set true to force run in shell, false otherwise.
         */
        explicit ProcessOptions(const string &$cwd, const std::map<string, string> &$env = {}, bool $shell = false);

        /**
         * Constructs options from JSON options.
         * @param $options Specify options.
         */
        explicit ProcessOptions(const json &$options);

        /**
         * Copy constructor.
         */
        ProcessOptions(ProcessOptions const &$other);

        /**
         * Move constructor.
         */
        ProcessOptions(ProcessOptions &&$other) noexcept;

        /**
         * Copy operator.
         */
        ProcessOptions &operator=(ProcessOptions const &$other);

        /**
         * Move operator.
         */
        ProcessOptions &operator=(ProcessOptions &&$other) noexcept;

        bool operator==(const ProcessOptions &$rhs) const;

        bool operator!=(const ProcessOptions &$rhs) const;

        /**
         * Default destructor.
         */
        virtual ~ProcessOptions() = default;

        string ToString() const override;

        /**
         * @return Returns working directory.
         */
        const string &getCwd() const;

        /**
         * @param $cwd Specify working directory.
         */
        void setCwd(const string &$cwd);

        /**
         * Returns environment variables map.
         */
        const std::map<string, string> &getEnv() const;

        /**
         * @param $env Specify environment variables map.
         */
        void setEnv(const std::map<string, string> &$env);

        /**
         * @return Returns true if shell is required, false otherwise.
         */
        bool isShell() const;

        /**
         * @param $shell Specify true to force run in shell, false otherwise.
         */
        void setShell(bool $shell);

        /**
         * @return Returns true if verbose process is required [default], false otherwise.
         */
        bool isVerbose() const;

        /**
         * @param verbose Specify false to disable verbose for process, true to enable [default]
         */
        void setVerbose(bool $verbose);

        friend std::ostream &operator<<(std::ostream &$os, const ProcessOptions &$data);

     private:
        string cwd = "";
        std::map<string, string> env;
        bool shell = false;
        bool verbose = true;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_PROCESSOPTIONS_HPP_
