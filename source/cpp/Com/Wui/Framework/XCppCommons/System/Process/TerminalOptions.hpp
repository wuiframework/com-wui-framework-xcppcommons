/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TERMINALOPTIONS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TERMINALOPTIONS_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * TerminalOptions wraps general ProcessOptions only for Terminal methods.
     */
    class TerminalOptions : public Com::Wui::Framework::XCppCommons::System::Process::ProcessOptions {
     public:
        /**
         * Constructs default (empty) options.
         */
        TerminalOptions();

        /**
         * Constructs from JSON options.
         * @param $options Specify options.
         */
        explicit TerminalOptions(const json &$options);

        /**
         * Constructs from values.
         * @param $cwd Specify demand working directory.
         * @param $env Specify environment variables map. Empty map will be ignored, whole variables will be removed/replaced otherwise.
         * @param $shell Set to true to force use shell.
         */
        explicit TerminalOptions(const string &$cwd, const std::map<string, string> &$env = {}, bool $shell = false);

        bool isDetached() const;

        void setDetached(bool $detached);

        TerminalOptions(const TerminalOptions &$other);

        TerminalOptions(TerminalOptions &&$other) noexcept;

        TerminalOptions &operator=(TerminalOptions const &$other);

        TerminalOptions &operator=(TerminalOptions &&$other) noexcept;

        string ToString() const override;

        friend std::ostream &operator<<(std::ostream &$os, const TerminalOptions &$options);

     private:
        bool detached = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TERMINALOPTIONS_HPP_
