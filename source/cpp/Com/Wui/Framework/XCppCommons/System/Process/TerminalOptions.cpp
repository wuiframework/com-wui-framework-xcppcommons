/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {

    TerminalOptions::TerminalOptions() = default;

    TerminalOptions::TerminalOptions(const json &$options)
            : ProcessOptions($options) {
        if ($options.find("detached") != $options.end()) {
            this->setDetached($options["detached"]);
        }
    }

    TerminalOptions::TerminalOptions(const string &$cwd, const std::map<string, string> &$env, bool $shell)
            : ProcessOptions($cwd, $env, $shell) {}

    TerminalOptions::TerminalOptions(const TerminalOptions &$other) = default;

    TerminalOptions::TerminalOptions(TerminalOptions &&$other) noexcept
            : ProcessOptions(std::move($other)),
              detached($other.detached) {
    }

    bool TerminalOptions::isDetached() const {
        return this->detached;
    }

    void TerminalOptions::setDetached(bool $detached) {
        this->detached = $detached;
    }

    TerminalOptions &TerminalOptions::operator=(TerminalOptions const &$other) {
        if (&$other != this) {
            this->ProcessOptions::operator=($other);
            this->detached = $other.detached;
        }
        return *this;
    }

    TerminalOptions &TerminalOptions::operator=(TerminalOptions &&$other) noexcept {
        if (&$other != this) {
            this->ProcessOptions::operator=($other);
            this->detached = $other.detached;
        }
        return *this;
    }

    string TerminalOptions::ToString() const {
        json data = json::parse(ProcessOptions::ToString());
        data["detached"] = this->detached;
        return data.dump();
    }

    std::ostream &operator<<(std::ostream &$os, const TerminalOptions &$options) {
        $os << $options.ToString();
        return $os;
    }
}
