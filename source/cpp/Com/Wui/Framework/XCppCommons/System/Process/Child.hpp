/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_CHILD_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_CHILD_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * This class provides low-level API for various operations with child processes.
     */
    class Child : public Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Spawns a shell then executes the $cmd within that shell.
         * @param $cmd The command to run with space-separated arguments.
         * @param $options Specify execute options.
         * @param $callback Called with outputs when process terminates.
         * @return Returns pointer to child process instance.
         */
        static shared_ptr<Child> Execute(const string &$cmd, const ExecuteOptions &$options = {},
                                         const function<void(int, const string &, const string &)> &$callback = nullptr);

        /**
         * Spawns new process using the given command with command line arguments in args.
         * @param $cmd The command to spawn.
         * @param $args Specify command line arguments.
         * @param $options Specify spawn options.
         * @return Returns pointer to child process instance.
         */
        static shared_ptr<Child> Spawn(const string &$cmd, const std::vector<string> &$args = {}, const SpawnOptions &$options = {});

        /**
         * Disconnect stdout and stderr from process. Ignored for detached process.
         * @return Returns true if succeed, false otherwise.
         */
        bool Disconnect();

        /**
         * Kill child process. Ignored for detached process.
         * @return Returns true if succeed, false otherwise.
         */
        bool Kill();

        /**
         * Wait while child process is running. This will block thread until child process exit, so handlers registration
         * must be done before this call.
         * Could be ignored if child is attached to custom io_service and io_service::run() is called outside.
         */
        void WaitForExit();

        /**
         * @return Returns true if it is still possible to receive messages from child process,
         * otherwise it is no longer possible to receive std:out/err.
         */
        bool isConnected() const;

        /**
         * @return Returns PID of currently created child process.
         */
        int getPid() const;

        /**
         * @return Returns reference to child's io_service.
         */
        const boost::asio::io_service &getIoService() const;

        /**
         * Register handler for on stdout data.
         * @param $onStdOutData Specify handler.
         * @return Returns pointer to this child process instance.
         */
        Child *setOnStdOutData(const function<void(const string &)> &$onStdOutData);

        /**
         * Register handler for on stderr data.
         * @param $onStdErrData Specify handler.
         * @return Returns pointer to this child process instance.
         */
        Child *setOnStdErrData(const function<void(const string &)> &$onStdErrData);

        /**
         * Register handler for on close.
         * @param $onClose Specify handler.
         * @return Returns pointer to this child process instance.
         */
        Child *setOnClose(const function<void(int)> &$onClose);

        /**
         * Register handler for on error.
         * @param $onError Specify handler.
         * @return Returns pointer to this child process instance.
         */
        Child *setOnError(const function<void(const string &)> &$onError);

        /**
         * Register handler for on exit.
         * @param $onExit Specify handler.
         * @return Returns pointer to this child process instance.
         */
        Child *setOnExit(const function<void(int)> &$onExit);

     protected:
        void ExecuteIn(const string &$cmd, const ExecuteOptions &$options,
                       const function<void(int, const string &, const string &)> &$callback);

        void SpawnIn(const string &$cmd, const std::vector<string> &$args, const SpawnOptions &$options);

     private:
        function<void(const string &)> onStdOutData = nullptr;
        function<void(const string &)> onStdErrData = nullptr;
        function<void(int)> onClose = nullptr;
        function<void(const string &)> onError = nullptr;
        function<void(int)> onExit = nullptr;
        function<void(int, const string &, const string &)> callback = nullptr;
        bool connected = false;
        boost::asio::io_service ioService{};
        boost::asio::streambuf stdOutBuffer{}, stdErrBuffer{};
        unique_ptr<boost::process::async_pipe> stdOutPipe = nullptr, stdErrPipe = nullptr, stdInPipe = nullptr;
        boost::process::child childProcess{};
        ProcessOptions options{};

        string stdOut = "";
        string stdErr = "";
        int exitCode = -1;

        void onExitHandler(int $exitCode);

        void handleReadStdOut(const boost::system::error_code &$err, std::size_t $bytes);

        void handleReadStdErr(const boost::system::error_code &$err, std::size_t $bytes);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_CHILD_HPP_
