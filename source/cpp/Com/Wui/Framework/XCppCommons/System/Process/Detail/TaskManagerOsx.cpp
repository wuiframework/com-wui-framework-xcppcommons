/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef MAC_PLATFORM

#include <errno.h>
#include <libproc.h>

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    std::vector<TaskInfo> TaskManager::Find(const string &$pattern) {
        std::vector<TaskInfo> retVal;

        const int pidCount = proc_listallpids(nullptr, 0);
        if (pidCount > 0) {
            // cannot use variable length arrays (static analyzer forbids these), otherwise we'd use "pidCount" for size
            int pidBuffer[1024] = {0};
            proc_listallpids(&pidBuffer, pidCount);

            for (auto i = 0; i < pidCount; ++i) {
                if (pidBuffer[i] > 0) {
                    char processName[PATH_MAX] = {0};
                    const auto processNameLength = proc_name(pidBuffer[i], &processName, sizeof(processName));
                    if (processNameLength > 0) {
                        string name(processName, processNameLength);

                        if (name == $pattern) {
                            char processPath[PROC_PIDPATHINFO_MAXSIZE] = {0};
                            const auto processPathLength = proc_pidpath(pidBuffer[i], &processPath, sizeof(processPath));
                            if (processPathLength > 0) {
                                string path(processPath, processPathLength);
                                retVal.emplace_back(static_cast<unsigned int>(pidBuffer[i]), std::move(path));
                            } else {
                                LogIt::Error("Failed to resolve process path for {0} with PID {1}: {2}",
                                             name, std::to_string(pidBuffer[i]), strerror(errno));
                            }
                        }
                    } else {
                        // this is normal on OS X, we cannot access some processes related to OS
                        if (errno == EPERM) {
                            LogIt::Debug("Not permitted to resolve process name from PID {0}", std::to_string(pidBuffer[i]));
                        } else {
                            LogIt::Warning("Failed to resolve process name from PID {0}: {1}", std::to_string(pidBuffer[i]),
                                           strerror(errno));
                        }
                    }
                }
            }
        } else {
            LogIt::Error("Failed to get list of PIDs");
        }

        return retVal;
    }
}

#endif  // MAC_PLATFORM
