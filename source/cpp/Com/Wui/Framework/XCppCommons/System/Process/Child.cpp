/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <boost/iostreams/device/file_descriptor.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/filtering_stream.hpp>

#include "../../sourceFilesMap.hpp"

#ifdef WIN_PLATFORM

#include <shellapi.h>  // NOLINT

#endif

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Events::IntervalTimer;
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;
    using Com::Wui::Framework::XCppCommons::System::IO::Path;
    using Com::Wui::Framework::XCppCommons::Primitives::String;
    using Com::Wui::Framework::XCppCommons::Primitives::ArrayList;
    using Com::Wui::Framework::XCppCommons::System::Environment;

    namespace bp = boost::process;

#ifdef WIN_PLATFORM
    string shellCmd = "cmd.exe /d /s /c \"{0}\"";
#else
    string shellCmd = "/bin/sh -c \"{0}\"";
#endif

    string getOutputString(boost::asio::streambuf &$buffer) {
        std::istream inputStream(&$buffer);
        std::ostringstream outputStream;
        outputStream << inputStream.rdbuf();
        return outputStream.str();
    }

    bp::environment getEnvironment(std::map<string, string> const &$data) {
        bp::environment retEnv;
        bp::native_environment native;

        for (bp::native_environment::iterator it = native.begin(); it != native.end(); ++it) {
            retEnv[it->get_name()].assign(it->to_vector());
        }

        string tmp;
        if (!$data.empty()) {
            std::vector<string> paths;
            std::for_each($data.begin(), $data.end(), [&](std::pair<const string, string> const &$item) {
                if (!boost::iequals(boost::to_upper_copy($item.first), "PATH")) {
                    retEnv[$item.first] = boost::trim_copy($item.second);
                    tmp += $item.first + "=" + Environment::Expand($item.second) + "\n";
                } else {
                    paths.push_back($item.second);
                }
            });
            if (!paths.empty()) {
                retEnv.erase("Path");
                retEnv.erase("path");
                string path = Environment::Expand(std::accumulate(paths.begin(), paths.end(), string(), [](string a, string b) {
                    if (a.empty()) {
                        return b;
                    }
                    return a + ";" + b;
                }));
                retEnv["PATH"] = path;
                tmp += "PATH=" + path + "\n";
            }
            LogIt::Debug("Updated env: \n\t{0}", tmp);
        } else {
            LogIt::Debug("No environment variable needs update.");
        }

        return retEnv;
    }

    shared_ptr<Child> Child::Execute(const string &$cmd, const ExecuteOptions &$options,
                                     const function<void(int, const string &, const string &)> &$callback) {
        shared_ptr<Child> process(new Child());

        process->ExecuteIn($cmd, $options, $callback);

        return process;
    }

    shared_ptr<Child> Child::Spawn(const string &$cmd, const std::vector<string> &$args, const SpawnOptions &$options) {
        shared_ptr<Child> process(new Child());

        process->SpawnIn($cmd, $args, $options);

        return process;
    }

    bool Child::Kill() {
        bool status = false;
        if (this->childProcess.valid() && this->childProcess.running()) {
            this->Disconnect();
            this->childProcess.terminate();
            status = true;
        }
        return status;
    }

    bool Child::Disconnect() {
        if (this->stdOutPipe && this->stdOutPipe->is_open()) {
            this->stdOutPipe->close();
        }
        if (this->stdErrPipe && this->stdErrPipe->is_open()) {
            this->stdErrPipe->close();
        }
        return false;
    }

    bool Child::isConnected() const {
        return this->connected;
    }

    int Child::getPid() const {
        return static_cast<int>(this->childProcess.id());
    }

    void Child::WaitForExit() {
        this->ioService.run();

        this->childProcess.wait();
        if (this->exitCode == -1) {
            this->exitCode = this->childProcess.exit_code();
        }
        if (this->onExit) {
            this->onExit(this->exitCode);
        }
        this->Disconnect();
        if (this->onClose) {
            this->onClose(this->exitCode);
        }
    }

    Child *Child::setOnStdOutData(const function<void(const string &)> &$onStdOutData) {
        this->onStdOutData = $onStdOutData;
        return this;
    }

    Child *Child::setOnStdErrData(const function<void(const string &)> &$onStdErrData) {
        this->onStdErrData = $onStdErrData;
        return this;
    }

    Child *Child::setOnClose(const function<void(int)> &$onClose) {
        this->onClose = $onClose;
        return this;
    }

    Child *Child::setOnError(const function<void(const string &)> &$onError) {
        this->onError = $onError;
        return this;
    }

    Child *Child::setOnExit(const function<void(int)> &$onExit) {
        this->onExit = $onExit;
        return this;
    }

    const boost::asio::io_service &Child::getIoService() const {
        return this->ioService;
    }

    void Child::onExitHandler(int $exitCode) {
        if (this->callback) {
            if (this->options.isVerbose()) {
                string out = this->stdOut.substr(0, 1024);
                if (this->stdOut.size() > 1024) {
                    out += "\n\n>>...(+" + std::to_string(this->stdOut.size() - 1024) + " chars)";
                }
                LogIt::Info("{0}\n{1}\nexit code: {2}", out, this->stdErr, $exitCode);
            }
            this->callback($exitCode, this->stdOut, this->stdErr);
            this->callback = nullptr;
        }
    }

    void Child::ExecuteIn(const string &$cmd, const ExecuteOptions &$options,
                          const function<void(int $exitCode, const string &$stdOut, const string &$stdErr)> &$callback) {
        this->stdOutPipe = std::make_unique<boost::process::async_pipe>(this->ioService);
        this->stdErrPipe = std::make_unique<boost::process::async_pipe>(this->ioService);
        this->stdInPipe = std::make_unique<boost::process::async_pipe>(this->ioService);
        this->callback = $callback;
        this->options = $options;

        this->setOnStdOutData([&](auto $data) {
            this->stdOut += $data;
            if (static_cast<int>(this->stdOut.size()) >= $options.getMaxBuffer()) {
                LogIt::Error("Execute stdout buffer size ({0}) exceeds maximum ({1})", this->stdOut.size(),
                             $options.getMaxBuffer());
                this->Kill();
                this->exitCode = OUT_OF_MEM;
            }
        });
        this->setOnStdErrData([&](auto $data) {
            this->stdErr += $data;
            if (static_cast<int>(this->stdErr.size()) >= $options.getMaxBuffer()) {
                LogIt::Error("Execute stderr buffer size ({0}) exceeds maximum ({1})", this->stdErr.size(),
                             $options.getMaxBuffer());
                this->Kill();
                this->exitCode = OUT_OF_MEM;
            }
        });
        this->setOnClose([&](int $exitCode) {
            this->onExitHandler($exitCode);
        });

        try {
            string cwd = $options.getCwd();
            string cmd = String::Format(shellCmd, $cmd);

            if (cwd.empty()) {
                cwd = ".";
            }
            cwd = FileSystem::ResolvePath(cwd);

            LogIt::Debug("Executing process {0} in {1}\nwith:\n{2}", cmd, cwd, $options);
            this->childProcess = bp::child(cmd,
                                           Com::Wui::Framework::XCppCommons::System::Process::Detail::Initializer(),
                                           bp::detail::start_dir_()(cwd),
                                           getEnvironment($options.getEnv()),
                                           bp::std_out > *this->stdOutPipe,
                                           bp::std_err > *this->stdErrPipe,
                                           bp::std_in < *this->stdInPipe,
                                           this->ioService);
            if (this->childProcess.valid()) {
                handleReadStdOut({}, 0);
                handleReadStdErr({}, 0);
            } else {
                LogIt::Error("Created child process is not valid. ({0})", this->childProcess.id());
            }
        } catch (std::exception &$ex) {
            LogIt::Error($ex);
            if (this->onError) {
                this->onError($ex.what());
            }
        }
    }

    void Child::SpawnIn(const string &$cmd, const std::vector<string> &$args, const SpawnOptions &$options) {
        this->stdOutPipe = std::make_unique<boost::process::async_pipe>(this->ioService);
        this->stdErrPipe = std::make_unique<boost::process::async_pipe>(this->ioService);
        this->stdInPipe = std::make_unique<boost::process::async_pipe>(this->ioService);
        this->options = $options;

        try {
            string cmd = $cmd;
            string cwd = $options.getCwd();

            if (cwd.empty()) {
                cwd = ".";
            }
            cwd = FileSystem::ResolvePath(cwd);

            const std::vector<string> &args = $args;

            if ($options.isShell()) {
                cmd = String::Format(shellCmd, Path::Quote(cmd) + " " + ArrayList::Join(args));
            } else {
                if (!FileSystem::Exists(cmd)) {
                    cmd = Path::Quote(cwd + "/" + cmd);
                }
                cmd += " " + ArrayList::Join(args);
            }

            if (!$options.isDetached()) {
                LogIt::Debug("Spawning process {0} in {1}\nwith:\n{2}", cmd, cwd, $options);
                this->childProcess = bp::child(cmd, Com::Wui::Framework::XCppCommons::System::Process::Detail::Initializer(),
                                               bp::detail::start_dir_()(cwd),
                                               getEnvironment($options.getEnv()),
                                               bp::std_out > *this->stdOutPipe,
                                               bp::std_err > *this->stdErrPipe,
                                               bp::std_in < *this->stdInPipe,
                                               this->ioService);

                if (this->childProcess.valid()) {
                    Child::handleReadStdOut({}, 0);
                    Child::handleReadStdErr({}, 0);
                } else {
                    LogIt::Error("Created child process is not valid. ({0})", this->childProcess.id());
                }
            } else {
                LogIt::Debug("Starting detached process {0} in {1}", cmd, cwd);
                this->childProcess = bp::child(cmd, Com::Wui::Framework::XCppCommons::System::Process::Detail::Initializer(),
                                               bp::detail::start_dir_()(cwd),
                                               getEnvironment($options.getEnv()));
                this->childProcess.detach();
            }
        } catch (std::exception &$ex) {
            LogIt::Error($ex);
            if (this->onError) {
                this->onError($ex.what());
            }
        }
    }

    void Child::handleReadStdOut(const boost::system::error_code &$err, std::size_t $bytes) {
        if ($bytes > 0) {
            string data = getOutputString(this->stdOutBuffer);
            if (this->onStdOutData) {
                this->onStdOutData(data);
            }
        }

        if (!$err) {
            if (this->stdOutPipe != nullptr) {
                boost::asio::async_read(*this->stdOutPipe, this->stdOutBuffer, boost::asio::transfer_at_least(1),
                                        boost::bind(&Child::handleReadStdOut, this, boost::asio::placeholders::error,
                                                    boost::asio::placeholders::bytes_transferred));
            }
        }
    }

    void Child::handleReadStdErr(const boost::system::error_code &$err, std::size_t $bytes) {
        if ($bytes > 0) {
            string data = getOutputString(this->stdErrBuffer);
            if (this->onStdErrData) {
                this->onStdErrData(data);
            }
        }

        if (!$err) {
            boost::asio::async_read(*this->stdErrPipe, this->stdErrBuffer, boost::asio::transfer_at_least(1),
                                    boost::bind(&Child::handleReadStdErr, this, boost::asio::placeholders::error,
                                                boost::asio::placeholders::bytes_transferred));
        }
    }
}
