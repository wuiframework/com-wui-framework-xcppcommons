/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {

    ProcessOptions::ProcessOptions()
            : cwd(""),
              env({}) {}

    ProcessOptions::ProcessOptions(const string &$cwd, const std::map<string, string> &$env, bool $shell)
            : cwd($cwd),
              env($env),
              shell($shell) {}

    ProcessOptions::ProcessOptions(const json &$options)
            : cwd(""),
              env({}) {
        if ($options.find("cwd") != $options.end()) {
            this->setCwd($options["cwd"]);
        }
        if ($options.find("env") != $options.end()) {
            json envs = $options["env"];
            this->env.clear();
            if (!envs.empty() && envs.is_object()) {
                for (json::const_iterator it = envs.begin(); it != envs.end(); ++it) {
                    this->env.emplace(it.key(), it.value());
                }
            }
        }
        if ($options.find("shell") != $options.end()) {
            this->setShell($options["shell"]);
        }
        if ($options.find("verbose") != $options.end()) {
            this->setVerbose($options["verbose"]);
        }
    }

    const string &ProcessOptions::getCwd() const {
        return this->cwd;
    }

    void ProcessOptions::setCwd(const string &$cwd) {
        this->cwd = $cwd;
    }

    const std::map<string, string> &ProcessOptions::getEnv() const {
        return this->env;
    }

    void ProcessOptions::setEnv(const std::map<string, string> &$env) {
        this->env = $env;
    }

    bool ProcessOptions::isShell() const {
        return this->shell;
    }

    void ProcessOptions::setShell(bool $shell) {
        this->shell = $shell;
    }

    ProcessOptions::ProcessOptions(ProcessOptions const &$other)
            : cwd($other.cwd),
              env($other.env),
              shell($other.shell) {
    }

    ProcessOptions::ProcessOptions(ProcessOptions &&$other) noexcept
            : cwd(std::move($other.cwd)),
              env(std::move($other.env)),
              shell($other.shell) {
    }

    ProcessOptions &ProcessOptions::operator=(ProcessOptions const &$other) {
        if (&$other != this) {
            this->cwd = $other.cwd;
            this->env = $other.env;
            this->shell = $other.shell;
            this->verbose = $other.verbose;
        }
        return *this;
    }

    ProcessOptions &ProcessOptions::operator=(ProcessOptions &&$other) noexcept {
        if (&$other != this) {
            this->cwd = std::move($other.cwd);
            this->env = std::move($other.env);
            this->shell = $other.shell;
            this->verbose = $other.verbose;
        }
        return *this;
    }

    bool ProcessOptions::operator==(const ProcessOptions &$rhs) const {
        return boost::iequals(cwd, $rhs.cwd) &&
               Com::Wui::Framework::XCppCommons::Primitives::ArrayList::Equal(env, $rhs.env) &&
               (shell == $rhs.shell);
    }

    bool ProcessOptions::operator!=(const ProcessOptions &$rhs) const {
        return !($rhs == *this);
    }

    std::ostream &operator<<(std::ostream &$os, const ProcessOptions &$data) {
        $os << $data.ToString();
        return $os;
    }

    string ProcessOptions::ToString() const {
        return json({{"cwd",     this->cwd},
                     {"env",     this->env},
                     {"shell",   this->shell},
                     {"verbose", this->verbose}}).dump();
    }

    bool ProcessOptions::isVerbose() const {
        return this->verbose;
    }

    void ProcessOptions::setVerbose(bool $verbose) {
        this->verbose = $verbose;
    }
}
