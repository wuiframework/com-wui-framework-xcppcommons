/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {

    TaskInfo::TaskInfo(unsigned int $pid, const string &$path) {
        this->pid = $pid;
        this->path = $path;
    }

    unsigned int TaskInfo::getPid() const {
        return this->pid;
    }

    const string &TaskInfo::getPath() const {
        return this->path;
    }

    bool TaskInfo::operator==(const TaskInfo &$rhs) const {
        return pid == $rhs.pid;
    }

    bool TaskInfo::operator!=(const TaskInfo &$rhs) const {
        return !($rhs == *this);
    }

    std::ostream &operator<<(std::ostream &$os, const TaskInfo &$info) {
        $os << "pid: " << $info.pid << " path: " << $info.path;
        return $os;
    }
}
