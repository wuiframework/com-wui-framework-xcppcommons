/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

#ifdef WIN_PLATFORM

#include <shellapi.h>  // NOLINT
#include <dshow.h>

#endif

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::System::IO::FileSystem;
    using Com::Wui::Framework::XCppCommons::System::IO::Path;
    using Com::Wui::Framework::XCppCommons::Events::IntervalTimer;
    using Com::Wui::Framework::XCppCommons::Primitives::String;
    using Com::Wui::Framework::XCppCommons::Primitives::ArrayList;
    using Com::Wui::Framework::XCppCommons::Interfaces::IResponse;
    using Com::Wui::Framework::XCppCommons::System::ResponseApi::ResponseFactory;

    namespace fs = boost::filesystem;

    /** tools **/
    string SearchExecutableInPath(const string &$fileName, const string &$path = "") {
#ifdef WIN_PLATFORM
        string path = $path;
        if (path.empty()) {
            path = getenv("PATH");
            if (path.empty()) {
                throw std::runtime_error("Environment variable PATH not found.");
            }
        }

        typedef boost::tokenizer<boost::char_separator<char>> tokenizer;
        boost::char_separator<char> sep(";");
        tokenizer tok(path, sep);
        for (tokenizer::iterator it = tok.begin(); it != tok.end(); ++it) {
            boost::filesystem::path p = *it;
            p /= $fileName;
            boost::array<string, 6> extensions =
                    {"", ".exe", ".com", ".bat", ".cmd", ".msc"};
            for (auto &extension : extensions) {
                boost::filesystem::path p2 = p;
                p2 += extension;
                boost::system::error_code ec;
                bool file = boost::filesystem::is_regular_file(p2, ec);

                if (!ec && file && (SHGetFileInfoA(p2.string().c_str(), 0, nullptr, 0, SHGFI_EXETYPE) != 0u)) {
                    return p2.string();
                }
            }
        }
#else
        fs::path path;
        if ($path.empty()) {
            path = boost::process::search_path($fileName);
        } else {
            path = boost::process::search_path($fileName, {$path});
        }
        return path.string();
#endif
        return "";
    }

    bool CheckIfWuiApp(const string &$path) {
        bool retVal = false;
#ifdef WIN_PLATFORM
        if (FileSystem::Exists($path)) {
            string path = FileSystem::NormalizePath($path);
            DWORD versionInfoSize = GetFileVersionInfoSizeA(path.c_str(), nullptr);
            auto *versionInfoBuffer = new BYTE[versionInfoSize];
            GetFileVersionInfoA(path.c_str(), 0, versionInfoSize, versionInfoBuffer);

            typedef struct {
                WORD wLanguage;
                WORD wCodePage;
            } LanguageCodePage;
            UINT translateInfoSize;
            LanguageCodePage *translateInfo;

            VerQueryValueA(versionInfoBuffer,
                           "\\VarFileInfo\\Translation",
                           reinterpret_cast<LPVOID *>(&translateInfo),
                           &translateInfoSize);

            for (int i = 0; i < static_cast<int>(translateInfoSize / sizeof(LanguageCodePage)); i++) {
                char subBlock[256];
                if (SUCCEEDED(StringCchPrintfA(subBlock, 255,
                                               "\\StringFileInfo\\%04x%04x\\BaseProjectName",
                                               translateInfo[i].wLanguage,
                                               translateInfo[i].wCodePage))) {
                    LPSTR value;
                    UINT inNameSize;
                    if (VerQueryValueA(versionInfoBuffer, subBlock, reinterpret_cast<LPVOID *>(&value), &inNameSize) != 0) {
                        string str(value, inNameSize);
                        if (String::StartsWith(str, "com-wui-framework-")) {
                            retVal = true;
                        }
                    }
                }
            }
            delete[] versionInfoBuffer;
        }
#endif
        return retVal;
    }

    /** end tools **/

    void Terminal::Execute(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                           const function<void(int, const std::vector<string> &)> &$callback) {
        Terminal::Execute($cmd, $args, TerminalOptions($cwd), $callback);
    }

    void Terminal::Execute(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                           const shared_ptr<IResponse> &$response) {
        Terminal::Execute($cmd, $args, TerminalOptions($cwd), $response);
    }

    void Terminal::Execute(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                           const function<void(int, const std::vector<string> &)> &$callback) {
        Terminal::Execute($cmd, $args, $options, ResponseFactory::getResponse($callback));
    }

    void Terminal::Execute(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                           const shared_ptr<IResponse> &$response) {
        int exitCode = -1;
        string cmd = $cmd;
        string cwd = String::Remove($options.getCwd(), "\"");
        const TerminalOptions &options = $options;

        if (cwd.empty()) {
            cwd = ".";
        }
        cwd = FileSystem::ResolvePath(cwd);

        if (!$options.isShell()) {
            cmd = Terminal::ResolveCmd(cmd, cwd);
        }

        std::vector<string> args = $args;
        if (cmd.empty() || options.isShell()) {
#ifdef WIN_PLATFORM
            cmd = "\"" + SearchExecutableInPath("cmd") + "\"";
            args.insert(args.begin(), $cmd);
            args.insert(args.begin(), "/d /s /c \"");
            args.emplace_back("\"");
#else
            args.insert(args.begin(), $cmd);
#endif
        } else {
            if (CheckIfWuiApp(cmd)) {
                LogIt::Debug("Executing command has been found as WUI Framework application. Adding \"--disable-console\" argument.");
                args.emplace_back("--disable-console");
            }
            cmd = Path::Quote(cmd);
        }

        if ($options.isVerbose()) {
            LogIt::Info("{2} > {0} {1}", cmd, ArrayList::Join(args), cwd);
        }

        cmd = cmd + " " + ArrayList::Join(args);

        int counter = 0;
        shared_ptr<IntervalTimer> progress = nullptr;
        boost::asio::io_service progService;
        shared_ptr<boost::thread> progThread;
        auto progressSet = [&]() {
            progress.reset(new IntervalTimer(boost::posix_time::millisec{250}, [&]() {
                $response->OnChange(string("."));
                if (counter++ >= 100) {
                    counter = 0;
                }
            }, &progService));
            progress->Start();
            progThread.reset(new boost::thread(boost::bind(&boost::asio::io_service::run, &progService)));
        };
        auto progressClear = [&]() {
            if (progress != nullptr) {
                progress->Stop();
                progress.reset();
                if (progThread != nullptr) {
                    progThread->join();
                    progThread.reset();
                }
            }
        };

        auto onExitHandler = [&](int $exitCode, const string &$stdOut, const string &$stdErr) {
            progressClear();
            LogIt::Debug("{0}\n{1}\nexit code: {2}", $stdOut, $stdErr, exitCode);
            $response->OnComplete(exitCode, {$stdOut, $stdErr});
        };

        $response->OnStart();
        try {
            LogIt::Debug("Executing process {0} in {1}", cmd, cwd);
            ExecuteOptions opts{json::parse(options.ToString())};

            auto childProcess = Child::Execute(cmd, opts, onExitHandler);
            childProcess->setOnExit([&](int $exitCode) {
                exitCode = $exitCode;
            });
            progressSet();
            childProcess->WaitForExit();
        } catch (std::exception &$ex) {
            LogIt::Error($ex);
        }
    }

    void Terminal::Spawn(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                         const function<void(int, const std::vector<string> &$std)> &$callback) {
        Terminal::Spawn($cmd, $args, TerminalOptions($cwd), $callback);
    }

    void Terminal::Spawn(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                         const shared_ptr<IResponse> &$response) {
        Terminal::Spawn($cmd, $args, TerminalOptions($cwd), $response);
    }

    void Terminal::Spawn(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                         const function<void(int, const std::vector<string> &$std)> &$callback) {
        Terminal::Spawn($cmd, $args, $options, ResponseFactory::getResponse($callback));
    }

    void Terminal::Spawn(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                         const shared_ptr<IResponse> &$response) {
        string cmd = $cmd;
        string cwd = String::Remove($options.getCwd(), "\"");
        const TerminalOptions &options = $options;
        string stdOut, stdErr;

        if (cwd.empty()) {
            cwd = ".";
        }
        cwd = FileSystem::ResolvePath(cwd);

        if (!$options.isShell()) {
            cmd = Terminal::ResolveCmd(cmd, cwd);
        }

        SpawnOptions opts{json::parse(options.ToString())};
        opts.setCwd(cwd);

        std::vector<string> args = $args;
        if (cmd.empty()) {
            cmd = $cmd;
            opts.setShell(true);
        } else {
            if (CheckIfWuiApp(cmd)) {
                LogIt::Debug("Executing command has been found as WUI Framework application. Adding \"--disable-console\" argument.");
                args.emplace_back("--disable-console");
            }
            cmd = Path::Quote(cmd);
        }

        if ($options.isVerbose()) {
            LogIt::Info("{2} > {0} {1}", cmd, ArrayList::Join(args), cwd);
        }

        bool fired = false;
        auto onExitHandler = [&](int $exitCode) {
            if (!fired) {
                fired = true;
                LogIt::Debug("{0}\n{1}\nexit code: {2}", stdOut, stdErr, $exitCode);
                try {
                    $response->OnComplete($exitCode, {stdOut, stdErr});
                } catch (const std::exception &ex) {
                    LogIt::Error(ex);
                }
            }
        };

        auto onErrorHandler = [&](const string &$error) {
            stdErr += $error;
            LogIt::Error($error);
            onExitHandler(-1);
        };

        try {
            LogIt::Debug("Spawning process {0} in {1}", cmd, cwd);
            $response->OnStart();
            auto childProcess = Child::Spawn(cmd, args, opts);
            if (!opts.isDetached()) {
                childProcess->setOnStdOutData([&](const string &$data) {
                    stdOut += $data;
                    $response->OnChange($data);
                });
                childProcess->setOnStdErrData([&](const string &$data) {
                    stdErr += $data;
                    $response->OnChange($data);
                    if (String::Contains($data, "fatal:") || String::Contains($data, "error:")) {
                        onExitHandler(-1);
                        childProcess->Kill();
                    }
                });
                childProcess->setOnExit(onExitHandler);
                childProcess->setOnClose(onExitHandler);
                childProcess->setOnError(onErrorHandler);
                childProcess->WaitForExit();
            }
        } catch (std::exception &$ex) {
            LogIt::Error($ex);
            onErrorHandler($ex.what());
        }
    }

    void Terminal::Open(const string &$path, const function<void(int)> &$callback) {
        Terminal::Open($path, false, $callback);
    }

    void Terminal::Open(const string &$path, bool $detach, const function<void(int)> &$callback) {
        Terminal::Open($path, $detach, ResponseFactory::getResponse($callback));
    }

    void Terminal::Open(const string &$path, const shared_ptr<IResponse> &$response) {
        Terminal::Open($path, false, $response);
    }

    void Terminal::Open(const string &$path, bool $detach, const shared_ptr<IResponse> &$response) {
        auto onOpenHandler = [&](int $exitCode, const string &$error) {
            LogIt::Error("Open failed with {0}, {1}", $exitCode, $error);
            if ($response != nullptr) {
                $response->Send($exitCode);
            }
        };

        LogIt::Info("open> {0}", $path);
        if (!$path.empty()) {
            Terminal::Execute($path, {}, "", [&](int $exitCode, const std::vector<string> &$std) {
                onOpenHandler($exitCode, "");
            });
        } else {
            onOpenHandler(-1, "Undefined or empty path for open.");
        }
    }

    void Terminal::Elevate(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                           const function<void(int, const std::vector<string> &)> &$callback) {
        Terminal::Elevate($cmd, $args, TerminalOptions($cwd), $callback);
    }

    void Terminal::Elevate(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                           const function<void(int, const std::vector<string> &)> &$callback) {
        Terminal::Elevate($cmd, $args, $options, ResponseFactory::getResponse($callback));
    }

    void Terminal::Elevate(const string &$cmd, const std::vector<string> &$args, const string &$cwd,
                           const shared_ptr<IResponse> &$response) {
        Terminal::Elevate($cmd, $args, TerminalOptions($cwd), $response);
    }

    void Terminal::Elevate(const string &$cmd, const std::vector<string> &$args, const TerminalOptions &$options,
                           const shared_ptr<IResponse> &$response) {
        string cmd = $cmd;
        string cwd = String::Remove($options.getCwd(), "\"");
        TerminalOptions options = $options;

        if (cwd.empty()) {
            cwd = ".";
        }
        cwd = FileSystem::ResolvePath(cwd);
        options.setCwd(cwd);

        string args;
        if (!$args.empty()) {
            args = $args[0];
            if (args.size() > 1) {
                args += " ";
                for (int i = 1; i < static_cast<int>($args.size()); i++) {
                    args += $args[i];
                    if (i != static_cast<int>($args.size()) - 1) {
                        args += " ";
                    }
                }
            }
        }

#ifdef WIN_PLATFORM
        string cmyy = "-Command \"(Start-Process -WindowStyle hidden '" + $cmd + "'";

        if (!args.empty()) {
            cmyy += " -ArgumentList '" + args + "'";
        }

        cmyy += " -Verb RunAs -PassThru).Id\"";

        cmd = SearchExecutableInPath("powershell.exe");
        Terminal::Execute(cmd, {cmyy}, options, $response);
#else
        Terminal::Execute("sudo " + cmd, {args}, options, $response);
#endif
    }

    string Terminal::UnquoteCmdPath(const string &$path) {
        string retVal = $path;
        const string quote = "\"";

        if (String::Contains(retVal, quote)) {
            if (String::StartsWith(retVal, quote)) {
                int quoteOccurence = String::OccurrenceCount(retVal, quote);
                if (quoteOccurence == 2) {
                    if (String::EndsWith(retVal, quote)) {
                        retVal = String::Substring(retVal, 1, retVal.length() - 1);
                    } else {
                        int index = String::IndexOf(retVal, quote);
                        int index2 = String::IndexOf(retVal, quote, true, index + 1);
                        int found = String::IndexOf(retVal, " --");
                        if (!(index <= found && found <= index2)) {
                            retVal = String::Remove(retVal, quote);
                        } else {
                            found = String::IndexOf(retVal, " -");
                            if (!(index <= found && found <= index2)) {
                                retVal = String::Remove(retVal, quote);
                            }
                        }
                    }
                } else if ((quoteOccurence) > 2 && (quoteOccurence % 2 == 0)) {
                    int index = String::IndexOf(retVal, quote);
                    int index2 = String::IndexOf(retVal, quote, true, index + 1);
                    int found = String::IndexOf(retVal, " --");
                    if (!(index <= found && found <= index2)) {
                        retVal = String::Substring(retVal, 1);
                        retVal = retVal.replace(static_cast<unsigned int>(String::IndexOf(retVal, quote)), 1, "");
                    } else if (found > 0) {
                        retVal = String::Substring(retVal, 1, retVal.length() - 1);
                    } else {
                        found = String::IndexOf(retVal, " -");
                        if (!(index <= found && found <= index2)) {
                            retVal = String::Substring(retVal, 1);
                            retVal = retVal.replace(static_cast<unsigned int>(String::IndexOf(retVal, quote)), 1, "");
                        } else if (found > 0) {
                            retVal = String::Substring(retVal, 1, retVal.length() - 1);
                        }
                    }
                }
            }
        }

        return retVal;
    }

    string Terminal::ResolveCmd(const string &$cmd, const string &$cwd) {
        string cmd = $cmd;
        string cmdOrig = cmd;
        string cwd = String::Remove($cwd, "\"");
        cmd = Terminal::UnquoteCmdPath(cmd);

        if (cwd.empty()) {
            cwd = ".";
        }
        cwd = FileSystem::ResolvePath(cwd);

        if (!fs::path(cmd).is_absolute() || !FileSystem::Exists(cmd)) {
            LogIt::Debug(R"(File not found "{0}" searching in "{1}")", cmd, cwd);
            string tmpCmd = SearchExecutableInPath(cmd, cwd);
            if (tmpCmd.empty()) {
                LogIt::Debug("File not found \"{0}\" searching in system path.", cmd);
                tmpCmd = SearchExecutableInPath(cmd);
                if (tmpCmd.empty()) {
                    cmd = "";
                } else {
                    cmd = tmpCmd;
                }
            } else {
                cmd = tmpCmd;
            }
        }
        return cmd;
    }
}
