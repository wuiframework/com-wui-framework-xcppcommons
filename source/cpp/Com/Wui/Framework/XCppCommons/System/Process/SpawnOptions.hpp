/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_SPAWNOPTIONS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_SPAWNOPTIONS_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * SpawnOptions class contains each properties for Child::Spawn method.
     */
    class SpawnOptions : public ProcessOptions {
     public:
        /**
         * Constructs default (empty) options.
         */
        SpawnOptions();

        /**
         * Constructs options from values.
         * @param $cwd Specify demand working directory.
         * @param $env Specify environment variables map. Empty map will be ignored, whole variables will be removed/replaced otherwise.
         * @param $shell Set to true to force use shell.
         * @param $detached Set to true to start process as detached, false otherwise.
         */
        explicit SpawnOptions(const string &$cwd, const std::map<string, string> &$env = {}, bool $shell = false, bool $detached = false);

        /**
         * Constructs options from JSON.
         * @param $options Specify options.
         */
        explicit SpawnOptions(const json &$options);

        /**
         * Copy constructor.
         */
        SpawnOptions(const SpawnOptions &$other);

        /**
         * Move constructor.
         */
        SpawnOptions(SpawnOptions &&$other) noexcept;

        /**
         * Copy operator.
         */
        SpawnOptions &operator=(SpawnOptions const &$other);

        /**
         * Move operator.
         */
        SpawnOptions &operator=(SpawnOptions &&$other) noexcept;

        /**
         * Defalut destructor.
         */
        virtual ~SpawnOptions() = default;

        bool operator==(const SpawnOptions &rhs) const;

        bool operator!=(const SpawnOptions &rhs) const;

        string ToString() const override;

        /**
         * @return Returns true for detached process, false for daemon.
         */
        bool isDetached() const;

        /**
         * @param $detached Set true for detached process, false for daemon.
         */
        void setDetached(bool $detached);

        friend std::ostream &operator<<(std::ostream &$os, const SpawnOptions &$options);

     private:
        bool detached = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_SPAWNOPTIONS_HPP_
