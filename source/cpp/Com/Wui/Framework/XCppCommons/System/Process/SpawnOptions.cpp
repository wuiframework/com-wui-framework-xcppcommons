/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Process {

    SpawnOptions::SpawnOptions() = default;

    SpawnOptions::SpawnOptions(const string &$cwd, const std::map<string, string> &$env, bool $shell, bool $detached)
            : ProcessOptions($cwd, $env, $shell),
              detached($detached) {}

    SpawnOptions::SpawnOptions(const json &$options)
            : ProcessOptions($options) {
        if ($options.find("detached") != $options.end()) {
            this->setDetached($options["detached"]);
        }
    }

    bool SpawnOptions::isDetached() const {
        return this->detached;
    }

    void SpawnOptions::setDetached(bool $detached) {
        this->detached = $detached;
    }

    SpawnOptions::SpawnOptions(const SpawnOptions &$other) = default;

    SpawnOptions::SpawnOptions(SpawnOptions &&$other) noexcept
            : ProcessOptions(std::move($other)),
              detached($other.detached) {}

    bool SpawnOptions::operator==(const SpawnOptions &rhs) const {
        return static_cast<const ProcessOptions &>(*this) == static_cast<const ProcessOptions &>(rhs) &&
               detached == rhs.detached;
    }

    bool SpawnOptions::operator!=(const SpawnOptions &rhs) const {
        return !(rhs == *this);
    }

    string SpawnOptions::ToString() const {
        json data = json::parse(ProcessOptions::ToString());
        data["detached"] = this->detached;
        return data.dump();
    }

    SpawnOptions &SpawnOptions::operator=(const SpawnOptions &$other) {
        if (&$other != this) {
            this->ProcessOptions::operator=($other);
            this->detached = $other.detached;
        }
        return *this;
    }

    SpawnOptions &SpawnOptions::operator=(SpawnOptions &&$other) noexcept {
        if (&$other != this) {
            this->ProcessOptions::operator=($other);
            this->detached = $other.detached;
        }
        return *this;
    }

    std::ostream &operator<<(std::ostream &$os, const SpawnOptions &$options) {
        $os << $options.ToString();
        return $os;
    }
}
