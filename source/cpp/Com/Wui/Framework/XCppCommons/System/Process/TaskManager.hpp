/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGER_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * TaskManager class provides static methods for various operations on existing tasks.
     */
    class TaskManager : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                        private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * Find running task which match to given pattern.
         * @param $pattern Specify pattern.
         * @return Returns list of found tasks info or empty list if no task found.
         */
        static std::vector<TaskInfo> Find(const string &$pattern);

        /**
         * Terminate specified task and all of its threads.
         * @param $pid Specify task PID.
         * @return Returns true if succeed or task PID not exists, false otherwise.
         */
        static bool Terminate(unsigned int $pid);

        /**
         * Terminate specified task and all of its threads.
         * @param $info Specify TaskInfo instance.
         * @return Returns true if succeed or task PID not exists, false otherwise.
         */
        static bool Terminate(const TaskInfo &$info);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKMANAGER_HPP_
