/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKINFO_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKINFO_HPP_

#include <ostream>

namespace Com::Wui::Framework::XCppCommons::System::Process {
    /**
     * TaskInfo class holds information about task like PID or full executable name (path).
     */
    class TaskInfo {
     public:
        /**
         * Constructs TaskInfo instance from PID and path.
         * @param $pid Specify task PID.
         * @param $path Specify full task path with name.
         */
        TaskInfo(unsigned int $pid, const string &$path);

        /**
         * @return Returns task PID.
         */
        unsigned int getPid() const;

        /**
         * @return Returns task executable path.
         */
        const string &getPath() const;

        /**
         * Binary equality operator.
         * @param $rhs Right hand instance.
         * @return Returns true if both PIDs match.
         */
        bool operator==(const TaskInfo &$rhs) const;

        /**
         * Binary inequality operator.
         * @param $rhs Right hand instance
         * @return Returns true if both PIDs not match.
         */
        bool operator!=(const TaskInfo &$rhs) const;

        /**
         * Stream operator produces string "pid: <pid> name: <name>"
         */
        friend std::ostream &operator<<(std::ostream &$os, const TaskInfo &$info);

     private:
        unsigned int pid = 0;
        string path;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_PROCESS_TASKINFO_HPP_
