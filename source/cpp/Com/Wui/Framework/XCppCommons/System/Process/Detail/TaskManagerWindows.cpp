/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifdef WIN_PLATFORM

#include "../../../sourceFilesMap.hpp"

#include <Tlhelp32.h>

namespace Com::Wui::Framework::XCppCommons::System::Process {
    using Com::Wui::Framework::XCppCommons::Primitives::String;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    std::vector<TaskInfo> TaskManager::Find(const string &$pattern) {
        std::vector<TaskInfo> retVal;

#if !defined(UNICODE)
        string fileName = $pattern;
        if (!String::EndsWith(fileName, ".exe")) {
            fileName += ".exe";
        }
        const HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPALL, 0);
        PROCESSENTRY32 pEntry = {0};
        pEntry.dwSize = sizeof(pEntry);
        BOOL hRes = Process32First(hSnapShot, &pEntry);
        while (hRes != 0) {
            if (strcmp(pEntry.szExeFile, fileName.c_str()) == 0) {
                const HANDLE hProcess = OpenProcess(PROCESS_QUERY_LIMITED_INFORMATION, 0, pEntry.th32ProcessID);
                if (hProcess != nullptr) {
                    DWORD len = 1024;
                    TCHAR buf[1024] = {0};
                    QueryFullProcessImageName(hProcess, 0, buf, &len);
                    string path(buf, len);

                    retVal.emplace_back(static_cast<unsigned int>(pEntry.th32ProcessID), std::move(path));
                    CloseHandle(hProcess);
                }
            }
            hRes = Process32Next(hSnapShot, &pEntry);
        }
        CloseHandle(hSnapShot);
#else
#warning "TaskManager::Find not implement for UNICODE"
#endif

        return retVal;
    }

    bool TaskManager::Terminate(unsigned int $pid) {
        bool status = false;

        const HANDLE hProcess = OpenProcess(PROCESS_TERMINATE, 0, (DWORD)$pid);
        if (hProcess != nullptr) {
            status = TerminateProcess(hProcess, STATUS_CONTROL_C_EXIT) != 0;
        }

        return status;
    }
}

#endif  // WIN_PLATFORM
