/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_URL_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_URL_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Net {
    /**
     * This class provides URL elements.
     */
    class Url : public Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Constructs default (empty) Url.
         */
        Url() = default;

        /**
         * Constructs Url from url-string.
         * @param $url Specify url.
         */
        explicit Url(const string &$url);

        /**
         * @return Returns Href component.
         */
        const string getHref() const;

        /**
         * @param $href Specify Href component.
         */
        void setHref(const string &$href);

        /**
         * @return Returns protocol component.
         */
        const string &getProtocol() const;

        /**
         * @param $protocol Specify protocol component.
         */
        void setProtocol(const string &$protocol);

        /**
         * @return Returns host component.
         */
        const string getHost() const;

        /**
         * @return Returns authentication component.
         */
        const string &getAuth() const;

        /**
         * @param $auth Specify authentication component.
         */
        void setAuth(const string &$auth);

        /**
         * @return Returns hostname component.
         */
        const string &getHostname() const;

        /**
         * @param $hostname Specify hostname component.
         */
        void setHostname(const string &$hostname);

        /**
         * @return Returns port component.
         */
        int getPort() const;

        /**
         * @param $port Specify port component.
         */
        void setPort(int $port);

        /**
         * @return Returns pathname component.
         */
        const string &getPathname() const;

        /**
         * @param $pathname Specify pathname component.
         */
        void setPathname(const string &$pathname);

        /**
         * @return Returns search string.
         */
        const string getSearch() const;

        /**
         * @return Returns path component.
         */
        const string getPath() const;

        /**
         * @return Returns query string.
         */
        const string &getQuery() const;

        /**
         * @param $query Specify query string.
         */
        void setQuery(const string &$query);

        /**
         * @return Returns Hash.
         */
        const string &getHash() const;

        /**
         * @param $hash Specify Hash.
         */
        void setHash(const string &$hash);

        string ToString() const override;

        /**
         * @param $url Initialize Url from specified string.
         */
        void FromString(const string &$url);

        /**
         * @return Returns Url as JSON object.
         */
        json ToJson() const;

        /**
         * Encode UTF-8 data into URL except reserved characters "A-Za-z0-9;,/?:@&=+$-_.!~*'()#".
         * @param $data Data to encode.
         * @return Returns encoded data.
         */
        static string Encode(const string &$data);

        /**
         * Decode UTF-8 data from URL (percentage decoding).
         * @param $data Data to decode.
         * @return Returns decoded data.
         */
        static string Decode(const string &$data);

        /**
         * Encode input data except "A-Za-z0-9-_.!~*'()".
         * @param $data Data to encode.
         * @return Returns encoded data.
         */
        static string EncodeComponent(const string &$data);

        /**
         * Decode input data.
         * @param $data Data to decode.
         * @return Returns decoded data.
         */
        static string DecodeComponent(const string &$data);

     private:
        string protocol = "";
        string auth = "";
        string hostName = "";
        int port = 0;
        string hash = "";
        string pathName = "";
        string query = "";
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_URL_HPP_
