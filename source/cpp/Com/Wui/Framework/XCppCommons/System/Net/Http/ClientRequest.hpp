/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENTREQUEST_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENTREQUEST_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    /**
     * ClientRequest class provides methods to manage HTTP requests.
     */
    class ClientRequest :public Com::Wui::Framework::XCppCommons::Primitives::BaseObject{
     public:
        /**
         * Constructs default client request instance.
         */
        ClientRequest();

        /**
         * Constructs client request from specified URL.
         * @param $url Specify URL address.
         * @param $socketWrite Specify socket write function handler.
         * @param $owner Specify HttpClient which owns this instance.
         */
        ClientRequest(const json &$url, const function<void(const string &)> &$socketWrite, Client *$owner);

        ClientRequest(ClientRequest const &) = default;

        ClientRequest(ClientRequest &&) noexcept = default;

        /**
         * Copy operator override.
         */
        ClientRequest &operator=(ClientRequest const &$other);

        ClientRequest &operator=(ClientRequest &&) noexcept = default;

        virtual ~ClientRequest() = default;

        /**
         * Write content data into http request.
         * @param $data Data to be written.
         * @return Returns true if succeed, false otherwise.
         */
        bool Write(const string &$data);

        /**
         * Finalize request preparation and send it to http server.
         */
        void End();

        /**
         * Aborts request.
         */
        void Abort();

        /**
         * @return Returns owning http client instance.
         */
        const Client *getOwner() const;

        /**
         * @param $onError Callback function to be registered as OnError handler.
         */
        void setOnError(const function<void(const string &)> &$onError);

        /**
         * @param $socketAbort Callback function to be registred as SocketAbort handler.
         */
        void setSocketAbort(const function<void()> &$socketAbort);

        /**
         * @return Returns method type of this http request.
         */
        const Enums::HttpMethodType &getMethodType() const;

     private:
        json url;
        function<void(const string &)> socketWrite;
        function<void()> socketAbort;
        function<void(const string &)> onError;
        Client *owner;
        string requestData;
        Enums::HttpMethodType methodType;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENTREQUEST_HPP_
