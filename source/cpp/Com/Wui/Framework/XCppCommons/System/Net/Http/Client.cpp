/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    using Com::Wui::Framework::XCppCommons::Enums::HttpClientType;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    using boost::asio::ip::tcp;

    Client::Client()
            : ioService(),
              clientType(HttpClientType::INVALID) {
        this->response = ClientResponse(this);
    }

    const ClientRequest &Client::Request(const json &$url, const function<void(ClientResponse &)> &$callback) {
        this->request = ClientRequest($url, boost::bind(&Client::SocketWrite, this, boost::placeholders::_1), this);
        this->request.setSocketAbort(std::bind(&Client::Abort, this));
        this->responseCallback = $callback;

        this->url.FromString($url["href"]);
        string protocol = $url["protocol"];
        if (boost::iequals(protocol, "http:")) {
            this->clientType = HttpClientType::HTTP;
        } else {
            this->clientType = HttpClientType::HTTPS;
        }

        string host = $url["hostname"];
        if (!host.empty()) {
            if (this->clientType == HttpClientType::HTTP) {
                this->ConnectHttp(host);
            } else if (this->clientType == HttpClientType::HTTPS) {
                this->ConnectHttps(host);
            } else {
                LogIt::Error("HttpClient:\tInvalid client type (unsupported protocol).");
            }
        } else {
            LogIt::Error("HttpClient:\thost missing.");
        }

        return this->request;
    }

    void Client::ConnectHttp(string $host) {
        this->socket = std::make_unique<tcp::socket>(this->ioService);

        try {
            tcp::resolver resolver(this->ioService);
            string port = "http";
            if (this->url.getPort() > 0) {
                port = std::to_string(this->url.getPort());
            }
            tcp::resolver::query query($host, port);
            tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
            boost::system::error_code ec;
            boost::asio::connect(*this->socket, endpoint_iterator, ec);
        }
        catch (const std::exception &ex) {
            LogIt::Error(ex);
        }
    }

    void Client::ConnectHttps(string $host) {
        boost::asio::ssl::context ctx(this->ioService, boost::asio::ssl::context::sslv23);

        this->sslSocket = std::make_unique<boost::asio::ssl::stream<tcp::socket>>(this->ioService, ctx);

        try {
            boost::asio::ip::tcp::resolver sslResolver(this->ioService);
            string port = "https";
            if (this->url.getPort() > 0) {
                port = std::to_string(this->url.getPort());
            }
            boost::asio::ip::tcp::resolver::query query($host, port);
            boost::system::error_code ec;
            tcp::resolver::iterator endpoint_iterator = sslResolver.resolve(query, ec);

            boost::asio::connect(this->sslSocket->lowest_layer(), endpoint_iterator);
            this->sslSocket->lowest_layer().set_option(tcp::no_delay(true));

            this->sslSocket->set_verify_mode(boost::asio::ssl::verify_none);
            this->sslSocket->set_verify_callback(boost::asio::ssl::rfc2818_verification($host));
            this->sslSocket->handshake(boost::asio::ssl::stream<tcp::socket>::client);
        }
        catch (const std::exception &ex) {
            LogIt::Error(ex);
        }
    }

    void Client::SocketWrite(const string &$data) {
        boost::asio::streambuf request;
        boost::system::error_code ec;
        std::ostream request_stream(&request);

        request_stream << $data << std::flush;

        this->response.setOnHeaderParsed([&]() {
            if (this->responseCallback) {
                this->responseCallback(this->response);
            }
        });

        this->response.StartCapture();

        switch (this->clientType) {
            case HttpClientType::HTTPS: {
                boost::asio::write(*this->sslSocket, request, ec);
                break;
            }
            case HttpClientType::HTTP: {
                boost::asio::write(*this->socket, request, ec);
                break;
            }
            default: {
                return;
            }
        }
    }

    HttpClientType Client::getClientType() const {
        return this->clientType;
    }

    void Client::WaitForFinished() {
        this->ioService.run();
    }

    boost::asio::ip::tcp::socket *Client::getSocket() const {
        return this->socket.get();
    }

    boost::asio::ssl::stream<boost::asio::ip::tcp::socket> *Client::getSslSocket() const {
        return this->sslSocket.get();
    }

    const boost::asio::io_service &Client::getIoService() const {
        return this->ioService;
    }

    const ClientResponse &Client::getResponse() const {
        return this->response;
    }

    const ClientRequest &Client::getRequest() const {
        return this->request;
    }
}
