/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENTRESPONSE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENTRESPONSE_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    /**
     * ClientResponse contains logic for handling http server response.
     */
    class ClientResponse:Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
     public:
        /**
         * Constructs default instance.
         */
        ClientResponse();

        /**
         * Constructs instance with specified owner.
         * @param $owner Specify client which holds this instance.
         */
        explicit ClientResponse(Client const *$owner);

        /**
         * Copy constructor.
         * @param $other Specify instance to be copied.
         */
        ClientResponse(ClientResponse const &$other) = default;

        /**
         * Move constructor.
         * @param $other Specify instance to be moved, current instance content will be released before.
         */
        ClientResponse(ClientResponse &&$other) = default;

        /**
         * Copy operator override.
         */
        ClientResponse &operator=(const ClientResponse &$other);

        /**
         * Move operator override.
         */
        ClientResponse &operator=(ClientResponse &&$other) = default;

        virtual ~ClientResponse() = default;

        /**
         * @return Returns response status.
         */
        int getStatusCode() const;

        /**
         * @return Returns protocol version.
         */
        const string &getHttpVersion() const;

        /**
         * @return Returns reference to response header map.
         */
        const std::map<string, string> &getHeaders() const;

        /**
         * @return Returns response header map as JSON object.
         */
        json getHeadersJson() const;

        /**
         * @param $key Specify demand header key.
         * @return Returns value for specified header key.
         */
        string getHeaderValue(const string &$key);

        /**
         * @return Returns content data.
         */
        const string &getContent() const;

        /**
         * Attach on data received callback.
         * @param $onData Specify callback function.
         */
        void setOnData(const function<void(const string &)> &$onData);

        /**
         * Attach on close callback to be called when connection is closed before whole response was received.
         * OnEnd callback is called before anyway.
         * @param $onClose Specify callback function.
         */
        void setOnClose(const function<void()> &$onClose);

        /**
         * Attach on end callback to be called before response finalize.
         * @param $onEnd
         */
        void setOnEnd(const function<void()> &$onEnd);

        /**
         * Starts response capturing process.
         */
        void StartCapture();

        /**
         * Attach on header parsed callback to be called immediately after header was handled before content.
         * @param $onHeaderParsed
         */
        void setOnHeaderParsed(const function<void(void)> &$onHeaderParsed);

     private:
        Client const *owner;
        int statusCode;
        string httpVersion{};
        std::map<string, string> headers;
        string content{};
        boost::asio::streambuf buffer{};
        int contentLength = 0;
        int currentLength = 0;
        bool isChunked = false;
        unsigned int chunkSize = 0;
        function<void(void)> headerParsed;

        void handleReadStatus(const boost::system::error_code &$err, std::size_t $bytes);

        void handleReadHeader(const boost::system::error_code &$err, std::size_t $bytes);

        void handleReadContent(const boost::system::error_code &$err, std::size_t $bytes);

        void handleReadChunkSize(const boost::system::error_code &$err, std::size_t $bytes);

        void handleReadChunkData(const boost::system::error_code &$err, std::size_t $bytes);

        void handleReadChunk(const boost::system::error_code &$err, std::size_t $bytes);

        function<void(const string &)> onData;
        function<void(void)> onClose;
        function<void(void)> onEnd;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENTRESPONSE_HPP_
