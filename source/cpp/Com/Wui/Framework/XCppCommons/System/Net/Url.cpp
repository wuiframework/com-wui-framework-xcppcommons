/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Net {
    static const boost::regex urlParseExpr("^(?:(?:(?:([^:/#\\?]+:)?(?:(?://)(?:(?:((?:[^:@/#\\?]+)(?:\\:(?:[^:@/#\\?]*))?)@)?"
                                                   "(([^:/#\\?\\]\\[]+|\\[[^/\\]@#?]+\\])(?:\\:([0-9]+))?))?)?)?(((?:/?(?:[^/\\?#]"
                                                   "+/+)*)(?:[^\\?#]*))?(\\?([^#]+))?)))(#.*)?");

    static const string reserved = ";,/?:@&=+$!*'()";
    static const string excluded = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.!~*'()";  // RFC2396 + #

    Url::Url(const string &$url) {
        this->FromString($url);
    }

    const string Url::getHref() const {
        string retVal, tmp;
        if (!this->getProtocol().empty()) {
            retVal += this->getProtocol() + "//";
        }
        if (!this->getAuth().empty()) {
            retVal += this->getAuth() + "@";
        }
        if (!this->getHostname().empty()) {
            retVal += this->getHostname();
        }
        if (this->getPort() != 0) {
            retVal += ":" + std::to_string(this->getPort());
        }
        if (!this->getPathname().empty()) {
            retVal += this->getPathname();
        }
        retVal += this->getSearch();
        retVal += this->getHash();
        return retVal;
    }

    void Url::setHref(const string &$href) {
        this->FromString($href);
    }

    const string &Url::getProtocol() const {
        return this->protocol;
    }

    void Url::setProtocol(const string &$protocol) {
        this->protocol = $protocol;
    }

    const string Url::getHost() const {
        string retVal;
        if (!this->getHostname().empty()) {
            retVal += this->getHostname();
        }
        if (this->getPort() != 0) {
            retVal += ":" + std::to_string(this->getPort());
        }
        return retVal;
    }

    const string &Url::getAuth() const {
        return this->auth;
    }

    void Url::setAuth(const string &$auth) {
        this->auth = $auth;
    }

    const string &Url::getHostname() const {
        return this->hostName;
    }

    void Url::setHostname(const string &$hostname) {
        this->hostName = $hostname;
    }

    int Url::getPort() const {
        return this->port;
    }

    void Url::setPort(int $port) {
        this->port = $port;
    }

    const string &Url::getPathname() const {
        return this->pathName;
    }

    void Url::setPathname(const string &$pathname) {
        this->pathName = $pathname;
    }

    const string Url::getSearch() const {
        if (!this->getQuery().empty()) {
            return "?" + this->getQuery();
        }
        return "";
    }

    const string Url::getPath() const {
        string retVal;
        if (!this->getPathname().empty()) {
            retVal += this->getPathname();
        }
        if (!this->getQuery().empty()) {
            retVal += "?" + this->getQuery();
        }
        return retVal;
    }

    const string &Url::getQuery() const {
        return query;
    }

    void Url::setQuery(const string &$query) {
        this->query = $query;
    }

    const string &Url::getHash() const {
        return this->hash;
    }

    void Url::setHash(const string &$hash) {
        this->hash = $hash;
    }

    string Url::ToString() const {
        return this->getHref();
    }

    void Url::FromString(const string &$url) {
        boost::cmatch match;

        if (boost::regex_match($url.c_str(), match, urlParseExpr)) {
            this->protocol = string(match[1].first, match[1].second);
            this->auth = string(match[2].first, match[2].second);
            this->hostName = string(match[4].first, match[4].second);
            string port = string(match[5].first, match[5].second);
            if (!port.empty()) {
                this->port = boost::lexical_cast<int>(port);
            }
            this->pathName = string(match[7].first, match[7].second);
            this->query = string(match[9].first, match[9].second);
            this->hash = string(match[10].first, match[10].second);
        }
    }

    json Url::ToJson() const {
        return json{{"href",     this->getHref()},
                    {"protocol", this->getProtocol()},
                    {"host",     this->getHost()},
                    {"auth",     this->getAuth()},
                    {"hostname", this->getHostname()},
                    {"port",     this->getPort()},
                    {"pathname", this->getPathname()},
                    {"search",   this->getSearch()},
                    {"path",     this->getPath()},
                    {"query",    this->getQuery()},
                    {"hash",     this->getHash()}};
    }

    boost::regex encodeChars(R"((?:[^A-Za-z0-9;,/?:@&=+$\-_.!~*'()#\[\]%]|%(?:[^A-Za-z0-9]|[0-9A-Fa-f][^0-9A-Fa-f]))+)");  // RFC3986

    string Url::Encode(const string &$data) {
        string retVal = $data;
        retVal = boost::regex_replace(retVal, encodeChars,
                                      [&](const boost::smatch &$what) -> string {
                                          string tmp = $what[0].str();
                                          if (tmp[0] == '%') {
                                              return boost::replace_all_copy(tmp, "%", Url::EncodeComponent("%"));
                                          }
                                          return Url::EncodeComponent(tmp);
                                      }, boost::match_any);
        return retVal;
    }

    string Url::Decode(const string &$data) {
        string retVal = Url::DecodeComponent($data);
        return retVal;
    }

    string Url::EncodeComponent(const string &$data) {
        std::ostringstream oss;
        for (char i : $data) {
            if (std::find(excluded.begin(), excluded.end(), i) == excluded.end()) {
                oss << "%" << std::setw(2) << std::setfill('0') << std::hex << std::uppercase
                    << static_cast<unsigned int>(static_cast<unsigned char>(i));
            } else {
                oss << i;
            }
        }
        return oss.str();
    }

    string Url::DecodeComponent(const string &$data) {
        std::ostringstream oss;

        for (size_t i = 0; i < $data.size(); i++) {
            if ($data[i] == '%') {
                std::istringstream iss($data.substr(static_cast<unsigned int>(i + 1), 2));
                int symbol;
                iss >> std::hex >> std::setw(2) >> symbol;
                oss << (unsigned char) symbol;
                i += 2;
            } else {
                oss << $data[i];
            }
        }
        return oss.str();
    }
}
