/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    using Com::Wui::Framework::XCppCommons::System::Net::Http::ClientResponse;
    using Com::Wui::Framework::XCppCommons::Enums::HttpClientType;
    using Com::Wui::Framework::XCppCommons::Primitives::String;

    void ClientResponse::StartCapture() {
        if (this->owner->getClientType() == HttpClientType::HTTPS) {
            boost::asio::async_read_until(*this->owner->getSslSocket(), buffer, "\r\n",
                                          boost::bind(&ClientResponse::handleReadStatus, this,
                                                      boost::asio::placeholders::error, boost::placeholders::_2));
        } else {
            boost::asio::async_read_until(*this->owner->getSocket(), buffer, "\r\n",
                                          boost::bind(&ClientResponse::handleReadStatus, this,
                                                      boost::asio::placeholders::error, boost::placeholders::_2));
        }
    }

    void ClientResponse::handleReadStatus(const boost::system::error_code &$err, std::size_t) {
        std::istream responseStream(&this->buffer);
        string header;
        std::getline(responseStream, header);

        boost::regex expr("^HTTP\\/\\d+\\.\\d+\\s+(\\d+)\\s+(\\w+.*)?");
        boost::cmatch match;
        boost::regex_match(header.c_str(), match, expr);
        if (match.size() > 1 && match[1].length() > 0) {
            this->statusCode = boost::lexical_cast<int>(match[1].str());
        }

        if (this->owner->getClientType() == HttpClientType::HTTPS) {
            boost::asio::async_read_until(*this->owner->getSslSocket(), buffer, "\r\n\r\n",
                                          boost::bind(&ClientResponse::handleReadHeader, this,
                                                      boost::asio::placeholders::error, boost::placeholders::_2));
        } else {
            boost::asio::async_read_until(*this->owner->getSocket(), buffer, "\r\n\r\n",
                                          boost::bind(&ClientResponse::handleReadHeader, this,
                                                      boost::asio::placeholders::error, boost::placeholders::_2));
        }
    }

    void ClientResponse::handleReadHeader(const boost::system::error_code &$err, std::size_t) {
        std::istream responseStream(&this->buffer);
        string header;
        this->headers.clear();

        while (std::getline(responseStream, header) && header != "\r") {
            unsigned int splitIndex = header.find(":");
            if (splitIndex > 0) {
                string key = header.substr(0, splitIndex);
                string value = header.substr(splitIndex + 1);
                boost::trim(key);
                boost::trim(value);
                this->headers.emplace(String::ToLowerCase(key), value);
            }
        }

        auto it = this->headers.find("content-length");
        auto itChunk = this->headers.find("transfer-encoding");
        if (it != this->headers.end()) {
            string len = it->second;
            this->contentLength = boost::lexical_cast<int>(len);
            this->currentLength = 0;
        } else if (itChunk != this->headers.end()) {
            this->isChunked = true;
        } else {
            this->contentLength = 0;
            this->currentLength = 0;
        }

        if (this->headerParsed) {
            this->headerParsed();
        }

        if (this->isChunked) {
            this->chunkSize = 0;
            handleReadChunk($err, 0);
        } else if (this->contentLength >= 0 &&
                   this->owner->getRequest().getMethodType() != Enums::HttpMethodType::HEAD) {
            handleReadContent($err, this->buffer.size());
        } else {
            if (this->onEnd) {
                this->onEnd();
            }
        }
    }

    void ClientResponse::handleReadChunkSize(const boost::system::error_code &$err, std::size_t) {
        this->chunkSize = 0;
        if (!$err) {
            if (this->owner->getClientType() == HttpClientType::HTTPS) {
                boost::asio::async_read_until(*this->owner->getSslSocket(), buffer, "\r\n",
                                              boost::bind(&ClientResponse::handleReadChunkData, this,
                                                          boost::asio::placeholders::error,
                                                          boost::asio::placeholders::bytes_transferred()));
            } else {
                boost::asio::async_read_until(*this->owner->getSocket(), buffer, "\r\n",
                                              boost::bind(&ClientResponse::handleReadChunkData, this,
                                                          boost::asio::placeholders::error,
                                                          boost::asio::placeholders::bytes_transferred()));
            }
        }
    }

    void ClientResponse::handleReadChunkData(const boost::system::error_code &$err, std::size_t $bytes) {
        if ($bytes > 0) {
            std::istream responseStream(&this->buffer);
            string chunkSize;
            std::getline(responseStream, chunkSize);
            boost::replace_all(chunkSize, "\r", "");

            this->chunkSize = std::stoul(string(chunkSize.c_str(), 8), nullptr, 16);
            if (this->chunkSize == 0) {
                this->handleReadChunk(boost::asio::error::eof, buffer.size());
            } else {
                unsigned int len = (this->chunkSize + 2);
                if (this->buffer.size() >= len) {
                    this->handleReadChunk($err, $bytes);
                    return;
                } else {
                    len -= this->buffer.size();
                }

                if (this->owner->getClientType() == HttpClientType::HTTPS) {
                    boost::asio::async_read(*this->owner->getSslSocket(), buffer,
                                            boost::asio::transfer_exactly(len),
                                            boost::bind(&ClientResponse::handleReadChunk, this,
                                                        boost::asio::placeholders::error,
                                                        boost::asio::placeholders::bytes_transferred()));
                } else {
                    boost::asio::async_read(*this->owner->getSocket(), buffer, boost::asio::transfer_exactly(len),
                                            boost::bind(&ClientResponse::handleReadChunk, this,
                                                        boost::asio::placeholders::error,
                                                        boost::asio::placeholders::bytes_transferred()));
                }
            }
        }
    }

    void ClientResponse::handleReadChunk(const boost::system::error_code &$err, std::size_t $bytes) {
        if ($bytes > 0) {
            std::istream responseStream(&this->buffer);

            std::ostringstream oss;
            oss << responseStream.rdbuf();

            this->content = oss.str();
            this->content.erase(this->content.length() - 2);  // remove chunk trailing \r\n
            this->currentLength += this->chunkSize;

            if (this->onData) {
                this->onData(this->content);
            }
        }
        if (!$err) {
            this->handleReadChunkSize($err, $bytes);
        } else if ($err == boost::asio::error::eof) {
            if (this->onEnd) {
                this->onEnd();
            }
        } else {
            if (this->onEnd) {
                this->onEnd();
            }
            if (this->onClose) {
                this->onClose();
            }
        }
    }

    void ClientResponse::handleReadContent(const boost::system::error_code &$err, std::size_t $bytes) {
        if ($bytes > 0) {
            std::istream responseStream(&this->buffer);

            std::ostringstream oss;
            oss << responseStream.rdbuf();

            this->content = oss.str();
            this->currentLength += $bytes;

            if (this->onData) {
                this->onData(this->content);
            }
        }
        if (!$err) {
            if (this->currentLength < this->contentLength) {
                if (this->owner->getClientType() == HttpClientType::HTTPS) {
                    boost::asio::async_read(*this->owner->getSslSocket(), buffer, boost::asio::transfer_at_least(1),
                                            boost::bind(&ClientResponse::handleReadContent, this,
                                                        boost::asio::placeholders::error,
                                                        boost::asio::placeholders::bytes_transferred));
                } else {
                    boost::asio::async_read(*this->owner->getSocket(), buffer, boost::asio::transfer_at_least(1),
                                            boost::bind(&ClientResponse::handleReadContent, this,
                                                        boost::asio::placeholders::error,
                                                        boost::asio::placeholders::bytes_transferred));
                }
            } else {
                if (this->onEnd) {
                    this->onEnd();
                }
            }
        } else {
            if (this->onEnd) {
                this->onEnd();
            }
            if (this->onClose) {
                this->onClose();
            }
        }
    }

    json ClientResponse::getHeadersJson() const {
        json object = {};

        if (!this->headers.empty()) {
            for (std::map<string, string>::const_iterator it = this->headers.cbegin();
                 it != this->headers.cend(); ++it) {
                object[it->first] = it->second;
            }
        }
        return object;
    }

    string ClientResponse::getHeaderValue(const string &$key) {
        std::map<string, string>::iterator cit =
                std::find_if(this->headers.begin(), this->headers.end(),
                             [&$key](const std::pair <string, string> &item) {
                                 if ($key.empty()) {
                                     return false;
                                 }
                                 string key = boost::to_lower_copy($key);
                                 string key2 = boost::to_lower_copy(item.first);
                                 return boost::iequals(key, key2);
                             });
        if (cit != this->headers.end()) {
            return cit->second;
        }
        return "";
    }

    const string &ClientResponse::getHttpVersion() const {
        return this->httpVersion;
    }

    const std::map <string, string> &ClientResponse::getHeaders() const {
        return this->headers;
    }

    const string &ClientResponse::getContent() const {
        return this->content;
    }

    void ClientResponse::setOnData(const function<void(const string &)> &$onData) {
        this->onData = $onData;
    }

    void ClientResponse::setOnClose(const function<void(void)> &$onClose) {
        this->onClose = $onClose;
    }

    void ClientResponse::setOnEnd(const function<void(void)> &$onEnd) {
        this->onEnd = $onEnd;
    }

    void ClientResponse::setOnHeaderParsed(const function<void(void)> &$onHeaderParsed) {
        this->headerParsed = $onHeaderParsed;
    }

    ClientResponse::ClientResponse(Client const *$owner) {
        this->owner = $owner;
        this->statusCode = 0;
    }

    int ClientResponse::getStatusCode() const {
        return this->statusCode;
    }

    ClientResponse::ClientResponse() {
        this->owner = nullptr;
        this->statusCode = 0;
        this->httpVersion = "";
    }

    ClientResponse &ClientResponse::operator=(const ClientResponse &$other) {
        this->owner = $other.owner;
        this->statusCode = $other.statusCode;
        this->onClose = $other.onClose;
        this->onEnd = $other.onEnd;
        this->onData = $other.onData;
        this->httpVersion = $other.httpVersion;
        this->headers = $other.headers;
        this->content = $other.content;
        this->contentLength = $other.contentLength;
        this->headerParsed = $other.headerParsed;
        return *this;
    }
}
