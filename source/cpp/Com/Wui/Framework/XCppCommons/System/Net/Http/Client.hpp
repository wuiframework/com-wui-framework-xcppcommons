/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENT_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENT_HPP_

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    /**
     * Client class provides HTTP and HTTPS connection to a server.
     */
    class Client : public Com::Wui::Framework::XCppCommons::Primitives::BaseObject {
        typedef Com::Wui::Framework::XCppCommons::Enums::HttpClientType HttpClientType;
        typedef Com::Wui::Framework::XCppCommons::System::Net::Http::ClientResponse ClientResponse;

     public:
        /**
         * Construct a client, based on its type and connect to a server.
         * @param $type Type of the client. Http or Https
         * @param $host Host to connect to.
         */
        Client();

        /**
         * @return Returns type of the client.
         */
        HttpClientType getClientType() const;

        /**
         * Creates http client request to specified URL.
         * @param $url Specify URL for request.
         * @param $callback Callback function will be called
         * @return
         */
        const ClientRequest &Request(const json &$url, const function<void(ClientResponse &)> &$callback);

        /**
         * Waits while client is active.
         */
        void WaitForFinished();

        /**
         * @return Returns standard socket (used for HTTP).
         */
        boost::asio::ip::tcp::socket *getSocket() const;

        /**
         * @return Returns SSL socket (used for HTTPS).
         */
        boost::asio::ssl::stream<boost::asio::ip::tcp::socket> *getSslSocket() const;

        /**
         * @return Returns reference to current io_service.
         */
        const boost::asio::io_service &getIoService() const;

        /**
         * @return Returns reference to client response.
         */
        const ClientResponse &getResponse() const;

        /**
         * @return Returns reference to client request.
         */
        const ClientRequest &getRequest() const;

     private:
        void SocketWrite(const string &$data);

        void ConnectHttp(string $host);

        void ConnectHttps(string $host);

        void Abort() {
            this->ioService.post([this]() {
                boost::system::error_code ec;
                if (this->getClientType() == HttpClientType::HTTP) {
                    this->getSocket()->close(ec);
                } else if (this->getClientType() == HttpClientType::HTTPS) {
                    this->getSslSocket()->shutdown(ec);
                    this->getSslSocket()->lowest_layer().cancel(ec);
                    this->getSslSocket()->lowest_layer().close(ec);
                }
            });
        }

        HttpClientType clientType;

        boost::asio::io_service ioService;
        unique_ptr<boost::asio::ip::tcp::socket> socket;
        unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>> sslSocket;
        ClientResponse response;
        ClientRequest request;
        function<void(ClientResponse &)> responseCallback;
        Com::Wui::Framework::XCppCommons::System::Net::Url url{};
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_SYSTEM_NET_HTTP_CLIENT_HPP_
