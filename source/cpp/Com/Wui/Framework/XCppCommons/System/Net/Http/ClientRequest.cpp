/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System::Net::Http {
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;
    using Com::Wui::Framework::XCppCommons::Enums::HttpMethodType;

    ClientRequest::ClientRequest() {
        this->owner = nullptr;
        this->methodType = HttpMethodType::GET;
    }

    ClientRequest::ClientRequest(const json &$url, const function<void(const string &)> &$socketWrite, Client *$owner)
            : url($url),
              socketWrite($socketWrite),
              owner($owner),
              methodType(HttpMethodType::GET) {
        if (!this->url.empty()) {
            string method = this->url.value("method", "GET");

            this->methodType = HttpMethodType::fromString(method);
            if (this->methodType != HttpMethodType::unknown) {
                if (this->onError) {
                    this->onError("Selected ClientRequest method is not supported \"" + method +
                                  "\" , please use GET, POST, or HEAD instead.");
                }
            }
        }
    }

    void ClientRequest::End() {
        string msg;
        string header;
        string body;

        if (!this->url.empty()) {
            string method = this->url.value("method", "GET");

            json headers = this->url.value("headers", json());

            auto generateHeader = [&headers]() -> string {
                string acc;
                for (json::iterator it = headers.begin(); it != headers.end(); ++it) {
                    acc += "\r\n" + it.key() + ": " + it.value().get<string>();
                }
                return acc;
            };

            header = generateHeader();

            if (this->methodType == HttpMethodType::POST) {
                header += "\r\ncontent-length: " + std::to_string(this->requestData.length());
                body = "\r\n\r\n" + this->requestData;
            }
            string path = this->url["path"].get<string>();
            if (path.empty()) {
                path = "/";
            }
            msg += method + " " + path + " HTTP/1.1\r\nHost: " +
                   this->url["hostname"].get<string>();
            if (!header.empty()) {
                msg += header;
            }
            if (!body.empty()) {
                msg += body;
            } else {
                msg += "\r\n\r\n";
            }

            LogIt::Debug("Sending request\n{0}", msg);
            if (this->socketWrite) {
                this->socketWrite(msg);
            }
        } else {
            if (this->onError) {
                this->onError("Input URL can not be empty.");
            }
        }
    }

    bool ClientRequest::Write(const string &$data) {
        this->requestData += $data;
        return true;
    }

    void ClientRequest::Abort() {
        if (this->socketAbort) {
            this->socketAbort();
        }
    }

    const Client *ClientRequest::getOwner() const {
        return this->owner;
    }

    void ClientRequest::setOnError(const function<void(const string &)> &$onError) {
        this->onError = $onError;
    }

    const Enums::HttpMethodType &ClientRequest::getMethodType() const {
        return this->methodType;
    }

    void ClientRequest::setSocketAbort(const function<void()> &$socketAbort) {
        this->socketAbort = $socketAbort;
    }

    ClientRequest &ClientRequest::operator=(ClientRequest const &$other) {
        this->url = $other.url;
        this->socketWrite = $other.socketWrite;
        this->owner = $other.owner;
        this->onError = $other.onError;
        this->requestData = $other.requestData;
        this->socketAbort = $other.socketAbort;
        this->methodType = $other.methodType;
        return *this;
    }
}
