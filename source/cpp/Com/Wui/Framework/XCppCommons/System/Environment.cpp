/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::System {

    string Environment::Expand(const string &$data) {
        string retVal = $data;
#ifdef WIN_PLATFORM
        auto *buff = new char[8192];
        DWORD size = ExpandEnvironmentStringsA(retVal.c_str(), buff, 8192);
        if (size > 8192) {
            delete[] buff;
            buff = new char[size];
            size = ExpandEnvironmentStringsA(retVal.c_str(), buff, size);
        }
        if (size != 0) {
            retVal = string(buff);
        }
#elif defined(LINUX_PLATFORM) || defined(MAC_PLATFORM)
        auto inputStringWithoutDollarSign = $data;
        if (!inputStringWithoutDollarSign.empty() && inputStringWithoutDollarSign.front() == '$') {
            inputStringWithoutDollarSign.erase(0, 1);
        }

        const auto resolvedEnvironmentVariable = std::getenv(inputStringWithoutDollarSign.c_str());

        if (resolvedEnvironmentVariable != nullptr) {
            retVal = resolvedEnvironmentVariable;
        }
#else
#warning Expand not implemented on this platform
#endif
        return retVal;
    }
}
