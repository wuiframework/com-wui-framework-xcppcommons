/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_ICONNECTOR_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_ICONNECTOR_HPP_

namespace Com::Wui::Framework::XCppCommons::Interfaces {
    /**
     * IConnector interface defines API for IResponse owner.
     */
    class IConnector {
     public:
        /**
         * Send method with string data will be called by IResponse implementation to write/send.
         * @param $data Specify data to be processed by connector implementation.
         */
        virtual void Send(const string &$data) const = 0;

        /**
         * @return Returns owner ID string.
         */
        virtual string getOwnerId() const = 0;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_ICONNECTOR_HPP_
