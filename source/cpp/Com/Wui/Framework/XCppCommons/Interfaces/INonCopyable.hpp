/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_INONCOPYABLE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_INONCOPYABLE_HPP_

namespace Com::Wui::Framework::XCppCommons::Interfaces {
    /**
     * This interface class defines NonCopyable in derived classes.
     */
    class INonCopyable {
     protected:
        INonCopyable() = delete;  // NOLINT

        ~INonCopyable() = delete;  // NOLINT

     private:
        INonCopyable(const INonCopyable &) = delete;  // NOLINT

        INonCopyable &operator=(const INonCopyable &) = delete;  // NOLINT
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_INONCOPYABLE_HPP_
