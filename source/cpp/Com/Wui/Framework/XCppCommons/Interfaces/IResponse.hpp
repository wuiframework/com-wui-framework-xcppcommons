/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_IRESPONSE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_IRESPONSE_HPP_

namespace Com::Wui::Framework::XCppCommons::Interfaces {
    /**
     * IResponse define interface for response handlers.
     */
    class IResponse : public IConnector {
     public:
        /**
         * Sends exit code number.
         * @param $exitCode Specify exit code to send.
         */
        virtual void Send(int $exitCode) const = 0;

        /**
         * Sends Json array created from arguments [$status, $message] or $status if $message is empty (default).
         * @param $status Specify status.
         * @param $message Specify message string or use default (empty).
         */
        virtual void Send(bool $status, const string &$message = "") const = 0;

        void Send(const string &$data) const override = 0;

        /**
         * Sends Json object.
         * @param $object Specify Json object.
         */
        virtual void Send(const json &$object) const = 0;

        /**
         * Sends 'onstart' message.
         */
        virtual void OnStart() const = 0;

        /**
         * Sends 'onstart' message with string data.
         * @param $data Specify data to send.
         */
        virtual void OnStart(const string &$data) const = 0;

        /**
         * Sends 'onchange' message with string data.
         * @param $data Specify data to send.
         */
        virtual void OnChange(const string &$data) const = 0;

        /**
         * Sends 'onchange' message with Json object.
         * @param $object Specify Json object to send.
         */
        virtual void OnChange(const json &$object) const = 0;

        /**
         * Sends 'oncomplete' message with status.
         * @param $status Specify status to send.
         */
        virtual void OnComplete(bool $status) const = 0;

        /**
         * Sends 'oncomplete' message with string data.
         * @param $data Specify data to send.
         */
        virtual void OnComplete(const string &$data) const = 0;

        /**
         * Sends 'oncomplete' message with Json array [$exitCode, [$std[0], $std[1]].
         * @param $exitCode Specify exit code.
         * @param $std Specify vector of stdout and stderr.
         */
        virtual void OnComplete(int $exitCode, const std::vector<string> &$std) const = 0;

        /**
         * Sends 'oncomplete' message with Json array [$object, $data].
         * @param $object Specify Json object.
         * @param $data Specify data to send.
         */
        virtual void OnComplete(const json &$object, const string &$data = "") const = 0;

        /**
         * Sends 'onerror' message with string error.
         * @param $error Specify error description.
         */
        virtual void OnError(const string &$error) const = 0;

        /**
         * Sends 'onerror' message with exception information.
         * @param $ex Specify std::exception.
         */
        virtual void OnError(const std::exception &$ex) const = 0;

        /**
         * NOT USED
         */
        virtual void OnMessage() const = 0;

        /**
         * NOT USED
         */
        virtual void FireEvent(const string &$name) const = 0;

        /**
         * @return Returns response ID.
         */
        virtual int getId() const = 0;

        string getOwnerId() const override = 0;

        /**
         * Register abort handler for current response.
         * @param $handler Specify handler.
         */
        virtual void AddAbortHandler(const function<void()> &$handler) = 0;

        /**
         * Run each registered abort handlers in order: first-added first-called.
         */
        virtual void Abort() = 0;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACES_IRESPONSE_HPP_
