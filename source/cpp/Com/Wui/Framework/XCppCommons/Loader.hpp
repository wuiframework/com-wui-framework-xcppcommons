/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_LOADER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_LOADER_HPP_

namespace Com::Wui::Framework::XCppCommons {
    /**
     * Loader class provides automatic loading of application.
     */
    class Loader {
     public:
        /**
         * Creates application instance and load it.
         * This method must be redefined in demand project Loader class.
         * @param $argc An input arguments count.
         * @param $argv An input array of arguments.
         * @return Returns exit code.
         */
        static int Load(const int $argc, const char *$argv[]) {
            Com::Wui::Framework::XCppCommons::Application application;
            return Load(application, $argc, $argv);
        }

        /**
         * Initialize application environment and other stuff and run application instance Run method.
         * @param $application Specify application instance.
         * @param $argc An input arguments count.
         * @param $argv An input array of arguments.
         * @return Returns exit code.
         */
        static int Load(Com::Wui::Framework::XCppCommons::Application &$application, const int $argc, const char *$argv[]) {
#ifdef WIN_PLATFORM
            HMODULE hModule = GetModuleHandle(NULL);
            boost::filesystem::path cPath, fPath;
#ifdef UNICODE
            WCHAR pathBuf[MAX_PATH] = {0};
            GetModuleFileName(hModule, pathBuf, MAX_PATH);
            fPath = std::wstring(pathBuf);
            strcpy(const_cast<char *>($argv[0]), fPath.filename().string().c_str());  // NOLINT(runtime/printf)
            cPath = fPath.parent_path().normalize().make_preferred();
            SetCurrentDirectory(cPath.wstring().c_str());
#else
            CHAR pathBuf[MAX_PATH] = {0};
            GetModuleFileName(hModule, pathBuf, MAX_PATH);
            fPath = string(pathBuf);
            strcpy(const_cast<char *>($argv[0]), fPath.filename().string().c_str());  // NOLINT(runtime/printf)
            cPath = fPath.parent_path().normalize().make_preferred();
            SetCurrentDirectory(cPath.string().c_str());
#endif
            chdir(cPath.string().c_str());
            boost::filesystem::current_path(cPath);

            EnvironmentArgs::getInstance().setExecutablePath(boost::filesystem::path(fPath).parent_path().string());
            EnvironmentArgs::getInstance().setExecutableName(boost::filesystem::path(fPath).filename().string());
#else
            EnvironmentArgs::getInstance().setExecutablePath(boost::filesystem::path($argv[0]).parent_path().string());
            EnvironmentArgs::getInstance().setExecutableName(boost::filesystem::path($argv[0]).filename().string());
#endif

            EnvironmentArgs::getInstance().Load();
            Enums::LogLevel logLevel = Enums::LogLevel::ALL;
            if (EnvironmentArgs::getInstance().IsProductionMode()) {
                logLevel = Enums::LogLevel::ERROR;
            }

            Utils::LogIt::Init(logLevel, IOApi::IOHandlerFactory::getHandler(Enums::IOHandlerType::OUTPUT_FILE, "LogIt"));

#ifdef WIN_PLATFORM
            SetUnhandledExceptionFilter(Loader::UnhandledException);
            SetErrorMode(SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX | SEM_NOALIGNMENTFAULTEXCEPT);
#endif
            signal(SIGSEGV, Loader::SignalHandler);
            signal(SIGINT, Loader::SignalHandler);
            signal(SIGILL, Loader::SignalHandler);
#ifdef WIN_PLATFORM
            signal(SIGABRT_COMPAT, Loader::SignalHandler);
            signal(SIGBREAK, Loader::SignalHandler);
            signal(SIGABRT2, Loader::SignalHandler);
#endif
            signal(SIGFPE, Loader::SignalHandler);
            signal(SIGSEGV, Loader::SignalHandler);
            signal(SIGTERM, Loader::SignalHandler);
            signal(SIGABRT, Loader::SignalHandler);

            Utils::LogIt::Info("Application loaded.");

            try {
#ifdef WIN_PLATFORM
                bool disableConsole = false;
                for (int i = 0; i < $argc; i++) {
                    if (string($argv[i]) == "--disable-console") {
                        disableConsole = true;
                    }
                }

                bool isAttached = false;
                if (!disableConsole && AttachConsole(ATTACH_PARENT_PROCESS)) {
                    // cppcheck-suppress ignoredReturnValue
                    freopen("CONOUT$", "w", stdout);
                    // cppcheck-suppress ignoredReturnValue
                    freopen("CONOUT$", "w", stderr);
                    // cppcheck-suppress ignoredReturnValue
                    freopen("CONIN$", "r", stdin);
                    setvbuf(stderr, NULL, _IONBF, 0);
                    setvbuf(stdout, NULL, _IONBF, 0);
                    isAttached = true;
                    std::cout << std::endl;
                }
#endif
                int retVal = $application.Run($argc, $argv);

#ifdef WIN_PLATFORM
                if (isAttached) {
                    std::cout << std::endl;

                    fclose(stdout);
                    fclose(stderr);
                    fclose(stdin);
                }
#endif
                return retVal;
            } catch (std::exception &$ex) {
                Utils::LogIt::Error($ex);
            } catch (...) {
                Utils::LogIt::Error("Application failed with unknown exception.");
            }

            return -1;
        }

     private:
#ifdef WIN_PLATFORM

        static LONG WINAPI UnhandledException(LPEXCEPTION_POINTERS $exc) {
            Utils::LogIt::Error("Application crashed with unhandled exception.");
            return EXCEPTION_CONTINUE_SEARCH;
        }

#endif

        static void SignalHandler(int $signal) {
            Utils::LogIt::Error("Application crashed with signal code {0}.", $signal);
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_LOADER_HPP_
