/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_IOHANDLERFACTORY_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_IOHANDLERFACTORY_HPP_

namespace Com::Wui::Framework::XCppCommons::IOApi {
    /**
     * IOHandlerFactory class provides factory for input and output resource handlers.
     */
    class IOHandlerFactory {
        typedef Com::Wui::Framework::XCppCommons::Interfaces::IOHandler IOHandler;

     public:
        /**
         * @param $handlerType Specify IO handler type.
         * @param $owner Sets handler owner.
         * @return Returns instance of specified IO handler or newly created one.
         */
        static shared_ptr<IOHandler> getHandler(const Com::Wui::Framework::XCppCommons::Enums::IOHandlerType $handlerType,
                                        const string &$owner = "");

        /**
         * @param $handler Specify handler to check type.
         * @return Returns type of handler instance.
         */
        static Com::Wui::Framework::XCppCommons::Enums::IOHandlerType getHandlerType(const shared_ptr<IOHandler> &$handler);

        /**
         * Destroys all available instances of IO handlers.
         */
        static void DestroyAll();

     private:
        static string handlerIdGenerator(const Com::Wui::Framework::XCppCommons::Enums::IOHandlerType $handlerType, const string &$owner);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_IOHANDLERFACTORY_HPP_
