/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::IOApi::IOHandlerFactory;
using Com::Wui::Framework::XCppCommons::Enums::IOHandlerType;
using Com::Wui::Framework::XCppCommons::Interfaces::IOHandler;
using Com::Wui::Framework::XCppCommons::IOApi::Handlers::BaseOutputHandler;
using Com::Wui::Framework::XCppCommons::IOApi::Handlers::ConsoleHandler;
using Com::Wui::Framework::XCppCommons::IOApi::Handlers::OutputFileHandler;

static std::map<string, shared_ptr<IOHandler>> handlerList;

shared_ptr<IOHandler> IOHandlerFactory::getHandler(const IOHandlerType $handlerType, const string &$owner) {
    string key = std::to_string($handlerType) + "_" + $owner;

    if (handlerList.find(key) == handlerList.end()) {
        string handlerName = handlerIdGenerator($handlerType, $owner);
        switch ($handlerType) {
            case IOHandlerType::CONSOLE: {
                handlerList.emplace(key, std::shared_ptr<IOHandler>(new ConsoleHandler(handlerName)));
                break;
            }
            case IOHandlerType::OUTPUT_FILE: {
                handlerList.emplace(key, std::shared_ptr<IOHandler>(new OutputFileHandler(handlerName)));
                break;
            }
        }
    }
    return handlerList.at(key);
}

IOHandlerType IOHandlerFactory::getHandlerType(const shared_ptr<IOHandler> &$handler) {
    if ($handler == nullptr) {
        return IOHandlerType::CONSOLE;
    }
    if (dynamic_cast<OutputFileHandler *>($handler.get()) != nullptr) {
        return IOHandlerType::OUTPUT_FILE;
    }
    return IOHandlerType::CONSOLE;
}

void IOHandlerFactory::DestroyAll() {
    handlerList.clear();
}

string IOHandlerFactory::handlerIdGenerator(const Com::Wui::Framework::XCppCommons::Enums::IOHandlerType $handlerType,
                                            const string &$owner) {
    string handlerName;

    switch ($handlerType) {
        case IOHandlerType::OUTPUT_FILE: {
            handlerName = "handlers\\output.txt";
            if (!$owner.empty()) {
                handlerName = $owner + "\\" + handlerName;
            }
            break;
        }
        default: {
            break;
        }
    }

    return handlerName;
}
