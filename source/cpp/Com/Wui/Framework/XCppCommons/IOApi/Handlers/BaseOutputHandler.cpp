/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::IOApi::Handlers::BaseOutputHandler;

static int handlerIterator = 0;

BaseOutputHandler::BaseOutputHandler(const string &$name) {
    if (!$name.empty()) {
        this->name = $name;
    } else {
        this->name = "OutputHandler_" + std::to_string(++handlerIterator);
    }
}

void BaseOutputHandler::Init() {
    this->setEncoding("UTF-8");
}

string BaseOutputHandler::Name() {
    return this->name;
}

string BaseOutputHandler::Encoding() {
    return this->encoding;
}

string BaseOutputHandler::NewLine() {
    return this->newLine;
}

void BaseOutputHandler::Print(const string &$message) {
    throw std::runtime_error("Unimplemented method \"Print\" of abstract class BaseOutputHandler");
}

void BaseOutputHandler::Println(const string &$message) {
    throw std::runtime_error("Unimplemented method \"Println\" of abstract class BaseOutputHandler");
}

void BaseOutputHandler::Clear() {
    throw std::runtime_error("Unimplemented method \"Clear\" of abstract class BaseOutputHandler");
}

void BaseOutputHandler::setEncoding(const string &$value) {
    this->encoding = $value;
}

void BaseOutputHandler::setNewLine(const string &$value) {
    this->newLine = $value;
}
