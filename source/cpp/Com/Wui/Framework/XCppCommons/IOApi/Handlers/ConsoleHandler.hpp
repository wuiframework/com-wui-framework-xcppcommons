/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_CONSOLEHANDLER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_CONSOLEHANDLER_HPP_

namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers {
    /**
     * ConsoleHandler class provides handling of stdout.
     */
    class ConsoleHandler
            : public Com::Wui::Framework::XCppCommons::IOApi::Handlers::BaseOutputHandler {
     public:
        /**
         * @param $name Specify handler name.
         */
        explicit ConsoleHandler(const string &$name = "");

        /**
         * Initialize console handler.
         */
        void Init() override;

        /**
         * Prints specified message to stdout.
         * @param $message String to print.
         */
        void Print(const string &$message) override;

        /**
         * Prints specified message to stdout with new lina at the end.
         * @param $message String to print.
         */
        void Println(const string &$message) override;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_CONSOLEHANDLER_HPP_
