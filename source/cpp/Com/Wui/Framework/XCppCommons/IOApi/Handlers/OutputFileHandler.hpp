/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_OUTPUTFILEHANDLER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_OUTPUTFILEHANDLER_HPP_

namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers {
    /**
     * OutputFileHandler class provides handling of output file stream.
     */
    class OutputFileHandler
            : public Com::Wui::Framework::XCppCommons::IOApi::Handlers::BaseOutputHandler {
     public:
        /**
         * @param name
         */
        explicit OutputFileHandler(const string &$name = "");

        /**
         * Closes opened file before instance release.
         */
        virtual ~OutputFileHandler();

        /**
         * Initialize output file handler.
         */
        void Init() override;

        /**
         * Writes specified message to file.
         * @param $message String to be written in file.
         */
        void Print(const string &$message) override;

        /**
         * Writes specified message to file with new line after message.
         * @param $message String to be written in file.
         */
        void Println(const string &$message) override;

        /**
         * @return Returns path of currently opened output file.
         */
        const string &getOutputFilePath() const;

        /**
         * @return Returns base path or empty as current cwd.
         */
        const string &getBasePath() const;

        /**
         * @param $basePath Specify base path where file will be created or leave empty to use current cwd.
         */
        void setBasePath(const string &$basePath);

     private:
        std::ofstream outFile;
        string outFilePath;
        string basePath;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_OUTPUTFILEHANDLER_HPP_
