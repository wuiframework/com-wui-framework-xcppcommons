/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::IOApi::Handlers::ConsoleHandler;

ConsoleHandler::ConsoleHandler(const string &$name)
        : BaseOutputHandler($name) {
    this->setNewLine("\n");
}

void ConsoleHandler::Init() {
}

void ConsoleHandler::Print(const string &$message) {
    std::cout << $message;
}

void ConsoleHandler::Println(const string &$message) {
    std::cout << $message << this->NewLine();
}
