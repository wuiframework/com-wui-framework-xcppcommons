/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_BASEOUTPUTHANDLER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_BASEOUTPUTHANDLER_HPP_

namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers {
    /**
     * BaseOutputHandler class provides abstract API definition for handling output to custom types.
     */
    class BaseOutputHandler
            : public Com::Wui::Framework::XCppCommons::Interfaces::IOHandler {
        typedef Com::Wui::Framework::XCppCommons::Interfaces::IOHandler IOHandler;

     public:
        /**
         * @param name Specify handler name.
         */
        explicit BaseOutputHandler(const string &$name = "");

        /**
         * Provides handler initialization, which is specific for each types of output handler.
         */
        virtual void Init();

        /**
         * @return Returns handler's name;
         */
        virtual string Name();

        /**
         * @return Returns string representation of encoding.
         */
        virtual string Encoding();

        /**
         * @return Returns string representation of handler specific new line symbol.
         */
        virtual string NewLine();

        /**
         * Prints message to handler output.
         * @param $message String to print.
         */
        virtual void Print(const string &$message);

        /**
         * Prints message to handler output with handler specific new line at the end.
         * @param $message String to print.
         */
        virtual void Println(const string &$message);

        /**
         * Clean up handler.
         */
        virtual void Clear();

     protected:
        virtual void setEncoding(const string &$value);

        virtual void setNewLine(const string &$value);

     private:
        string name;
        shared_ptr<IOHandler> handler;
        string encoding;
        string newLine;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_IOAPI_HANDLERS_BASEOUTPUTHANDLER_HPP_
