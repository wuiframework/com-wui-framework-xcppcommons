/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/format.hpp>

#include <fstream>

#include "../../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::IOApi::Handlers {

    OutputFileHandler::OutputFileHandler(const string &$name)
            : BaseOutputHandler($name),
              basePath("") {
#ifdef _WINDOWS_
        this->setNewLine("\r\n");
#else
        this->setNewLine("\n");
#endif
    }

    void OutputFileHandler::Init() {
        BaseOutputHandler::Init();

        using boost::filesystem::path;
        using boost::system::error_code;
        using boost::filesystem::create_directories;
        using boost::posix_time::ptime;
        using boost::posix_time::second_clock;
        using boost::posix_time::to_simple_string;
        using boost::gregorian::day_clock;

        this->outFilePath = "";

        ptime todayUtc(day_clock::universal_day(), second_clock::universal_time().time_of_day());

        std::stringstream ss;
        if (!this->basePath.empty()) {
            path base(this->basePath);
            if (base.is_relative() || boost::filesystem::exists(base)) {
                ss << this->basePath + "/";
            }
        }
        ss << "log/" << boost::str(boost::format("%04d/%02d") % todayUtc.date().year() % todayUtc.date().month().as_number());
        path dirPath(ss.str());
        dirPath.make_preferred();

        ss.str("");
        ss << boost::str(boost::format("%02d_%02d_%04d.txt") % todayUtc.date().day().as_number() %
                         todayUtc.date().month().as_number() % todayUtc.date().year());
        string fileName = ss.str();
        path filePath = dirPath / ss.str();

        error_code returnedError;

        if (!create_directories(dirPath, returnedError)) {
            if (returnedError != boost::system::errc::success) {
                dirPath = boost::filesystem::temp_directory_path() / EnvironmentArgs::getInstance().getProjectName() /
                          dirPath;
                filePath = dirPath / fileName;
                if (!create_directories(dirPath, returnedError)) {
                    if (returnedError != boost::system::errc::success) {
                        throw std::runtime_error(returnedError.message());
                    }
                }
            }
        }

        try {
            this->outFile.open(filePath.string(), std::ofstream::out | std::ofstream::app | std::ofstream::ate);
            this->outFilePath = filePath.string();
        } catch (std::ofstream::failure &failure) {
            std::cerr << "Opening of OutputFileHandler failed: " << failure.what() << std::endl;
        }
    }

    void OutputFileHandler::Print(const string &$message) {
        if (this->outFile.is_open()) {
            this->outFile << $message;
            this->outFile.flush();
        } else {
            std::cout << "Output file is not accessible to write data: " << $message;
        }
    }

    void OutputFileHandler::Println(const string &$message) {
        this->Print($message + this->NewLine());
    }

    OutputFileHandler::~OutputFileHandler() {
        if (this->outFile.is_open()) {
            this->outFile.close();
        }
    }

    const string &OutputFileHandler::getOutputFilePath() const {
        return this->outFilePath;
    }

    const string &OutputFileHandler::getBasePath() const {
        return basePath;
    }

    void OutputFileHandler::setBasePath(const string &$basePath) {
        OutputFileHandler::basePath = $basePath;
    }
}
