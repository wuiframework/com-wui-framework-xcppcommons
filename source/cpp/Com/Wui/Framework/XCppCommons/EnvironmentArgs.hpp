/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_ENVIRONMENTARGS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_ENVIRONMENTARGS_HPP_

namespace Com::Wui::Framework::XCppCommons {
    /**
     * EnvironmentArgs provides environment args mainly set at build time.
     */
    class EnvironmentArgs {
     public:
        /**
         * @return Returns singleton instance of EnvironmentArgs.
         */
        static EnvironmentArgs &getInstance() {
            static EnvironmentArgs instance;
            return instance;
        }

        /**
         * Loads environment args from specified file. Should be used at start of application.
         * @param $path Specify environment args definition file.
         * Use empty (by default) to load from ./resource/configs/default.config.jsonp path.
         */
        void Load(const string &$path = "") {
            string path = $path;
            if (path.size() == 0) {
                path = "resource/configs/default.config.jsonp";
            }
            json data = Com::Wui::Framework::XCppCommons::Utils::JSON::ParseJsonp(
                    Com::Wui::Framework::XCppCommons::System::IO::FileSystem::Read(path));

            if (data.find("projectName") != data.end()) {
                this->projectName = data["projectName"];
            }
            if (data.find("projectVersion") != data.end()) {
                this->projectVersion = data["projectVersion"];
            }
            if (data.find("appName") != data.end()) {
                this->appName = data["appName"];
            }
            if (data.find("buildTime") != data.end()) {
                this->buildTime = data["buildTime"];
            }
            if (data.find("projectDescription") != data.end()) {
                this->projectDescription = data["projectDescription"];
            }
            if (data.find("projectAuthor") != data.end()) {
                this->projectAuthor = data["projectAuthor"];
            }
            if (data.find("projectLicense") != data.end()) {
                this->projectLicense = data["projectLicense"];
            }

#ifdef DEV
            this->isProductionMode = false;
#else
            this->isProductionMode = true;
#endif
        }

        /**
         * @return Returns project name.
         */
        const string &getProjectName() const {
            return this->projectName;
        }

        /**
         * @return Returns project version string.
         */
        const string &getProjectVersion() const {
            return this->projectVersion;
        }

        /**
         * @return Returns application name.
         */
        const string &getAppName() const {
            return this->appName;
        }

        /**
         * @return Returns build time string.
         */
        const string &getBuildTime() const {
            return this->buildTime;
        }

        /**
         * @return Returns project description string.
         */
        const string &getProjectDescription() const {
            return this->projectDescription;
        }

        /**
         * @return Returns author of that project.
         */
        const string &getProjectAuthor() const {
            return this->projectAuthor;
        }

        /**
         * @return Returns project license string.
         */
        const string &getProjectLicense() const {
            return this->projectLicense;
        }

        /**
         * @return Returns true if production mode is selected, false otherwise.
         */
        bool IsProductionMode() const {
            return this->isProductionMode;
        }

        /**
         * @return Returns executable name with extension.
         */
        const string &getExecutableName() const {
            return this->executableName;
        }

        /**
         * @param $value Sets new executable name (with extension).
         */
        void setExecutableName(const string &$value) {
            this->executableName = $value;
        }

        /**
         * @return Returns executable path without executable name.
         */
        const string &getExecutablePath() const {
            return this->executablePath;
        }

        /**
         * @param $value Sets new executable path (without executable namme).
         */
        void setExecutablePath(const string &$value) {
            this->executablePath = $value;
        }

        EnvironmentArgs(EnvironmentArgs const &) = delete;

        void operator=(EnvironmentArgs const &) = delete;

     private:
        EnvironmentArgs() = default;

        ~EnvironmentArgs() = default;

        string projectName;
        string projectVersion;
        string appName;
        string buildTime;
        string projectDescription;
        string projectAuthor;
        string projectLicense;
        bool isProductionMode;
        string executableName;
        string executablePath;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_ENVIRONMENTARGS_HPP_
