/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_JSON_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_JSON_HPP_

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * JSON class contains static methods for custom operations on JSON data.
     */
    class JSON : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                 private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * Parse json data from JSONP.
         * @param $data Specify JSONP input data in string.
         * @return Returns inner json data of empty json object.
         */
        static json ParseJsonp(const string &$data);

        /**
         * Combine two json objects. $other is merged to $base, so properties and objects in $others will by replaced to $base while
         * properties existing in $base will be untouch.
         * Note that json arrays will be replaced complelty if specified in $other (no concatenation)
         * @param $base Specify base json object.
         * @param $other Specify second json object.
         * @return Returns combination of $other merged to $base object. If $other is empty $base will be returned.
         */
        static json CombineJson(const json &$base, const json &$other);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_JSON_HPP_
