/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"
#include "reflectionData.hpp"

using Com::Wui::Framework::XCppCommons::Utils::Reflection;

int Reflection::Invoke(const string &$method, const int $argc, const char **$argv) {
    if (ReflectionData::getReflectionData().find($method) != ReflectionData::getReflectionData().end()) {
        return ReflectionData::getReflectionData().at($method)($argc, $argv);
    }
    return -1;
}
