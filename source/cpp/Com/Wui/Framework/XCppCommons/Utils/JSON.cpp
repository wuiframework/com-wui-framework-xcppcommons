/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017, NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Utils {
    using Com::Wui::Framework::XCppCommons::Primitives::String;
    using Com::Wui::Framework::XCppCommons::Utils::LogIt;

    json JSON::ParseJsonp(const string &$data) {
        json retVal = {};
        auto start = boost::find_first($data, "({"), stop = boost::find_last($data, "});");

        if (stop.begin() > start.end()) {
            string match = string(start.end() - 1, stop.begin() + 1);
            match = boost::regex_replace(match, boost::regex(R"(((?:^\s*)|(?:{|,\s*))(\"?(?:\$)?[\w$*_]+\"?)\s*:)"),
                                         [](const boost::smatch &$what) -> string {
                                             string key = $what[2].str();
                                             if (boost::regex_search(key.c_str(), boost::regex(R"(["*]|^\d|^var$)"))) {
                                                 throw std::runtime_error(("Keys can not contain ['*', '\"'] chars or start with number: " +
                                                                           $what[2]).c_str());
                                             }
                                             return String::Replace($what[0].str(), key, "\"" + key + "\"");
                                         });
            retVal = json::parse(match);
        }

        return retVal;
    }

    json JSON::CombineJson(const json &$base, const json &$other) {
        json combined = $base;

        for (json::const_iterator it = $other.begin(); it != $other.end(); ++it) {
            if (it.value().is_object()) {
                if (combined.find(it.key()) != combined.end()) {
                    combined[it.key()] = JSON::CombineJson(combined[it.key()], it.value());
                } else {
                    combined[it.key()] = it.value();
                }
            } else {
                combined[it.key()] = it.value();
            }
        }

        return combined;
    }
}
