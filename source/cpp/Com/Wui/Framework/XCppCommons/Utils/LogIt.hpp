/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_LOGIT_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_LOGIT_HPP_

#if defined(__clang__)
// clang doesn't have any built-ins that would enable tracing the caller's translation unit or line number
#   define __INSERT_TRACE_INFO() const TraceInfo &$trace = TraceInfo {}
#else
#   define __INSERT_TRACE_INFO() const TraceInfo &$trace = TraceInfo {__builtin_FILE(), __builtin_LINE()}
#endif

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * LogIt class provides static API for logging to selected output handler.
     */
    class LogIt {
        /**
         * TraceInfo structure holds tracing data (file name, line number) for logger.
         */
        struct TraceInfo {
         public:
            TraceInfo() {}

            TraceInfo(const string &$file, int $line)
                    : file($file),
                      line($line) {
            }

            /**
             * @return Returns file name.
             */
            const string &getFile() const { return this->file; }

            /**
             * @return Returns line number.
             */
            int getLine() const { return this->line; }

            /**
             * Bool operator for operations like if (traceInfo) { // have some tracing info }.
             */
            explicit operator bool() const { return !this->file.empty(); }

         private:
            string file;
            int line = 0;
        };

        typedef Com::Wui::Framework::XCppCommons::Primitives::String String;
        typedef Com::Wui::Framework::XCppCommons::Enums::LogLevel LogLevel;

     public:
        /**
         * Log message at Info level.
         * @param $message Message which should be logged.
         */
        static void Info(const string &$message, __INSERT_TRACE_INFO());

        /**
         * Log message at Info level.
         * @param $message Message which should be logged.
         * @param $arg1 Argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1>
        static void Info(const string &$message, const T1 &$arg1, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::INFO, String::Format($message, $arg1), $trace);
        }

        /**
         * Log message at Info level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2>
        static void Info(const string &$message, const T1 &$arg1, const T2 &$arg2, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::INFO, String::Format($message, $arg1, $arg2), $trace);
        }

        /**
         * Log message at Info level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $arg3 Third argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2, typename T3>
        static void Info(const string &$message, const T1 &$arg1, const T2 &$arg2, const T3 &$arg3, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::INFO, String::Format($message, $arg1, $arg2, $arg3), $trace);
        }


        /**
         * Log message at Warning level.
         * @param $message Message which should be logged.
         */
        static void Warning(const string &$message, __INSERT_TRACE_INFO());

        /**
         * Log message at Warning level.
         * @param $message Message which should be logged.
         * @param $arg1 Argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1>
        static void Warning(const string &$message, const T1 &$arg1, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::WARNING, String::Format($message, $arg1), $trace);
        }

        /**
         * Log message at Warning level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2>
        static void Warning(const string &$message, const T1 &$arg1, const T2 &$arg2, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::WARNING, String::Format($message, $arg1, $arg2), $trace);
        }

        /**
         * Log message at Warning level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $arg3 Third argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2, typename T3>
        static void Warning(const string &$message, const T1 &$arg1, const T2 &$arg2, const T3 &$arg3,
                            __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::WARNING, String::Format($message, $arg1, $arg2, $arg3), $trace);
        }

        /**
         * Log message at Error level.
         * @param $message Message which should be logged.
         */
        static void Error(const string &$message, __INSERT_TRACE_INFO());

        /**
         * Log message at Error level.
         * @param $message Message which should be logged.
         * @param $arg1 Argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1>
        static void Error(const string &$message, const T1 &$arg1, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::ERROR, String::Format($message, $arg1), $trace);
        }

        /**
         * Log message at Error level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2>
        static void Error(const string &$message, const T1 &$arg1, const T2 &$arg2, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::ERROR, String::Format($message, $arg1, $arg2), $trace);
        }

        /**
         * Log message at Error level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $arg3 Third argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2, typename T3>
        static void Error(const string &$message, const T1 &$arg1, const T2 &$arg2, const T3 &$arg3,
                          __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::ERROR, String::Format($message, $arg1, $arg2, $arg3), $trace);
        }

        /**
         * Log message at Error level.
         * @param $exception Exception to be logged.
         */
        static void Error(const std::exception &$exception, __INSERT_TRACE_INFO());

        /**
         * Log message at Debug level.
         * @param $message Message which should be logged.
         */
        static void Debug(const string &$message, __INSERT_TRACE_INFO());

        /**
         * Log message at Debug level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1>
        static void Debug(const string &$message, const T1 &$arg1, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::DEBUG, String::Format($message, $arg1), $trace);
        }

        /**
         * Log message at Debug level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2>
        static void Debug(const string &$message, const T1 &$arg1, const T2 &$arg2, __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::DEBUG, String::Format($message, $arg1, $arg2), $trace);
        }

        /**
         * Log message at Debug level.
         * @param $message Message which should be logged.
         * @param $arg1 First argument to format into message.
         * @param $arg2 Second argument to format into message.
         * @param $arg3 Third argument to format into message.
         * @param $trace Do NOT modify.
         */
        template<typename T1, typename T2, typename T3>
        static void Debug(const string &$message, const T1 &$arg1, const T2 &$arg2, const T3 &$arg3,
                          __INSERT_TRACE_INFO()) {
            LogIt::save(LogLevel::DEBUG, String::Format($message, $arg1, $arg2, $arg3), $trace);
        }

        /**
         * @param $logLevel Specify demand log level.
         */
        static void setLevel(const Com::Wui::Framework::XCppCommons::Enums::LogLevel &$logLevel);

        /**
         * Initialize LogIt static behavior.
         * Affect it only by first call in application before another method from LogIt class.
         * @param $logLevel Select message level to by logged.
         * @param $handler IO handler where messages will be printed.
         */
        static void Init(const LogLevel &$logLevel,
                         const shared_ptr<Com::Wui::Framework::XCppCommons::Interfaces::IOHandler> &$handler = nullptr);

        /**
         * @param $handler Specify handler to be called when LogIt prints data to IOHandler.
         */
        static void setOnPrint(const function<void(const string &)> &$handler);

     private:
        static void save(const Com::Wui::Framework::XCppCommons::Enums::LogLevel &$logLevel, const string &$message,
                         const TraceInfo &$trace);

        static string formatter(const Com::Wui::Framework::XCppCommons::Enums::LogLevel &$logLevel, const string &$message,
                                const string &$newLine, const TraceInfo &$trace);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_LOGIT_HPP_
