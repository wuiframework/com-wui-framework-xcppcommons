/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTENCODER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTENCODER_HPP_

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * ObjectEncoder class provides static methods focused on object or string encoding.
     */
    class ObjectEncoder {
     public:
        /**
         * @param $input A input string for encoding.
         * @return Returns string encoded in base64 format, which can be used as http parameter.
         */
        static string Base64(string $input);

        /**
         * Encode input string to WSS data string.
         * @param $input Specify input string.
         * @return Returns encoded WSS data.
         */
        static string WSS(string $input);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTENCODER_HPP_
