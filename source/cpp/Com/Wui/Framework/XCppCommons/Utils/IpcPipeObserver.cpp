/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Utils::IpcPipeObserver;
using Com::Wui::Framework::XCppCommons::Structures::IpcPipe;

IpcPipeObserver::IpcPipeObserver(const string &$name, const int $pid)
        : name($name + "_" + std::to_string($pid)) {
}

const string &IpcPipeObserver::getName() const {
    return this->name;
}

bool IpcPipeObserver::Initialize(bool $isServer, unsigned int $timeout) {
    this->pipe = std::make_unique<IpcPipe>(this->name, $isServer);
    return this->pipe->Open($timeout);
}

void IpcPipeObserver::Send(const string &$command) {
    this->pipe->Write($command);
}

const string IpcPipeObserver::Receive() {
    return this->pipe->Read();
}
