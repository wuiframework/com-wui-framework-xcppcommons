/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Utils::ObjectValidator;

using boost::any_cast;

bool ObjectValidator::IsSet(const any &$input) {
    return !$input.empty();
}

bool ObjectValidator::IsEmptyOrNull(const any &$input) {
    if (!IsSet($input)) {
        return true;
    }

    if ($input.type() == typeid(nullptr)) {
        return true;
    }

    if ($input.type() == typeid(std::string)) {
        string input = any_cast<string>($input);
        if (input.empty()) {
            return true;
        }
    }
    return false;
}

bool ObjectValidator::IsObject(const any &$input) {
    return false;
}

bool ObjectValidator::IsBoolean(const any &$input) {
    return IsType<bool>($input);
}

bool ObjectValidator::IsDigit(const any &$input) {
    bool isDigit = true;
    if ($input.type() == typeid(std::string) || $input.type() == typeid(const char *)) {
        string strInput;
        try {
            strInput = boost::any_cast<string>($input);
        } catch (std::exception &ex) {
            try {
                const auto *temp = boost::any_cast<const char *>($input);
                strInput = string(temp);
            } catch (std::exception &ex) {
                isDigit = false;
            }
        }

        try {
            boost::lexical_cast<int>(strInput);
        } catch (boost::bad_lexical_cast &ex) {
            try {
                boost::lexical_cast<double>(strInput);
            } catch (boost::bad_lexical_cast &exc) {
                isDigit = false;
            }
        }
    } else {
        if (IsType<double>($input)) {
            isDigit = (!std::isnan(boost::any_cast<double>($input)));
        } else if (IsType<int>($input)) {
            isDigit = (boost::any_cast<int>($input) != std::numeric_limits<int>::max());
        } else {
            isDigit = false;
        }
    }
    return isDigit;
}

bool ObjectValidator::IsInteger(const any &$input) {
    return IsType<int>($input);
}

bool ObjectValidator::IsDouble(const any &$input) {
    return IsType<double>($input);
}

bool ObjectValidator::IsString(const any &$input) {
    return (IsType<string>($input) || IsType<const char *>($input));
}

bool ObjectValidator::IsHexadecimal(const string &$input) {
    bool isHex = true;
    string inputLower = $input;
    any anyInput = $input;
    if (!IsEmptyOrNull(anyInput)) {
        std::transform(inputLower.begin(), inputLower.end(), inputLower.begin(), ::tolower);
        if (inputLower.find("0x") == 0) {
            inputLower = inputLower.substr(2);
        } else if (inputLower.find('#') == 0) {
            inputLower = inputLower.substr(1);
        } else {
            inputLower = "0x" + inputLower;
        }
    } else {
        isHex = false;
    }

    try {
        (void) (std::stoul(inputLower, nullptr, 16));
    } catch (std::exception &ex) {
        isHex = false;
    }
    return isHex;
}
