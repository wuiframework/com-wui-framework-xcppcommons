/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTVALIDATOR_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTVALIDATOR_HPP_

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * ObjectValidator class provides static methods focused on object type validations.
     */
    class ObjectValidator {
     public:
        /**
         * @param $input An object to be validated.
         * @return Returns true, if object is defined, otherwise false.
         */
        static bool IsSet(const any &$input);

        /**
         * @param $input Validate this type of object.
         * @return Returns true, if object is empty or null, otherwise false.
         */
        static bool IsEmptyOrNull(const any &$input);

        /**
         * @param $input Validate this type of object.
         * @return Returns true, if $input is object, otherwise false.
         */
        static bool IsObject(const any &$input);

        /**
         * @param $input Validate this type of object.
         * @return Returns true, if object is type of boolean, otherwise false.
         */
        static bool IsBoolean(const any &$input);

        /**
         * @param $input Validate this type of object.
         * @return Returns true, if object is number, otherwise false.
         */
        static bool IsDigit(const any &$input);

        /**
         * @param $input Validate this type of object.
         * @return Returns true if object is integer, otherwise false.
         */
        static bool IsInteger(const any &$input);

        /**
         * @param $input Validate this type of object.
         * @return Returns true, if object is type of floating point number, otherwise false.
         */
        static bool IsDouble(const any &$input);

        /**
         * @param $input Validate this type of object.
         * @return Returns true, if object is type of string, otherwise false.
         */
        static bool IsString(const any &$input);

        /**
         * @param $input Validate if string is in HEX format.
         * @return Returns true, if string represents hexadecimal number, otherwise false.
         */
        static bool IsHexadecimal(const string &$input);

        /**
         * Check if any type is T.
         * @tparam T Specify type to check.
         * @param $input Specify data to check.
         * @return Returns true if $input is type of T, false otherwise.
         */
        template<typename T>
        static bool IsType(const any &$input) {
            return ($input.type() == typeid(T));
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTVALIDATOR_HPP_
