/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Utils {
    string Convert::TimeToGMTFormat(boost::posix_time::ptime &$time) {
        std::ostringstream timeStream;
        boost::gregorian::date_facet *facet(new boost::gregorian::date_facet("%a, %d %b %Y"));
        boost::posix_time::time_facet *timeFacet(new boost::posix_time::time_facet(" %H:%M:%S GMT"));
        timeStream.imbue(std::locale(timeStream.getloc(), facet));
        timeStream << $time.date();
        timeStream.imbue(std::locale(timeStream.getloc(), timeFacet));
        timeStream << $time;
        return timeStream.str();
    }

    long Convert::TimeToSeconds(long long $time) {
        return static_cast<long>($time / 1000LL);
    }

    long Convert::LastModifiedToTimestamp(const string &$timeString) {
        int retVal = 0;

        boost::regex expr(R"((\d{2})\s([a-zA-Z]{3})\s(\d{4})\s((\d{2}):(\d{2}):(\d{2})))");

        boost::match_results<std::string::const_iterator> what;
        if (boost::regex_search($timeString.begin(), $timeString.end(), what, expr, boost::match_default)) {
            string cons = what[3].str() + "-" + what[2] + "-" + what[1].str() + " " + what[4];
            boost::posix_time::ptime time(boost::posix_time::time_from_string(cons));
            boost::posix_time::time_duration duration = time - boost::posix_time::time_from_string("1970-01-01 00:00:00");
            retVal = duration.total_seconds();
        }

        return retVal;
    }

    const string &Convert::AnyToString(const any &$data) {
        return Convert::AnyToType<string>($data);
    }
}
