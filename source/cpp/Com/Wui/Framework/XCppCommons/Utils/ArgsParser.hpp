/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_ARGSPARSER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_ARGSPARSER_HPP_

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * ArgsParser class provides static functions for program arguments parsing.
     */
    class ArgsParser : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                       private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * @param $args [out] Specify instance of ProgramArgs to holds parsed options.
         * @param $argc Specify count of command line arguments count.
         * @param $argv Specify array of arguments strings.
         * @return Returns 0 if succeed, 1 if print help or version has been done, -1 if error occurred.
         */
        static int Parse(Com::Wui::Framework::XCppCommons::Structures::ProgramArgs &$args,
                         const int $argc, const char **$argv);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_ARGSPARSER_HPP_
