/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Utils::LogIt;
using Com::Wui::Framework::XCppCommons::Utils::Convert;
using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
using Com::Wui::Framework::XCppCommons::Enums::IOHandlerType;
using Com::Wui::Framework::XCppCommons::Interfaces::IOHandler;
using Com::Wui::Framework::XCppCommons::IOApi::IOHandlerFactory;
using Com::Wui::Framework::XCppCommons::Primitives::String;

using std::exception;

static shared_ptr<Com::Wui::Framework::XCppCommons::Interfaces::IOHandler> output;
static LogLevel level = LogLevel::ALL;
static function<void(const string &)> onPrintHandler = nullptr;

class Pair {
 public:
    Pair(const string &$key, const string &$val) {
        this->key = $key;
        this->val = $val;
    }

    string key;
    string val;
};

// order is from first to last
#ifdef WIN_PLATFORM
std::vector<Pair> anonymizeMap = {
        {"TMP",          ""},
        {"USERPROFILE",  ""},
        {"HOMEPATH",     ""},
        {"USERDOMAIN",   ""},
        {"COMPUTERNAME", ""},
        {"USERNAME",     ""}
};
#else
std::vector<Pair> anonymizeMap = {
        {"HOME",        ""},
        {"HOSTNAME",    ""},
        {"LOGNAME",     ""},
        {"USER",        ""}
};
#endif

static string anonymize(const string &$data) {
#ifdef WIN_PLATFORM
    string keyFormat = "%{0}%";
#else
    string keyFormat = "${0}";
#endif

    string data = $data;
    for (auto &item : anonymizeMap) {
        if (item.val.empty()) {
            char *tmp = getenv(item.key.c_str());
            if (tmp != nullptr) {
                item.val = string(tmp);
            }
        }
        if (!item.val.empty()) {
            string tmp = boost::replace_all_copy(item.val, "\\", "<?ls?>");
            boost::replace_all(tmp, "/", "<?ls?>");
            boost::replace_all(tmp, "<?ls?>", R"((\\|\/))");
            boost::replace_all(tmp, ".", "\\.");
            boost::replace_all(tmp, "-", "\\-");
            boost::replace_all(tmp, "$", "\\$");
            boost::replace_all(tmp, "?", "\\?");
            boost::replace_all(tmp, "+", "\\+");
            boost::replace_all(tmp, "*", "\\*");
            boost::replace_all(tmp, "^", "\\^");

            boost::regex expr(tmp, boost::regex::icase);
            data = boost::regex_replace(data, expr, String::Format(keyFormat, item.key));
        }
    }

    return data;
}

void LogIt::Info(const string &$message, const TraceInfo &$trace) {
    LogIt::save(LogLevel::INFO, $message, $trace);
}

void LogIt::Warning(const string &$message, const TraceInfo &$trace) {
    LogIt::save(LogLevel::WARNING, $message, $trace);
}

void LogIt::Error(const string &$message, const TraceInfo &$trace) {
    LogIt::save(LogLevel::ERROR, $message, $trace);
}

void LogIt::Error(const exception &$exception, const TraceInfo &$trace) {
    LogIt::save(LogLevel::ERROR, $exception.what(), $trace);
}

void LogIt::Debug(const string &$message, const TraceInfo &$trace) {
    LogIt::save(LogLevel::DEBUG, $message, $trace);
}

LogLevel getLevel() {
    LogLevel retVal = level;
    return retVal;
}

void LogIt::setLevel(const LogLevel &$logLevel) {
    level = $logLevel;
}

void LogIt::save(const LogLevel &$logLevel, const string &$message, const TraceInfo &$trace) {
    if (output == nullptr) {
        LogIt::Init(LogLevel::ALL, nullptr);
    }
    string message = LogIt::formatter($logLevel, $message, output->NewLine(), $trace);

    const LogLevel tmp = getLevel();

    if (tmp == LogLevel::ALL || (tmp == LogLevel::DEBUG && $logLevel == LogLevel::DEBUG) ||
        (tmp == LogLevel::INFO && $logLevel == LogLevel::INFO) ||
        (tmp == LogLevel::WARNING && ($logLevel == LogLevel::INFO || $logLevel == LogLevel::WARNING)) ||
        (tmp == LogLevel::ERROR && ($logLevel == LogLevel::INFO || $logLevel == LogLevel::WARNING ||
                                    $logLevel == LogLevel::ERROR))) {
        output->Print(message);
        if (onPrintHandler != nullptr) {
            onPrintHandler(message);
        }
    }
}

string LogIt::formatter(const LogLevel &$logLevel, const string &$message, const string &$newLine,
                        const TraceInfo &$trace) {
    using boost::posix_time::ptime;
    using boost::posix_time::second_clock;
    using boost::posix_time::to_iso_extended_string;
    using boost::gregorian::day_clock;

    ptime todayUtc(day_clock::universal_day(), second_clock::universal_time().time_of_day());

    std::ostringstream oss;
    oss << $logLevel.toString() << ": " << Convert::TimeToGMTFormat(todayUtc);

    if ($trace) {
        string traceFile = String::Replace($trace.getFile(), "\\", "/");
        const string tmp = String::ToLowerCase(traceFile);
        const int from = String::IndexOf(tmp, String::Replace(EnvironmentArgs::getInstance().getProjectName(), "-", "/"));
        if (from >= 0) {
            traceFile = String::Substring(traceFile, static_cast<unsigned>(from));

            if (String::ContainsIgnoreCase(tmp, "source/cpp/" + traceFile)) {
                traceFile = "source/cpp/" + traceFile;
            } else if (String::ContainsIgnoreCase(tmp, "dependencies/" + traceFile)) {
                traceFile = "dependencies/" + traceFile;
            }
        }

        oss << " at " << traceFile << ":" << $trace.getLine();
    }

    oss << $newLine << "\t" << $message << $newLine;

    return anonymize(oss.str());
}

void LogIt::setOnPrint(const function<void(const string &)> &$handler) {
    onPrintHandler = $handler;
}

void LogIt::Init(const LogLevel &$logLevel,
                 const shared_ptr<Com::Wui::Framework::XCppCommons::Interfaces::IOHandler> &$handler) {
    LogIt::setLevel($logLevel);
    if (output == nullptr) {
        if ($handler == nullptr) {
            output = IOHandlerFactory::getHandler(IOHandlerType::CONSOLE, "LogIt");
        } else {
            output = $handler;
        }
        output->Init();
    }
}
