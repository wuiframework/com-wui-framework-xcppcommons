/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_CONVERT_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_CONVERT_HPP_

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * Convert class provides static methods focused on object conversions to string or to other type of objects.
     */
    class Convert {
     public:
        /**
         * Converts time to lexical GMT format.
         * @param $time Time structure to convert.
         * @return Returns string fo input time in GMT format.
         */
        static string TimeToGMTFormat(boost::posix_time::ptime &$time);

        /**
         * Converts time in milliseconds to time in seconds.
         * @param $time Specify time to convert.
         * @return Returns converted time.
         */
        static long TimeToSeconds(long long $time);

        /**
         * Converts time in string format "Thu, 22 Jun 2017 07:50:47 GMT" to unix timestamp.
         * @param $timeString Specify input time in string.
         * @return Returns timestamp from 1970-01-01 00:00:00
         */
        static long LastModifiedToTimestamp(const string &$timeString);

        /**
         * Cast any type to string, while $data has to be created from std::string. Note that check for any is string must be done before.
         * @param $data Specify $data to be converted.
         * @return
         */
        static const string &AnyToString(const any &$data);

        /**
         * Cast any $data to demand type T. Note that check for any is T must be done before.
         * @tparam T Specify type to be casted in.
         * @param $data Specify data to cast.
         * @return Returns reference to casted $data.
         */
        template<typename T>
        static const T &AnyToType(const any &$data) {
            return *boost::any_cast<T>(&$data);
        }
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_CONVERT_HPP_
