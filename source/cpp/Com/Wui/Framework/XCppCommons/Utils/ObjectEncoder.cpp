/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <utility>

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Utils::ObjectEncoder;

using std::vector;
using boost::archive::iterators::base64_from_binary;
using boost::archive::iterators::transform_width;

string ObjectEncoder::Base64(string $input) {
    using fromBinaryIterator = base64_from_binary<transform_width<string::const_iterator, 6, 8>>;
    auto tmp = string(fromBinaryIterator(begin($input)), fromBinaryIterator(end($input)));
    return tmp.append((3 - $input.size() % 3) % 3, '=');
}

string ObjectEncoder::WSS(string $input) {
    vector<unsigned char> output;
    string bytesRaw = std::move($input);
    unsigned char frame[10] = {0};

    int32_t indexStartRawData;
    int32_t length = bytesRaw.length();

    frame[0] = (unsigned char) 129;
    if (length <= 125) {
        frame[1] = (unsigned char) length;
        indexStartRawData = 2;
    } else if (length >= 126 && length <= 65535) {
        frame[1] = (unsigned char) 126;
        frame[2] = (unsigned char) ((length >> 8) & 255);
        frame[3] = (unsigned char) (length & 255);
        indexStartRawData = 4;
    } else {
        frame[1] = (unsigned char) 127;
        frame[2] = (unsigned char) (((uint64_t) length >> 56) & 255);
        frame[3] = (unsigned char) (((uint64_t) length >> 48) & 255);
        frame[4] = (unsigned char) (((uint64_t) length >> 40) & 255);
        frame[5] = (unsigned char) (((uint64_t) length >> 32) & 255);
        frame[6] = (unsigned char) ((length >> 24) & 255);
        frame[7] = (unsigned char) ((length >> 16) & 255);
        frame[8] = (unsigned char) ((length >> 8) & 255);
        frame[9] = (unsigned char) (length & 255);

        indexStartRawData = 10;
    }

    output.resize(static_cast<size_t>(indexStartRawData + length));

    int32_t inputIndex = 0;
    int32_t outputIndex = 0;

    for (inputIndex = 0; inputIndex < indexStartRawData; inputIndex++) {
        output[outputIndex] = frame[inputIndex];
        outputIndex++;
    }

    for (inputIndex = 0; inputIndex < length; inputIndex++) {
        output[outputIndex] = static_cast<unsigned char>(bytesRaw[inputIndex]);
        outputIndex++;
    }

    string string_out = string(output.begin(), output.end());

    return string_out;
}
