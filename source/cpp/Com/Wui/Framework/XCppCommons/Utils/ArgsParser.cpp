/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Utils::ArgsParser;
namespace po = boost::program_options;

int ArgsParser::Parse(Com::Wui::Framework::XCppCommons::Structures::ProgramArgs &$args, const int $argc,
                      const char **$argv) {
    try {
        po::store(po::command_line_parser($argc, $argv)
                          .options(*$args.getDescription().get())
                          .run(),
                  *$args.getVariablesMap().get());
        po::notify(*$args.getVariablesMap());

        if ($args.IsHelp()) {
            $args.PrintHelp();
            return 1;
        }

        if ($args.IsVersion()) {
            $args.PrintVersion();
            return 1;
        }
    }
    catch (std::exception &ex) {
        LogIt::Error(ex);
        return -1;
    }

    return 0;
}
