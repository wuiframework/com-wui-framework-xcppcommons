/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_IPCPIPEOBSERVER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_IPCPIPEOBSERVER_HPP_

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * IpcPipeObserver class holds interaction logic for bi-dir IPC.
     */
    class IpcPipeObserver {
     public:
        /**
         * Constructs observer with specified name and pid.
         * @param $name An IPC pipe name.
         * @param $pid A pid of server process to create or connect to from client.
         */
        IpcPipeObserver(const string &$name, const int $pid);

        /**
         * @param $isServer Specify observer type, true for server or false for client.
         * @param $timeout Specify client maximum timeaut to connect to server, ignored in server configuration.
         * @return Returns true if succeed, false otherwise.
         */
        bool Initialize(bool $isServer, unsigned int $timeout = 0);

        /**
         * @return Returns observer name.
         */
        const string &getName() const;

        /**
         * @param $command A command to send.
         */
        void Send(const string &$command);

        /**
         * @return Returns received message.
         */
        const string Receive();

     private:
        IpcPipeObserver(const IpcPipeObserver &) = delete;

        void operator=(const IpcPipeObserver &) = delete;

        string name;
        unique_ptr<Com::Wui::Framework::XCppCommons::Structures::IpcPipe> pipe;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_IPCPIPEOBSERVER_HPP_
