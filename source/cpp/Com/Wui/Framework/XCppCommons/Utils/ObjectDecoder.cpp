/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Utils::ObjectDecoder;

using boost::archive::iterators::transform_width;
using boost::archive::iterators::binary_from_base64;

// cppcheck-suppress unusedFunction
string ObjectDecoder::Base64(string $input) {
    using fromB64Iterator = transform_width<binary_from_base64<string::const_iterator>, 8, 6>;
    return boost::algorithm::trim_right_copy_if(
            string(fromB64Iterator(std::begin($input)), fromB64Iterator(std::end($input))), [](char $c) {
                return $c == '\0';
            });
}

// cppcheck-suppress unusedFunction
string ObjectDecoder::WSS(const char *$input, const int $length) {
    char secondByte = $input[1];
    int32_t dataLength = secondByte & 127;
    int32_t indexFirstMask = 2;

    if (dataLength == 126) {
        indexFirstMask = 4;
    } else if (dataLength == 127) {
        indexFirstMask = 10;
    }

    char keys[4];
    memcpy(&keys, &$input[indexFirstMask], 4 * sizeof(char));
    int32_t indexFirstDataByte = indexFirstMask + 4;
    auto *decoded = new char[$length - indexFirstDataByte];

    for (int32_t inputIndex = indexFirstDataByte, outputIndex = 0; inputIndex < $length; inputIndex++, outputIndex++) {
        decoded[outputIndex] = $input[inputIndex] ^ keys[outputIndex % 4];
    }

    decoded[$length - indexFirstDataByte - 1] = '\0';
    string output(decoded);
    delete[] decoded;

    return output;
}
