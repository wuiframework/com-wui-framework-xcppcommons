/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTDECODER_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTDECODER_HPP_

namespace Com::Wui::Framework::XCppCommons::Utils {
    /**
     * ObjectDecoder class provides static methods focused on decoding of encoded object or strings.
     */
    class ObjectDecoder {
     public:
        /**
         * @param $input String value, which should be decoded.
         * @return Returns decoded string, if input string has been recognized as base64 encoded.
         */
        static string Base64(string $input);

        /**
         * Decode string from WSS data.
         * @param $input Specify input data pointer.
         * @param $length Specify length.
         * @return Returns decoded string.
         */
        static string WSS(const char *$input, const int $length);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_UTILS_OBJECTDECODER_HPP_
