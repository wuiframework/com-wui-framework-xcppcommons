/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_BASEENUM_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_BASEENUM_HPP_

namespace Com::Wui::Framework::XCppCommons::Primitives {
    /**
     * This templated class defines Base for each Enums stored as string..
     * @tparam T Class type of current enum.
     */
    template<class T>
    class BaseEnum {
     public:
        /**
         * Constructs default BaseEnum.
         */
        BaseEnum()
                : valueIt(T::keyIndex.end()) {
        }

        explicit BaseEnum(const string &$val, const string &$alias = "") {
            valueIt = T::keyIndex.find($val);
            if (valueIt == T::keyIndex.end()) {
                valueIt = T::keyIndex.insert(std::make_pair($val, std::make_pair(T::keyIndex.size(), $alias))).first;
            }
            T::keyMap.insert(std::make_pair(valueIt->second.first, static_cast<T *>(this)));
        }

        virtual ~BaseEnum() {}

        /**
         * Creates BaseEnum from specified type name.
         * @param $type Specify enum type.
         * @return Returns BaseEnum object.
         */
        static const T &fromString(const string &$type) {
            auto it = std::find_if(T::keyIndex.cbegin(), T::keyIndex.cend(),
                                   [&](const std::pair<string, std::pair<size_t, string>> &$item) -> bool {
                                       bool match = false;
                                       if (($item.first == boost::to_upper_copy($type)) ||
                                           (boost::to_upper_copy($item.second.second) == boost::to_upper_copy($type))) {
                                           match = true;
                                       }
                                       return match;
                                   });
            if (it != T::keyIndex.end()) {
                return *(T::keyMap[it->second.first]);
            }
            return T::unknown;
        }

        /**
         * @return Returns string name of enumeration item
         */
        const string &toString() const {
            if (valueIt != T::keyIndex.end()) {
                if (!valueIt->second.second.empty()) {
                    return valueIt->second.second;
                }
                return valueIt->first;
            }
            return T::keyEmpty;
        }

        bool operator==(const BaseEnum &other) const {
            return valueIt == other.valueIt;
        }

        bool operator==(const string &$key) const {
            return valueIt == this->fromString($key).valueIt;
        }

        bool operator!=(const BaseEnum &other) const {
            return valueIt != other.valueIt;
        }

        bool operator!=(const string &$key) const {
            return valueIt != this->fromString($key).valueIt;
        }

        operator string() const {
            return toString();
        }

     private:
        std::unordered_map<string, std::pair<size_t, string>>::iterator valueIt;
    };

    template<class T>
    std::ostream &operator<<(std::ostream &$os, const Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<T> &$obj) {
        $os << $obj.toString();
        return $os;
    }
}

#define WUI_ENUM_DECLARE(SubClass) \
 private: \
    explicit SubClass(const string &$str, const string &$alias = "") \
      : Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<SubClass>($str, $alias) {} \
    static std::unordered_map<size_t, SubClass *> keyMap; \
    static std::unordered_map<std::string, std::pair<size_t, string>> keyIndex; \
    static string keyEmpty; \
    friend class Com::Wui::Framework::XCppCommons::Primitives::BaseEnum<SubClass>; \
 public: \
    static const SubClass unknown; \
    SubClass() {}

#define WUI_ENUM_IMPLEMENT(SubClass) \
    std::unordered_map<std::string, std::pair<size_t, string>> SubClass::keyIndex; \
    std::unordered_map<size_t, SubClass *> SubClass::keyMap; \
    string SubClass::keyEmpty; \
    const SubClass SubClass::unknown(string("unknown"));

#define INTERN_WUI_ENUM_CONST_IMPLEMENT_1(SubClass, constName) \
    const SubClass SubClass::constName(#constName);

#define INTERN_WUI_ENUM_CONST_IMPLEMENT_2(SubClass, constName, alias) \
    const SubClass SubClass::constName(#constName, alias);

#define INTERN_WUI_ENUM_CONST_IMPLEMENT_SELECTOR(x, A, B, C, FUNC, ...) FUNC

#define WUI_ENUM_CONST_IMPLEMENT(...) INTERN_WUI_ENUM_CONST_IMPLEMENT_SELECTOR( \
    , ##__VA_ARGS__, \
    INTERN_WUI_ENUM_CONST_IMPLEMENT_2(__VA_ARGS__), \
    INTERN_WUI_ENUM_CONST_IMPLEMENT_1(__VA_ARGS__) \
)

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_BASEENUM_HPP_
