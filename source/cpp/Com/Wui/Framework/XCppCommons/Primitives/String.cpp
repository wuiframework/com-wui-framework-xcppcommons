/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <boost/format.hpp>
#include <boost/regex.hpp>
#include <boost/crc.hpp>
#include <boost/uuid/sha1.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>

#include "../sourceFilesMap.hpp"

#ifdef WIN_PLATFORM
#define NEWLINE "\r\n"
#else
#define NEWLINE "\n"
#endif

using Com::Wui::Framework::XCppCommons::Primitives::String;

using boost::contains;
using boost::starts_with;

char String::chars[] = "abcdefghijklmnopqrstuvwxyz"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "1234567890"
        "!@#$%^&*()"
        "`~-_=+[{]}\\|;:'\",<.>/? ";

string String::NewLine() {
    return NEWLINE;
}

string String::Space(int $count) {
    if ($count < 0) {
        return string();
    }
    return string(static_cast<unsigned int>($count), ' ');
}

string String::ToUpperCase(const string &$input) {
    return boost::algorithm::to_upper_copy($input);
}

string String::ToLowerCase(const string &$input) {
    return boost::algorithm::to_lower_copy($input);
}

bool String::Contains(const string &$input, const string &$searchString) {
    return !$searchString.empty() && boost::contains($input, $searchString);
}

bool String::StartsWith(const string &$input, const string &$searchString) {
    return boost::starts_with($input, $searchString);
}

string String::Replace(const string &$input, const string &$searchString, const string &$replaceBy) {
    string retVal = $input;
    boost::replace_all(retVal, $searchString, $replaceBy);
    return retVal;
}

string String::Remove(const string &$input, const string &$searchString) {
    return Replace($input, $searchString, "");
}

string String::FormatPrepare(const string &$input) {
    string tmp = String::Replace($input, "%", "%%");
    tmp = String::Replace(String::Replace(tmp, "}}", "#@*$*"), "{{", "*$*@#");
    boost::regex expression("{(\\d+)(?:|:([a-zA-Z0-9]+))}");
    boost::match_results<string::const_iterator> what;
    std::string::const_iterator start = tmp.begin(), end = tmp.end();
    boost::match_flag_type flags = boost::match_default;
    string copy = tmp;
    std::map<string, string> repMap;
    while (boost::regex_search(start, end, what, expression, flags)) {
        int cnt = boost::lexical_cast<int>(what[1]) + 1;
        string res = String::Replace(what[0], what[1], std::to_string(cnt));
        if (repMap.find(what[0]) == repMap.end()) {
            repMap.emplace(what[0], res);
        }
        start = what[0].second;
        flags |= boost::match_prev_avail;
        flags |= boost::match_not_bob;
    }

    for (auto it = repMap.rbegin(); it != repMap.rend(); ++it) {
        boost::replace_all(copy, it->first, it->second);
    }
    copy = String::Replace(String::Replace(copy, "{", "%"), "*$*@#", "{");
    copy = String::Replace(String::Replace(copy, "}", "%"), "#@*$*", "}");
    return copy;
}

string String::Tab(int $count) {
    if ($count < 0) {
        return string();
    }
    return string(static_cast<unsigned int>($count), '\t');
}

unsigned int String::Length(const string &$input) {
    return $input.length();
}

bool String::IsEmpty(const string &$input) {
    return $input.empty();
}

int String::IndexOf(const string &$input, const string &$searchString, bool $fromStartPosition, int $offset) {
    if ($fromStartPosition) {
        return $input.find($searchString, static_cast<unsigned int>($offset));
    }
    return $input.find_last_of($searchString, Length($input) - static_cast<unsigned int>($offset));
}

string String::getCharacterAt(const string &$input, int $index) {
    if ($index >= static_cast<int>(Length($input)) || $index < 0 || $input.empty()) {
        return string();
    }

    string charAt;
    charAt += $input[$index];   // add desired char to the empty string and get one character only

    return charAt;
}

int String::getCodeAt(const string &$input, int $index) {
    string character = getCharacterAt($input, $index);

    if (character.empty()) {   // if getCharacterAt found no such character at $index, return -1 (fail)
        return -1;
    }

    return static_cast<int>(character[0]);      // else return code of the found character
}

uint64_t String::getCrc(const string &$input) {
    boost::crc_32_type result;
    result.process_bytes($input.data(), $input.length());
    return result.checksum();
}

string String::getSha1(const string &$input) {
    boost::uuids::detail::sha1 sha1;
    sha1.process_bytes($input.data(), $input.size());

    unsigned hash[5] = {0};
    sha1.get_digest(hash);

    char buf[41] = {0};

    for (int i = 0; i < 5; i++) {
        std::snprintf(buf + (i << 3), sizeof(buf), "%08x", hash[i]);
    }

    return std::string(buf);
}

bool String::Contains(const string &$input, const std::vector<string> &$searchStrings) {
    for (auto &elem : $searchStrings) {
        if (Contains($input, elem)) {
            return true;
        }
    }
    return false;
}

bool String::ContainsIgnoreCase(const string &$input, const string &$searchString) {
    if ($searchString.empty()) {
        return false;
    }

    auto it = std::search(
            $input.begin(), $input.end(),
            $searchString.begin(), $searchString.end(),
            [](char ch1, char ch2) {
                return std::tolower(ch1) == std::tolower(ch2);
            });
    return (it != $input.end());
}

bool String::ContainsIgnoreCase(const string &$input, const std::vector<string> &$searchStrings) {
    for (auto &elem : $searchStrings) {
        if (ContainsIgnoreCase($input, elem)) {
            return true;
        }
    }
    return false;
}

int String::OccurrenceCount(const string &$input, const string &$searchFor) {
    if ($searchFor.empty()) {
        return 0;
    }

    int count = 0;
    size_t nPos = $input.find($searchFor, 0);
    while (nPos != string::npos) {
        count++;
        nPos = $input.find($searchFor, nPos + 1);
    }
    return count;
}

bool String::EndsWith(const string &$input, const string &$searchString) {
    return boost::ends_with($input, $searchString);
}

bool String::PatternMatched(const string &$pattern, const string &$value) {
    string value = $value;

    if (boost::iequals($value, $pattern)) {
        return true;
    }

    if (Contains($pattern, "*")) {
        std::vector<string> parts;
        boost::split(parts, $pattern, boost::is_any_of("*"));
        unsigned int partsLength = parts.size();
        unsigned int lastPart = partsLength - 1;
        unsigned int partIndex;

        for (partIndex = 0; partIndex < partsLength; partIndex++) {
            if (!IsEmpty(parts[partIndex])) {
                if (partIndex == 0 && !StartsWith(value, parts[partIndex])) {
                    return false;
                }

                if (partIndex == lastPart && !EndsWith(value, parts[partIndex])) {
                    return false;
                }

                int searchIndex = IndexOf(value, parts[partIndex]);
                if (searchIndex == static_cast<int>(std::string::npos)) {
                    return false;
                }

                if (searchIndex == 0) {
                    if ((partIndex == lastPart - 1 && IsEmpty(parts[partIndex + 1])) || (partIndex < lastPart - 1)) {
                        value = Substring(value, searchIndex + Length(parts[partIndex]), Length(value));
                    }
                } else {
                    if ((partIndex == 1 && IsEmpty(parts[0])) || (partIndex > 1)) {
                        value = Substring(value, searchIndex + Length(parts[partIndex]), Length($value));
                    }
                }
            }
        }
    } else {
        return false;
    }

    return true;
}

string String::Substring(const string &$input, unsigned $start, unsigned $end) {
    if ($start > $input.size()) {
        return string();
    }
    return $input.substr($start, $end - $start);
}

std::vector<string> String::Split(const string &$input, const std::vector<string> &$splitBy) {
    std::vector<string> output;
    if (!$splitBy.empty()) {
        std::vector<std::vector<string>> buffer;
        std::vector<string> tmp;
        tmp.push_back($input);

        buffer.push_back(tmp);

        unsigned splitIndex;
        for (splitIndex = 0; splitIndex < $splitBy.size(); splitIndex++) {
            string splitter = $splitBy[splitIndex];
            std::vector<std::vector<string>> lastData = buffer;
            buffer.clear();

            unsigned dataIndex;
            unsigned dataPartIndex;
            unsigned lastDataLength = lastData.size();
            for (dataIndex = 0; dataIndex < lastDataLength; dataIndex++) {
                for (dataPartIndex = 0; dataPartIndex < lastData[dataIndex].size(); dataPartIndex++) {
                    boost::split(tmp, lastData[dataIndex][dataPartIndex], boost::is_any_of(splitter));
                    buffer.push_back(tmp);
                }
            }
        }

        unsigned bufferIndex;
        unsigned partIndex;
        for (bufferIndex = 0; bufferIndex < buffer.size(); bufferIndex++) {
            for (partIndex = 0; partIndex < buffer[bufferIndex].size(); partIndex++) {
                output.push_back(buffer[bufferIndex][partIndex]);
            }
        }
    } else {
        output.push_back($input);
    }

    return output;
}

string String::StripSlashes(const string &$input) {
    if ($input.empty()) {
        return string();
    }

    string input = Remove($input, "\\");
    input = Replace(input, "\0", "\u0000");
    return input;
}

string String::StripTags(const string &$input, const string &$allowed) {
    boost::random::mt19937 gen;
    boost::random::uniform_int_distribution<> index_dist(0, std::strlen(String::chars));

    string randomString;
    string input = $input;

    if (!$allowed.empty()) {
        unsigned randomStrinLen = 10;
        for (unsigned i = 0; i < randomStrinLen; ++i) {
            randomString += chars[index_dist(gen)];
        }
        input = Replace($input, $allowed, randomString);
    }

    boost::regex commentsAndJsTags(R"(<!--[\s\S]*?-->|<script[\s\S]*?<\/script>)");
    boost::regex tags("<\\/?([a-z][a-z0-9]*)\\b[^>]*>");

    input = boost::regex_replace(input, commentsAndJsTags, "");
    input = boost::regex_replace(input, tags, "");

    if (!$allowed.empty()) {
        input = Replace(input, randomString, $allowed);
    }
    return input;
}

string String::StripComments(const string &$input) {
    if ($input.empty()) {
        return string("");
    }
    boost::regex commentsAndJsTags(R"(\/\*([\s\S]*?)\*\/|\/\/.*(?=[\n\r]))");
    return boost::regex_replace($input, commentsAndJsTags, "");
}

string String::HardWrap(const string &$input, unsigned $length) {
    string output;
    unsigned wrapSize = $length;
    unsigned wrapsCount = Length($input) / wrapSize + 1;
    unsigned index;

    for (index = 0; index < wrapsCount; index++) {
        string wrapPart = $input;
        wrapPart = Substring(wrapPart, index * wrapSize, (index + 1) * wrapSize);
        output += wrapPart;
        if (index < wrapsCount - 1) {
            output += NewLine();
        }
    }

    return output;
}

std::vector<char> String::ToArray(const string &$input) {
    return std::vector<char>($input.begin(), $input.end());
}

int String::ToInteger(const string &$input) {
    int result;
    try {
        result = boost::lexical_cast<int>($input);
    }
    catch (std::exception &ex) {
        result = std::numeric_limits<int>::max();
    }
    return result;
}

double String::ToDouble(const string &$input) {
    double result;
    try {
        result = boost::lexical_cast<double>($input);
    }
    catch (boost::bad_lexical_cast &ex) {
        result = std::numeric_limits<double>::quiet_NaN();
    }
    return result;
}

bool String::ToBoolean(const string &$input) {
    string input = boost::algorithm::to_lower_copy($input);
    return !$input.empty() && (IsEqual(input, "1") || IsEqual(input, "true") || IsEqual(input, "on"));
}

string String::IntToHex(int $input) {
    std::stringstream stream;
    stream << std::hex << $input;
    return stream.str();
}

int String::ToHexadecimal(const string &$input) {
    string hexString;
    unsigned index;
    for (index = 0; index < Length($input); index++) {
        hexString += IntToHex(getCodeAt($input, index));
    }

    int output = std::stoi(hexString, nullptr, 16);

    return output;
}

bool String::IsEqual(const string &$input1, const string &$input2) {
    return boost::iequals($input1, $input2);
}

bool String::VersionIsLower(const string &$value, const string &$pattern) {
    if (String::IsEqual($value, $pattern)) {
        return false;
    }

    if ($value.empty() || $pattern.empty()) {
        return false;
    }

    std::vector<string> valueParts = String::Split($value, std::vector<string>({"."}));
    std::vector<string> patternParts = String::Split($pattern, std::vector<string>({"."}));

    bool status = false;

    unsigned index;
    unsigned partsLength = patternParts.size();
    for (index = 0; index < partsLength; index++) {
        if (index < valueParts.size()) {
            if (valueParts[index] == patternParts[index] || patternParts[index] == "x" || patternParts[index] == "*") {
                continue;
            }
            if (String::ToInteger(valueParts[index]) > String::ToInteger(patternParts[index])) {
                break;
            }
            if (String::ToInteger(valueParts[index]) < String::ToInteger(patternParts[index])) {
                status = true;
                break;
            }
        } else {
            break;
        }
    }
    return status;
}
