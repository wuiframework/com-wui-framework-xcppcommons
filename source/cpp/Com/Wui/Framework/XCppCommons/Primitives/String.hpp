/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_STRING_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_STRING_HPP_

namespace Com::Wui::Framework::XCppCommons::Primitives {
    /**
     * String class provides static methods focused on string validations and handling.
     */
    class String : private Com::Wui::Framework::XCppCommons::Interfaces::INonCopyable,
                   private Com::Wui::Framework::XCppCommons::Interfaces::INonMovable {
     public:
        /**
         * @return Returns new line in selected format.
         */
        static string NewLine();

        /**
         * @param [$count=1] Set number or returned spaces.
         * @return Returns escaped space char.
         */
        static string Space(int $count = 1);

        /**
         * @param [$count=1] Set number or returned tabs.
         * @return Returns escaped tab char.
         */
        static string Tab(int $count = 1);

        /**
         * @param $input Input string to count length.
         * @return Length of input string.
         */
        static unsigned int Length(const string &$input);

        /**
         * @param $input String to check if empty.
         * @return True/false if empty.
         */
        static bool IsEmpty(const string &$input);

        /**
         * @param $input Input string for conversion.
         * @return Returns input string in upper case format.
         */
        static string ToUpperCase(const string &$input);

        /**
         * @param $input Input string for conversion.
         * @return Returns input string in lower case format.
         */
        static string ToLowerCase(const string &$input);

        /**
         * @param $input Input string for validation.
         * @param $searchString String or character witch will be searched.
         * @param $fromStartPosition [$fromStartPosition=true] If true searching is done from start position,
             * if false searching is done from end to start position.
         * @param $offset [$offset=0] Set start position for search.
         * @return Returns index of searched string value in input string or std::string::npos if value has not been found.
         */
        static int IndexOf(const string &$input, const string &$searchString, bool $fromStartPosition = true,
                           int $offset = 0);

        /**
         * @param $input Input string for search.
         * @param $index Index of desired character.
         * @return Returns desired character.
         */
        static string getCharacterAt(const string &$input, int $index);

        /**
         * @param $input Input string for search.
         * @param $index Index of desired character.
         * @return Returns Unicode value of desired character, if index exist, otherwise null.
         */
        static int getCodeAt(const string &$input, int $index);

        /**
         * @param $input Input string for calculation.
         * @return Returns crc32 checksum useful for validation of string manipulation.
         */
        static uint64_t getCrc(const string &$input);

        /**
         * @param $input String value for, which should be calculated hash value.
         * @return Returns irreversible hash of input string useful for e.g. password check.
         */
        static string getSha1(const string &$input);

        /**
          * @param $input Input string for search.
          * @param $searchString String key to search for.
          * @return Returns true if found in $input string, false otherwise.
          */
        static bool Contains(const string &$input, const string &$searchString);

        /**
         * Overload for array of strings.
         * @param $input Input string for search.
         * @param $searchStrings String keys to search for.
         * @return Returns true if any of $searchStrings found in $input string, false otherwise.
         */
        static bool Contains(const string &$input, const std::vector<string> &$searchStrings);

        /**
         * Case insensitive implementation of Contain.
          * @param $input Input string for search.
          * @param $searchString String key to search for.
          * @return Returns true if found in $input string, false otherwise.
          */
        static bool ContainsIgnoreCase(const string &$input, const string &$searchString);

        /**
         * Case insensitive implementation of Contain for string arrays.
         * Overload for array of strings.
         * @param $input Input string for search.
         * @param $searchStrings String keys to search for.
         * @return Returns true if any of $searchStrings found in $input string, false otherwise.
         */
        static bool ContainsIgnoreCase(const string &$input, const std::vector<string> &$searchStrings);

        /**
         * @param $input Input string for validation.
         * @param $searchFor String or character witch will be searched.
         * @return Returns count of searched value in input string.
         */
        static int OccurrenceCount(const string &$input, const string &$searchFor);

        /**
         * @param $input Input string for validation.
         * @param $searchString String or character witch will be validated.
         * @return Returns true if input string starts with searched value otherwise false.
         */
        static bool StartsWith(const string &$input, const string &$searchString);

        /**
         * @param $input Input string for search.
         * @param $searchString String subset to search for in $input string.
         * @return Returns true if $input string ends with $searchString, false otherwise.
         */
        static bool EndsWith(const string &$input, const string &$searchString);

        /**
         * @param $pattern Validation pattern for value match with wildcard level of freedom
         * @param $value Actual string value for match with pattern
         * @return Returns true, if pattern has been matched, otherwise returns false.
         */
        static bool PatternMatched(const string &$pattern, const string &$value);

        /**
         * @param $input Input string for search in.
         * @param $start Start index of substring.
         * @param $end End index of substring, if is not equal to end of input string.
         * @return Returns substring, if start and end index has been found, otherwise null.
         */
        static string Substring(const string &$input, unsigned $start, unsigned $end = static_cast<unsigned>(std::string::npos));

        /**
         * @param $input Input string to replace.
         * @param $searchString A substring to be searched for.
         * @param $replaceBy A substitute string.
         * @return Returns modified copy of $input string.
         */
        static string Replace(const string &$input, const string &$searchString, const string &$replaceBy);

        /**
         * @param $input Input string to be modified.
         * @param $searchString A substring to be searched for.
         * @return Returns copy of $input string with removed all matching $searchString's.
         */
        static string Remove(const string &$input, const string &$searchString);

        /**
         * Format argument to string. Only positional argument are supported ({#} where # is index started from 0,
         * to escape bracket use them twice like {{!this!}} returns !this!).
         * @param $input Input string for formatting.
         * @param $args Variadic argument to be searched for in input string.
         * @return Returns input string with replaced values byt positional argument.
         */
        template<typename... Args>
        static string Format(const string $input, Args... $args) {
            boost::format message(FormatPrepare($input));
            string output = $input;
            try {
                output = Format(message, $args...);
            } catch (...) {
                // ignore message in incorrect format
            }
            return output;
        }

        /**
         * @param $input Input string for split.
         * @param $splitBy Open-ended string args for search in input string.
         * @return Returns array of strings as result of split.
         */
        static std::vector<string> Split(const string &$input, const std::vector<string> &$splitBy);

        /**
         * Double backslashes are made into a single backslash.
         * @param $input Input string for conversion.
         * @return Returns a string with backslashes stripped off.
         */
        static string StripSlashes(const string &$input);

        /**
         * @param $input Input string for conversion.
         * @param $allowed Skip strip of those tags.
         * @return Returns a string with HTML and JavaScript tags stripped off.
         */
        static string StripTags(const string &$input, const string &$allowed = string());

        /**
         * @param $input Input string for conversion.
         * @return Returns a string with comments stripped off.
         */
        static string StripComments(const string &$input);

        /**
         *
         * @param $input Input string for conversion.
         * @param $length Set maximum length of each line.
         * @return Returns a string separated to lines with defined maximum length regardless on spaces positions.
         */
        static string HardWrap(const string &$input, unsigned $length = 300);

        /**
         * @param $input Input string for conversion.
         * @return Returns array of characters which input string contains.
         */
        static std::vector<char> ToArray(const string &$input);

        /**
         * @param $input  Input string for conversion.
         * @return Returns number representation of input string. Conversion is done in numeral system 10.
         */
        static int ToInteger(const string &$input);

        /**
         * @param $input  Input string for conversion.
         * @return Returns number representation of input string. Conversion is done with floating point.
         */
        static double ToDouble(const string &$input);

        /**
         * @param $input  Input string for conversion.
         * @return Returns boolean representation of input string.
         */
        static bool ToBoolean(const string &$input);

        /**
         * @param $input  Input string for conversion.
         * @return Returns hexadecimal representation of input string, if it is possible, otherwise empty string.
         */
        static int ToHexadecimal(const string &$input);

        /**
         * @param $value Input string for validation.
         * @param $pattern Pattern, which should be used for validation condition.
         * @return Returns true, if version is lower than expected pattern, otherwise false.
         */
        static bool VersionIsLower(const string &$value, const string &$pattern);

        /**
         * This function is canse insensitive.
         * @param $input1 First input string to compare.
         * @param $input2 Second input string to compare.
         * @return Returns true if both input strings are equal, false otherwise.
         */
        static bool IsEqual(const string &$input1, const string &$input2);

     private:
        static inline string Format(boost::format &$message) {  // NOLINT(runtime/references)
            return $message.str();
        }

        template<typename TValue, typename... Args>
        static string Format(boost::format &$message, TValue $arg, Args... $args) {  // NOLINT(runtime/references)
            $message % $arg;
            return Format($message, $args...);
        }

        static string FormatPrepare(const string &$input);

        static char chars[];

        static string IntToHex(int $input);
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_STRING_HPP_
