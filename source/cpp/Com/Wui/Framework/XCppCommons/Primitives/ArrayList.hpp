/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_ARRAYLIST_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_ARRAYLIST_HPP_

namespace Com::Wui::Framework::XCppCommons::Primitives {
    class ArrayList {
     public:
        template<typename Map>
        static bool Equal(Map const &lhs, Map const &rhs) {
            return (lhs.size() == rhs.size()) && std::equal(lhs.begin(), lhs.end(), rhs.begin(), [](auto a, auto b) {
                return (boost::iequals(a.first, b.first) && boost::iequals(a.second, b.second));
            });
        }

        static string Join(const std::vector<string> &$vector, const string &$delimiter = " ");

     public:
        ArrayList();

        int Length() const;

     private:
        int length;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_PRIMITIVES_ARRAYLIST_HPP_
