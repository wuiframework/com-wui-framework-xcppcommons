/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

namespace Com::Wui::Framework::XCppCommons::Primitives {
    // cppcheck-suppress uninitMemberVar
    ArrayList::ArrayList()
            : length(0) {}

    int ArrayList::Length() const {
        return this->length;
    }

    string ArrayList::Join(const std::vector<string> &$vector, const string &$delimiter) {
        return std::accumulate($vector.begin(), $vector.end(), string(),
                               [&](const string &a, const string &b) {
                                   string retVal;
                                   if (a.empty()) {
                                       retVal = b;
                                   } else {
                                       retVal = a + $delimiter + b;
                                   }
                                   return retVal;
                               });
    }
}
