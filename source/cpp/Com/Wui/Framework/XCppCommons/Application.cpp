/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Application;

using Com::Wui::Framework::XCppCommons::Utils::LogIt;
using Com::Wui::Framework::XCppCommons::Enums::LogLevel;
using Com::Wui::Framework::XCppCommons::Enums::IOHandlerType;
using Com::Wui::Framework::XCppCommons::IOApi::IOHandlerFactory;
using Com::Wui::Framework::XCppCommons::EnvironmentArgs;
using Com::Wui::Framework::XCppCommons::Structures::ProgramArgs;
using Com::Wui::Framework::XCppCommons::Utils::ArgsParser;

int Application::Run(const int $argc, const char **$argv) {
    ProgramArgs ba;

    if (ArgsParser::Parse(ba, $argc, $argv) == 0) {
        LogIt::Info("This is Com::Wui::Framework::XCppCommons library");
        std::cout << "This is Com::Wui::Framework::XCppCommons library" << std::endl;
    }
    return 0;
}
