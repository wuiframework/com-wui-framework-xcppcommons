/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Structures::IpcPipe;
using Com::Wui::Framework::XCppCommons::Events::ThreadPool;
using Com::Wui::Framework::XCppCommons::Events::Args::EventArgs;

namespace ip = boost::interprocess;

IpcPipe::IpcPipe(const string &$name, bool $isServer)
        : pipeName($name),
          isServer($isServer) {
    this->pipePartNameIn = this->pipeName + "_in";
    this->pipePartNameOut = this->pipeName + "_out";
}

IpcPipe::~IpcPipe() {
    if (this->isServer) {
        ip::message_queue::remove(this->pipePartNameIn.c_str());
        ip::message_queue::remove(this->pipePartNameOut.c_str());
    }
}

const string &IpcPipe::getPipeName() const {
    return this->pipeName;
}

const string &IpcPipe::getMsgDelimiter() const {
    return this->msgDelimiter;
}

void IpcPipe::setMsgDelimiter(const string &$msgDelimiter) {
    this->msgDelimiter = $msgDelimiter;
}

bool IpcPipe::Open(unsigned int $timeout) {
    bool retVal = true;
    if (this->isServer) {
        retVal &= this->CreateMsgQueue(this->pipePartNameIn, this->msgQueueIn);
        retVal &= this->CreateMsgQueue(this->pipePartNameOut, this->msgQueueOut);
    } else {
        retVal &= this->OpenMsgQueue(this->pipePartNameIn, this->msgQueueIn, $timeout);
        retVal &= this->OpenMsgQueue(this->pipePartNameOut, this->msgQueueOut, $timeout);
    }

    return retVal;
}

void IpcPipe::Write(const string &$message) {
    shared_ptr<ip::message_queue> activeQueue;
    if (this->isServer) {
        activeQueue = this->msgQueueOut;
    } else {
        activeQueue = this->msgQueueIn;
    }

    if (activeQueue) {
        try {
            activeQueue->send(($message + this->msgDelimiter).c_str(),
                              $message.size() + this->msgDelimiter.size(), 0);
        } catch (ip::interprocess_exception &ex) {
            std::cout << ex.what() << std::endl;
            return;
        }
    }
}

const string IpcPipe::Read() {
    shared_ptr<ip::message_queue> activeQueue;
    if (this->isServer) {
        activeQueue = this->msgQueueIn;
    } else {
        activeQueue = this->msgQueueOut;
    }

    if (activeQueue) {
        try {
            ip::message_queue::size_type receivedSize;
            unsigned int priority;
            string receivedString;
            receivedString.resize(this->MAX_MESSAGE_LENGTH);
            if (activeQueue->try_receive(&receivedString[0], this->MAX_MESSAGE_LENGTH, receivedSize, priority)) {
                return receivedString.substr(0, receivedString.find(this->msgDelimiter));
            }
        } catch (ip::interprocess_exception &ex) {
            std::cout << ex.what() << std::endl;
        }
    }
    return "";
}

bool IpcPipe::CreateMsgQueue(const string &$name, shared_ptr<ip::message_queue> &$msgQueue) {
    try {
        ip::message_queue::remove($name.c_str());
        ip::permissions permissions;
        permissions.set_unrestricted();
        $msgQueue.reset(new ip::message_queue(
                ip::create_only,
                $name.c_str(),
                this->MAX_MESSAGES_IN_QUEUE,
                this->MAX_MESSAGE_LENGTH,
                permissions));
        return true;
    } catch (ip::interprocess_exception &ex) {
        ip::message_queue::remove($name.c_str());
        $msgQueue = nullptr;
        return false;
    }
}

bool IpcPipe::OpenMsgQueue(const string &$name, shared_ptr<ip::message_queue> &$msgQueue, unsigned int $timeout) {
    bool retVal = false;

    auto openFunc = [&retVal, &$msgQueue, &$name]() -> bool {
        try {
            $msgQueue.reset(new ip::message_queue(
                    ip::open_only,
                    $name.c_str()));
            retVal = true;
            return true;
        } catch (ip::interprocess_exception &ex) {
            $msgQueue = nullptr;
            return false;
        }
    };

    if ($timeout == 0) {
        openFunc();
    } else {
        ThreadPool threadPool;
        threadPool.AddThread("open_pipe:" + $name, [&retVal, &$msgQueue, &$name,
                &$timeout, &openFunc](const EventArgs &args) {
            bool isRunning = true;
            boost::chrono::high_resolution_clock::time_point startTime = boost::chrono::high_resolution_clock::now();
            while (isRunning) {
                if (openFunc()) {
                    isRunning = false;
                    continue;
                }

                auto duration = boost::chrono::duration_cast<boost::chrono::milliseconds>(
                        boost::chrono::high_resolution_clock::now() - startTime).count();

                if (duration >= $timeout) {
                    isRunning = false;
                    continue;
                }
                boost::this_thread::sleep_for(boost::chrono::milliseconds(1));
            }
        });

        threadPool.Execute();
    }
    return retVal;
}
