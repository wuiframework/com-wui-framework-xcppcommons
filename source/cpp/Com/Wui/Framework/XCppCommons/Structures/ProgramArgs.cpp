/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#include <boost/regex.hpp>
#include <boost/algorithm/string/regex.hpp>

#include "../sourceFilesMap.hpp"

using Com::Wui::Framework::XCppCommons::Structures::ProgramArgs;
using Com::Wui::Framework::XCppCommons::EnvironmentArgs;

namespace po = boost::program_options;

ProgramArgs::ProgramArgs() {
    this->description.reset(new po::options_description("Basic options"));
    this->description->add_options()
            ("help,h", po::bool_switch(&this->isHelp), "Prints application help description.")
            ("version,v", po::bool_switch(&this->isVersion), "Prints application version message.")
            ("disable-console", "Disable attaching to parent console.");

    this->variablesMap.reset(new po::variables_map());
}

bool ProgramArgs::IsHelp() {
    return this->isHelp;
}

bool ProgramArgs::IsVersion() {
    return this->isVersion;
}

string wordWrap(string $sentence, int $width, int $tabsBefore) {
    std::ostringstream oss;
    std::vector<string> split;
    boost::algorithm::split(split, $sentence, boost::is_any_of("\n "));

    int cnt = 0;
    for (int i = 0; i < static_cast<int>(split.size() - 1); i++) {
        oss << split[i];
        cnt += split[i].size();

        if ((cnt + static_cast<int>(split[i + 1].size())) > $width) {
            oss << "\n" << string(static_cast<unsigned int>($tabsBefore), '\t');
            cnt = 0;
        } else {
            oss << " ";
            cnt++;
        }
    }
    oss << split[split.size() - 1];

    return oss.str();
}

void ProgramArgs::PrintHelp() {
    std::ostringstream oss;
    string license = EnvironmentArgs::getInstance().getProjectLicense();
    std::vector<string> splitResult;
    boost::split_regex(splitResult, license, boost::regex("(\\., )|(\\. )"));
    string copyright;
    unsigned int length = 0;
    for (auto &it : splitResult) {
        if (boost::algorithm::contains(it, "Copyright")) {
            if (!copyright.empty()) {
                copyright += "\n\t\t\t\t";
            }
            copyright += it + ".";
            length += it.size() + 2;
        } else {
            break;
        }
    }

    license = boost::trim_copy(license.substr(length, license.size()));
    license = wordWrap(license, 50, 4);

    oss << "______________________________________________________________________" << std::endl;
    oss << std::endl;
    oss << EnvironmentArgs::getInstance().getAppName() << std::endl;
    oss << "\t- " << EnvironmentArgs::getInstance().getProjectDescription() << std::endl;
    oss << "\tcopyright:  " << copyright << std::endl;
    oss << "\tauthor:     " << EnvironmentArgs::getInstance().getProjectAuthor() << std::endl;
    oss << "\tlicense:    " << license << std::endl;
    oss << "______________________________________________________________________" << std::endl;
    oss << "//////////////////////////////////////////////////////////////////////" << std::endl;
    oss << std::endl;
    oss << *this->description << std::endl;
    oss << "NOTE:" << std::endl;
    oss << wordWrap("\tThis tool can depends on several prerequisites for more info see README.md"
                            " distributed with this material.", 55, 1) << std::endl;
    oss << std::endl;
    oss << "----------------------------------------------------------------------" << std::endl;
    string res = oss.str();
    boost::replace_all(res, "\t", string(4, ' '));
    std::cout << res;
}

void ProgramArgs::PrintVersion() {
    std::cout << EnvironmentArgs::getInstance().getAppName();
    std::cout << " v" << EnvironmentArgs::getInstance().getProjectVersion() << std::endl;
}

boost::shared_ptr<po::options_description> ProgramArgs::getDescription() const {
    return this->description;
}

boost::shared_ptr<po::variables_map> ProgramArgs::getVariablesMap() const {
    return this->variablesMap;
}
