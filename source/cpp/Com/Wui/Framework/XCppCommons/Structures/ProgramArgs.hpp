/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_STRUCTURES_PROGRAMARGS_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_STRUCTURES_PROGRAMARGS_HPP_

namespace Com::Wui::Framework::XCppCommons::Structures {
    /**
     * ProgramArgs class provides base class for program options used in applications under WUI framework.
     * This class holds logic for command line argument parsing and printing help and version of program.
     */
    class ProgramArgs {
     public:
        /**
         * Empty constructor is used for configuration of all program options available for current args.
         */
        ProgramArgs();

        /**
         * @return Returns true if help option match with command line, false otherwise.
         */
        virtual bool IsHelp();

        /**
         * @return Returns true if version option match with command line, false otherwise.
         */
        virtual bool IsVersion();

        /**
         * Prints help description directly to stdout.
         */
        virtual void PrintHelp();

        /**
         * Prints program version directly to stdout.
         */
        virtual void PrintVersion();

        /**
         * @return Returns pointer to internal options description structure.
         */
        boost::shared_ptr<boost::program_options::options_description> getDescription() const;

        /**
         * @return Returns pointer to internal variables map.
         */
        boost::shared_ptr<boost::program_options::variables_map> getVariablesMap() const;

     private:
        boost::shared_ptr<boost::program_options::options_description> description;
        boost::shared_ptr<boost::program_options::variables_map> variablesMap;
        bool isHelp = false;
        bool isVersion = false;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_STRUCTURES_PROGRAMARGS_HPP_
