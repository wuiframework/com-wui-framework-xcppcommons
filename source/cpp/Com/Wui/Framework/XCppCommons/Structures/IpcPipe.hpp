/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_STRUCTURES_IPCPIPE_HPP_
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_STRUCTURES_IPCPIPE_HPP_

namespace Com::Wui::Framework::XCppCommons::Structures {
    /**
     * IpcPipe class provides abstraction layer for inter-process text message communication.
     */
    class IpcPipe {
     public:
        /**
         * Construct IpcPipe instance with specified name and role.
         * @param $name A name of pipe.
         * @param $isServer Specify if pipe is on server or client site.
         */
        IpcPipe(const string &$name, bool $isServer);

        virtual ~IpcPipe();

        /**
         * @return Returns pipe name.
         */
        const string &getPipeName() const;

        /**
         * @return Returns multiple message delimiter.
         */
        const string &getMsgDelimiter() const;

        /**
         * @param msg$Delimiter Specify delimiter between message.
         */
        void setMsgDelimiter(const string &$msgDelimiter);

        /**
         * Opens pipe for client site configuration of creates and opens pipe for server.
         * @param $timeout Specify timeout for client configuration only, timeout for server is ignored.
         * @return Returns true if succeed, false otherwise.
         */
        bool Open(unsigned int $timeout = 0);

        /**
         * Write message into pipe.
         * @param $message A message to be send.
         */
        void Write(const string &$message);

        /**
         * Reads message from pipe.
         * @return Returns received message or empty string if no message is available.
         */
        const string Read();

        IpcPipe() = delete;

        IpcPipe(const IpcPipe &) = delete;

        void operator=(const IpcPipe &)= delete;

     private:
        bool CreateMsgQueue(const string &$name, shared_ptr<boost::interprocess::message_queue> &$msgQueue);

        bool OpenMsgQueue(const string &$name, shared_ptr<boost::interprocess::message_queue> &$msgQueue,
                          unsigned int timeout = 0);

        shared_ptr<boost::interprocess::message_queue> msgQueueIn;
        shared_ptr<boost::interprocess::message_queue> msgQueueOut;
        string pipeName;
        string pipePartNameIn;
        string pipePartNameOut;
        string msgDelimiter = "\n";
        bool isServer = false;
        const unsigned int MAX_MESSAGE_LENGTH = 254;
        const unsigned int MAX_MESSAGES_IN_QUEUE = 50;
    };
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_STRUCTURES_IPCPIPE_HPP_
