/** WARNING: this file has been automatically generated from C++ interfaces and classes, which exist in this package. */
// NOLINT (legal/copyright)

#ifndef COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACESMAP_HPP_  // NOLINT
#define COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACESMAP_HPP_

namespace Com {
    namespace Wui {
        namespace Framework {
            namespace XCppCommons {
                class Application;
                class EnvironmentArgs;
                class Loader;
                namespace Enums {
                    enum HttpClientType : int;
                    class HttpMethodType;
                    enum HttpResponseType : int;
                    enum IOHandlerType : int;
                    class LogLevel;
                }
                namespace Events {
                    class IntervalTimer;
                    class ThreadPool;
                    namespace Args {
                        class EventArgs;
                    }
                }
                namespace Interfaces {
                    class IBaseObject;
                    class IConnector;
                    class INonCopyable;
                    class INonMovable;
                    class IOHandler;
                    class IResponse;
                }
                namespace IOApi {
                    class IOHandlerFactory;
                    namespace Handlers {
                        class BaseOutputHandler;
                        class ConsoleHandler;
                        class OutputFileHandler;
                    }
                }
                namespace Primitives {
                    class ArrayList;
                    template<class T> class BaseEnum;
                    class BaseObject;
                    class String;
                }
                namespace Structures {
                    class IpcPipe;
                    class ProgramArgs;
                }
                namespace System {
                    class Environment;
                    namespace IO {
                        class FileGlob;
                        class FileSystem;
                        class Path;
                    }
                    namespace Net {
                        class Url;
                        namespace Http {
                            class Client;
                            class ClientRequest;
                            class ClientResponse;
                        }
                    }
                    namespace Process {
                        class Child;
                        class ExecuteOptions;
                        class ProcessOptions;
                        class SpawnOptions;
                        class TaskInfo;
                        class TaskManager;
                        class Terminal;
                        class TerminalOptions;
                        namespace Detail {
                        }
                    }
                    namespace ResponseApi {
                        class ResponseFactory;
                        namespace Handlers {
                            class BaseResponse;
                            class CallbackResponse;
                        }
                    }
                }
                namespace Utils {
                    class ArgsParser;
                    class Convert;
                    class IpcPipeObserver;
                    class JSON;
                    class LogIt;
                    class ObjectDecoder;
                    class ObjectEncoder;
                    class ObjectValidator;
                    class Reflection;
                }
            }
        }
    }
}

#endif  // COM_WUI_FRAMEWORK_XCPPCOMMONS_INTERFACESMAP_HPP_  // NOLINT
